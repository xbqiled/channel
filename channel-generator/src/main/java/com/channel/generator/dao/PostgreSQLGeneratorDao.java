package com.channel.generator.dao;

import org.apache.ibatis.annotations.Mapper;

/**
 * PostgreSQL代码生成器
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @since 2018-07-24
 */
@Mapper
public interface PostgreSQLGeneratorDao extends GeneratorDao {

}
