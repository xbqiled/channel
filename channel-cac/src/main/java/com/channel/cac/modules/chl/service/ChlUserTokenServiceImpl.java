package com.channel.cac.modules.chl.service;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.channel.api.entity.chl.ChlUserTokenEntity;
import com.channel.api.service.chl.ChlUserTokenService;
import com.channel.common.utils.R;
import com.channel.common.utils.TokenGenerator;
import com.channel.cac.modules.chl.dao.ChlUserTokenDao;
import org.springframework.stereotype.Service;

import java.util.Date;


@Service("chlUserTokenService")
@com.alibaba.dubbo.config.annotation.Service(
		interfaceClass = ChlUserTokenService.class,
		timeout = 10000,
		version = "${api.service.version}"
)
public class ChlUserTokenServiceImpl extends ServiceImpl<ChlUserTokenDao, ChlUserTokenEntity> implements ChlUserTokenService {
	//1小时后过期
	private final static int EXPIRE = 3600 * 24;

	@Override
	public R createToken(long userId) {
		//生成一个token
		String token = TokenGenerator.generateValue();
		//当前时间
		Date now = new Date();
		//过期时间
		Date expireTime = new Date(now.getTime() + EXPIRE * 1000);

		//判断是否生成过token
		ChlUserTokenEntity tokenEntity = this.selectById(userId);
		if(tokenEntity == null){
			tokenEntity = new ChlUserTokenEntity();
			tokenEntity.setUserId(userId);
			tokenEntity.setToken(token);
			tokenEntity.setUpdateTime(now);
			tokenEntity.setExpireTime(expireTime);
			//保存token
			this.insert(tokenEntity);
		}else{
			tokenEntity.setToken(token);
			tokenEntity.setUpdateTime(now);
			tokenEntity.setExpireTime(expireTime);

			//更新token
			this.updateById(tokenEntity);
		}

		R r = R.ok().put("token", token).put("expire", EXPIRE);
		return r;
	}

	@Override
	public void logout(long userId) {
		//生成一个token
		String token = TokenGenerator.generateValue();
		//修改token
		ChlUserTokenEntity tokenEntity = new ChlUserTokenEntity();
		tokenEntity.setUserId(userId);
		tokenEntity.setToken(token);
		this.updateById(tokenEntity);
	}


	@Override
	public ChlUserTokenEntity queryByToken(String token) {
		return baseMapper.queryByToken(token);
	}
}
