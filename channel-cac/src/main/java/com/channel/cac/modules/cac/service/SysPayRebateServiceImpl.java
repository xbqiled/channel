package com.channel.cac.modules.cac.service;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.channel.api.entity.cac.SysPayRebateEntity;
import com.channel.api.service.cac.SysPayRebateService;
import com.channel.cac.modules.cac.dao.SysPayRebateDao;
import com.channel.common.utils.PageUtils;
import com.channel.common.utils.Query;
import org.springframework.stereotype.Service;

import java.util.Map;



@Service("sysPayRebateService")
@com.alibaba.dubbo.config.annotation.Service(
        interfaceClass = SysPayRebateService.class,
        timeout = 10000,
        version = "${api.service.version}"
)
public class SysPayRebateServiceImpl extends ServiceImpl<SysPayRebateDao, SysPayRebateEntity> implements SysPayRebateService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        Page<SysPayRebateEntity> page = this.selectPage(
                new Query<SysPayRebateEntity>(params).getPage(),
                new EntityWrapper<SysPayRebateEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public SysPayRebateEntity getPayRebateByChannel(String channelId) {
        return baseMapper.getPayRebateByChannel(channelId);
    }

    @Override
    public SysPayRebateEntity getPayRebateByOrder(String orderNo) {
        return baseMapper.getPayRebateByOrder(orderNo);
    }
}
