package com.channel.cac.modules.cac.dao;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.channel.api.entity.cac.SigninchannelEntity;
import com.channel.api.entity.cac.SigninchannelVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * VIEW
 * 
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-05-28 14:35:42
 */
public interface SigninchannelDao extends BaseMapper<SigninchannelEntity> {

    List<SigninchannelVo> querySignCfgPage(Pagination page, @Param("channelId") String channelId, @Param("startTime") String startTime, @Param("endTime") String endTime);

    SigninchannelEntity getSignByChannelId(@Param("channelId") String channelId);

}
