package com.channel.cac.modules.chl.dao;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.channel.api.entity.chl.ChlConfigEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * 系统配置信息
 *
 */
@Mapper
public interface ChlConfigDao extends BaseMapper<ChlConfigEntity> {

	/**
	 * 根据key，查询value
	 */
	ChlConfigEntity queryByKey(String paramKey);


	/**
	 * 根据key，更新value
	 */
	int updateValueByKey(@Param("paramKey") String paramKey, @Param("paramValue") String paramValue);
	
}
