package com.channel.cac.modules.cac.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.channel.api.entity.cac.EditnickawardEntity;
import com.channel.api.entity.cac.EditnickawardVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * VIEW
 * 
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-09-18 18:58:08
 */
public interface EditnickawardDao extends BaseMapper<EditnickawardEntity> {

    List<EditnickawardVo> queryEditNickAwardPage(Pagination page, @Param("channelId") String channelId, @Param("startTime") String startTime, @Param("endTime") String endTime);

    EditnickawardVo getEditNickAwardByChannelId(@Param("channelId") String channelId);
	
}
