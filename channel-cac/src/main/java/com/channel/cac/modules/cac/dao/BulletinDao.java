package com.channel.cac.modules.cac.dao;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.channel.api.entity.cac.BulletinEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 
 * 
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-03-06 16:05:02
 */
public interface BulletinDao extends BaseMapper<BulletinEntity> {

    List<Map<String, Object>> queryBulletinPage(Pagination page, @Param("channelId")String channelId, @Param("title")String userId, @Param("startTime")String startTime, @Param("endTime")String endTime);

    List<Map<String, Object>> getBulletinInfo(@Param("channelId")String channelId, @Param("id")String id);
}
