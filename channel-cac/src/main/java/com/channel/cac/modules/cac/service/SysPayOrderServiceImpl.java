package com.channel.cac.modules.cac.service;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.channel.api.entity.cac.*;
import com.channel.api.entity.chl.ChlUserEntity;
import com.channel.api.service.cac.SysPayOrderService;
import com.channel.api.service.cac.SysPayRebateService;
import com.channel.cac.modules.cac.dao.AccountinfoDao;
import com.channel.cac.modules.cac.dao.SysPayOrderDao;
import com.channel.cac.modules.cac.dao.UserbaseattrDao;
import com.channel.common.constant.ConfigConstant;
import com.channel.common.constant.EnumConstant;
import com.channel.common.json.GsonCancelResult;
import com.channel.common.json.GsonReduceResult;
import com.channel.common.json.GsonResult;
import com.channel.common.utils.HttpHandler;
import com.channel.common.utils.PageUtils;
import com.channel.common.utils.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.Map;



@Service("sysPayOrderService")
@com.alibaba.dubbo.config.annotation.Service(
        interfaceClass = SysPayOrderService.class,
        retries = 0,
        timeout = 20000,
        version = "${api.service.version}"
)
public class SysPayOrderServiceImpl extends ServiceImpl<SysPayOrderDao, SysPayOrderEntity> implements SysPayOrderService {

    @Autowired
    private SysPayRebateService sysPayRebateService;

    @Autowired
    private UserbaseattrDao userbaseattrDao;

    @Autowired
    private AccountinfoDao accountinfoDao;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        Page<SysPayOrderEntity> page = this.selectPage(
                new Query<SysPayOrderEntity>(params).getPage(),
                new EntityWrapper<SysPayOrderEntity>()
        );
        return new PageUtils(page);
    }

    @Override
    public PageUtils queryOrderPage(Map<String, Object> params, ChlUserEntity userEntity) {
        String current = (String)params.get(ConfigConstant.LIMIT);
        String size  = (String)params.get(ConfigConstant.PAGE);

        //根据拓展Server获取渠道ID
        String channelId = ObjectUtil.isNotNull(userEntity.getChannelId()) ? userEntity.getChannelId(): "";
        String userId = ObjectUtil.isNotNull(params.get("userId")) ? (String)params.get("userId") : "";
        String payName = ObjectUtil.isNotNull(params.get("payName")) ? (String)params.get("payName") : "";

        String status = ObjectUtil.isNotNull(params.get("status")) ? (String)params.get("status") : "";
        String handlerType = ObjectUtil.isNotNull(params.get("handlerType")) ? (String)params.get("handlerType") : "";
        String startTime = ObjectUtil.isNotNull(params.get("startTime")) ? (String)params.get("startTime") : "";

        String endTime = ObjectUtil.isNotNull(params.get("endTime")) ? (String)params.get("endTime") : "";
        Page<PayOrderVo> page = new Page<>(new Integer(size), new Integer(current));
        List<PayOrderVo> list = this.baseMapper.queryOrderPage(page, channelId, payName, status, userId, handlerType, startTime, endTime);
        return  new PageUtils(page.setRecords(list));
    }

    @Override
    public GsonReduceResult reduceFee(String userId, String note, String payFee, Integer force, ChlUserEntity userEntity) {
        //判断用户是否为当前渠道的用户
        AccountinfoEntity userInfo = accountinfoDao.getUserInfo(userId, userEntity.getChannelId());
        if (userInfo == null) {
            return  null;
        }

        GsonReduceResult result = HttpHandler.reduceFee(Integer.parseInt(userId), new BigDecimal(payFee), force);
        //调用接口扣分
        if (result != null && result.getRet() == 0 && result.getRealCancel() > 0) {
            SysPayOrderEntity insertOrder = new SysPayOrderEntity();

            String oderId = IdUtil.fastSimpleUUID();
            insertOrder.setOrderNo(oderId);
            insertOrder.setUserId(Integer.valueOf(userId));

            insertOrder.setNote(note);
            insertOrder.setChannelId(userEntity.getChannelId());
            insertOrder.setStatus(4);

            insertOrder.setHandlerType(EnumConstant.HANDLE_TYPE.REQUEST.getValue());
            insertOrder.setPayFee(new BigDecimal(0-result.getRealCancel()));
            insertOrder.setCreateBy(userEntity.getUsername());

            insertOrder.setCreateTime(LocalDateTime.now());
            this.insert(insertOrder);
            return result;
        } else {
            return new GsonReduceResult(result.getRet());
        }
    }


    @Override
    public List<SysPayOrderEntity> queryRechargeOrderList(String channelId) {
        return baseMapper.queryRechargeOrderList(channelId);
    }

    @Override
    public CancelResultVo rechargeCancel(Integer id, String note, Integer force, ChlUserEntity userEntity) {
        //查询订单信息
        SysPayOrderEntity orderEntity  = selectById(id);
        Integer accountID = orderEntity.getUserId();

        String payType = EnumConstant.PayTypeNameOf(orderEntity.getPayName());
        Integer type = 0;
        if (StrUtil.isNotEmpty(payType)) {
             type = Integer.valueOf(payType);
        }
        String orderId = orderEntity.getOrderNo();

        BigDecimal amount = orderEntity.getPayFee();
        BigDecimal rebateCoin = new BigDecimal(0);
        //判断是否有返利
        SysPayRebateEntity sysPayRebateEntity = sysPayRebateService.getPayRebateByOrder(orderId);
        if (sysPayRebateEntity != null && sysPayRebateEntity.getRebateFee().intValue() > 0) {
            rebateCoin = new BigDecimal(sysPayRebateEntity.getRebateFee().multiply(new BigDecimal(100)).toString());
        }

        //请求接口调用回撤
        GsonCancelResult result = HttpHandler.rechargeCancel(accountID, amount, rebateCoin, type, orderId, force);
        if (result != null && result.getRet() == 0 && result.getRealCancel() > 0) {
            //保存扣减订单信息
            SysPayOrderEntity insertOrder = new SysPayOrderEntity();
            BeanUtil.copyProperties(orderEntity, insertOrder);

            String cancelOrderId = IdUtil.fastSimpleUUID();
            insertOrder.setOrderNo(cancelOrderId);
            insertOrder.setNote(note + "  回撤订单号:" + orderEntity.getOrderNo());
            insertOrder.setPayFee(new BigDecimal(0-result.getRealCancel()/100));

            insertOrder.setCreateBy(userEntity.getUsername());
            insertOrder.setCreateTime(LocalDateTime.now());
            insertOrder.setStatus(4);

            insertOrder.setHandlerType(4);
            orderEntity.setStatus(3);
            orderEntity.setHandlerType(3);
            orderEntity.setModifyTime(LocalDateTime.now());
            orderEntity.setModifyBy(userEntity.getUsername());
            this.insert(insertOrder);

            this.updateAllColumnById(orderEntity);
            return new CancelResultVo(result.getRet(), result.getMsg(), result.getRealCancel()/100, cancelOrderId);
        } else {
            return new CancelResultVo(result.getRet());
        }
    }


    @Override
    public Boolean recharge(String userId, Integer isManual, Double damaMulti, String payFee, String depositor,
                            String account, String remark, String payType, Integer rate, ChlUserEntity chlUserEntity) {
        AccountinfoEntity userInfo = accountinfoDao.getUserInfo(userId, chlUserEntity.getChannelId());
        if (userInfo == null || StrUtil.isBlank(userInfo.gettNickname())) {
            return Boolean.FALSE;
        }

        String orderNo = IdUtil.simpleUUID();
        SysPayOrderEntity sysPayOrderEntity = new SysPayOrderEntity();
        sysPayOrderEntity.setUserId(Integer.parseInt(userId));
        sysPayOrderEntity.setStatus(1);

        sysPayOrderEntity.setAccount(account);
        sysPayOrderEntity.setChannelId(chlUserEntity.getChannelId());
        sysPayOrderEntity.setCreateBy(chlUserEntity.getUsername());

        sysPayOrderEntity.setCreateTime(LocalDateTime.now());
        sysPayOrderEntity.setOrderNo(orderNo);
        sysPayOrderEntity.setDepositor(depositor);

        sysPayOrderEntity.setPoundage(new BigDecimal(0));
        Float fee = Float.parseFloat(payFee);
        sysPayOrderEntity.setPayFee(new BigDecimal(fee));

        if (Integer.valueOf(payType) == Integer.valueOf(EnumConstant.PAY_TYPE.PAYTYPE18.getValue())) {
            sysPayOrderEntity.setHandlerType(EnumConstant.HANDLE_TYPE.GIVE.getValue());
        } else {
            sysPayOrderEntity.setHandlerType(EnumConstant.HANDLE_TYPE.RECHARGE.getValue());
        }

        sysPayOrderEntity.setPayName(EnumConstant.PayTypeValueOf(payType));
        sysPayOrderEntity.setNote(remark);

        //判断是否存在30秒前同样的充值信息
        List<SysPayOrderEntity>  list = baseMapper.queryNowRechargeList(sysPayOrderEntity.getChannelId(), sysPayOrderEntity.getUserId().toString(), sysPayOrderEntity.getPayFee(), DateUtil.format(DateUtil.offsetSecond(new Date(), -30), ConfigConstant.FULL_DATE_TIME_FORMAT));
        if (CollUtil.isNotEmpty(list) && list.size() > 0) {
            return Boolean.FALSE;
        }

        //请求接口
        GsonResult result = HttpHandler.handerRechargeOrder(sysPayOrderEntity.getUserId(), sysPayOrderEntity.getOrderNo(), sysPayOrderEntity.getPayFee().intValue(), 0,  Integer.valueOf(payType), isManual, damaMulti, rate);
        if (result != null && result.getRet() == 0) {
            this.insert(sysPayOrderEntity);
            //判断是否有返利
            if (result.getRebateCoin() != null && result.getRebateCoin().longValue() > 0) {
                SysPayRebateEntity rebateEntity = new SysPayRebateEntity();
                rebateEntity.setStatus(1);

                rebateEntity.setUserId(Integer.parseInt(userId));
                rebateEntity.setChannelId(chlUserEntity.getChannelId());
                rebateEntity.setCreateBy(chlUserEntity.getUsername());

                rebateEntity.setRebateType(1);
                //把分转换成元
                rebateEntity.setRebateFee(new BigDecimal(result.getRebateCoin()/100));
                rebateEntity.setOrderNo(orderNo);

                rebateEntity.setCreateTime(new Date());
                sysPayRebateService.insert(rebateEntity);
            }
            return Boolean.TRUE;
        } else {
            return Boolean.FALSE;
        }
    }


    @Override
    public Boolean approval(String status, String id, String note, ChlUserEntity entity) {
        ApprovalPayVo approvalPayVo = baseMapper.getPayOrderVo(id);
        String payType = EnumConstant.PayTypeNameOf(approvalPayVo.getPayName());

        //更新状态信息
        if (Integer.parseInt(status) == 2) {
            baseMapper.approval(entity.getUsername(), id, status, note);
            return  Boolean.TRUE;
        } else {
            GsonResult result = HttpHandler.rechargeOrder(approvalPayVo.getUserId(), approvalPayVo.getOrderNo(),
                    approvalPayVo.getPayFee().intValue(), 0, StrUtil.isEmpty(payType) ? 1 : Integer.valueOf(payType), approvalPayVo.getReturnRate());
            if (result != null && result.getRet() == 0) {
                //审批更改数据信息
                baseMapper.approval(entity.getUsername(), id, status, note);
                if (result.getRebateCoin() != null && result.getRebateCoin().longValue() > 0) {
                    SysPayRebateEntity rebateEntity = new SysPayRebateEntity();
                    rebateEntity.setStatus(1);

                    rebateEntity.setUserId(approvalPayVo.getUserId());
                    rebateEntity.setChannelId(entity.getChannelId());
                    rebateEntity.setCreateBy(entity.getUsername());

                    rebateEntity.setRebateType(1);
                    rebateEntity.setRebateFee(new BigDecimal(result.getRebateCoin()/100));
                    rebateEntity.setOrderNo(approvalPayVo.getOrderNo());

                    rebateEntity.setCreateTime(new Date());
                    sysPayRebateService.insert(rebateEntity);
                }
                return  Boolean.TRUE;
            } else {
                return  Boolean.FALSE;
            }
        }
    }

    @Override
    public PageUtils queryRechargeStatisPage(Map<String, Object> params, ChlUserEntity userEntity) {
        String current = (String)params.get(ConfigConstant.LIMIT);
        String size  = (String)params.get(ConfigConstant.PAGE);

        String channelId = ObjectUtil.isNotNull(userEntity.getChannelId()) ? userEntity.getChannelId(): "";
        String startTime = ObjectUtil.isNotNull(params.get("startTime")) && ((String) params.get("startTime")).length() == 13 ? (String)params.get("startTime")  : "";
        String endTime = ObjectUtil.isNotNull(params.get("endTime")) && ((String) params.get("endTime")).length() == 13 ?  (String)params.get("endTime") : "";
        String promoterId = ObjectUtil.isNotNull(params.get("promoterId"))?  (String)params.get("promoterId") : "";

        if (StrUtil.isNotBlank(startTime)) {
            startTime = DateUtil.format(new Date(Long.valueOf(startTime)), ConfigConstant.DATE_FORMAT);
        }

        if (StrUtil.isNotBlank(endTime)) {
            endTime = DateUtil.format(new Date(Long.valueOf(endTime)), ConfigConstant.DATE_FORMAT);
        }

        Page<Map<String, Object>> page = new Page<>(new Integer(size), new Integer(current));
        List<Map<String, Object>> list = this.baseMapper.queryRechargeStatisPage(page, promoterId, channelId,  startTime, endTime);
        return  new PageUtils(page.setRecords(list));
    }

    @Override
    public PageUtils queryRechargeUserNumPage(Map<String, Object> params, ChlUserEntity userEntity) {
        String current = (String)params.get(ConfigConstant.LIMIT);
        String size  = (String)params.get(ConfigConstant.PAGE);

        String channelId = ObjectUtil.isNotNull(userEntity.getChannelId()) ? userEntity.getChannelId(): "";
        String startTime = ObjectUtil.isNotNull(params.get("startTime")) && ((String) params.get("startTime")).length() == 13 ? (String)params.get("startTime")  : "";
        String endTime = ObjectUtil.isNotNull(params.get("endTime")) && ((String) params.get("endTime")).length() == 13 ?  (String)params.get("endTime") : "";
        String promoterId = ObjectUtil.isNotNull(params.get("promoterId"))?  (String)params.get("promoterId") : "";

        if (StrUtil.isNotBlank(startTime)) {
            startTime = DateUtil.format(new Date(Long.valueOf(startTime)), ConfigConstant.DATE_FORMAT);
        }

        if (StrUtil.isNotBlank(endTime)) {
            endTime = DateUtil.format(new Date(Long.valueOf(endTime)), ConfigConstant.DATE_FORMAT);
        }

        Page<Map<String, Object>> page = new Page<>(new Integer(size), new Integer(current));
        List<Map<String, Object>> list = this.baseMapper.queryRechargeUserNumPage(page, promoterId, channelId,  startTime, endTime);
        return  new PageUtils(page.setRecords(list));
    }


    @Override
    public Map<String, Object> queryAllRechargeData(Map<String, Object> params, ChlUserEntity userEntity) {
        String channelId = ObjectUtil.isNotNull(userEntity.getChannelId()) ? userEntity.getChannelId(): "";
        String startTime = ObjectUtil.isNotNull(params.get("startTime")) && ((String) params.get("startTime")).length() == 13 ? (String)params.get("startTime")  : "";

        String endTime = ObjectUtil.isNotNull(params.get("endTime")) && ((String) params.get("endTime")).length() == 13 ?  (String)params.get("endTime") : "";
        String promoterId = ObjectUtil.isNotNull(params.get("promoterId"))?  (String)params.get("promoterId") : "";
        if (StrUtil.isNotBlank(startTime)) {
            startTime = DateUtil.format(new Date(Long.valueOf(startTime)), ConfigConstant.DATE_FORMAT);
        }

        if (StrUtil.isNotBlank(endTime)) {
            endTime = DateUtil.format(new Date(Long.valueOf(endTime)), ConfigConstant.DATE_FORMAT);
        }
        return this.baseMapper.queryAllRechargeData(promoterId, channelId,  startTime, endTime);
    }
}
