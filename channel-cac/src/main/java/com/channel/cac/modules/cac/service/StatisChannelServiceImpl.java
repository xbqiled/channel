package com.channel.cac.modules.cac.service;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.channel.api.entity.cac.StatisChannelEntity;
import com.channel.api.entity.cac.StatisChannelVo;
import com.channel.api.entity.chl.ChlUserEntity;
import com.channel.api.service.cac.StatisChannelService;
import com.channel.cac.modules.cac.dao.StatisChannelDao;
import com.channel.common.constant.ConfigConstant;
import com.channel.common.utils.PageUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;


@Service("statisChannelService")
@com.alibaba.dubbo.config.annotation.Service(
        interfaceClass = StatisChannelService.class,
        timeout = 50000,
        version = "${api.service.version}"
)
public class StatisChannelServiceImpl extends ServiceImpl<StatisChannelDao, StatisChannelEntity> implements StatisChannelService {

    private static String TABLE_NAME = "statis_channel";

    /**
     * <B>拼接SQL语句</B>
     * @param list
     * @return
     */
    private String spliceQuerySql(List<String> list) {
        StringBuffer sb = new StringBuffer();
        sb.append("(");
        if (CollUtil.isNotEmpty(list)) {
            for (int i = 0; i < list.size(); i++) {
                sb.append(" select * From " + list.get(i));
                if (i < list.size() - 1) {
                    sb.append("  union all ");
                }
            }
        }
        sb.append(")");
        return sb.toString();
    }


    private String spliceInSql(List<String> list) {
        StringBuffer sb = new StringBuffer();
        sb.append("(");
        if (CollUtil.isNotEmpty(list)) {
            for (int i = 0; i < list.size(); i++) {
                sb.append("'" + list.get(i) + "'");
                if (i < list.size() - 1) {
                    sb.append(" , ");
                }
            }
        }
        sb.append(")");
        return sb.toString();
    }

    private List<String> getIntervalMonth(String startTime, String endTime) {
        List<String> list = new ArrayList<>();
        //转化成月份
        Date startMonthDate = DateUtil.parse(startTime, "yyyy-MM-dd");
        Date endMonthDate = DateUtil.parse(endTime, "yyyy-MM-dd");

        String endMonth = DateUtil.format(endMonthDate, "yyyyMM");
        Date nextDate = startMonthDate;

        //判断时间不等于结束时间
        while (!DateUtil.format(nextDate, "yyyyMM").equals(endMonth)) {
            list.add(TABLE_NAME + DateUtil.format(nextDate, "yyyyMM"));
            //获取下一个月
            nextDate = DateUtil.offsetMonth(nextDate, 1);
        }

        list.add(TABLE_NAME + endMonth);
        return list;
    }


    @Override
    public PageUtils getStatisDayReportPage(Map<String, Object> params, ChlUserEntity userEntity) {
        StringBuffer tableSql = new StringBuffer();
        //获取当前时间
        String current = (String)params.get(ConfigConstant.LIMIT);
        String size  = (String)params.get(ConfigConstant.PAGE);

        //根据拓展Server获取渠道ID
        String channelId = ObjectUtil.isNotNull(userEntity.getChannelId()) ? userEntity.getChannelId() : "";
        String startTime = ObjectUtil.isNotNull(params.get("startTime")) ? (String)params.get("startTime") : "";
        String endTime = ObjectUtil.isNotNull(params.get("endTime")) ? (String)params.get("endTime") : "";

        Page<StatisChannelVo> page = new Page<>(new Integer(size), new Integer(current));
        List<String> list = getIntervalMonth(startTime, endTime);
        String sql = spliceInSql(list);
        List<String> tableList = baseMapper.queryHaveTab(sql);

        //组成查询SQL
        tableSql.append(spliceQuerySql(tableList));
        List<StatisChannelVo> resultList = baseMapper.getDayReportPage(page, tableSql.toString(), channelId, startTime, endTime);
        return new PageUtils(page.setRecords(resultList));
    }


    @Override
    public PageUtils getStatisMonthReportPage(Map<String, Object> params, ChlUserEntity userEntity) {
        String current = (String)params.get(ConfigConstant.LIMIT);
        String size  = (String)params.get(ConfigConstant.PAGE);

        //根据拓展Server获取渠道ID
        String channelId = ObjectUtil.isNotNull(userEntity.getChannelId()) ? userEntity.getChannelId() : "";
        String monthTime = ObjectUtil.isNotNull(params.get("monthTime")) ? (String)params.get("monthTime") : "";
        Page<StatisChannelVo> page = new Page<>(new Integer(size), new Integer(current));


        Date queryMonth = DateUtil.parse(monthTime, "yyyy-MM");
        String month = DateUtil.format(queryMonth, "yyyyMM");
        List<StatisChannelVo> resultList =  baseMapper.getMonthReportPage(page, TABLE_NAME + month, channelId, monthTime);
        return new PageUtils(page.setRecords(resultList));
    }
}
