package com.channel.cac.modules.cac.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.channel.api.entity.cac.ChanneltaskEntity;
import com.channel.api.entity.cac.TaskVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * VIEW
 *
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-12-31 12:30:32
 */
public interface ChanneltaskDao extends BaseMapper<ChanneltaskEntity> {

    List<TaskVo> queryTaskPage(Pagination page, @Param("channelId") String channelId, @Param("startTime") String startTime, @Param("endTime") String endTime);

    ChanneltaskEntity getTaskByChannelId(@Param("channelId") String channelId);

    List<Map<String, Object>> getTaskCfg(@Param("taskId") String taskId);

}
