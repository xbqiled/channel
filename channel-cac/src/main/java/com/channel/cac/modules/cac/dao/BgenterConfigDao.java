package com.channel.cac.modules.cac.dao;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.channel.api.entity.cac.BgenterConfigEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-11-15 13:23:00
 */
public interface BgenterConfigDao extends BaseMapper<BgenterConfigEntity> {

    List<BgenterConfigEntity> queryEnterGameConfigPage(Pagination page, @Param("channelId") String channelId);

    BgenterConfigEntity getConfigByChannelId(@Param("channelId") String channelId);
	
}
