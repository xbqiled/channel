package com.channel.cac.modules.cac.service;

import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import cn.jpush.api.push.PushResult;
import cn.jpush.api.push.model.audience.Audience;
import cn.jpush.api.report.MessagesResult;
import cn.jpush.api.report.ReceivedsResult;
import cn.jpush.api.schedule.ScheduleMsgIdsResult;
import cn.jpush.api.schedule.ScheduleResult;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.channel.api.entity.cac.PushConfigEntity;
import com.channel.api.entity.cac.PushRecordEntity;
import com.channel.api.entity.chl.ChlUserEntity;
import com.channel.api.service.cac.PushRecordService;
import com.channel.cac.modules.cac.dao.PushConfigDao;
import com.channel.cac.modules.cac.dao.PushRecordDao;
import com.channel.common.constant.ConfigConstant;
import com.channel.common.push.jpush.JPushConfig;
import com.channel.common.push.jpush.JPushServer;
import com.channel.common.push.jpush.model.JPushRequest;
import com.channel.common.utils.PageUtils;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Map;


@Service("pushRecordService")
@com.alibaba.dubbo.config.annotation.Service(
        interfaceClass = PushRecordService.class,
        timeout = 30000,
        version = "${api.service.version}"
)
public class PushRecordServiceImpl extends ServiceImpl<PushRecordDao, PushRecordEntity> implements PushRecordService {

    @Autowired
    private  PushConfigDao pushConfigDao;

    @Override
    public PageUtils queryPushRecordPage(Map<String, Object> params, ChlUserEntity userEntity) {
        String current = (String) params.get(ConfigConstant.LIMIT);
        String size = (String) params.get(ConfigConstant.PAGE);
        Page<Map<String, Object>> page = new Page<>(new Integer(size), new Integer(current));

        String sendSys = ObjectUtil.isNotNull(params.get("sendSys")) ? (String) params.get("sendSys") : "";
        String title = ObjectUtil.isNotNull(params.get("title")) ? (String) params.get("title") : "";
        String channelId = ObjectUtil.isNotNull(userEntity.getChannelId()) ? userEntity.getChannelId() : "";
        String startTime = ObjectUtil.isNotNull(params.get("startTime")) ? (String) params.get("startTime") : "";
        String endTime = ObjectUtil.isNotNull(params.get("endTime")) ? (String) params.get("endTime") : "";

        List<Map<String, Object>> list = baseMapper.queryPushRecordPage(page, sendSys, title, channelId, startTime, endTime);
        return new PageUtils(page.setRecords(list));
    }

    @Override
    public Boolean sendPush(ChlUserEntity userEntity, String devicePlatform, Integer timeType, String retainTime, Long timeLong, Integer sendType, String alias, String title, String content) {
        try {
            PushConfigEntity pushConfig = pushConfigDao.getActivateConfig(userEntity.getChannelId());
            if (pushConfig == null) {
                return null;
            }
            JPushConfig config = new Gson().fromJson(pushConfig.getConfig(), JPushConfig.class);
            JPushRequest request = new JPushRequest();
            String[] aliasArray;
            String retainTimeStr = "";
            Audience audience = null;

            PushRecordEntity push = new PushRecordEntity();
            push.setChannelId(userEntity.getChannelId());
            push.setPlatform(devicePlatform);

            push.setTitle(title);
            push.setContent(content);
            push.setPushTime(new Date());

            push.setSendType(sendType);
            if (StrUtil.isNotBlank(alias) && sendType != null && sendType == 1) {
                push.setAudience(alias);
                aliasArray = getAlias(alias);

                if (aliasArray == null || aliasArray.length == 0) {
                    return null;
                }
                audience = Audience.alias(aliasArray);
            }

            push.setTimeType(timeType);
            if (retainTime != null && timeType != null && timeType == 1) {
                retainTimeStr = DateUtil.format(new Date(new Long(retainTime)), DatePattern.NORM_DATETIME_FORMAT);
                push.setRetainTime(new Date(new Long(retainTime)));
            }


            push.setTimeLong(timeLong);
            push.setPlatformId(pushConfig.getPlatformId());
            push.setConfigId(pushConfig.getId());

            push.setSendSys(1);
            push.setPushBy(userEntity.getUsername());
            //发送
            JPushServer server = new JPushServer(config);
            request.setTitle(title);
            request.setContent(content);

            request.setDevicePlatform(devicePlatform);
            request.setTimeToAlive(timeLong);
            request.setApnsProduction(Boolean.TRUE);

            if (timeType == 0) {
                PushResult result = server.sendPush(request, audience == null ? Audience.all() : audience);
                if (result.statusCode == 0) {
                    //保存信息
                    if (result != null) {
                        push.setMsgId(new Long (result.msg_id).toString());
                    }
                    this.insert(push);
                    return Boolean.TRUE;
                } else {
                    return Boolean.FALSE;
                }
            } else {
                ScheduleResult result = server.createSingleSchedule("推送" + IdUtil.simpleUUID(), retainTimeStr, request, audience == null ? Audience.all() : audience);
                if (StrUtil.isNotBlank(result.getSchedule_id())) {
                    //保存信息
                    if (result != null) {
                        push.setMsgId(result.getSchedule_id());
                    }
                    this.insert(push);
                    return Boolean.TRUE;
                } else {
                    return Boolean.FALSE;
                }
            }
        } catch (Exception e) {
            return null;
        }
    }

    private String [] getAlias(String aliasStr) {
        if (aliasStr != null && aliasStr.contains(",")) {
            return aliasStr.split(",");
        } else {
            return new String[] {aliasStr};
        }
    }

    @Override
    public Map<String, Object> getObj(Integer id) {
        return baseMapper.getPushRecordById(id);
    }

    @Override
    public ReceivedsResult getReport(Integer id) {
        try {
            PushRecordEntity push = this.selectById(id);
            PushConfigEntity pushConfig = pushConfigDao.getActivateConfig(push.getChannelId());
            if (pushConfig == null) {
                return null;
            }

            JPushConfig config = new Gson().fromJson(pushConfig.getConfig(), JPushConfig.class);
            JPushServer server = new JPushServer(config);
            String msgId = "";

            if (push.getTimeType() != null && push.getTimeType() == 1) {
                ScheduleMsgIdsResult scheduleMsgIdsResult = server.getScheduleMsgIds(push.getMsgId());
                if (scheduleMsgIdsResult != null &&  scheduleMsgIdsResult.getMsgids() != null) {

                }
            } else {
                msgId = push.getMsgId();
            }

            ReceivedsResult result = server.getReportReceiveds(msgId);
            return result;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }
}
