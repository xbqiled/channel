package com.channel.cac.modules.cac.service;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.channel.api.entity.cac.SysCashOrderEntity;
import com.channel.api.entity.chl.ChlUserEntity;
import com.channel.api.service.cac.SysCashOrderService;
import com.channel.cac.modules.cac.dao.SysCashOrderDao;
import com.channel.cac.modules.cac.dao.UserbaseattrDao;
import com.channel.common.constant.ConfigConstant;
import com.channel.common.constant.EnumConstant;
import com.channel.common.json.GsonResult;
import com.channel.common.utils.HttpHandler;
import com.channel.common.utils.PageUtils;
import com.channel.common.utils.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;



@Service("sysCashOrderService")
@com.alibaba.dubbo.config.annotation.Service(
        interfaceClass = SysCashOrderService.class,
        timeout = 20000,
        version = "${api.service.version}"
)
public class SysCashOrderServiceImpl extends ServiceImpl<SysCashOrderDao, SysCashOrderEntity> implements SysCashOrderService {

    @Autowired
    private  UserbaseattrDao userbaseattrDao;

    @Override
    public PageUtils queryPage(Map<String, Object> params, ChlUserEntity userEntity) {
        String startTime = params.get("startTime") != null && !params.get("startTime").equals("") ? params.get("startTime").toString() : null;
        String endTime = params.get("endTime") != null && !params.get("endTime").equals("") ? params.get("endTime").toString() : null;

        Page<SysCashOrderEntity> page = this.selectPage(
                new Query<SysCashOrderEntity>(params).getPage(),
                new EntityWrapper<SysCashOrderEntity>()
                .eq("channel_id", userEntity.getChannelId())
                .eq(params.get("userId") != null && !params.get("userId").equals("") , "user_id", params.get("userId"))
                .ge(StrUtil.isNotBlank(startTime), "create_time", DateUtil.parse(startTime))
                .le(StrUtil.isNotBlank(endTime),  "create_time", DateUtil.parse(endTime)).orderDesc(new ArrayList<String>())
              );
        return new PageUtils(page);
    }


    @Override
    public List<SysCashOrderEntity> queryCashOrderList(String channelId) {
        return baseMapper.queryCashOrderList(channelId);
    }

    @Override
    public PageUtils queryOrderPage(Map<String, Object> params, ChlUserEntity userEntity) {
        String current = (String)params.get(ConfigConstant.LIMIT);
        String size  = (String)params.get(ConfigConstant.PAGE);

        //根据拓展Server获取渠道ID
        String channelId = ObjectUtil.isNotNull(userEntity.getChannelId()) ? userEntity.getChannelId(): "";
        String userId = ObjectUtil.isNotNull(params.get("userId")) ? (String)params.get("userId") : "";
        String status = ObjectUtil.isNotNull(params.get("status")) ? (String)params.get("status") : "";

        String startTime = ObjectUtil.isNotNull(params.get("startTime")) ? (String)params.get("startTime") : "";
        String endTime = ObjectUtil.isNotNull(params.get("endTime")) ? (String)params.get("endTime") : "";

        Page<SysCashOrderEntity> page = new Page<>(new Integer(size), new Integer(current));
        List<SysCashOrderEntity> list = this.baseMapper.queryOrderPage(page, channelId, userId, status, startTime, endTime);
        return  new PageUtils(page.setRecords(list));
    }



    @Override
    public Boolean approval(String id, String status, String note, ChlUserEntity entity) {
        SysCashOrderEntity order = this.selectById(id);
        //调用接口处理
        String code = StrUtil.isNotBlank(status)  && new Integer(status) == EnumConstant.APPROVE_STATUS.FAIL_STATUS.getValue() ? "1" : "0";
        GsonResult result = HttpHandler.cashOrderResult(order.getUserId(), order.getCashNo(), Integer.parseInt(code), note);
        if (result != null && result.getRet() == 0) {
            //审批更改数据信息
            baseMapper.approval(entity.getUsername(), id, status, note);
            return  Boolean.TRUE;
        } else {
            return  Boolean.FALSE;
        }
    }
}
