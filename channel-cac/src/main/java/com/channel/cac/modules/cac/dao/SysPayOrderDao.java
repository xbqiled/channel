package com.channel.cac.modules.cac.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.channel.api.entity.cac.ApprovalPayVo;
import com.channel.api.entity.cac.PayOrderVo;
import com.channel.api.entity.cac.SysPayOrderEntity;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * 
 * 
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-02-24 19:28:02
 */
public interface SysPayOrderDao extends BaseMapper<SysPayOrderEntity> {

    void approval(@Param("createBy")String createBy , @Param("id") String id, @Param("status") String status, @Param("note") String note);

    List<PayOrderVo> queryOrderPage (Pagination page, @Param("channelId")String channelId,  @Param("payName")String payName, @Param("status")String status, @Param("userId")String userId, @Param("handlerType")String handlerType, @Param("startTime")String startTime, @Param("endTime")String endTime);

    List<Map<String, Object>> queryRechargeStatisPage(Pagination page, @Param("promoterId") String promoterId, @Param("channelId") String channelId, @Param("startTime") String startTime, @Param("endTime") String endTime);

    List<Map<String, Object>> queryRechargeUserNumPage(Pagination page, @Param("promoterId") String promoterId, @Param("channelId") String channelId, @Param("startTime") String startTime, @Param("endTime") String endTime);

    Map<String, Object> queryAllRechargeData(@Param("promoterId") String promoterId, @Param("channelId") String channelId, @Param("startTime") String startTime, @Param("endTime") String endTime);

    List<SysPayOrderEntity> queryNowRechargeList(@Param("channelId") String channelId, @Param("userId") String userId, @Param("payFee") BigDecimal payFee, @Param("queryTime") String queryTime);

    List<SysPayOrderEntity> queryRechargeOrderList(@Param("channelId") String channelId);

    ApprovalPayVo getPayOrderVo(@Param("orderId") String orderId);
}
