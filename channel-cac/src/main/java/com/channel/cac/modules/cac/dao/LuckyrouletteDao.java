package com.channel.cac.modules.cac.dao;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.channel.api.entity.cac.InviteDepositChannelVo;
import com.channel.api.entity.cac.LuckyRouletteVo;
import com.channel.api.entity.cac.LuckyrouletteEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * VIEW
 * 
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-04-22 16:07:30
 */
public interface LuckyrouletteDao extends BaseMapper<LuckyrouletteEntity> {

    List<LuckyRouletteVo> queryLuckyRoulettePage(Pagination page, @Param("channelId") String channelId, @Param("startTime") String startTime, @Param("endTime") String endTime);

    LuckyrouletteEntity getLuckyRouletteByChannelId(@Param("channelId") String channelId);
	
}
