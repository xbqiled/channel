package com.channel.cac.modules.cac.service;

import cn.hutool.http.HttpRequest;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.channel.api.entity.cac.*;
import com.channel.api.entity.chl.ChlUserEntity;
import com.channel.api.service.cac.MessageService;
import com.channel.cac.modules.cac.dao.MessageDao;
import com.channel.common.constant.ConfigConstant;
import com.channel.common.constant.UrlConstant;
import com.channel.common.utils.PageUtils;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Service("msgService")
@com.alibaba.dubbo.config.annotation.Service(
        interfaceClass = MessageService.class,
        timeout = 10000,
        version = "${api.service.version}"
)
public class MessageServiceImpl extends ServiceImpl<MessageDao, MessageEntity> implements MessageService {

    @Override
    public MessageEntity queryById(Long id) {
        return baseMapper.queryById(id);
    }

    /**
     * 查询列表
     * @param params
     * @return
     */
    @Override
    public PageUtils queryPage(Map<String, Object> params,ChlUserEntity entity) {
        String current = (String)params.get(ConfigConstant.LIMIT);
        String size  = (String)params.get(ConfigConstant.PAGE);

        Page<MessageEntity> page = new Page<>(new Integer(size), new Integer(current));
        List<MessageEntity> list = this.baseMapper.queryPage(page, params);
        return  new PageUtils(page.setRecords(list));
    }

    /**
     * <B>消息推送</B>
     * @param msg
     * @return
     */
    @Override
    public int send(MessageEntity msg,ChlUserEntity entity) {
        JSONObject json = new JSONObject();
        msg.setGuId(System.currentTimeMillis()/1000);
        json.put("guid",msg.getGuId()+"");
        json.put("title",msg.getTitle());
        json.put("subTitle",msg.getSubTitle());
        json.put("content",msg.getContent());
        json.put("accountId",msg.getAccountId()==null?0:msg.getAccountId());
        json.put("pushType",msg.getPushType());
        json.put("channelType",entity.getChannelId());
        if(msg.getPushType() == 2){
            if(msg.getStart()!=null && !"".equalsIgnoreCase(msg.getStart())){
                json.put("start",Timestamp.valueOf(msg.getStart()).getTime()/1000);
            }else{
                return 1;
            }
            if(msg.getFinish()!=null && !"".equalsIgnoreCase(msg.getFinish())){
                json.put("finish",Timestamp.valueOf(msg.getFinish()).getTime()/1000);
            }else{
                return 2;
            }
            if((Timestamp.valueOf(msg.getStart()).getTime()/1000) >= (Timestamp.valueOf(msg.getFinish()).getTime()/1000)){
                return 3;
            }
        }else{
            json.put("start",0);
            json.put("finish",0);
        }
        json.put("m_sessId","");
        json.put("m_serviceId","");
        json.put("m_Function","addPush");
        System.out.println(json.toJSONString());
        msg.setStatus(2);
        String result = HttpRequest.post(UrlConstant.GAME_API_URL+UrlConstant.MESSAGE_URL).timeout(HttpRequest.TIMEOUT_DEFAULT).body(json.toJSONString()).execute().body();
        JSONObject resultJson = JSONObject.parseObject(result);
        if(!resultJson.containsKey("ret")){
            return 1;
        }
        if(resultJson.getIntValue("ret")==0){
            msg.setStatus(1);
        }
        msg.setCreateTime(new Date());
        this.baseMapper.insert(msg);
        System.out.println("消息推送接口返回:-->"+result);
        return resultJson.getIntValue("ret");
    }


}
