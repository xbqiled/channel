package com.channel.cac.modules.cac.dao;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.channel.api.entity.cac.GameRoomVo;
import com.channel.api.entity.cac.UsergamedataEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-04-10 12:50:44
 */
public interface UsergamedataDao extends BaseMapper<UsergamedataEntity> {

    List<UsergamedataEntity> queryUserCtrlPage(Pagination page, @Param("channelId") String channelId, @Param("userId") String userId, @Param("gameRoomId") String gameRoomId,
                                               @Param("type") String type, @Param("betLimitStart") String betLimitStart, @Param("betLimitEnd") String betLimitEnd,
                                               @Param("refreshTime") String refreshTime, @Param("orderStr") String orderStr);

    List<Map<String, Object>> queryUserOnlinePage(Pagination page, @Param("channelId") String channelId, @Param("userId") String userId, @Param("gameRoomId") String gameRoomId);

    List<Map<String, Object>> queryGameRoom();

    List<GameRoomVo> queryGameRoomList();

    List<GameRoomVo> queryGameList();

    List<GameRoomVo> queryAllGameList();
}
