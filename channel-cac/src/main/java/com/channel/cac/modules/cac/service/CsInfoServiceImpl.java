package com.channel.cac.modules.cac.service;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.channel.api.entity.cac.CsInfoEntity;
import com.channel.api.entity.chl.ChlUserEntity;
import com.channel.api.service.cac.CsInfoService;
import com.channel.cac.modules.cac.dao.CsInfoDao;
import com.channel.common.constant.ConfigConstant;
import com.channel.common.utils.PageUtils;
import com.channel.common.utils.Query;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Map;



@Service("csInfoService")
@com.alibaba.dubbo.config.annotation.Service(
        interfaceClass = CsInfoService.class,
        timeout = 10000,
        version = "${api.service.version}"
)
public class CsInfoServiceImpl extends ServiceImpl<CsInfoDao, CsInfoEntity> implements CsInfoService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        Page<CsInfoEntity> page = this.selectPage(
                new Query<CsInfoEntity>(params).getPage(),
                new EntityWrapper<CsInfoEntity>()
        );

        return new PageUtils(page);
    }


    @Override
    public Boolean saveCsInfo(CsInfoEntity csInfoEntity, ChlUserEntity userEntity) {
        csInfoEntity.setChannelId(userEntity.getChannelId().toString());
        csInfoEntity.setCreateTime(new Date());
        csInfoEntity.setCreateBy(userEntity.getUsername());
        return this.insert(csInfoEntity);
    }

    @Override
    public Boolean modifyCsInfo(CsInfoEntity csInfoEntity, ChlUserEntity userEntity) {
        CsInfoEntity resultInfo = this.selectById(csInfoEntity.getId());
        csInfoEntity.setCreateTime(resultInfo.getCreateTime());
        csInfoEntity.setCreateBy(resultInfo.getCreateBy());

        csInfoEntity.setChannelId(userEntity.getChannelId().toString());
        return  this.updateAllColumnById(csInfoEntity);
    }

    @Override
    public PageUtils queryCsInfoList(Map<String, Object> params, ChlUserEntity userEntity) {
        String current = (String)params.get(ConfigConstant.LIMIT);
        String size  = (String)params.get(ConfigConstant.PAGE);

        //根据拓展Server获取渠道ID
        String channelId = ObjectUtil.isNotNull(userEntity.getChannelId()) ? userEntity.getChannelId().toString() : "";
        String csName = ObjectUtil.isNotNull(params.get("csName")) ? (String)params.get("csName") : "";

        Page<CsInfoEntity> page = new Page<>(new Integer(size), new Integer(current));
        List<CsInfoEntity> list = this.baseMapper.queryCsInfoList(page, channelId, csName);
        return  new PageUtils(page.setRecords(list));
    }
}
