package com.channel.cac.modules.cac.service;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.channel.api.entity.cac.VideoBalanceVo;
import com.channel.api.entity.cac.ChannelEntity;
import com.channel.api.entity.chl.ChlUserEntity;
import com.channel.api.service.cac.ChannelService;
import com.channel.cac.modules.cac.dao.ChannelDao;
import com.channel.common.utils.PageUtils;
import com.channel.common.utils.Query;
import org.springframework.stereotype.Service;

import java.util.Map;



@Service("channelService")
@com.alibaba.dubbo.config.annotation.Service(
        interfaceClass = ChannelService.class,
        timeout = 20000,
        version = "${api.service.version}"
)
public class ChannelServiceImpl extends ServiceImpl<ChannelDao, ChannelEntity> implements ChannelService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        Page<ChannelEntity> page = this.selectPage(
                new Query<ChannelEntity>(params).getPage(),
                new EntityWrapper<ChannelEntity>()
        );
        return new PageUtils(page);
    }


    @Override
    public VideoBalanceVo getVideoBalance(ChlUserEntity chlUserEntity) {
        return baseMapper.getVideoBalance(chlUserEntity.getChannelId());
    }


    @Override
    public ChannelEntity findById(ChlUserEntity chlUserEntity) {
        return baseMapper.findChannelById(chlUserEntity.getChannelId());
    }
}
