package com.channel.cac.modules.chl.service;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.channel.api.entity.chl.ChlUserEntity;
import com.channel.api.entity.chl.ChlWhiteIpEntity;
import com.channel.api.service.chl.ChlWhiteIpService;
import com.channel.common.annotation.ChlLog;
import com.channel.common.utils.PageUtils;
import com.channel.common.utils.Query;
import com.channel.cac.modules.chl.dao.ChlWhiteIpDao;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Map;

@Service("chlWhiteIpService")
@com.alibaba.dubbo.config.annotation.Service(
        interfaceClass = ChlWhiteIpService.class,
        timeout = 10000,
        version = "${api.service.version}"
)
public class ChlWhiteIpServiceImpl extends ServiceImpl<ChlWhiteIpDao, ChlWhiteIpEntity> implements ChlWhiteIpService {

    @Override
    public PageUtils queryPage(Map<String, Object> params, ChlUserEntity userEntity) {
        String paramKey = (String)params.get("paramKey");

        Page<ChlWhiteIpEntity> page = this.selectPage(
                new Query<ChlWhiteIpEntity>(params).getPage(),
                new EntityWrapper<ChlWhiteIpEntity>()
                .like(StringUtils.isNotBlank(paramKey),"note", paramKey).eq("user_id",  userEntity.getUserId())
        );

        return new PageUtils(page);
    }

    @Override
    public void save(ChlWhiteIpEntity chlWhiteIpEntity, ChlUserEntity userEntity) {
        chlWhiteIpEntity.setCreateTime(new Date());
        chlWhiteIpEntity.setUserId(userEntity.getUserId().toString());
        chlWhiteIpEntity.setUsername(userEntity.getUsername());
        this.insert(chlWhiteIpEntity);
    }


    @Override
    public ChlWhiteIpEntity queryByIp(String ip, String userId) {
        return baseMapper.queryByIp(ip, userId);
    }

}
