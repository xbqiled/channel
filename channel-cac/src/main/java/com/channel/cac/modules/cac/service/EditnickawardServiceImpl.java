package com.channel.cac.modules.cac.service;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.channel.api.entity.cac.EditnickawardEntity;
import com.channel.api.entity.cac.EditnickawardVo;
import com.channel.api.entity.chl.ChlUserEntity;
import com.channel.api.service.cac.EditnickawardService;
import com.channel.cac.modules.cac.dao.EditnickawardDao;
import com.channel.common.constant.ConfigConstant;
import com.channel.common.json.GsonNickRward;
import com.channel.common.json.GsonResult;
import com.channel.common.utils.HttpHandler;
import com.channel.common.utils.PageUtils;
import com.google.gson.Gson;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;



@Service("editnickawardService")
@com.alibaba.dubbo.config.annotation.Service(
        interfaceClass = EditnickawardService.class,
        timeout = 20000,
        version = "${api.service.version}"
)
public class EditnickawardServiceImpl extends ServiceImpl<EditnickawardDao, EditnickawardEntity> implements EditnickawardService {

    @Override
    public PageUtils queryNickAwardPage(Map<String, Object> params, ChlUserEntity userEntity) {
        String current = (String) params.get(ConfigConstant.LIMIT);
        String size = (String) params.get(ConfigConstant.PAGE);

        String channelId = ObjectUtil.isNotNull(userEntity.getChannelId()) ? userEntity.getChannelId() : "";
        String startTime = ObjectUtil.isNotNull(params.get("startTime")) ? (String) params.get("startTime") : "";
        String endTime = ObjectUtil.isNotNull(params.get("endTime")) ? (String) params.get("endTime") : "";

        Page<EditnickawardVo> page = new Page<>(new Integer(size), new Integer(current));
        List<EditnickawardVo> list = this.baseMapper.queryEditNickAwardPage(page, channelId, startTime, endTime);
        return  new PageUtils(page.setRecords(list));
    }

    @Override
    public EditnickawardVo findByChannelId(String channelId) {
        return baseMapper.getEditNickAwardByChannelId(channelId);
    }

    @Override
    public int saveNcikAwardCfg(String channelId, Integer openType, Integer award) {
        GsonResult result = HttpHandler.configNickAward(openType, channelId, award);
        if (result != null) {
            return result.getRet();
        }
        return 1;
    }


    @Override
    public int channelOpenType(String channelId, String openType) {
        EditnickawardVo model  = baseMapper.getEditNickAwardByChannelId(channelId);
        if (model != null && StrUtil.isNotEmpty(model.getChannelId())) {

            Integer type = model.getDepositType() == 1 ? 0 : 1;
            String cfg = new String(model.getStrJsonAward());
            if (StrUtil.isNotEmpty(cfg)) {
                GsonNickRward nickRward = new Gson().fromJson(cfg, GsonNickRward.class);
                GsonResult result = HttpHandler.configNickAward(type, channelId, nickRward.getAward());
                if (result != null) {
                    return result.getRet();
                }
            }
        }
        return 1;
    }
}
