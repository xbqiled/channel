package com.channel.cac.modules.cac.service;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.channel.api.entity.cac.UserbaseattrEntity;
import com.channel.api.service.cac.UserbaseattrService;
import com.channel.common.utils.PageUtils;
import com.channel.common.utils.Query;
import com.channel.cac.modules.cac.dao.UserbaseattrDao;
import org.springframework.stereotype.Service;

import java.util.Map;



@Service("userbaseattrService")
@com.alibaba.dubbo.config.annotation.Service(
        interfaceClass = UserbaseattrService.class,
        timeout = 10000,
        version = "${api.service.version}"
)
public class UserbaseattrServiceImpl extends ServiceImpl<UserbaseattrDao, UserbaseattrEntity> implements UserbaseattrService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        Page<UserbaseattrEntity> page = this.selectPage(
                new Query<UserbaseattrEntity>(params).getPage(),
                new EntityWrapper<UserbaseattrEntity>()
        );
        return new PageUtils(page);
    }

}
