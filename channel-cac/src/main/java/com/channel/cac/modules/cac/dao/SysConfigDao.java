package com.channel.cac.modules.cac.dao;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.channel.api.entity.cac.SysConfigEntity;

/**
 * 系统配置信息表
 * 
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-04-05 15:43:49
 */
public interface SysConfigDao extends BaseMapper<SysConfigEntity> {

    /**
     * 根据key，查询value
     */
    SysConfigEntity queryByKey(String paramKey);

}
