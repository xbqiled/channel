package com.channel.cac.modules.shiro.service;


import com.channel.api.entity.chl.ChlMenuEntity;
import com.channel.api.entity.chl.ChlUserEntity;
import com.channel.api.entity.chl.ChlUserTokenEntity;
import com.channel.api.service.shiro.ShiroService;
import com.channel.api.service.chl.ChlMenuService;
import com.channel.api.service.chl.ChlUserService;
import com.channel.api.service.chl.ChlUserTokenService;
import com.channel.common.constant.Constant;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;


@Service("shiroService")
@com.alibaba.dubbo.config.annotation.Service(
        interfaceClass = ShiroService.class,
        version = "${api.service.version}"
)
public class ShiroServiceImpl implements ShiroService {

    @Autowired
    private ChlMenuService chlMenuService;

    @Autowired
    private ChlUserService chlUserService;

    @Autowired
    private ChlUserTokenService chlUserTokenService;

    @Override
    public Set<String> getUserPermissions(long userId) {
        List<String> permsList;

        //系统管理员，拥有最高权限
        if (userId == Constant.SUPER_ADMIN) {
            List<ChlMenuEntity> menuList = chlMenuService.queryAll();
            permsList = new ArrayList<>(menuList.size());
            for (ChlMenuEntity menu : menuList) {
                permsList.add(menu.getPerms());
            }

        } else {
            permsList = chlUserService.queryAllPerms(userId);
        }
        //用户权限列表
        Set<String> permsSet = new HashSet<>();
        for (String perms : permsList) {
            if (StringUtils.isBlank(perms)) {
                continue;
            }
            permsSet.addAll(Arrays.asList(perms.trim().split(",")));
        }
        return permsSet;
    }

    @Override
    public ChlUserTokenEntity queryByToken(String token) {
        return chlUserTokenService.queryByToken(token);
    }

    @Override
    public ChlUserEntity queryUser(Long userId) {
        return chlUserService.selectById(userId);
    }
}
