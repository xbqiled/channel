package com.channel.cac.modules.cac.service;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.channel.api.entity.cac.SysPayPlatformEntity;
import com.channel.api.service.cac.SysPayPlatformService;
import com.channel.cac.modules.cac.dao.SysPayPlatformDao;
import com.channel.common.utils.PageUtils;
import com.channel.common.utils.Query;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;



@Service("sysPayPlatformService")
@com.alibaba.dubbo.config.annotation.Service(
        interfaceClass = SysPayPlatformService.class,
        timeout = 10000,
        version = "${api.service.version}"
)
public class SysPayPlatformServiceImpl extends ServiceImpl<SysPayPlatformDao, SysPayPlatformEntity> implements SysPayPlatformService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        Page<SysPayPlatformEntity> page = this.selectPage(
                new Query<SysPayPlatformEntity>(params).getPage(),
                new EntityWrapper<SysPayPlatformEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public List<Map<String, Object>> getPayPlatFormData() {
        return baseMapper.getPayPlatFormData();
    }

    @Override
    public String getPlatformName(Integer configId) {
        return baseMapper.getPlatformName(configId);
    }
}
