package com.channel.cac.modules.cac.dao;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.channel.api.entity.cac.TipSoundTokenEntity;
import org.apache.ibatis.annotations.Param;

/**
 * 
 * 
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-05-19 14:18:20
 */
public interface TipSoundTokenDao extends BaseMapper<TipSoundTokenEntity> {

    TipSoundTokenEntity getTonkenByChannelId(@Param("channelId") String channelId);

    boolean delTonkenByChannelId(@Param("channelId") String channelId);
	
}
