package com.channel.cac.modules.cac.service;

import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.channel.api.entity.cac.BulletinEntity;
import com.channel.api.entity.cac.BulletinVo;
import com.channel.api.entity.chl.ChlUserEntity;
import com.channel.api.service.cac.BulletinService;
import com.channel.cac.modules.cac.dao.BulletinDao;
import com.channel.common.constant.ConfigConstant;
import com.channel.common.constant.UrlConstant;
import com.channel.common.json.GsonResult;
import com.channel.common.utils.HttpHandler;
import com.channel.common.utils.PageUtils;
import com.channel.common.utils.Query;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;




@Service("bulletinService")
@com.alibaba.dubbo.config.annotation.Service(
        interfaceClass = BulletinService.class,
        timeout = 10000,
        version = "${api.service.version}"
)
public class BulletinServiceImpl extends ServiceImpl<BulletinDao, BulletinEntity> implements BulletinService {


    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        Page<BulletinEntity> page = this.selectPage(
                new Query<BulletinEntity>(params).getPage(),
                new EntityWrapper<BulletinEntity>()
        );
        return new PageUtils(page);
    }


    @Override
    public PageUtils queryBulletinPage(Map<String, Object> params, ChlUserEntity userEntity) {
        String current = (String)params.get(ConfigConstant.LIMIT);
        String size  = (String)params.get(ConfigConstant.PAGE);

        //根据拓展Server获取渠道ID
        String channelId = ObjectUtil.isNotNull(userEntity.getChannelId()) ? userEntity.getChannelId().toString() : "";
        String title = ObjectUtil.isNotNull(params.get("title")) ? (String)params.get("title") : "";
        String startTime = ObjectUtil.isNotNull(params.get("startTime")) ? (String)params.get("startTime") : "";
        String endTime = ObjectUtil.isNotNull(params.get("endTime")) ? (String)params.get("endTime") : "";

        Page<Map<String, Object>> page = new Page<>(new Integer(size), new Integer(current));
        List<Map<String, Object>> list = this.baseMapper.queryBulletinPage(page, channelId, title, startTime, endTime);
        return  new PageUtils(page.setRecords(list));
    }


    @Override
    public List<Map<String, Object>> getBulletinInfo(String id, ChlUserEntity userEntity) {
        String channelId = ObjectUtil.isNotNull(userEntity.getChannelId()) ? userEntity.getChannelId().toString() : "";
        return this.baseMapper.getBulletinInfo(channelId, id);
    }


    @Override
    public int addBulletin(BulletinVo bulletinVo, ChlUserEntity userEntity) {
        String guid = IdUtil.simpleUUID();
        //String hyperlink = UrlConstant.FEONT_SYSTM_URL +  UrlConstant.BULLETIN_URL + guid;
        String hyperlink = bulletinVo.getImageUrl();

        Integer start = Integer.parseInt(bulletinVo.getStart().substring(0, 10));
        Integer finish = Integer.parseInt(bulletinVo.getFinish().substring(0, 10));

        GsonResult result = HttpHandler.addBulletin(guid, start, finish, bulletinVo.getTitle(), bulletinVo.getContent(), hyperlink , new String[]{userEntity.getChannelId().toString()});
        if (result != null && result.getRet() == 0) {
            return  0;
        } else {
            return  1;
        }
    }


    @Override
    public int  modifyBulletin(BulletinVo bulletinVo, ChlUserEntity userEntity) {
        //String hyperlink = UrlConstant.FEONT_SYSTM_URL +  UrlConstant.BULLETIN_URL + bulletinVo.getTid();
        String hyperlink = bulletinVo.getImageUrl();
                //转换成数组
        String [] channleList = {userEntity.getChannelId().toString()};

        Integer start = Integer.parseInt(bulletinVo.getStart().substring(0, 10));
        Integer finish = Integer.parseInt(bulletinVo.getFinish().substring(0, 10));

        GsonResult result = HttpHandler.modifyBulletin(bulletinVo.getTid(), start, finish, bulletinVo.getTitle(), bulletinVo.getContent(), hyperlink , channleList);
        if (result != null && result.getRet() == 0) {
            return  0;
        } else {
            return  1;
        }
    }


    @Override
    public int delBulletin(String [] bulletinId) {
        GsonResult result = HttpHandler.batchDelBulletin(bulletinId);
        if (result != null && result.getRet() == 0) {
            return  0;
        } else {
            return  1;
        }
    }
}
