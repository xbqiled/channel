package com.channel.cac.modules.cac.service;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.channel.api.entity.cac.ChannelVipEntity;
import com.channel.api.entity.cac.ChannelsuccourEntity;
import com.channel.api.entity.cac.SuccourVo;
import com.channel.api.entity.cac.VipVo;
import com.channel.api.entity.chl.ChlUserEntity;
import com.channel.api.service.cac.ChannelsuccourService;
import com.channel.cac.modules.cac.dao.ChannelsuccourDao;
import com.channel.common.constant.ConfigConstant;
import com.channel.common.json.GsonResult;
import com.channel.common.json.GsonSuccour;
import com.channel.common.json.RequestVip;
import com.channel.common.utils.HttpHandler;
import com.channel.common.utils.PageUtils;
import com.google.gson.Gson;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;


@Service("channelsuccourService")
@com.alibaba.dubbo.config.annotation.Service(
        interfaceClass = ChannelsuccourService.class,
        timeout = 20000,
        version = "${api.service.version}"
)
public class ChannelsuccourServiceImpl extends ServiceImpl<ChannelsuccourDao, ChannelsuccourEntity> implements ChannelsuccourService {


    @Override
    public PageUtils querySuccourPage(Map<String, Object> params, ChlUserEntity chlUser) {
        String current = (String) params.get(ConfigConstant.LIMIT);
        String size = (String) params.get(ConfigConstant.PAGE);

        String channelId = ObjectUtil.isNotNull(chlUser.getChannelId()) ? chlUser.getChannelId() : "";
        String startTime = ObjectUtil.isNotNull(params.get("startTime")) ? (String) params.get("startTime") : "";
        String endTime = ObjectUtil.isNotNull(params.get("endTime")) ? (String) params.get("endTime") : "";

        Page<SuccourVo> page = new Page<>(new Integer(size), new Integer(current));
        List<SuccourVo> list = this.baseMapper.querySuccourPage(page, channelId, startTime, endTime);
        return new PageUtils(page.setRecords(list));
    }


    @Override
    public int channelOpenType(String channelId, String openType) {
        ChannelsuccourEntity channelSuccour = baseMapper.getSuccourByChannelId(channelId);
        if (channelSuccour != null && StrUtil.isNotEmpty(channelSuccour.getTChannel())) {

            Integer type = channelSuccour.getTOpentype() == 1 ? 0 : 1;
            String jsonStr = new String(channelSuccour.getTSuccourinfo());
            if (StrUtil.isNotEmpty(jsonStr)) {
                GsonSuccour gsonSuccour = new Gson().fromJson(jsonStr, GsonSuccour.class);
                GsonResult result = HttpHandler.cfgSuccour(channelId, type, gsonSuccour.getCoinLimit(), gsonSuccour.getSuccourAward(),
                        gsonSuccour.getTotalSuccourCnt(), gsonSuccour.getShareConditionCnt());
                if (result != null) {
                    return result.getRet();
                }
            }
        }
        return 1;
    }


    @Override
    public GsonSuccour findByChannelId(String channelId) {
        ChannelsuccourEntity channelSuccour = baseMapper.getSuccourByChannelId(channelId);
        if (channelSuccour != null && StrUtil.isNotEmpty(channelSuccour.getTChannel())) {
            String jsonStr = new String(channelSuccour.getTSuccourinfo());
            if (StrUtil.isNotEmpty(jsonStr)) {
                GsonSuccour gsonSuccour = new Gson().fromJson(jsonStr, GsonSuccour.class);
                gsonSuccour.setOpenType(channelSuccour.getTOpentype());
                return gsonSuccour;
            }
        }
        return null;
    }


    @Override
    public int saveOrUpdateConfig(ChlUserEntity userEntity, Integer openType, Long coinLimit, Long succourAward, Integer totalSuccourCnt, Integer shareConditionCnt) {

        String channelId = userEntity.getChannelId();
        GsonResult result = HttpHandler.cfgSuccour(channelId, openType, coinLimit, succourAward, totalSuccourCnt, shareConditionCnt);
        if (result != null) {
            return result.getRet();
        }
        return 1;
    }
}
