package com.channel.cac.modules.cac.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.channel.api.entity.cac.ChannelVipEntity;
import com.channel.api.entity.cac.VipVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * VIEW
 * 
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-12-17 14:48:34
 */
public interface ChannelVipDao extends BaseMapper<ChannelVipEntity> {

    List<VipVo> queryVipPage(Pagination page, @Param("channelId") String channelId, @Param("startTime") String startTime, @Param("endTime") String endTime);

    ChannelVipEntity getVipByChannelId(@Param("channelId") String channelId);

}
