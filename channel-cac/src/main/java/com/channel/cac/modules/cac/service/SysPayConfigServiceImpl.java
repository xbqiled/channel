package com.channel.cac.modules.cac.service;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.channel.api.entity.cac.PayChannelVo;
import com.channel.api.entity.cac.SysPayConfigEntity;
import com.channel.api.entity.cac.SysPayConfigVo;
import com.channel.api.entity.chl.ChlUserEntity;
import com.channel.api.service.cac.SysPayConfigService;
import com.channel.cac.modules.cac.dao.SysPayConfigDao;
import com.channel.common.constant.ConfigConstant;
import com.channel.common.json.GsonPayValue;
import com.channel.common.utils.PageUtils;
import com.channel.common.utils.PayImageUtil;
import com.channel.common.utils.Query;
import com.channel.common.validator.ValidatorUtils;
import com.google.gson.Gson;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;



@Service("sysPayConfigService")
@com.alibaba.dubbo.config.annotation.Service(
        interfaceClass = SysPayConfigService.class,
        timeout = 10000,
        version = "${api.service.version}"
)
public class SysPayConfigServiceImpl extends ServiceImpl<SysPayConfigDao, SysPayConfigEntity> implements SysPayConfigService {

    @Override
    public PageUtils queryPage(Map<String, Object> params, ChlUserEntity userEntity) {
        Page<SysPayConfigEntity> page = this.selectPage(
                new Query<SysPayConfigEntity>(params).getPage(),
                new EntityWrapper<SysPayConfigEntity>().eq("channel_id", userEntity.getChannelId())
                        .eq(params.get("userId") != null && !params.get("userId").equals("") , "user_id", params.get("userId"))
                        .ge(params.get("startTime") != null && !params.get("startTime").equals(""), "create_time", params.get("startTime"))
                        .le(params.get("endTime") != null && !params.get("endTime").equals(""),  "create_time", params.get("endTime"))
        );
        return new PageUtils(page);
    }

    @Override
    public Boolean updateConfigStatus(String id, String status) {
        String commitStatus = StrUtil.isNotBlank(status) && status.equals("1") ? "0" : "1";
        return baseMapper.updateStatus(commitStatus, id);
    }

    @Override
    public PageUtils queryPayConfigPay(Map<String, Object> params, ChlUserEntity userEntity) {
        String current = (String)params.get(ConfigConstant.LIMIT);
        String size  = (String)params.get(ConfigConstant.PAGE);

        //根据拓展Server获取渠道ID
        String channelId = ObjectUtil.isNotNull(userEntity.getChannelId()) ? userEntity.getChannelId() : "";
        String payName = ObjectUtil.isNotNull(params.get("payName")) ? (String)params.get("payName") : "";

        Page<PayChannelVo> page = new Page<>(new Integer(size), new Integer(current));
        List<PayChannelVo> list = this.baseMapper.queryPayConfigPage(page, payName, channelId);
        return  new PageUtils(page.setRecords(list));
    }


    @Override
    public Boolean saveConfig(SysPayConfigVo sysPayConfigVo, ChlUserEntity chlUserEntity) {
        sysPayConfigVo.setChannelId(chlUserEntity.getChannelId());
        sysPayConfigVo.setCreateTime(new Date());
        sysPayConfigVo.setCreateBy(chlUserEntity.getUsername());

        sysPayConfigVo.setIcon(PayImageUtil.getIconUrl(sysPayConfigVo.getPayType()));
        SysPayConfigEntity model = new SysPayConfigEntity();
        BeanUtil.copyProperties(sysPayConfigVo, model);

        this.insert(model);
        return Boolean.TRUE;
    }


    @Override
    public Boolean updateConfig(SysPayConfigVo sysPayConfigVo, ChlUserEntity chlUserEntity) {
        SysPayConfigEntity model = new SysPayConfigEntity();

        SysPayConfigEntity sysPayConfig = this.selectById(sysPayConfigVo.getId());
        sysPayConfig.setIcon(PayImageUtil.getIconUrl(sysPayConfig.getPayType()));
        sysPayConfigVo.setCreateBy(sysPayConfig.getCreateBy());

        sysPayConfigVo.setCreateTime(sysPayConfig.getCreateTime());
        sysPayConfigVo.setPayValueConfig(sysPayConfig.getPayValueConfig());
        BeanUtil.copyProperties(sysPayConfigVo, model);

        model.setChannelId(chlUserEntity.getChannelId());
        this.updateAllColumnById(model);//全部更新
        return Boolean.TRUE;
    }


    @Override
    public List<GsonPayValue> getPayValueConfig(String id) {
        SysPayConfigEntity sysPayConfigEntity =  this.selectById(id);
        if (StrUtil.isNotEmpty(sysPayConfigEntity.getPayValueConfig())) {

            GsonPayValue [] array = new Gson().fromJson(sysPayConfigEntity.getPayValueConfig(), GsonPayValue[].class);
            List<GsonPayValue> resultList = Arrays.asList(array);
            return resultList;
        }
        return  null;
    }


    @Override
    public Boolean updatePayValueConfig(Integer id, GsonPayValue  [] gsonPayValue) {
        baseMapper.updateConfig(id, new Gson().toJson(gsonPayValue));
        return Boolean.TRUE;
    }
}
