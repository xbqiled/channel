package com.channel.cac.modules.cac.dao;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.channel.api.entity.cac.StatisAccountEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 
 * 
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-06-16 15:38:03
 */
public interface StatisAccountDao extends BaseMapper<StatisAccountEntity> {

    List<Map<String, Object>> queryStatisAccountDataPage(Pagination page, @Param("tableSql") String tableSql, @Param("channelId") String channelId, @Param("userId") String userId, @Param("startTime") String startTime, @Param("endTime") String endTime);

    Map<String, Object> queryStatiAccountData(@Param("tableSql") String tableSql, @Param("channelId") String channelId, @Param("userId") String userId, @Param("startTime") String startTime, @Param("endTime") String endTime);

    List<String> queryHaveTab(@Param("tableSql") String tableSql);

    List<StatisAccountEntity> queryStatisAccountPage(Pagination page, @Param("tableName") String tableName,
                                                     @Param("channelId") String channelId,
                                                     @Param("userId") String userId,
                                                     @Param("startTime") String startTime,
                                                     @Param("endTime") String endTime);

}
