package com.channel.cac.modules.cac.dao;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.channel.api.entity.cac.BgTransferEntity;
import org.apache.ibatis.annotations.Param;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import java.util.List;

/**
 * 
 * 
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-11-15 13:23:00
 */
public interface BgTransferDao extends BaseMapper<BgTransferEntity> {

   List<BgTransferEntity> queryBgTransferPage(Pagination page, @Param("userId") String userId,  @Param("channelId") String channelId, @Param("startTime") String startTime, @Param("endTime") String endTime);
	
}
