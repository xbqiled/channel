package com.channel.cac.modules.cac.dao;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.channel.api.entity.cac.MessageEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @author Howard
 * @email howardwong2312@gmail.com
 * @date 2020-02-03 16:01:03
 */
public interface MessageDao extends BaseMapper<MessageEntity> {

    List<MessageEntity> queryPage(Pagination page, @Param("obj") Map<String,Object> obj);

    MessageEntity queryById(@Param("id") Long id);
}
