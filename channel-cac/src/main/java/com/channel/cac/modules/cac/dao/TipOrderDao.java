package com.channel.cac.modules.cac.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.channel.api.entity.cac.TipOrderEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 
 * 
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-05-19 10:25:39
 */
public interface TipOrderDao extends BaseMapper<TipOrderEntity> {

    List<Map<String, Object>> queryTipInfoOrder (@Param("channelId")String channelId);

}
