package com.channel.cac.modules.cac.service;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.channel.api.entity.cac.AccountinfoEntity;
import com.channel.api.entity.cac.UserMailEntity;
import com.channel.api.entity.cac.UserMailVo;
import com.channel.api.entity.chl.ChlUserEntity;
import com.channel.api.service.cac.UserMailService;
import com.channel.cac.modules.cac.dao.AccountinfoDao;
import com.channel.cac.modules.cac.dao.UserMailDao;
import com.channel.common.constant.ConfigConstant;
import com.channel.common.json.GsonResult;
import com.channel.common.utils.HttpHandler;
import com.channel.common.utils.PageUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;


@Service("userMailService")
@com.alibaba.dubbo.config.annotation.Service(
        interfaceClass = UserMailService.class,
        timeout = 10000,
        version = "${api.service.version}"
)
public class UserMailServiceImpl extends ServiceImpl<UserMailDao, UserMailEntity> implements UserMailService {


    @Autowired
    private AccountinfoDao accountinfoDao;


    @Override
    public UserMailEntity findbyId(String id, String userId,  ChlUserEntity userEntity) {
        return baseMapper.findbyId(userEntity.getChannelId(), id, userId);
    }

    /**
     * 查询邮件信息
     * @param params
     * @param userEntity
     * @return
     */
    @Override
    public PageUtils queryUserMailPage(Map<String, Object> params,  ChlUserEntity userEntity) {
        String current = (String)params.get(ConfigConstant.LIMIT);
        String size  = (String)params.get(ConfigConstant.PAGE);

        //根据拓展Server获取渠道ID
        String channelId = ObjectUtil.isNotNull(userEntity.getChannelId()) ? userEntity.getChannelId().toString() : "";
        String title = ObjectUtil.isNotNull(params.get("title")) ? (String)params.get("title") : "";
        String userId = ObjectUtil.isNotNull(params.get("userId")) ? (String)params.get("userId") : "";
        String startTime = ObjectUtil.isNotNull(params.get("startTime")) ? (String)params.get("startTime") : "";
        String endTime = ObjectUtil.isNotNull(params.get("endTime")) ? (String)params.get("endTime") : "";

        Page<Map<String, Object>> page = new Page<>(new Integer(size), new Integer(current));
        List<Map<String, Object>> list = this.baseMapper.queryMailPage(page, userId, channelId, title, startTime, endTime);
        return  new PageUtils(page.setRecords(list));
    }

    /**
     * <B>发送Email信息</B>
     * @param userMailVo
     * @param entity
     * @return
     */
    @Override
    public int sendMail(UserMailVo userMailVo, ChlUserEntity entity) {

        String title = userMailVo.getTitle();
        String content = userMailVo.getContent();

        String rewardId = userMailVo.getRewardId();
        String rewardNum = userMailVo.getRewardNum();

        if (StrUtil.isBlank(userMailVo.getUserList()) && userMailVo.getSendType() == 4) {
            return 3;
        }

        int [] userList = {};
        if (StrUtil.isNotBlank(userMailVo.getUserList())) {
            String userStr = userMailVo.getUserList();
            if (userStr.contains(",")) {

                String[] strDesc = userStr.split(",");
                if (strDesc != null && strDesc.length > 0) {
                    userList = new int [strDesc.length];
                    for (int i = 0; i < strDesc.length; i++) {
                        userList[i] = Integer.parseInt(strDesc[i]);
                    }
                }
            } else {
                AccountinfoEntity account = accountinfoDao.getUserInfo(userStr, entity.getChannelId());
                if (account != null && StrUtil.isNotBlank(account.gettAccountname())) {
                    userList = new int[1];
                    userList[0] = Integer.parseInt(userStr);
                } else {
                    return 1;
                }
            }
        }

        int [] channelList = {};
        //以渠道方式发送
        if (userMailVo.getSendType() == 5) {
            channelList = new int[1];
            channelList[0] = Integer.parseInt(entity.getChannelId());
        }

        GsonResult result = HttpHandler.sendMail(title, content, rewardId, rewardNum, channelList, userList, userMailVo.getSendType());
        if (result != null && result.getRet() == 0) {
            return result.getRet();
        } else {
            return 2;
        }
    }
}
