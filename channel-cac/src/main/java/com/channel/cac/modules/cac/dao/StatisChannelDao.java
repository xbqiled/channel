package com.channel.cac.modules.cac.dao;

import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.channel.api.entity.cac.StatisChannelEntity;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.channel.api.entity.cac.StatisChannelVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 
 * 
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2020-02-28 14:18:36
 */
public interface StatisChannelDao extends BaseMapper<StatisChannelEntity> {

    List<String> queryHaveTab(@Param("tableSql") String tableSql);


    List<StatisChannelVo> getDayReportPage(Pagination page, @Param("tableName")String tableName,
                                           @Param("channelId")String channelId, @Param("startTime")String startTime,
                                           @Param("endTime")String endTime);


    List<StatisChannelVo> getMonthReportPage(Pagination page,  @Param("tableName")String tableName,
                                             @Param("channelId")String channelId,
                                             @Param("monthTime")String monthTime);
	
}
