package com.channel.cac.modules.cac.service;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.channel.api.entity.cac.ChannelVipEntity;
import com.channel.api.entity.cac.VipVo;
import com.channel.api.entity.chl.ChlUserEntity;
import com.channel.api.service.cac.ChannelVipService;
import com.channel.cac.modules.cac.dao.ChannelVipDao;
import com.channel.common.constant.ConfigConstant;
import com.channel.common.json.GsonResult;
import com.channel.common.json.GsonVip;
import com.channel.common.json.RequestVip;
import com.channel.common.utils.HttpHandler;
import com.channel.common.utils.PageUtils;
import com.google.gson.Gson;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;


@Service("channelVipService")
@com.alibaba.dubbo.config.annotation.Service(interfaceClass = ChannelVipService.class, timeout = 10000, version = "${api.service.version}")
public class ChannelVipServiceImpl extends ServiceImpl<ChannelVipDao, ChannelVipEntity> implements ChannelVipService {

    @Override
    public PageUtils queryChannelVipPage(Map<String, Object> params, ChlUserEntity chlUser) {
        String current = (String) params.get(ConfigConstant.LIMIT);
        String size = (String) params.get(ConfigConstant.PAGE);

        String channelId = ObjectUtil.isNotNull(chlUser.getChannelId()) ? chlUser.getChannelId() : "";
        String startTime = ObjectUtil.isNotNull(params.get("startTime")) ? (String) params.get("startTime") : "";
        String endTime = ObjectUtil.isNotNull(params.get("endTime")) ? (String) params.get("endTime") : "";

        Page<VipVo> page = new Page<>(new Integer(size), new Integer(current));
        List<VipVo> list = this.baseMapper.queryVipPage(page, channelId, startTime, endTime);
        return new PageUtils(page.setRecords(list));
    }

    @Override
    public int channelOpenType(String channelId, String openType) {
        ChannelVipEntity channelVip = baseMapper.getVipByChannelId(channelId);
        if (channelVip != null && StrUtil.isNotEmpty(channelVip.getTChannel())) {

            Integer type = channelVip.getTOpentype() == 1 ? 0 : 1;
            String jsonStr = new String(channelVip.getTVipinfo());
            if (StrUtil.isNotEmpty(jsonStr)) {
                RequestVip requestVip = new Gson().fromJson(jsonStr, RequestVip.class);
                GsonResult result = HttpHandler.cfgVip(channelId, type, requestVip.getVip1(), requestVip.getVip2(),
                        requestVip.getVip3(), requestVip.getVip4(), requestVip.getVip5(), requestVip.getVip6(),
                        requestVip.getVip7(), requestVip.getVip8(), requestVip.getVip9());
                if (result != null) {
                    return result.getRet();
                }
            }
        }
        return 1;
    }

    @Override
    public RequestVip findByChannelId(String channelId) {
        ChannelVipEntity channelVip = baseMapper.getVipByChannelId(channelId);
        if (channelVip != null && StrUtil.isNotEmpty(channelVip.getTChannel())) {
            String jsonStr = new String(channelVip.getTVipinfo());
            if (StrUtil.isNotEmpty(jsonStr)) {
                RequestVip vip = new Gson().fromJson(jsonStr, RequestVip.class);
                vip.setOpenType(channelVip.getTOpentype());
                return vip;
            }
        }
        return null;
    }

    @Override
    public int saveOrUpdateConfig(ChlUserEntity userEntity, Integer openType,
                                  GsonVip vip1, GsonVip vip2, GsonVip vip3, GsonVip vip4, GsonVip vip5,
                                  GsonVip vip6, GsonVip vip7, GsonVip vip8, GsonVip vip9) {

        String channelId = userEntity.getChannelId();
        GsonResult result = HttpHandler.cfgVip(channelId, openType, vip1, vip2, vip3, vip4, vip5, vip6, vip7, vip8, vip9);
        if (result != null) {
            return result.getRet();
        }

        return 1;
    }
}
