package com.channel.cac.modules.cac.dao;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.channel.api.entity.cac.OnlineplayerEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * VIEW
 * 
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-05-21 15:14:50
 */
public interface OnlineplayerDao extends BaseMapper<OnlineplayerEntity> {
    List<Map<String, Object>> queryUserOnlinePage(Pagination page, @Param("nickName") String nickName, @Param("channelId") String channelId,
                                                  @Param("userId") String userId, @Param("body") String body);

    List<Map<String, Object>> queryLoginByTime(Pagination page, @Param("channelId") String channelId);
    Long queryOnlineNum  (@Param("channelId") String channelId);
    Long queryhallNum  (@Param("channelId") String channelId);
}
