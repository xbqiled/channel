package com.channel.cac.modules.cac.service;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.channel.api.entity.cac.BindUrlEntity;
import com.channel.api.entity.cac.BindUrlVo;
import com.channel.api.entity.chl.ChlUserEntity;
import com.channel.api.service.cac.BindUrlService;
import com.channel.api.service.cac.ChannelService;
import com.channel.cac.modules.cac.dao.BindUrlDao;
import com.channel.common.constant.ConfigConstant;
import com.channel.common.json.GsonResult;
import com.channel.common.utils.HttpHandler;
import com.channel.common.utils.PageUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;


@Service("BindUrlService")
@com.alibaba.dubbo.config.annotation.Service(
        interfaceClass = BindUrlService.class,
        timeout = 20000,
        version = "${api.service.version}"
)
public class BindUrlServiceImpl extends ServiceImpl<BindUrlDao, BindUrlEntity> implements BindUrlService {

    @Override
    public PageUtils queryBindUrlPage(Map<String, Object> params, ChlUserEntity userEntity) {
        String current = (String) params.get(ConfigConstant.LIMIT);
        String size = (String) params.get(ConfigConstant.PAGE);
        Page<BindUrlVo> page = new Page<>(new Integer(size), new Integer(current));

        String url = ObjectUtil.isNotNull(params.get("url")) ? (String) params.get("url") : "";
        String channelId = ObjectUtil.isNotNull(userEntity.getChannelId()) ? userEntity.getChannelId() : "";
        String accountId = ObjectUtil.isNotNull(params.get("accountId")) ? (String) params.get("accountId") : "";
        List<BindUrlVo> list = baseMapper.queryBindUrlPage(page, channelId, accountId, url);
        return new PageUtils(page.setRecords(list));
    }

    @Override
    public BindUrlEntity findByChannelId(String url, String accountId, ChlUserEntity userEntity) {
        return baseMapper.findByChannelId(userEntity.getChannelId(), accountId, url);
    }

    @Override
    public int saveEntity(String url, String accountId, ChlUserEntity userEntity) {
        GsonResult result = HttpHandler.cfgBindUrl(url, 1, userEntity.getChannelId(), Long.valueOf(accountId));
        return result.getRet();
    }

    @Override
    public int updateEntity(String url, String accountId, ChlUserEntity userEntity) {
        GsonResult result = HttpHandler.cfgBindUrl(url, 1,  userEntity.getChannelId(), Long.valueOf(accountId));
        return result.getRet();
    }

    @Override
    public int delEntity(String url, String accountId, ChlUserEntity userEntity) {
        GsonResult result = HttpHandler.cfgBindUrl(url, 2, userEntity.getChannelId(), Long.valueOf(accountId));
        return result.getRet();
    }
}
