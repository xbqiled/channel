package com.channel.cac.modules.cac.service;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.channel.api.entity.cac.StatisGameEntity;
import com.channel.api.entity.cac.StatisGameVo;
import com.channel.api.entity.chl.ChlUserEntity;
import com.channel.api.service.cac.StatisGameService;
import com.channel.cac.modules.cac.dao.StatisGameDao;
import com.channel.common.constant.ConfigConstant;
import com.channel.common.utils.PageUtils;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Map;


@Service("statisGameService")
@com.alibaba.dubbo.config.annotation.Service(interfaceClass = StatisGameService.class, timeout = 50000, version = "${api.service.version}"
)
public class StatisGameServiceImpl extends ServiceImpl<StatisGameDao, StatisGameEntity> implements StatisGameService {


    private static String TABLE_NAME = "statis_game";

    @Override
    public PageUtils queryStatisGamePage(Map<String, Object> params, ChlUserEntity chlUser) {
        String current = (String) params.get(ConfigConstant.LIMIT);
        String size = (String) params.get(ConfigConstant.PAGE);
        Page<StatisGameVo> page = new Page<>(new Integer(size), new Integer(current));
        String channelId = ObjectUtil.isNotNull(chlUser.getChannelId()) ? chlUser.getChannelId() : "";
        String gameId = ObjectUtil.isNotNull(params.get("gameId")) ? (String) params.get("gameId") : "";
        String startTime = ObjectUtil.isNotNull(params.get("startTime")) ? (String) params.get("startTime") : "";

        String endTime = ObjectUtil.isNotNull(params.get("endTime")) ? (String) params.get("endTime") : "";
        String order = ObjectUtil.isNotNull(params.get("order")) ? (String)params.get("order") : "";
        String sortby = ObjectUtil.isNotNull(params.get("sortby")) ? (String)params.get("sortby") : "";

        String orderStr = packGameOrderStr(order, sortby);
        Date data = DateUtil.parse(startTime, "yyyy-MM-dd");
        String monthTable = DateUtil.format(data, "yyyyMM");

        List<StatisGameVo> resultList = baseMapper.queryStatisGamePage(page, TABLE_NAME + monthTable,
                channelId, gameId, startTime, endTime, orderStr);
        return new PageUtils(page.setRecords(resultList));
    }


    @Override
    public PageUtils queryStatisUserGamePage(Map<String, Object> params, ChlUserEntity chlUser) {
        String current = (String) params.get(ConfigConstant.LIMIT);
        String size = (String) params.get(ConfigConstant.PAGE);
        Page<StatisGameVo> page = new Page<>(new Integer(size), new Integer(current));

        String channelId = ObjectUtil.isNotNull(chlUser.getChannelId()) ? chlUser.getChannelId() : "";
        String userId = ObjectUtil.isNotNull(params.get("userId")) ? (String) params.get("userId") : "";
        String gameId = ObjectUtil.isNotNull(params.get("gameId")) ? (String) params.get("gameId") : "";

        String startTime = ObjectUtil.isNotNull(params.get("startTime")) ? (String) params.get("startTime") : "";
        String endTime = ObjectUtil.isNotNull(params.get("endTime")) ? (String) params.get("endTime") : "";
        String order = ObjectUtil.isNotNull(params.get("order")) ? (String)params.get("order") : "";

        String sortby = ObjectUtil.isNotNull(params.get("sortby")) ? (String)params.get("sortby") : "";
        String orderStr = packUserGameOrderStr(order, sortby);
        Date data = DateUtil.parse(startTime, "yyyy-MM-dd");

        String monthTable = DateUtil.format(data, "yyyyMM");
        List<StatisGameVo> resultList = baseMapper.queryStatisUserGamePage(page, TABLE_NAME + monthTable,
                channelId, userId, gameId, startTime, endTime, orderStr);
        return new PageUtils(page.setRecords(resultList));
    }


    @Override
    public List<Map<String, String>> queryGameList() {
        return baseMapper.queryGameList();
    }


    String packGameOrderStr(String order, String sortby) {
        StringBuffer sb = new StringBuffer();
        if (StrUtil.isBlank(order) || StrUtil.isBlank(sortby)) {
            sb.append("  order by a.statisMonthTime desc  ");
            return sb.toString();
        } else {
            sb.append("order by ");
            if (sortby.equals("gameCount")) {
                sb.append(" a.game_count  ");
            } else if (sortby.equals("gameFee")) {
                sb.append(" a.game_fee ");
            } else if (sortby.equals("betFee")) {
                sb.append(" a.bet_fee ");
            } else if (sortby.equals("revenueFee")) {
                sb.append(" a.revenue_fee ");
            } else if (sortby.equals("gameTimeLength")) {
                sb.append(" a.game_time_length ");
            }
            sb.append(order);
            return sb.toString();
        }
    }


    String packUserGameOrderStr(String order, String sortby) {
        StringBuffer sb = new StringBuffer();
        if (StrUtil.isBlank(order) || StrUtil.isBlank(sortby)) {
            sb.append("  order by a.statis_time desc  ");
            return sb.toString();
        } else {
            sb.append("order by ");
            if (sortby.equals("gameCount")) {
                sb.append(" a.game_count  ");
            } else if (sortby.equals("gameFee")) {
                sb.append(" a.game_fee ");
            } else if (sortby.equals("betFee")) {
                sb.append(" a.bet_fee ");
            } else if (sortby.equals("revenueFee")) {
                sb.append(" a.revenue_fee ");
            } else if (sortby.equals("gameTimeLength")) {
                sb.append(" a.game_time_length ");
            }
            sb.append(order);
            return sb.toString();
        }
    }
}
