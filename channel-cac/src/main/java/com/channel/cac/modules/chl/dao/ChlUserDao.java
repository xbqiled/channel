package com.channel.cac.modules.chl.dao;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.channel.api.entity.chl.ChlUserEntity;

import java.util.List;

/**
 * ${comments}
 * 
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2018-12-03 13:03:04
 */
public interface ChlUserDao extends BaseMapper<ChlUserEntity> {

    /**
     * 查询用户的所有权限
     * @param userId  用户ID
     */
    List<String> queryAllPerms(Long userId);

    /**
     * 查询用户的所有菜单ID
     */
    List<Long> queryAllMenuId(Long userId);

    /**
     * 根据用户名，查询系统用户
     */
    ChlUserEntity queryByUserName(String username);
}
