package com.channel.cac.modules.cac.dao;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.channel.api.entity.cac.SysPayPlatformEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 
 * 
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-02-24 19:27:36
 */
public interface SysPayPlatformDao extends BaseMapper<SysPayPlatformEntity> {

    List<Map<String, Object>> getPayPlatFormData();

    String getPlatformName(@Param("configId")Integer configId);

}
