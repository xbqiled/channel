package com.channel.cac.modules.cac.service;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.channel.api.entity.cac.LuckyRouletteVo;
import com.channel.api.entity.cac.LuckyrouletteEntity;
import com.channel.api.entity.chl.ChlUserEntity;
import com.channel.api.service.cac.LuckyrouletteService;
import com.channel.cac.modules.cac.dao.LuckyrouletteDao;
import com.channel.common.constant.ConfigConstant;
import com.channel.common.json.GsonLuckRoulette;
import com.channel.common.json.GsonResult;
import com.channel.common.utils.HttpHandler;
import com.channel.common.utils.PageUtils;
import com.channel.common.utils.Query;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;


@Service("luckyrouletteService")
@com.alibaba.dubbo.config.annotation.Service(
        interfaceClass = LuckyrouletteService.class,
        timeout = 10000,
        version = "${api.service.version}"
)
public class LuckyrouletteServiceImpl extends ServiceImpl<LuckyrouletteDao, LuckyrouletteEntity> implements LuckyrouletteService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        Page<LuckyrouletteEntity> page = this.selectPage(
                new Query<LuckyrouletteEntity>(params).getPage(),
                new EntityWrapper<LuckyrouletteEntity>()
        );

        return new PageUtils(page);
    }


    @Override
    public int channelOpenType(String channelId, String openType) {
        LuckyrouletteEntity model  = baseMapper.getLuckyRouletteByChannelId(channelId);
        if (model != null && StrUtil.isNotEmpty(model.getTChannel())) {

            Integer type = model.gettOpentype() == 1 ? 0 : 1;
            List<GsonLuckRoulette> diamondcfg = packUpdateLuckRoulette(new String(model.getTDiamondcfg()));
            List<GsonLuckRoulette> goldcfg = packUpdateLuckRoulette(new String(model.getTGoldcfg()));
            List<GsonLuckRoulette> silvercfg = packUpdateLuckRoulette(new String(model.getTSilvercfg()));
                GsonResult result = HttpHandler.luckyRoulette(type, Long.valueOf(channelId), diamondcfg, goldcfg, silvercfg, model.getTStart(), model.getTFinish(), model.gettDiamondcost().intValue(),  model.getTGoldcost().intValue(), model.getTSilvercost().intValue());
            if (result != null) {
                return result.getRet();
            }
        }
        return 1;
    }


    private List<GsonLuckRoulette> packUpdateLuckRoulette(String cfg) {
        List<GsonLuckRoulette> list = new ArrayList<>();
        JsonParser parser = new JsonParser();
        Gson gson = new Gson();

        if (StrUtil.isNotEmpty(cfg)) {
            JsonArray jsonArray = parser.parse(cfg).getAsJsonArray();
            for (JsonElement user : jsonArray) {
                //使用GSON，直接转成Bean对象

                GsonLuckRoulette gsonLuckRoulette = gson.fromJson(user, GsonLuckRoulette.class);
                list.add(gsonLuckRoulette);
            }
        }
        return list;
    }


    @Override
    public PageUtils queryLuckyRoulettePage(Map<String, Object> params, ChlUserEntity userEntity) {
        String current = (String) params.get(ConfigConstant.LIMIT);
        String size = (String) params.get(ConfigConstant.PAGE);

        String channelId = ObjectUtil.isNotNull(userEntity.getChannelId()) ? userEntity.getChannelId() : "";
        String startTime = ObjectUtil.isNotNull(params.get("startTime")) ? (String) params.get("startTime") : "";
        String endTime = ObjectUtil.isNotNull(params.get("endTime")) ? (String) params.get("endTime") : "";

        Page<LuckyRouletteVo> page = new Page<>(new Integer(size), new Integer(current));
        List<LuckyRouletteVo> list = this.baseMapper.queryLuckyRoulettePage(page, channelId, startTime, endTime);
        return new PageUtils(page.setRecords(list));
    }


    @Override
    public int saveLuckRoulette(ChlUserEntity userEntity, Integer openType, List<GsonLuckRoulette> diamondcfg, List<GsonLuckRoulette> goldcfg, List<GsonLuckRoulette> silvercfg, String startTimeStr, String finishTimeStr, Integer goldcost, Integer diamondcost, Integer silvercost) {
        //渠道编码
        String channelId = userEntity.getChannelId();

        //时间转换
        Date startDate = DateUtil.parse(startTimeStr);
        Date finishDate = DateUtil.parse(finishTimeStr);

        Long start = startDate.getTime() / 1000;
        Long finish = finishDate.getTime() / 1000;

        GsonResult result = HttpHandler.luckyRoulette(openType, Long.valueOf(channelId), diamondcfg, goldcfg, silvercfg, start, finish, diamondcost, goldcost, silvercost);
        if (result != null) {
            return result.getRet();
        }

        return 1;
    }
}
