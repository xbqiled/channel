package com.channel.cac.modules.cac.service;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.channel.api.entity.cac.SysPayDisplayEntity;
import com.channel.api.service.cac.SysPayDisplayService;
import com.channel.cac.modules.cac.dao.SysPayDisplayDao;
import com.channel.common.utils.PageUtils;
import com.channel.common.utils.Query;
import org.springframework.stereotype.Service;

import java.util.Map;



@Service("sysPayDisplayService")
@com.alibaba.dubbo.config.annotation.Service(
        interfaceClass = SysPayDisplayService.class,
        timeout = 30000,
        version = "${api.service.version}"
)
public class SysPayDisplayServiceImpl extends ServiceImpl<SysPayDisplayDao, SysPayDisplayEntity> implements SysPayDisplayService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        Page<SysPayDisplayEntity> page = this.selectPage(
                new Query<SysPayDisplayEntity>(params).getPage(),
                new EntityWrapper<SysPayDisplayEntity>()
        );

        return new PageUtils(page);
    }




}
