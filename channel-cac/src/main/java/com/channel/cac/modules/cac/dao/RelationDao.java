package com.channel.cac.modules.cac.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.channel.api.entity.cac.RelationEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * VIEW
 * 
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-05-07 17:05:21
 */
public interface RelationDao extends BaseMapper<RelationEntity> {

    List<Map<String, Object>>  queryRelationPage(Pagination page, @Param("channelId") String channelId , @Param("accountId") String accountId  , @Param("promoterId") String promoterId);

}
