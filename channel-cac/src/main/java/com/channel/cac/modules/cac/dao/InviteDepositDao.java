package com.channel.cac.modules.cac.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.channel.api.entity.cac.InviteDepositChannelVo;
import com.channel.api.entity.cac.InviteDepositEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * VIEW
 * 
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-07-31 12:40:41
 */
public interface InviteDepositDao extends BaseMapper<InviteDepositEntity> {

    List<InviteDepositChannelVo> queryInviteDepositPage(Pagination page, @Param("channelId") String channelId, @Param("startTime") String startTime, @Param("endTime") String endTime);

    InviteDepositChannelVo getInviteDepositByChannelId(@Param("channelId") String channelId);

}
