package com.channel.cac.modules.cac.dao;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.channel.api.entity.cac.BindMobileEntity;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 
 * 
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-04-09 13:47:05
 */
public interface BindMobileDao extends BaseMapper<BindMobileEntity> {

    BindMobileEntity findByChannelId(@Param("channelId") String channelId);

    void updateBind(@Param("channelId") String channelId, @Param("modifyTime") Date modifyTime,
                    @Param("modifyBy") String modifyBy, @Param("giveValue") String giveValue,
                    @Param("rebate") String rebate,  @Param("exchange") String exchange,
                    @Param("initCoin")String initCoin, @Param("minExchange") String minExchange ,
                    @Param("damaMulti") String damaMulti, @Param("rebateDamaMulti") String rebateDamaMulti,
                    @Param("exchangeCount") String exchangeCount, @Param("promoteType") String promoteType, @Param("resetDamaLeftCoin")String resetDamaLeftCoin);
}
