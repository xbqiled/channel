package com.channel.cac.modules.chl.service;


import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.channel.api.entity.chl.ChlMenuEntity;
import com.channel.api.service.chl.ChlMenuService;
import com.channel.api.service.chl.ChlRoleMenuService;
import com.channel.api.service.chl.ChlUserService;
import com.channel.common.constant.Constant;
import com.channel.common.utils.MapUtils;
import com.channel.cac.modules.chl.dao.ChlMenuDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


@Service("sysMenuService")
@com.alibaba.dubbo.config.annotation.Service(
		interfaceClass = ChlMenuService.class,
		timeout = 10000,
		version = "${api.service.version}"
)
public class ChlMenuServiceImpl extends ServiceImpl<ChlMenuDao, ChlMenuEntity> implements ChlMenuService {

	@Autowired
	private ChlUserService chlUserService;
	@Autowired
	private ChlRoleMenuService chlRoleMenuService;


	@Override
	public List<ChlMenuEntity> queryChannelList(Long userId) {
		return baseMapper.queryChannelList(userId);
	}


	@Override
	public List<ChlMenuEntity> queryListParentId(Long parentId, List<Long> menuIdList) {
		List<ChlMenuEntity> menuList = queryListParentId(parentId);
		if(menuIdList == null){
			return menuList;
		}

		List<ChlMenuEntity> userMenuList = new ArrayList<>();
		for(ChlMenuEntity menu : menuList){
			if(menuIdList.contains(menu.getMenuId())){
				userMenuList.add(menu);
			}
		}
		return userMenuList;
	}

	@Override
	public List<ChlMenuEntity> queryListParentId(Long parentId) {
		return baseMapper.queryListParentId(parentId);
	}

	@Override
	public List<ChlMenuEntity> queryNotButtonList() {
		return baseMapper.queryNotButtonList();
	}

	@Override
	public List<ChlMenuEntity> getUserMenuList(Long userId) {
		//系统管理员，拥有最高权限
		if(userId == Constant.SUPER_ADMIN){
			return getAllMenuList(null);
		}

		//用户菜单列表
		List<Long> menuIdList = chlUserService.queryAllMenuId(userId);
		return getAllMenuList(menuIdList);
	}

	@Override
	public void delete(Long menuId){
		//删除菜单
		this.deleteById(menuId);
		//删除菜单与角色关联
		chlRoleMenuService.deleteByMap(new MapUtils().put("menu_id", menuId));
	}

	/**
	 * 获取所有菜单列表
	 */
	private List<ChlMenuEntity> getAllMenuList(List<Long> menuIdList){
		//查询根菜单列表
		List<ChlMenuEntity> menuList = queryListParentId(0L, menuIdList);
		//递归获取子菜单
		getMenuTreeList(menuList, menuIdList);
		return menuList;
	}

	/**
	 * 递归
	 */
	private List<ChlMenuEntity> getMenuTreeList(List<ChlMenuEntity> menuList, List<Long> menuIdList){
		List<ChlMenuEntity> subMenuList = new ArrayList<ChlMenuEntity>();

		for(ChlMenuEntity entity : menuList){
			//目录
			if(entity.getType() == Constant.MenuType.CATALOG.getValue()){
				entity.setList(getMenuTreeList(queryListParentId(entity.getMenuId(), menuIdList), menuIdList));
			}
			subMenuList.add(entity);
		}
		return subMenuList;
	}


	@Override
	public List<ChlMenuEntity> queryAll() {
		return baseMapper.queryAll();
	}
}
