package com.channel.cac.modules.cac.dao;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.channel.api.entity.cac.UserExtEntity;
import org.apache.ibatis.annotations.Param;

/**
 * 
 * 
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-09-20 15:21:18
 */
public interface UserExtDao extends BaseMapper<UserExtEntity> {


    Integer updateUserNote (@Param("note")String note, @Param("accountId")String accountId);

    Long queryUserByAccountId(@Param("accountId")String accountId);

}
