package com.channel.cac.modules.cac.dao;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.channel.api.entity.cac.PayChannelVo;
import com.channel.api.entity.cac.SysPayConfigEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 
 * 
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-02-24 19:27:36
 */
public interface SysPayConfigDao extends BaseMapper<SysPayConfigEntity> {

    List<PayChannelVo> queryPayConfigPage(Pagination page, @Param("payName") String payName, @Param("channelId") String channelId);

    Boolean updateStatus(@Param("status") String status, @Param("id") String id);

    Boolean updateConfig(@Param("id") Integer id, @Param("configValue") String configValue);
}
