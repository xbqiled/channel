package com.channel.cac.modules.chl.service;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.channel.api.entity.chl.ChlUserEntity;
import com.channel.api.service.chl.ChlRoleService;
import com.channel.api.service.chl.ChlUserRoleService;
import com.channel.api.service.chl.ChlUserService;
import com.channel.common.constant.Constant;
import com.channel.common.execption.AppException;
import com.channel.common.utils.PageUtils;
import com.channel.common.utils.Query;
import com.channel.cac.modules.chl.dao.ChlUserDao;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.crypto.hash.Sha256Hash;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;


@Service("ChlUserService")
@com.alibaba.dubbo.config.annotation.Service(
        interfaceClass = ChlUserService.class,
        timeout = 20000,
        version = "${api.service.version}"
)
public class ChlUserServiceImpl extends ServiceImpl<ChlUserDao, ChlUserEntity> implements ChlUserService {

    @Override
    public ChlUserEntity selectOne(ChlUserEntity ChlUserEntity) {
        return baseMapper.selectOne(ChlUserEntity);
    }

    @Autowired
    private ChlUserRoleService ChlUserRoleService;

    @Autowired
    private ChlRoleService ChlRoleService;

    @Override
    public List<String> queryAllPerms(Long userId) {
        return baseMapper.queryAllPerms(userId);
    }

    @Override
    public List<Long> queryAllMenuId(Long userId) {
        return baseMapper.queryAllMenuId(userId);
    }

    @Override
    public ChlUserEntity queryByUserName(String username) {
        return baseMapper.queryByUserName(username);
    }


    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        String username = (String)params.get("username");
        Long createUserId = (Long)params.get("createUserId");
        String channelId = params.get("channelId") == null ? null : (String)params.get("channelId") ;

        Page<ChlUserEntity> page = this.selectPage(
                new Query<ChlUserEntity>(params).getPage(),
                new EntityWrapper<ChlUserEntity>()
                        .like(StringUtils.isNotBlank(username),"username", username)
                        .eq(createUserId != null,"create_user_id", createUserId)
                        .eq(StrUtil.isNotBlank(channelId),"channel_id", channelId)
        );

        return new PageUtils(page);
    }

    @Override
    @Transactional
    public void save(ChlUserEntity user) {
        user.setCreateTime(new Date());
        //sha256加密
        String salt = RandomStringUtils.randomAlphanumeric(20);
        user.setPassword(new Sha256Hash(user.getPassword(), salt).toHex());

        user.setSalt(salt);
        this.insert(user);
        //检查角色是否越权
        checkRole(user);

        //保存用户与角色关系
        ChlUserRoleService.saveOrUpdate(user.getUserId(), user.getRoleIdList());
    }

    @Override
    @Transactional
    public void update(ChlUserEntity user) {
        if(StringUtils.isBlank(user.getPassword())){
            user.setPassword(null);
        }else{
            user.setPassword(new Sha256Hash(user.getPassword(), user.getSalt()).toHex());
        }
        this.updateById(user);

        //检查角色是否越权
        checkRole(user);

        //保存用户与角色关系
        ChlUserRoleService.saveOrUpdate(user.getUserId(), user.getRoleIdList());
    }


    @Override
    public void deleteBatch(Long[] userId) {
        this.deleteBatchIds(Arrays.asList(userId));
    }


    @Override
    public boolean updatePassword(Long userId, String password, String newPassword) {
        ChlUserEntity userEntity = new ChlUserEntity();
        userEntity.setPassword(newPassword);
        return this.update(userEntity,
                new EntityWrapper<ChlUserEntity>().eq("user_id", userId).eq("password", password));
    }


    /**
     * 检查角色是否越权
     */
    private void checkRole(ChlUserEntity user){
        if(user.getRoleIdList() == null || user.getRoleIdList().size() == 0){
            return;
        }
        //如果不是超级管理员，则需要判断用户的角色是否自己创建
        if(user.getCreateUserId() == Constant.SUPER_ADMIN){
            return ;
        }

        //查询用户创建的角色列表
        List<Long> roleIdList = ChlRoleService.queryRoleIdList(user.getCreateUserId());

        //判断是否越权
        if(!roleIdList.containsAll(user.getRoleIdList())){
            throw new AppException("新增用户所选角色，不是本人创建");
        }
    }
}
