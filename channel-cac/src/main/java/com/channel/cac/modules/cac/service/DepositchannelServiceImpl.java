package com.channel.cac.modules.cac.service;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.channel.api.entity.cac.DepositchannelEntity;
import com.channel.api.entity.cac.DepositchannelVo;
import com.channel.api.entity.chl.ChlUserEntity;
import com.channel.api.service.cac.DepositchannelService;
import com.channel.cac.modules.cac.dao.DepositchannelDao;
import com.channel.common.constant.ConfigConstant;
import com.channel.common.json.GsonRechargeRebateCfg;
import com.channel.common.json.GsonResult;
import com.channel.common.utils.HttpHandler;
import com.channel.common.utils.PageUtils;
import com.channel.common.utils.Query;
import com.google.gson.Gson;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;


@Service("depositService")
@com.alibaba.dubbo.config.annotation.Service(
        interfaceClass = DepositchannelService.class,
        timeout = 10000,
        version = "${api.service.version}"
)
public class DepositchannelServiceImpl extends ServiceImpl<DepositchannelDao, DepositchannelEntity> implements DepositchannelService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        Page<DepositchannelEntity> page = this.selectPage(
                new Query<DepositchannelEntity>(params).getPage(),
                new EntityWrapper<DepositchannelEntity>()
        );
        return new PageUtils(page);
    }

    @Override
    public DepositchannelEntity findByChannelId(String channelId) {
        return baseMapper.getDepositByChannelId(channelId);
    }

    @Override
    public int channelOpenType(String channelId, String openType) {
        DepositchannelEntity model  = baseMapper.getDepositByChannelId(channelId);
        if (model != null && StrUtil.isNotEmpty(model.getTChannel())) {

            Integer type = model.getTOpentype() == 1 ? 0 : 1;
            String str = new String(model.getTCfginfo());
            List<GsonRechargeRebateCfg> rabateCfg = new ArrayList<>();

            if (StrUtil.isNotBlank(str)) {
                GsonRechargeRebateCfg[] array = new Gson().fromJson(str, GsonRechargeRebateCfg[].class);
                rabateCfg = Arrays.asList(array);
            }

            GsonResult result = HttpHandler.rechargeRebate(channelId, model.getTStarttime(), model.getTEndtime(), type, rabateCfg);
            if (result != null) {
                return result.getRet();
            }
        }
        return 1;
    }

    @Override
    public PageUtils queryDepositPage(Map<String, Object> params, ChlUserEntity userEntity) {
        String current = (String) params.get(ConfigConstant.LIMIT);
        String size = (String) params.get(ConfigConstant.PAGE);

        String channelId = ObjectUtil.isNotNull(userEntity.getChannelId()) ? userEntity.getChannelId() : "";
        String startTime = ObjectUtil.isNotNull(params.get("startTime")) ? (String) params.get("startTime") : "";
        String endTime = ObjectUtil.isNotNull(params.get("endTime")) ? (String) params.get("endTime") : "";

        Page<DepositchannelVo> page = new Page<>(new Integer(size), new Integer(current));
        List<DepositchannelVo> list = this.baseMapper.queryDepositPage(page, channelId, startTime, endTime);
        return  new PageUtils(page.setRecords(list));
    }


    @Override
    public int saveRechargeRebateCfg(String channel, String beginTime, String endTime, Integer openType, List<GsonRechargeRebateCfg> rabateCfg) {
        //时间转换
        Long start = new Long(beginTime) / 1000;
        Long finish = new Long(endTime) / 1000;

        GsonResult result = HttpHandler.rechargeRebate(channel, start, finish, openType, rabateCfg);
        if (result != null) {
            return result.getRet();
        }

        return 1;
    }
}
