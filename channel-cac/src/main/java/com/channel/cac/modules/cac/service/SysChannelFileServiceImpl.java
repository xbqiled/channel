package com.channel.cac.modules.cac.service;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.channel.api.entity.cac.SysChannelFileEntity;
import com.channel.api.entity.chl.ChlUserEntity;
import com.channel.api.service.cac.SysChannelFileService;
import com.channel.cac.modules.cac.dao.SysChannelFileDao;
import com.channel.common.constant.ConfigConstant;
import com.channel.common.utils.PageUtils;
import com.channel.common.utils.Query;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;


@Service("sysChannelFileService")
@com.alibaba.dubbo.config.annotation.Service(
        interfaceClass = SysChannelFileService.class,
        timeout = 10000,
        version = "${api.service.version}"
)
public class SysChannelFileServiceImpl extends ServiceImpl<SysChannelFileDao, SysChannelFileEntity> implements SysChannelFileService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        Page<SysChannelFileEntity> page = this.selectPage(
                new Query<SysChannelFileEntity>(params).getPage(),
                new EntityWrapper<SysChannelFileEntity>()
        );

        return new PageUtils(page);
    }


    @Override
    public PageUtils queryFilePage(Map<String, Object> params, ChlUserEntity userEntity) {
        String current = (String)params.get(ConfigConstant.LIMIT);
        String size  = (String)params.get(ConfigConstant.PAGE);

        //根据拓展Server获取渠道ID
        String channelId = ObjectUtil.isNotNull(userEntity.getChannelId()) ? userEntity.getChannelId().toString() : "";

        Page<SysChannelFileEntity> page = new Page<>(new Integer(size), new Integer(current));
        List<SysChannelFileEntity> list = this.baseMapper.queryFilePage(page, channelId);
        return  new PageUtils(page.setRecords(list));
    }
}
