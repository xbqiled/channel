package com.channel.cac.modules.chl.service;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.channel.api.entity.chl.ChlUserRoleEntity;
import com.channel.api.service.chl.ChlUserRoleService;
import com.channel.common.utils.MapUtils;
import com.channel.cac.modules.chl.dao.ChlUserRoleDao;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


/**
 * 用户与角色对应关系
 *
 */
@Service("chlUserRoleService")
@com.alibaba.dubbo.config.annotation.Service(
		interfaceClass = ChlUserRoleService.class,
		timeout = 10000,
		version = "${api.service.version}"
)
public class ChlUserRoleServiceImpl extends ServiceImpl<ChlUserRoleDao, ChlUserRoleEntity> implements ChlUserRoleService {

	@Override
	public void saveOrUpdate(Long userId, List<Long> roleIdList) {
		//先删除用户与角色关系
		this.deleteByMap(new MapUtils().put("user_id", userId));

		if(roleIdList == null || roleIdList.size() == 0){
			return ;
		}

		//保存用户与角色关系
		List<ChlUserRoleEntity> list = new ArrayList<>(roleIdList.size());
		for(Long roleId : roleIdList){
			ChlUserRoleEntity ChlUserRoleEntity = new ChlUserRoleEntity();
			ChlUserRoleEntity.setUserId(userId);
			ChlUserRoleEntity.setRoleId(roleId);

			list.add(ChlUserRoleEntity);
		}
		this.insertBatch(list);
	}

	@Override
	public List<Long> queryRoleIdList(Long userId) {
		return baseMapper.queryRoleIdList(userId);
	}

	@Override
	public int deleteBatch(Long[] roleIds){
		return baseMapper.deleteBatch(roleIds);
	}
}
