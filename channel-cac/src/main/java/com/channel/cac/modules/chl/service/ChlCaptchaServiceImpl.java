package com.channel.cac.modules.chl.service;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.channel.api.entity.chl.ChlCaptchaEntity;
import com.channel.api.service.chl.ChlCaptchaService;
import com.channel.common.execption.AppException;
import com.channel.common.utils.DateUtils;
import com.channel.cac.modules.chl.dao.ChlCaptchaDao;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;

import java.util.Date;

/**
 * 验证码
 *
 */
@com.alibaba.dubbo.config.annotation.Service(
        interfaceClass = ChlCaptchaService.class,
        timeout = 10000,
        version = "${api.service.version}"
)
public class ChlCaptchaServiceImpl extends ServiceImpl<ChlCaptchaDao, ChlCaptchaEntity> implements ChlCaptchaService {

    @Autowired
    private RedisTemplate redisTemplate;

    @Override
    public boolean getCaptcha(String uuid, String code ) {
        if(StringUtils.isBlank(uuid)){
            throw new AppException("uuid不能为空");
        }

        ChlCaptchaEntity captchaEntity = new ChlCaptchaEntity();
        captchaEntity.setUuid(uuid);
        captchaEntity.setCode(code);
        //30分钟后过期
        captchaEntity.setExpireTime(DateUtils.addDateMinutes(new Date(), 30));
        //存储在REIDS之中
        return this.insert(captchaEntity);
    }

    @Override
    public boolean validate(String uuid, String code) {
        ChlCaptchaEntity captchaEntity = this.selectOne(new EntityWrapper<ChlCaptchaEntity>().eq("uuid", uuid));
        if(captchaEntity == null){
            return false;
        }

        //删除验证码
        this.deleteById(uuid);
        if(captchaEntity.getCode().equalsIgnoreCase(code) && captchaEntity.getExpireTime().getTime() >= System.currentTimeMillis()){
            return true;
        }
        return false;
    }
}
