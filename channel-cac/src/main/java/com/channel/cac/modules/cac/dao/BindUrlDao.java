package com.channel.cac.modules.cac.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.channel.api.entity.cac.BindUrlEntity;
import com.channel.api.entity.cac.BindUrlVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * VIEW
 * 
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-12-06 18:42:05
 */
public interface BindUrlDao extends BaseMapper<BindUrlEntity> {

    List<BindUrlVo> queryBindUrlPage(Pagination page,  @Param("channelId") String channelId, @Param("accountId") String accountId, @Param("url") String url);

    BindUrlEntity findByChannelId(@Param("channelId") String channelId, @Param("accountId") String accountId, @Param("url") String url);

}
