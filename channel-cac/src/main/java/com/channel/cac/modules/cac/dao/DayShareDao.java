package com.channel.cac.modules.cac.dao;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.channel.api.entity.cac.DayShareEntity;
import com.channel.api.entity.cac.DayShareVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * VIEW
 * 
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-12-15 16:02:59
 */
public interface DayShareDao extends BaseMapper<DayShareEntity> {

   List<DayShareVo> queryDaySharePage(Pagination page, @Param("channelId") String channelId, @Param("startTime") String startTime, @Param("endTime") String endTime);

   DayShareEntity getDayShareByChannelId(@Param("channelId") String channelId);


}
