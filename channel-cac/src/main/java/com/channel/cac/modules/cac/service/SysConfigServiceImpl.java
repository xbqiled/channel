package com.channel.cac.modules.cac.service;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.channel.api.entity.cac.SysConfigEntity;
import com.channel.api.service.cac.SysConfigService;
import com.channel.cac.modules.cac.dao.SysConfigDao;
import com.channel.common.execption.AppException;
import com.channel.common.utils.PageUtils;
import com.channel.common.utils.Query;
import com.google.gson.Gson;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import java.util.Map;


@Service("sysConfigService")
@com.alibaba.dubbo.config.annotation.Service(
        interfaceClass = SysConfigService.class,
        timeout = 10000,
        version = "${api.service.version}"
)
public class SysConfigServiceImpl extends ServiceImpl<SysConfigDao, SysConfigEntity> implements SysConfigService {

    @Override
    public <T> T getConfigObject(String key, Class<T> clazz) {
        String value = getValue(key);
        if(StringUtils.isNotBlank(value)){
            return new Gson().fromJson(value, clazz);
        }
        try {
            return clazz.newInstance();
        } catch (Exception e) {
            throw new AppException("获取参数失败");
        }
    }


    @Override
    public String getValue(String key) {
//        SysConfig config  = (SysConfig)redisTemplate.opsForValue().get(key);
//        if (config == null) {
        SysConfigEntity config = baseMapper.queryByKey(key);
//            redisTemplate.opsForValue().set(key, config);
//        }
        return config == null ? null : config.getParamValue();
    }


    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        Page<SysConfigEntity> page = this.selectPage(
                new Query<SysConfigEntity>(params).getPage(),
                new EntityWrapper<SysConfigEntity>()
        );

        return new PageUtils(page);
    }

}
