package com.channel.cac.modules.chl.service;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.channel.api.entity.chl.ChlRoleMenuEntity;
import com.channel.api.service.chl.ChlRoleMenuService;
import com.channel.cac.modules.chl.dao.ChlRoleMenuDao;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;


/**
 * 角色与菜单对应关系
 *
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2018-11-16 15:58:41
 */
@Service("chlRoleMenuService")
@com.alibaba.dubbo.config.annotation.Service(
		interfaceClass = ChlRoleMenuService.class,
		timeout = 10000,
		version = "${api.service.version}"
)
public class ChlRoleMenuServiceImpl extends ServiceImpl<ChlRoleMenuDao, ChlRoleMenuEntity> implements ChlRoleMenuService {

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void saveOrUpdate(Long roleId, List<Long> menuIdList) {
		//先删除角色与菜单关系
		deleteBatch(new Long[]{roleId});

		if(menuIdList.size() == 0){
			return ;
		}

		//保存角色与菜单关系
		List<ChlRoleMenuEntity> list = new ArrayList<>(menuIdList.size());
		for(Long menuId : menuIdList){
			ChlRoleMenuEntity ChlRoleMenuEntity = new ChlRoleMenuEntity();
			ChlRoleMenuEntity.setMenuId(menuId);
			ChlRoleMenuEntity.setRoleId(roleId);

			list.add(ChlRoleMenuEntity);
		}
		this.insertBatch(list);
	}

	@Override
	public List<Long> queryMenuIdList(Long roleId) {
		return baseMapper.queryMenuIdList(roleId);
	}

	@Override
	public int deleteBatch(Long[] roleIds){
		return baseMapper.deleteBatch(roleIds);
	}

}
