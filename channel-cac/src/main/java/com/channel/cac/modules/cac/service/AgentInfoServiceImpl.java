package com.channel.cac.modules.cac.service;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.channel.api.entity.cac.AgentInfoVo;
import com.channel.api.entity.cac.AccountinfoEntity;
import com.channel.api.entity.chl.ChlUserEntity;
import com.channel.api.service.cac.AgentInfoService;
import com.channel.cac.modules.cac.dao.AccountinfoDao;
import com.channel.common.constant.ConfigConstant;
import com.channel.common.json.GsonAgent;
import com.channel.common.json.GsonAgentLowerLevel;
import com.channel.common.utils.HttpHandler;
import com.channel.common.utils.PageUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


@Service("agentInfoService")
@com.alibaba.dubbo.config.annotation.Service(
        interfaceClass = AgentInfoService.class,
        timeout = 10000,
        version = "${api.service.version}"
)
public class AgentInfoServiceImpl  extends ServiceImpl<AccountinfoDao, AccountinfoEntity> implements  AgentInfoService {


    public List<AgentInfoVo> getAgentInfoPage(Integer accountId, ChlUserEntity userEntity) {
        List <AgentInfoVo> resultList = new ArrayList<>();
        AccountinfoEntity accountinfoEntity = baseMapper.getUserInfo(accountId.toString(), userEntity.getChannelId());

        if (accountinfoEntity != null && accountinfoEntity.gettAccountid() != null) {
            GsonAgent agentInfo = HttpHandler.getAgentInfo(accountId);

            //判断是否存在信息
            if (agentInfo != null && agentInfo.getAccountID().intValue() == accountId.intValue()) {
                AgentInfoVo infoVo = new AgentInfoVo();

                if (agentInfo.getNextNum() != null && agentInfo.getNextNum() > 0) {
                    infoVo.setHasChildren(Boolean.TRUE);
                } else {
                    infoVo.setHasChildren(Boolean.FALSE);
                }
                BeanUtil.copyProperties(agentInfo, infoVo);
                resultList.add(infoVo);
            }
        }
        return resultList;
    }


    public List<AgentInfoVo> getChildNoteList(Integer accountId, ChlUserEntity userEntity) {
        List <AgentInfoVo> resultList = new ArrayList<>();
        GsonAgentLowerLevel agentLowerLevel = HttpHandler.getAgentLowerLevel(accountId,1, 100);

        //判断是否存在信息
        if (agentLowerLevel != null && agentLowerLevel.getAccountID().intValue() == accountId.intValue()) {
            if (agentLowerLevel.getList() != null && agentLowerLevel.getList().length > 0) {
              for (GsonAgentLowerLevel.NoteList note : agentLowerLevel.getList()) {

                  AgentInfoVo infoVo = new AgentInfoVo();
                  BeanUtil.copyProperties(note, infoVo);
                  infoVo.setPromoterId(new Long(accountId));
                  infoVo.setAccountID(note.getId());
                  if (note.getNextNum() > 0) {
                      infoVo.setHasChildren(Boolean.TRUE);
                  } else {
                      infoVo.setHasChildren(Boolean.FALSE);
                  }
                  resultList.add(infoVo);
              }
            }
        }
        return resultList;
    }


    @Override
    public PageUtils getChildlistPage(Map<String, Object> params, ChlUserEntity userEntity) {
        List<GsonAgentLowerLevel.NoteList> resultList = new ArrayList<>();
        String current = (String) params.get(ConfigConstant.LIMIT);
        String size = (String) params.get(ConfigConstant.PAGE);

        Page<GsonAgentLowerLevel.NoteList> page = new Page<>(new Integer(size), new Integer(current));
        String accountId = ObjectUtil.isNotNull(params.get("accountId")) ? (String) params.get("accountId") : "";
        GsonAgentLowerLevel agentLowerLevel = HttpHandler.getAgentLowerLevel(Integer.valueOf(accountId), Integer.valueOf(current), Integer.valueOf(size));

        //判断是否存在信息
        if (agentLowerLevel != null && agentLowerLevel.getRet() == 0) {
            if (agentLowerLevel.getList() != null && agentLowerLevel.getList().length > 0) {
                for (GsonAgentLowerLevel.NoteList note : agentLowerLevel.getList()) {
                    resultList.add(note);
                }
            }
        }
        return new PageUtils(page.setRecords(resultList));
    }
}
