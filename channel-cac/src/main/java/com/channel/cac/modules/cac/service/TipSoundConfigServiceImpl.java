package com.channel.cac.modules.cac.service;

import com.channel.api.entity.cac.TipSoundConfigEntity;
import com.channel.api.service.cac.TipOrderService;
import com.channel.api.service.cac.TipSoundConfigService;
import com.channel.cac.modules.cac.dao.TipSoundConfigDao;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.channel.common.utils.PageUtils;
import com.channel.common.utils.Query;


@Service("tipSoundConfigService")
@com.alibaba.dubbo.config.annotation.Service(
        interfaceClass = TipSoundConfigService.class,
        timeout = 10000,
        version = "${api.service.version}"
)
public class TipSoundConfigServiceImpl extends ServiceImpl<TipSoundConfigDao, TipSoundConfigEntity> implements TipSoundConfigService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        Page<TipSoundConfigEntity> page = this.selectPage(
                new Query<TipSoundConfigEntity>(params).getPage(),
                new EntityWrapper<TipSoundConfigEntity>()
        );
        return new PageUtils(page);
    }

    @Override
    @CacheEvict(value = "tip:config:details", key = "#channelId")
    public TipSoundConfigEntity getConfigByChannelId(String channelId) {
        return baseMapper.getConfigByChannelId(channelId);
    }
}
