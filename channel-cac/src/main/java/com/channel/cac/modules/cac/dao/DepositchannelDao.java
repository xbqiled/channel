package com.channel.cac.modules.cac.dao;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.channel.api.entity.cac.DepositchannelEntity;
import com.channel.api.entity.cac.DepositchannelVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * VIEW
 * 
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-05-31 11:20:19
 */
public interface DepositchannelDao extends BaseMapper<DepositchannelEntity> {

    List<DepositchannelVo> queryDepositPage(Pagination page, @Param("channelId") String channelId, @Param("startTime") String startTime, @Param("endTime") String endTime);

    DepositchannelEntity getDepositByChannelId( @Param("channelId") String channelId);

}
