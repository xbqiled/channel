package com.channel.cac.modules.cac.dao;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.channel.api.entity.cac.SysPayRebateEntity;
import org.apache.ibatis.annotations.Param;

/**
 * 
 * 
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-06-05 11:46:18
 */
public interface SysPayRebateDao extends BaseMapper<SysPayRebateEntity> {

    SysPayRebateEntity getPayRebateByChannel(@Param("channelId") String channelId);

    SysPayRebateEntity getPayRebateByOrder(@Param("orderNo") String orderNo);

}
