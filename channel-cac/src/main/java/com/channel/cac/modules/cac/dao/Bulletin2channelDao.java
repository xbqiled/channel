package com.channel.cac.modules.cac.dao;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.channel.api.entity.cac.Bulletin2channelEntity;

/**
 *
 * 
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-03-28 15:24:52
 */
public interface Bulletin2channelDao extends BaseMapper<Bulletin2channelEntity> {
	
}
