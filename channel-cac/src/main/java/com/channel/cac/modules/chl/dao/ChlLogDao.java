package com.channel.cac.modules.chl.dao;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.channel.api.entity.chl.ChlLogEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 系统日志
 *
 */
public interface ChlLogDao extends BaseMapper<ChlLogEntity> {


    List<ChlLogEntity> queryLogPage(Pagination page, @Param("channelId") String channelId,
                                           @Param("userId") String userId, @Param("userName") String userName, @Param("operation") String operation,
                                           @Param("startTime") String startTime, @Param("endTime") String endTime);

}
