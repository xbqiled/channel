package com.channel.cac.modules.cac.service;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.channel.api.entity.cac.BgTransferEntity;
import com.channel.api.entity.chl.ChlUserEntity;
import com.channel.api.service.cac.BgTransferService;
import com.channel.cac.modules.cac.dao.BgTransferDao;
import com.channel.common.constant.ConfigConstant;
import com.channel.common.utils.PageUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;




@Service("otherBgTransferService")
@com.alibaba.dubbo.config.annotation.Service(
        interfaceClass = BgTransferService.class,
        timeout = 20000,
        version = "${api.service.version}"
)
public class BgTransferServiceImpl extends ServiceImpl<BgTransferDao, BgTransferEntity> implements BgTransferService {


    @Override
    public PageUtils queryBgTransferPage(Map<String, Object> params, ChlUserEntity userEntity) {
        String current = (String)params.get(ConfigConstant.LIMIT);
        String size  = (String)params.get(ConfigConstant.PAGE);
        Page<BgTransferEntity> page = new Page<>(new Integer(size), new Integer(current));

        //根据拓展Server获取渠道ID
        String channelId = ObjectUtil.isNotNull(userEntity.getChannelId()) ? userEntity.getChannelId() : "";
        String userId = ObjectUtil.isNotNull(params.get("userId")) ? (String) params.get("userId") : "";
        String startTime = ObjectUtil.isNotNull(params.get("startTime")) ? (String) params.get("startTime") : "";
        String endTime = ObjectUtil.isNotNull(params.get("endTime")) ? (String) params.get("endTime") : "";

        List<BgTransferEntity> resultList = baseMapper.queryBgTransferPage(page, userId, channelId, startTime, endTime);
        return new PageUtils(page.setRecords(resultList));
    }
}
