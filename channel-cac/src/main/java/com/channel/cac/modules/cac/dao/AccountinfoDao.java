package com.channel.cac.modules.cac.dao;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.channel.api.entity.cac.AccountinfoEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 
 * 
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-02-23 14:34:23
 */
public interface AccountinfoDao extends BaseMapper<AccountinfoEntity> {

    List<Map<String, Object>> queryUserInfo(Pagination page, @Param("channelId") String channelId, @Param("isOnLine") String isOnLine, @Param("promoterId") String promoterId,
                                            @Param("userId") String userId, @Param("userName") String userName, @Param("nickName") String nickName, @Param("ip") String ip,
                                            @Param("regStartTime") String regStartTime, @Param("regEndTime") String regEndTime, @Param("accountName") String accountName, @Param("vipLevel") String vipLevel,
                                            @Param("accountNum") String accountNum, @Param("phoneNumber") String phoneNumber, @Param("source") String source, @Param("doby") String doby);

    List<Map<String, Object>> queryUserPhoneList (Pagination page, @Param("channelId") String channelId, @Param("isOnLine") String isOnLine, @Param("promoterId") String promoterId,
                                            @Param("userId") String userId, @Param("userName") String userName, @Param("nickName") String nickName, @Param("ip") String ip,
                                            @Param("regStartTime") String regStartTime, @Param("regEndTime") String regEndTime, @Param("accountName") String accountName, @Param("vipLevel") String vipLevel,
                                            @Param("accountNum") String accountNum, @Param("phoneNumber") String phoneNumber, @Param("source") String source, @Param("doby") String doby);

    void updateStatus(@Param("tAccountid") String tAccountid, @Param("status") String status);

    Integer updateUserNote(@Param("tableName") String tableName, @Param("note") String note, @Param("userId") String userId);

    Map<String, Object> getUserInfoById(@Param("accountTableName") String accountTableName, @Param("userbaseattrTableName") String userbaseattrTableName, @Param("accountId") String accountId);

    List<Map<String, Object>> queryIpUserInfo(Pagination page, @Param("regIp") String regIp, @Param("channelId") String channelId,  @Param("regStartTime") String regStartTime, @Param("regEndTime") String regEndTime);

    List<Map<String, Object>> quereyDetailIpPage(Pagination page, @Param("channelId") String channelId,  @Param("ip") String ip,  @Param("regStartTime") String regStartTime, @Param("regEndTime") String regEndTime);

    Map<String, Object> queryRegUserCount(@Param("channelId") String channelId, @Param("regIp") String regIp, @Param("regStartTime") String regStartTime, @Param("regEndTime") String regEndTime);

    AccountinfoEntity getUserInfo(@Param("userId")String userId, @Param("channelId")String channelId);

    void modifyPw(@Param("userId")String userId, @Param("password")String password);

    Map queryGameRoom(@Param("gameRoomId")String gameRoomId);
}
