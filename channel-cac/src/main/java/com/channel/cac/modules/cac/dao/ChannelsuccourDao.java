package com.channel.cac.modules.cac.dao;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.channel.api.entity.cac.ChannelsuccourEntity;
import com.channel.api.entity.cac.SuccourVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * VIEW
 * 
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-12-31 12:23:45
 */
public interface ChannelsuccourDao extends BaseMapper<ChannelsuccourEntity> {

    List<SuccourVo> querySuccourPage(Pagination page, @Param("channelId") String channelId, @Param("startTime") String startTime, @Param("endTime") String endTime);

    ChannelsuccourEntity getSuccourByChannelId(@Param("channelId") String channelId);

}
