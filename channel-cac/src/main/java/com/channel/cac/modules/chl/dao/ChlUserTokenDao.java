package com.channel.cac.modules.chl.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.channel.api.entity.chl.ChlUserTokenEntity;

/**
 * 系统用户Token
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2017-03-23 15:22:07
 */
public interface ChlUserTokenDao extends BaseMapper<ChlUserTokenEntity> {

    ChlUserTokenEntity queryByToken(String token);
	
}
