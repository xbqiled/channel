package com.channel.cac.modules.cac.dao;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.channel.api.entity.cac.PushConfigEntity;
import org.apache.ibatis.annotations.Param;

/**
 * 
 * 
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2020-05-27 20:42:22
 */
public interface PushConfigDao extends BaseMapper<PushConfigEntity> {

    PushConfigEntity getActivateConfig(@Param("channelId") String channelId);

}
