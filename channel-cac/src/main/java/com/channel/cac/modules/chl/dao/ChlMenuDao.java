package com.channel.cac.modules.chl.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.channel.api.entity.chl.ChlMenuEntity;

import java.util.List;

/**
 * 菜单管理
 *
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2018-11-16 15:58:41
 */
public interface ChlMenuDao extends BaseMapper<ChlMenuEntity> {
	
	/**
	 * 根据父菜单，查询子菜单
	 * @param parentId 父菜单ID
	 */
	List<ChlMenuEntity> queryListParentId(Long parentId);
	
	/**
	 * 获取不包含按钮的菜单列表
	 */
	List<ChlMenuEntity> queryNotButtonList();


	/**
	 * <B>查询当前用户下归属于渠道的菜单</B>
	 * @param userId
	 * @return
	 */
	List<ChlMenuEntity> queryChannelList(Long userId);

	//查询所有菜单,包括按钮
	List<ChlMenuEntity> queryAll();

}
