package com.channel.cac.modules.cac.service;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.channel.api.entity.cac.DayShareEntity;
import com.channel.api.entity.cac.DayShareVo;
import com.channel.api.entity.chl.ChlUserEntity;
import com.channel.api.service.cac.DayShareService;
import com.channel.cac.modules.cac.dao.DayShareDao;
import com.channel.common.constant.ConfigConstant;
import com.channel.common.json.GsonDayShare;
import com.channel.common.json.GsonResult;
import com.channel.common.json.RequestDayShare;
import com.channel.common.utils.HttpHandler;
import com.channel.common.utils.PageUtils;
import com.google.gson.Gson;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;


@Service("dayshareService")
@com.alibaba.dubbo.config.annotation.Service(interfaceClass = DayShareService.class, timeout = 10000, version = "${api.service.version}")
public class DayShareServiceImpl extends ServiceImpl<DayShareDao, DayShareEntity> implements DayShareService {

    @Override
    public PageUtils queryDaySharePage(Map<String, Object> params, ChlUserEntity chlUser) {
        String current = (String) params.get(ConfigConstant.LIMIT);
        String size = (String) params.get(ConfigConstant.PAGE);

        String channelId = ObjectUtil.isNotNull(chlUser.getChannelId()) ? chlUser.getChannelId() : "";
        String startTime = ObjectUtil.isNotNull(params.get("startTime")) ? (String) params.get("startTime") : "";
        String endTime = ObjectUtil.isNotNull(params.get("endTime")) ? (String) params.get("endTime") : "";

        Page<DayShareVo> page = new Page<>(new Integer(size), new Integer(current));
        List<DayShareVo> list = this.baseMapper.queryDaySharePage(page, channelId, startTime, endTime);
        return new PageUtils(page.setRecords(list));
    }

    @Override
    public int channelOpenType(String channelId, String openType) {
        DayShareEntity model  = baseMapper.getDayShareByChannelId(channelId);
        if (model != null && StrUtil.isNotEmpty(model.getTChannel())) {

            Integer type = model.getTOpentype() == 1 ? 0 : 1;
            String jsonStr = new String(model.getTDayshareinfo());
            RequestDayShare requestDayShare  = new Gson().fromJson(jsonStr, RequestDayShare.class);

            if (requestDayShare != null) {
                requestDayShare.setOpenType(type);
                requestDayShare.setChannel(Long.valueOf(channelId));
            }

            GsonResult result = HttpHandler.cfgDayShare(requestDayShare);
            if (result != null) {
                return result.getRet();
            }
        }
        return 1;
    }


    @Override
    public DayShareEntity findByChannelId(String channelId) {
        return baseMapper.getDayShareByChannelId(channelId);
    }


    @Override
    public int saveOrUpdateConfig(ChlUserEntity userEntity, Integer opType, Long dayAward, Long awardDayShareCnt, List<GsonDayShare> dayShareList) {
        String channelId = userEntity.getChannelId();
        GsonResult result = HttpHandler.cfgDayShare(Long.valueOf(channelId), opType, dayAward, awardDayShareCnt, dayShareList);
        if (result != null) {
            return result.getRet();
        }

        return 1;
    }
}
