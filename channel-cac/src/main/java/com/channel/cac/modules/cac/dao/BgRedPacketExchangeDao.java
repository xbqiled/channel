package com.channel.cac.modules.cac.dao;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.channel.api.entity.cac.BgRedPacketExchangeEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 
 * 
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2020-01-14 14:39:56
 */
public interface BgRedPacketExchangeDao extends BaseMapper<BgRedPacketExchangeEntity> {

    List<Map<String, Object>> queryRedPacketExchangePage(Pagination page, @Param("tableSql") String tableSql, @Param("channelId") String channelId, @Param("userId") String userId,
                                                         @Param("startTime") String startTime, @Param("endTime") String endTime);

    List<String> queryHaveTab(@Param("tableSql") String tableSql);
}
