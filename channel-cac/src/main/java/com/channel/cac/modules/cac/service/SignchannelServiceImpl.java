package com.channel.cac.modules.cac.service;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.channel.api.entity.cac.SigninchannelEntity;
import com.channel.api.entity.cac.SigninchannelVo;
import com.channel.api.entity.chl.ChlUserEntity;
import com.channel.api.service.cac.SignchannelService;
import com.channel.cac.modules.cac.dao.SigninchannelDao;
import com.channel.common.constant.ConfigConstant;
import com.channel.common.json.GsonResult;
import com.channel.common.json.GsonSignAwardCfg;
import com.channel.common.utils.HttpHandler;
import com.channel.common.utils.PageUtils;
import com.channel.common.utils.Query;
import com.google.gson.Gson;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;


@Service("signchannelService")
@com.alibaba.dubbo.config.annotation.Service(
        interfaceClass = SignchannelService.class,
        timeout = 10000,
        version = "${api.service.version}"
)
public class SignchannelServiceImpl extends ServiceImpl<SigninchannelDao, SigninchannelEntity> implements SignchannelService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        Page<SigninchannelEntity> page = this.selectPage(
                new Query<SigninchannelEntity>(params).getPage(),
                new EntityWrapper<SigninchannelEntity>()
        );
        return new PageUtils(page);
    }

    @Override
    public SigninchannelEntity findByChannelId(String channelId) {
        return baseMapper.getSignByChannelId(channelId);
    }

    @Override
    public int channelOpenType(String channelId, String openType) {
        SigninchannelEntity model  = baseMapper.getSignByChannelId(channelId);
        if (model != null && StrUtil.isNotEmpty(model.gettChannel())) {

            Integer type = model.gettOpenType() == 1 ? 0 : 1;
            String str = new String(model.gettCfginfo());
            List<GsonSignAwardCfg> rabateCfg = new ArrayList<>();

            if (StrUtil.isNotBlank(str)) {
                GsonSignAwardCfg[] array = new Gson().fromJson(str, GsonSignAwardCfg[].class);
                rabateCfg = Arrays.asList(array);
            }

            GsonResult result = HttpHandler.signConfig(model.gettChannel(), model.gettStarttime(), model.gettEndtime(), type, model.gettIsCirculate(), rabateCfg);
            if (result != null) {
                return result.getRet();
            }
        }
        return 1;
    }

    @Override
    public int channelLoop(String channelId, String openType) {
        SigninchannelEntity model  = baseMapper.getSignByChannelId(channelId);
        if (model != null && StrUtil.isNotEmpty(model.gettChannel())) {

            Integer type = model.gettIsCirculate() == 1 ? 0 : 1;
            String str = new String(model.gettCfginfo());
            List<GsonSignAwardCfg> rabateCfg = new ArrayList<>();

            if (StrUtil.isNotBlank(str)) {
                GsonSignAwardCfg[] array = new Gson().fromJson(str, GsonSignAwardCfg[].class);
                rabateCfg = Arrays.asList(array);
            }

            GsonResult result = HttpHandler.signConfig(model.gettChannel(), model.gettStarttime(), model.gettEndtime(), model.gettOpenType(), type, rabateCfg);
            if (result != null) {
                return result.getRet();
            }
        }
        return 1;
    }

    @Override
    public PageUtils querySignCfgPage(Map<String, Object> params, ChlUserEntity userEntity) {
        String current = (String) params.get(ConfigConstant.LIMIT);
        String size = (String) params.get(ConfigConstant.PAGE);

        String channelId = ObjectUtil.isNotNull(userEntity.getChannelId()) ? userEntity.getChannelId() : "";
        String startTime = ObjectUtil.isNotNull(params.get("startTime")) ? (String) params.get("startTime") : "";
        String endTime = ObjectUtil.isNotNull(params.get("endTime")) ? (String) params.get("endTime") : "";

        Page<SigninchannelVo> page = new Page<>(new Integer(size), new Integer(current));
        List<SigninchannelVo> list = this.baseMapper.querySignCfgPage(page, channelId, startTime, endTime);
        return  new PageUtils(page.setRecords(list));
    }


    @Override
    public int saveSignCfg(String channel, String beginTime, String endTime, Integer openType, Integer isCirculate, Integer num, List<GsonSignAwardCfg> rabateCfg) {
        Long start = new Long(beginTime) / 1000;
        Long finish = new Long(endTime) / 1000;

        GsonResult result = HttpHandler.signConfig(channel, start, finish, openType, isCirculate, rabateCfg);
        if (result != null) {
            return result.getRet();
        }
        return 1;
    }
}
