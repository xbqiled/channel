package com.channel.cac.modules.cac.dao;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.channel.api.entity.cac.UserMailEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-03-07 13:39:08
 */
public interface UserMailDao extends BaseMapper<UserMailEntity> {

    List<Map<String, Object>> queryMailPage(Pagination page, @Param("userId") String userId, @Param("channelId") String channelId, @Param("title") String title,
                                            @Param("startTime") String startTime, @Param("endTime") String endTime);


    UserMailEntity findbyId( @Param("channelId") String channelId, @Param("id") String id, @Param("userId") String userId);
}
