package com.channel.cac.modules.cac.service;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.channel.api.entity.cac.TipSoundConfigEntity;
import com.channel.api.entity.cac.TipSoundTokenEntity;
import com.channel.api.service.cac.TipSoundTokenService;
import com.channel.cac.modules.cac.dao.TipSoundConfigDao;
import com.channel.cac.modules.cac.dao.TipSoundTokenDao;
import com.channel.common.json.GsonSound;
import com.channel.common.utils.HttpHandler;
import com.channel.common.utils.PageUtils;
import com.channel.common.utils.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Map;



@Service("tipSoundTokenService")
@com.alibaba.dubbo.config.annotation.Service(
        interfaceClass = TipSoundTokenService.class,
        timeout = 20000,
        version = "${api.service.version}"
)
public class TipSoundTokenServiceImpl extends ServiceImpl<TipSoundTokenDao, TipSoundTokenEntity> implements TipSoundTokenService {

    @Autowired
    private TipSoundConfigDao tipSoundConfigDao;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        Page<TipSoundTokenEntity> page = this.selectPage(
                new Query<TipSoundTokenEntity>(params).getPage(),
                new EntityWrapper<TipSoundTokenEntity>()
        );

        return new PageUtils(page);
    }

    public TipSoundTokenEntity getTonkenByChannelId(String channelId) {
        TipSoundTokenEntity tonken = baseMapper.getTonkenByChannelId(channelId);
        //首先判断是否存在
        if (tonken != null && StrUtil.isNotBlank(tonken.getAccessToken())) {
            //判断是否过期
            if (new Date().before(tonken.getExpiresTime())) {
                return tonken;
            } else {
                //创建新的token
                TipSoundTokenEntity toKens   = createTonKen(channelId);
                return toKens;
           }
        } else {
            //创建新的token
            TipSoundTokenEntity toKens   = createTonKen(channelId);
            return toKens;
        }
    }


    private TipSoundTokenEntity createTonKen(String channelId) {
        TipSoundConfigEntity  config = tipSoundConfigDao.getConfigByChannelId(channelId);
        TipSoundTokenEntity tonken = new TipSoundTokenEntity();
        //发送信息请求
        if (config != null && StrUtil.isNotBlank(config.getClientId()) && StrUtil.isNotBlank(config.getClientId()) && StrUtil.isNotBlank(config.getClientSecret())) {
            GsonSound sound = HttpHandler.getBaiduSoundTonken(config.getGrantType(), config.getClientId(), config.getClientSecret());

            if (sound != null && StrUtil.isNotBlank(sound.getAccess_token())) {
                tonken.setChannelId(channelId);
                tonken.setCreateTime(new Date());

                tonken.setAccessToken(sound.getAccess_token());
                tonken.setSessionKey(sound.getSession_key());
                tonken.setScope(sound.getScope());

                tonken.setRefreshToken(sound.getRefresh_token());
                tonken.setSessionSecret(sound.getSession_secret());
                tonken.setExpiresTime(DateUtil.offsetDay(new Date(), 29));

                baseMapper.delTonkenByChannelId(channelId);
                this.insert(tonken);
                return tonken;
            } else {
                return null;
            }
        } else {
            return  null;
        }
    }


}
