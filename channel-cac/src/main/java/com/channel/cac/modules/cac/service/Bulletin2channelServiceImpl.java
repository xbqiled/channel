package com.channel.cac.modules.cac.service;

import com.channel.api.entity.cac.Bulletin2channelEntity;
import com.channel.api.service.cac.Bulletin2channelService;
import com.channel.cac.modules.cac.dao.Bulletin2channelDao;
import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.channel.common.utils.PageUtils;
import com.channel.common.utils.Query;


@Service("bulletin2channelService")
public class Bulletin2channelServiceImpl extends ServiceImpl<Bulletin2channelDao, Bulletin2channelEntity> implements Bulletin2channelService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        Page<Bulletin2channelEntity> page = this.selectPage(
                new Query<Bulletin2channelEntity>(params).getPage(),
                new EntityWrapper<Bulletin2channelEntity>()
        );

        return new PageUtils(page);
    }

}
