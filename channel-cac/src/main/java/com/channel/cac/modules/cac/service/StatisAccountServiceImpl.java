package com.channel.cac.modules.cac.service;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.channel.api.entity.cac.StatisAccountEntity;
import com.channel.api.entity.chl.ChlUserEntity;
import com.channel.api.service.cac.StatisAccountService;
import com.channel.cac.modules.cac.dao.StatisAccountDao;
import com.channel.common.constant.ConfigConstant;
import com.channel.common.utils.PageUtils;
import org.springframework.stereotype.Service;

import java.util.*;


@Service("statisAccountService")
@com.alibaba.dubbo.config.annotation.Service(
        interfaceClass = StatisAccountService.class,
        timeout = 50000,
        version = "${api.service.version}"
)
public class StatisAccountServiceImpl extends ServiceImpl<StatisAccountDao, StatisAccountEntity> implements StatisAccountService {


    private static String TABLE_NAME = "statis_account";
    private static String TIME_TABLE_NAME = "statis_date";


    @Override
    public Map<String, Object> queryStatisAccountData(Map<String, Object> params, ChlUserEntity userEntity) {
        String current = (String) params.get(ConfigConstant.LIMIT);
        String size = (String) params.get(ConfigConstant.PAGE);
        Page<Map<String, Object>> page = new Page<>(new Integer(size), new Integer(current));

        String userId = ObjectUtil.isNotNull(params.get("userId")) ? (String) params.get("userId") : "";
        String channelId = ObjectUtil.isNotNull(userEntity.getChannelId()) ? userEntity.getChannelId() : "";
        String startTime = ObjectUtil.isNotNull(params.get("startTime")) ? (String) params.get("startTime") : "";
        String endTime = ObjectUtil.isNotNull(params.get("endTime")) ? (String) params.get("endTime") : "";

        //获取当前时间
        Date data = DateUtil.parse(startTime, "yyyyMMdd");
        String monthTable = DateUtil.format(data, "yyyyMM");
        List<Map<String, Object>>  resultList = baseMapper.queryStatisAccountDataPage(page, TIME_TABLE_NAME + monthTable, channelId, userId, startTime, endTime);
        Map<String, Object>  statis =  baseMapper.queryStatiAccountData(TIME_TABLE_NAME + monthTable, channelId, userId, startTime, endTime);

        Map<String, Object> map = new HashMap<>();
        map.put("page", new PageUtils(page.setRecords(resultList)));
        map.put("statis", statis);
        return  map;
    }


    @Override
    public PageUtils queryStatisAccountPage(Map<String, Object> params, ChlUserEntity userEntity) {
        String current = (String) params.get(ConfigConstant.LIMIT);
        String size = (String) params.get(ConfigConstant.PAGE);
        Page<StatisAccountEntity> page = new Page<>(new Integer(size), new Integer(current));

        String userId = ObjectUtil.isNotNull(params.get("userId")) ? (String) params.get("userId") : "";
        String channelId = ObjectUtil.isNotNull(userEntity.getChannelId()) ? userEntity.getChannelId() : "";
        String startTime = ObjectUtil.isNotNull(params.get("startTime")) ? (String) params.get("startTime") : "";
        String endTime = ObjectUtil.isNotNull(params.get("endTime")) ? (String) params.get("endTime") : "";

        Date data = DateUtil.parse(startTime, "yyyy-MM-dd");
        String monthTable = DateUtil.format(data, "yyyyMM");
        List<StatisAccountEntity>  resultList = baseMapper.queryStatisAccountPage(page, TABLE_NAME + monthTable, channelId, userId, startTime, endTime);
        return new PageUtils(page.setRecords(resultList));
    }


    /**
     * <B>获取间隔时间</B>
     * @param startTime
     * @param endTime
     * @return
     */
    private List<String> intervalMonth(String startTime, String endTime) {
        List<String> list = new ArrayList<>();
        //转化成月份
        Date startMonthDate = DateUtil.parse(startTime, "yyyyMMdd");
        Date endMonthDate = DateUtil.parse(endTime, "yyyyMMdd");

        String endMonth = DateUtil.format(endMonthDate, "yyyyMM");
        Date nextDate = startMonthDate;

        //判断时间不等于结束时间
        while (!DateUtil.format(nextDate, "yyyyMM").equals(endMonth)) {
            list.add(TIME_TABLE_NAME + DateUtil.format(nextDate, "yyyyMM"));
            //获取下一个月
            nextDate = DateUtil.offsetMonth(nextDate, 1);
        }

        list.add(TIME_TABLE_NAME + endMonth);
        return list;
    }


    /**
     * <B>拼接SQL语句</B>
     * @param list
     * @return
     */
    private String spliceSql(List<String> list) {
        StringBuffer sb = new StringBuffer();
        sb.append("(");
        if (CollUtil.isNotEmpty(list)) {
            for (int i = 0; i < list.size(); i++) {
                sb.append(" select * From " + list.get(i));
                if (i < list.size() - 1) {
                    sb.append("  union all ");
                }
            }
        }
        sb.append(")");
        return sb.toString();
    }


    private String spliceInSql(List<String> list) {
        StringBuffer sb = new StringBuffer();
        if (CollUtil.isNotEmpty(list)) {
            for (int i = 0; i < list.size(); i++) {
                sb.append("'" + list.get(i) + "'");
                if (i < list.size() - 1) {
                    sb.append(" , ");
                }
            }
        }
        return sb.toString();
    }
}
