package com.channel.cac.modules.cac.dao;

import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.channel.api.entity.cac.StatisGameEntity;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.channel.api.entity.cac.StatisGameVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 
 * 
 * @author pengbin
 * @date 2020-03-02 21:28:00
 */
public interface StatisGameDao extends BaseMapper<StatisGameEntity> {

    List<StatisGameVo> queryStatisGamePage(Pagination page, @Param("tabelName")String tabelName,
                                           @Param("channelId")String channelId,
                                           @Param("gameId")String gameId,
                                           @Param("startTime") String startTime,
                                           @Param("endTime") String endTime,
                                           @Param("orderSql") String orderSql);


    List<StatisGameVo> queryStatisUserGamePage(Pagination page, @Param("tabelName")String tabelName,
                                           @Param("channelId")String channelId,
                                           @Param("userId")String userId,
                                           @Param("gameId")String gameId,
                                           @Param("startTime") String startTime,
                                           @Param("endTime") String endTime,
                                           @Param("orderSql") String orderSql);


    List<Map<String, String>> queryGameList();
}
