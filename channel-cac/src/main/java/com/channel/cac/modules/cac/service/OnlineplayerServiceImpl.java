package com.channel.cac.modules.cac.service;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.channel.api.entity.cac.OnlineplayerEntity;
import com.channel.api.entity.chl.ChlUserEntity;
import com.channel.api.service.cac.OnlineplayerService;
import com.channel.cac.modules.cac.dao.OnlineplayerDao;
import com.channel.common.constant.ConfigConstant;
import com.channel.common.utils.PageUtils;
import com.channel.common.utils.Query;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;


@Service("onlineplayerService")
@com.alibaba.dubbo.config.annotation.Service(
        interfaceClass = OnlineplayerService.class,
        timeout = 10000,
        version = "${api.service.version}"
)
public class OnlineplayerServiceImpl extends ServiceImpl<OnlineplayerDao, OnlineplayerEntity> implements OnlineplayerService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        Page<OnlineplayerEntity> page = this.selectPage(
                new Query<OnlineplayerEntity>(params).getPage(),
                new EntityWrapper<OnlineplayerEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public PageUtils queryOnLinePlayerPage(Map<String, Object> params,  ChlUserEntity userEntity) {
        String current = (String)params.get(ConfigConstant.LIMIT);
        String size  = (String)params.get(ConfigConstant.PAGE);

        //根据拓展Server获取渠道ID
        String channelId = ObjectUtil.isNotNull(userEntity.getChannelId()) ? userEntity.getChannelId() : "";
        String userId = ObjectUtil.isNotNull(params.get("userId")) ? (String)params.get("userId") : "";
        String gameRoomId = ObjectUtil.isNotNull(params.get("gameRoomId")) ? (String)params.get("gameRoomId") : "";

        String nickName = ObjectUtil.isNotNull(params.get("nickName")) ? (String)params.get("nickName") : "";
        String roomType = ObjectUtil.isNotNull(params.get("roomType")) ? (String)params.get("roomType") : "";

        StringBuffer sb = new StringBuffer();

        if (StrUtil.isNotBlank(roomType)) {
            if (roomType.equals("1")) {
              sb.append( " and a.t_gameId = 0 " );
            } else {
                if (StrUtil.isNotEmpty(gameRoomId) && Integer.valueOf(gameRoomId) > 0) {
                    sb.append(" and a.t_gameId =  " + gameRoomId);
                } else {
                    sb.append(" and a.t_gameId > 0  ");
                }
            }
        }

        Page<Map<String, Object>> page = new Page<>(new Integer(size), new Integer(current));
        List<Map<String, Object>> list = this.baseMapper.queryUserOnlinePage(page, nickName, channelId, userId, sb.toString());
        return new PageUtils(page.setRecords(list));
    }

    @Override
    public PageUtils queryLoginByTime(Map<String, Object> params, ChlUserEntity userEntity) {
        String current = (String)params.get(ConfigConstant.LIMIT);
        String size  = (String)params.get(ConfigConstant.PAGE);
        Page<Map<String, Object>> page = new Page<>(new Integer(size), new Integer(current));
        List<Map<String, Object>> list = baseMapper.queryLoginByTime(page, userEntity.getChannelId());
        return new PageUtils(page.setRecords(list));
    }

    @Override
    public Long queryOnlineNum(ChlUserEntity userEntity) {
        return baseMapper.queryOnlineNum(userEntity.getChannelId());
    }

    @Override
    public Long queryHallNum(ChlUserEntity userEntity) {
        return baseMapper.queryhallNum(userEntity.getChannelId());
    }
}
