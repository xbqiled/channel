package com.channel.cac.modules.cac.dao;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.channel.api.entity.cac.UserbaseattrEntity;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;

/**
 * 
 * 
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-02-23 14:34:24
 */
public interface UserbaseattrDao extends BaseMapper<UserbaseattrEntity> {

    void addGoldCoinById(@Param("accountId") Integer accountId, @Param("goldCoin") BigDecimal goldCoin);

    void reduceGoldCoinById(@Param("accountId") Integer accountId, @Param("goldCoin") BigDecimal goldCoin);
	
}
