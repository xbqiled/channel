package com.channel.cac.modules.cac.service;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.channel.api.entity.cac.DdzMatchAwardVo;
import com.channel.api.entity.cac.DdzmatchrankawardEntity;
import com.channel.api.entity.cac.EditnickawardVo;
import com.channel.api.entity.chl.ChlUserEntity;
import com.channel.api.service.cac.DdzmatchrankawardService;
import com.channel.cac.modules.cac.dao.DdzmatchrankawardDao;
import com.channel.common.constant.ConfigConstant;
import com.channel.common.json.GsonLuckRouletteForm;
import com.channel.common.json.GsonMatchRankAward;
import com.channel.common.json.GsonNickRward;
import com.channel.common.json.GsonResult;
import com.channel.common.utils.HttpHandler;
import com.channel.common.utils.PageUtils;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


@Service("ddzmatchrankawardService")
@com.alibaba.dubbo.config.annotation.Service(
        interfaceClass = DdzmatchrankawardService.class,
        timeout = 20000,
        version = "${api.service.version}"
)
public class DdzmatchrankawardServiceImpl extends ServiceImpl<DdzmatchrankawardDao, DdzmatchrankawardEntity> implements DdzmatchrankawardService {


    @Override
    public PageUtils queryDdzMatchAwardPage(Map<String, Object> params, ChlUserEntity userEntity) {
        String current = (String) params.get(ConfigConstant.LIMIT);
        String size = (String) params.get(ConfigConstant.PAGE);

        String channelId = ObjectUtil.isNotNull(userEntity.getChannelId()) ? userEntity.getChannelId() : "";
        String optionType = ObjectUtil.isNotNull(params.get("optionType")) ? (String) params.get("optionType") : "";
        String startTime = ObjectUtil.isNotNull(params.get("startTime")) ? (String) params.get("startTime") : "";
        String endTime = ObjectUtil.isNotNull(params.get("endTime")) ? (String) params.get("endTime") : "";

        Page<DdzMatchAwardVo> page = new Page<>(new Integer(size), new Integer(current));
        List<DdzMatchAwardVo> list = this.baseMapper.queryDdzMatchAwardPage(page, channelId, optionType, startTime, endTime);
        return  new PageUtils(page.setRecords(list));
    }


    @Override
    public DdzMatchAwardVo findByChannelId(String channelId, String optionType) {
        return baseMapper.getDdzMatchAwardByChannelId(channelId, optionType);
    }

    @Override
    public int saveDdzMatchAwardCfg(String channelId, Integer openType, Integer optionType, List<GsonMatchRankAward> cfgList) {
        GsonResult result = HttpHandler.configDdzMatchRankAward(cfgList, channelId, optionType, openType);
        if (result != null) {
            return result.getRet();
        }
        return 1;
    }

    @Override
    public int channelOpenType(String channelId, String optionType, String openType) {
        DdzMatchAwardVo model  = baseMapper.getDdzMatchAwardByChannelId(channelId, optionType);
        if (model != null && StrUtil.isNotEmpty(model.getChannelId())) {
            List<GsonMatchRankAward> list = new ArrayList<>();

            Integer type = model.getDepositType() == 1 ? 0 : 1;
            String cfg = new String(model.getStrjsonaward());
            if (StrUtil.isNotEmpty(cfg)) {

                JsonParser parser = new JsonParser();
                JsonArray jsonArray = parser.parse(cfg).getAsJsonArray();
                for (JsonElement bean : jsonArray) {
                    //使用GSON，直接转成Bean对象
                    GsonMatchRankAward gsonMatchRankAward = new Gson().fromJson(bean, GsonMatchRankAward.class);
                    list.add(gsonMatchRankAward);
                }

                GsonResult result = HttpHandler.configDdzMatchRankAward(list, channelId, Integer.valueOf(optionType), type);
                if (result != null) {
                    return result.getRet();
                }
            }
        }
        return 1;
    }
}
