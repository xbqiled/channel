package com.channel.cac.modules.cac.service;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.channel.api.entity.cac.GameRoomVo;
import com.channel.api.entity.cac.AccountinfoEntity;
import com.channel.api.entity.cac.UsergamedataEntity;
import com.channel.api.entity.chl.ChlUserEntity;
import com.channel.api.service.cac.UsergamedataService;
import com.channel.cac.modules.cac.dao.AccountinfoDao;
import com.channel.cac.modules.cac.dao.UsergamedataDao;
import com.channel.common.constant.ConfigConstant;
import com.channel.common.json.GsonResult;
import com.channel.common.utils.HttpHandler;
import com.channel.common.utils.PageUtils;
import com.channel.common.utils.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



@Service("usergamedataService")
@com.alibaba.dubbo.config.annotation.Service(
        interfaceClass = UsergamedataService.class,
        timeout = 10000,
        version = "${api.service.version}"
)
public class UsergamedataServiceImpl extends ServiceImpl<UsergamedataDao, UsergamedataEntity> implements UsergamedataService {

    @Autowired
    private AccountinfoDao accountinfoDao;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        Page<UsergamedataEntity> page = this.selectPage(
                new Query<UsergamedataEntity>(params).getPage(),
                new EntityWrapper<UsergamedataEntity>()
        );
        return new PageUtils(page);
    }

    @Override
    public List<Map<String, Object>> getRoomTreeData() {
        List<Map<String, Object>> reusltList = new ArrayList<>();

        List<GameRoomVo> gameList = baseMapper.queryGameList();
        List<GameRoomVo> roomList = baseMapper.queryGameRoomList();

        reusltList = packageGameTree(gameList, roomList);
        return reusltList;
    }


    /**
     * <B>组装数据信息</B>
     * @return
     */
    List<Map<String, Object>> packageGameTree(List<GameRoomVo> gameList, List<GameRoomVo> roomList) {
        List<Map<String, Object>> reusltList = new ArrayList<>();

        for (GameRoomVo  game :  gameList) {
            Map<String, Object> map = new HashMap<>();
            List<Map<String, Object>> childrenList = new ArrayList<>();

            map.put("value", game.getGameKindType());
            map.put("label", game.getGameKindName());

            for (GameRoomVo room : roomList) {
                if (game.getGameKindType().intValue() == room.getGameKindType().intValue()) {
                    Map<String, Object> childrenMap = new HashMap<>();

                    childrenMap.put("value", room.getGameAtomTypeId());
                    childrenMap.put("label", room.getPhoneGameName());
                    childrenList.add(childrenMap);
                }
            }

            map.put("children", childrenList);
            reusltList.add(map);
        }
        return  reusltList;
    }


    @Override
    public List<Map<String, Object>> getAllRoomTreeData() {
        List<Map<String, Object>> reusltList = new ArrayList<>();

        List<GameRoomVo> gameList = baseMapper.queryAllGameList();
        List<GameRoomVo> roomList = baseMapper.queryGameRoomList();
        reusltList = packageGameTree(gameList, roomList);

        return reusltList;
    }

    @Override
    public PageUtils queryUserCtrlPage(Map<String, Object> params,  ChlUserEntity userEntity) {
        String current = (String)params.get(ConfigConstant.LIMIT);
        String size  = (String)params.get(ConfigConstant.PAGE);

        //根据拓展Server获取渠道ID
        String channelId = ObjectUtil.isNotNull(userEntity.getChannelId()) ? userEntity.getChannelId().toString() : "";
        String userId = ObjectUtil.isNotNull(params.get("userId")) ? (String)params.get("userId") : "";
        String gameRoomId = ObjectUtil.isNotNull(params.get("gameRoomId")) ? (String)params.get("gameRoomId") : "";

        String type = ObjectUtil.isNotNull(params.get("type")) ? (String)params.get("type") : "";
        String betLimitStart = ObjectUtil.isNotNull(params.get("betLimitStart")) ? (String) params.get("betLimitStart") : "";
        String betLimitEnd = ObjectUtil.isNotNull(params.get("betLimitEnd")) ? (String) params.get("betLimitEnd") : "";

        String refreshTime = ObjectUtil.isNotNull(params.get("refreshTime")) ? (String) params.get("refreshTime") : "";
        String order = ObjectUtil.isNotNull(params.get("order")) ? (String)params.get("order") : "";
        String sortby = ObjectUtil.isNotNull(params.get("sortby")) ? (String)params.get("sortby") : "";

        String orderStr = packOrderStr(order, sortby);

        Page<UsergamedataEntity> page = new Page<>(new Integer(size), new Integer(current));
        List<UsergamedataEntity> list = this.baseMapper.queryUserCtrlPage(page, channelId, userId, gameRoomId, type, betLimitStart, betLimitEnd, refreshTime, orderStr);
        return new PageUtils(page.setRecords(list));
    }


    String packOrderStr(String order, String sortby) {
        StringBuffer sb = new StringBuffer();
        if (StrUtil.isBlank(order) || StrUtil.isBlank(sortby)) {
            return "  order by a.t_type desc  ";
        } else {
            sb.append(" order by ");
            if (sortby.equals("type")) {
                sb.append(" a.t_type ");
            } else if (sortby.equals("betLimit")) {
                sb.append(" a.t_betLimit ");
            } else if (sortby.equals("rate")) {
                sb.append(" a.t_rate ");
            } else if (sortby.equals("round")) {
                sb.append(" a.t_round ");
            } else if (sortby.equals("tCtltotalscore")) {
                sb.append(" a.t_ctlTotalScore ");
            } else if (sortby.equals("totalRound")) {
                sb.append(" a.t_totalRound ");
            } else if (sortby.equals("histWin")) {
                sb.append(" a.t_histWin ");
            } else if (sortby.equals("todayWin")) {
                sb.append("  a.t_todayWin ");
            } else if (sortby.equals("totalPlayCount")) {
                sb.append("  a.t_totalPlayCount ");
            } else if (sortby.equals("todayRsfTime")) {
                sb.append("  a.t_todayRsfTime ");
            }
            sb.append(order);
            return sb.toString();
        }
    }


    @Override
    public PageUtils queryUserOnlinePage(Map<String, Object> params,  ChlUserEntity userEntity) {
        String current = (String)params.get(ConfigConstant.LIMIT);
        String size  = (String)params.get(ConfigConstant.PAGE);

        //根据拓展Server获取渠道ID
        String channelId = ObjectUtil.isNotNull(userEntity.getChannelId()) ? userEntity.getChannelId() : "";
        String userId = ObjectUtil.isNotNull(params.get("userId")) ? (String)params.get("userId") : "";
        String gameRoomId = ObjectUtil.isNotNull(params.get("gameRoomId")) ? (String)params.get("gameRoomId") : "";
        String nikeName = ObjectUtil.isNotNull(params.get("nikeName")) ? (String)params.get("nikeName") : "";
        Page<Map<String, Object>> page = new Page<>(new Integer(size), new Integer(current));
        List<Map<String, Object>> list = this.baseMapper.queryUserOnlinePage(page, channelId, userId, gameRoomId);
        return new PageUtils(page.setRecords(list));
    }


    @Override
    public List<Map<String, Object>> queryGameRoom() {
        return baseMapper.queryGameRoom();
    }


    @Override
    public int doSet(String accountId, String type, String betLimit, String rate, String round, String roomList, ChlUserEntity userEntity) {
        //判断accountId是否正确
        AccountinfoEntity account = accountinfoDao.getUserInfo(accountId, userEntity.getChannelId());
        if (account == null) {
            return 1;
        }

        Integer[] roomArr = {};
        if (StrUtil.isNotBlank(roomList)) {
            if (roomList.contains(",")) {

                String[] strDesc = roomList.split(",");
                if (strDesc != null && strDesc.length > 0) {
                    roomArr = new Integer [strDesc.length];

                    for (int i = 0; i < strDesc.length; i++) {
                        roomArr[i] = Integer.parseInt(strDesc[i]);
                    }
                }
            } else {
                roomArr = new Integer[1];
                roomArr[0] = Integer.parseInt(roomList);
            }
        }

        //调用接口处理
        GsonResult result = HttpHandler.userCtrl(new Integer(accountId), new Integer(type), roomArr, new Integer(betLimit), new Integer(rate), new Integer(round));
        if (result != null && result.getRet() == 0) {
            return 0;
        } else {
            return 2;
        }
    }

    /**
     * <B>解控</B>
     * @param accountId
     * @param roomId
     * @return
     */
    @Override
    public int cancelCtrl(String accountId, String roomId) {
        Integer[] roomArr = {Integer.parseInt(roomId)};
        GsonResult result = HttpHandler.cancelUserCtrl(new Integer(accountId), roomArr);
        if (result != null && result.getRet() == 0) {
            return 0;
        } else {
            return 2;
        }
    }

    /**
     * <B>删除控制数据</B>
     * @param accountId
     * @return
     */
    @Override
    public int delCtrlData(String accountId, String roomId) {
        Integer[] roomArr = {Integer.parseInt(roomId)};
        GsonResult result = HttpHandler.delCtrlDate(new Integer(accountId), roomArr);
        if (result != null && result.getRet() == 0) {
            return 0;
        } else {
            return 2;
        }
    }
}
