package com.channel.cac.modules.cac.service;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.channel.api.entity.cac.BgTransferEntity;
import com.channel.api.entity.cac.BgenterConfigEntity;
import com.channel.api.entity.chl.ChlUserEntity;
import com.channel.api.service.cac.BgenterConfigService;
import com.channel.cac.modules.cac.dao.BgenterConfigDao;
import com.channel.common.constant.ConfigConstant;
import com.channel.common.utils.PageUtils;
import com.channel.common.utils.Query;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.Map;


@Service("bgenterConfigService")
@com.alibaba.dubbo.config.annotation.Service(
        interfaceClass = BgenterConfigService.class,
        timeout = 20000,
        version = "${api.service.version}"
)
public class BgenterConfigServiceImpl extends ServiceImpl<BgenterConfigDao, BgenterConfigEntity> implements BgenterConfigService {

    @Override
    public PageUtils queryBgEnterGamePage(Map<String, Object> params, ChlUserEntity userEntity) {
        String current = (String)params.get(ConfigConstant.LIMIT);
        String size  = (String)params.get(ConfigConstant.PAGE);
        Page<BgenterConfigEntity> page = new Page<>(new Integer(size), new Integer(current));

        //根据拓展Server获取渠道ID
        String channelId = ObjectUtil.isNotNull(userEntity.getChannelId()) ? userEntity.getChannelId() : "";
        List<BgenterConfigEntity> resultList = baseMapper.queryEnterGameConfigPage(page, channelId);
        return new PageUtils(page.setRecords(resultList));
    }

    @Override
    public BgenterConfigEntity getConfigByChannelId(String channelId) {
        return baseMapper.getConfigByChannelId(channelId);
    }

    @Override
    public Boolean saveEnterConfig(BgenterConfigEntity bgenterConfig, ChlUserEntity userEntity) {
        BgenterConfigEntity config = baseMapper.getConfigByChannelId(userEntity.getChannelId());
        if (config != null) {
            return  Boolean.FALSE;
        }

        bgenterConfig.setCreateBy(userEntity.getUsername());
        bgenterConfig.setCreateTime(new Date());
        this.insert(bgenterConfig);
        return Boolean.TRUE;
    }

    @Override
    public Boolean modifyEnterConfig(BgenterConfigEntity bgenterConfig, ChlUserEntity userEntity) {
        bgenterConfig.setModifyBy(userEntity.getUsername());
        bgenterConfig.setModifyTime(new Date());
        this.updateById(bgenterConfig);
        return Boolean.TRUE;
    }
}
