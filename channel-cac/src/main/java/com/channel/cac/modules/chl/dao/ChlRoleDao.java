package com.channel.cac.modules.chl.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.channel.api.entity.chl.ChlRoleEntity;

import java.util.List;

/**
 * 角色管理
 *
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2018-11-16 15:58:41
 */
public interface ChlRoleDao extends BaseMapper<ChlRoleEntity> {

    /**
     * 查询用户创建的角色ID列表
     */
    List<Long> queryRoleIdList(Long createUserId);
}
