package com.channel.cac.modules.cac.service;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.channel.api.entity.cac.BgbetRecordEntity;
import com.channel.api.entity.chl.ChlUserEntity;
import com.channel.api.service.cac.BgbetRecordService;
import com.channel.cac.modules.cac.dao.BgbetRecordDao;
import com.channel.common.constant.ConfigConstant;
import com.channel.common.utils.PageUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;


@Service("bgbetRecordService")
@com.alibaba.dubbo.config.annotation.Service(
        interfaceClass = BgbetRecordService.class,
        timeout = 20000,
        version = "${api.service.version}"
)
public class BgbetRecordServiceImpl extends ServiceImpl<BgbetRecordDao, BgbetRecordEntity> implements BgbetRecordService {

    private static String BGBET_TABLE_NAME = "bgbet_record";

    @Override
    public PageUtils queryBgBetRecordPage(Map<String, Object> params, ChlUserEntity userEntity) {
        String current = (String)params.get(ConfigConstant.LIMIT);
        String size  = (String)params.get(ConfigConstant.PAGE);
        Page<BgbetRecordEntity> page = new Page<>(new Integer(size), new Integer(current));

        //根据拓展Server获取渠道ID
        String channelId = ObjectUtil.isNotNull(userEntity.getChannelId()) ? userEntity.getChannelId() : "";
        String userId = ObjectUtil.isNotNull(params.get("userId")) ? (String) params.get("userId") : "";
        String orderId = ObjectUtil.isNotNull(params.get("orderId")) ? (String) params.get("orderId") : "";

        String issueId = ObjectUtil.isNotNull(params.get("issueId")) ? (String) params.get("issueId") : "";
        String gameName = ObjectUtil.isNotNull(params.get("gameName")) ? (String) params.get("gameName") : "";
        String startTime = ObjectUtil.isNotNull(params.get("startTime")) ? (String) params.get("startTime") : "";

        StringBuffer tableSql = new StringBuffer();
        String endTime = ObjectUtil.isNotNull(params.get("endTime")) ? (String) params.get("endTime") : "";
        List<String> list = getIntervalMonth(startTime, endTime);
        String sql = spliceInSql(list);

        List<String> tableList = baseMapper.queryHaveTab(sql);
        tableSql.append(spliceQuerySql(tableList));

        List<BgbetRecordEntity> resultList = baseMapper.queryBgBetRecordPage(page, tableSql.toString(), channelId, orderId, userId, issueId, gameName, startTime, endTime);
        return new PageUtils(page.setRecords(resultList));
    }


    @Override
    public PageUtils queryStaticBgBetRecordPage(Map<String, Object> params, ChlUserEntity userEntity) {
        String current = (String)params.get(ConfigConstant.LIMIT);
        String size  = (String)params.get(ConfigConstant.PAGE);
        Page<Map<String, Object>> page = new Page<>(new Integer(size), new Integer(current));

        String channelId = ObjectUtil.isNotNull(userEntity.getChannelId()) ? userEntity.getChannelId() : "";
        String startTime = ObjectUtil.isNotNull(params.get("startTime")) ? (String) params.get("startTime") : "";
        String endTime = ObjectUtil.isNotNull(params.get("endTime")) ? (String) params.get("endTime") : "";

        Date queryMonth = DateUtil.parse(startTime, "yyyy-MM-dd");
        String month = DateUtil.format(queryMonth, "yyyyMM");

        List<Map<String, Object>> resultList = baseMapper.queryStaticBgBetRecordPage(page, BGBET_TABLE_NAME + month, channelId, startTime, endTime);
        return new PageUtils(page.setRecords(resultList));
    }


    private List<String> getIntervalMonth(String startTime, String endTime) {
        List<String> list = new ArrayList<>();
        //转化成月份
        Date startMonthDate = DateUtil.parse(startTime, DatePattern.NORM_DATETIME_PATTERN);
        Date endMonthDate = DateUtil.parse(endTime, DatePattern.NORM_DATETIME_PATTERN);

        String endMonth = DateUtil.format(endMonthDate, "yyyyMM");
        Date nextDate = startMonthDate;

        //判断时间不等于结束时间
        while (!DateUtil.format(nextDate, "yyyyMM").equals(endMonth)) {
            list.add(BGBET_TABLE_NAME + DateUtil.format(nextDate, "yyyyMM"));
            nextDate = DateUtil.offsetMonth(nextDate, 1);
        }

        list.add(BGBET_TABLE_NAME + endMonth);
        return list;
    }

    private String spliceQuerySql(List<String> list) {
        StringBuffer sb = new StringBuffer();
        sb.append("(");
        if (CollUtil.isNotEmpty(list)) {
            for (int i = 0; i < list.size(); i++) {
                sb.append(" select * From " + list.get(i));
                if (i < list.size() - 1) {
                    sb.append("  union all ");
                }
            }
        }
        sb.append(")");
        return sb.toString();
    }

    private String spliceInSql(List<String> list) {
        StringBuffer sb = new StringBuffer();
        sb.append("(");
        if (CollUtil.isNotEmpty(list)) {
            for (int i = 0; i < list.size(); i++) {
                sb.append("'" + list.get(i) + "'");
                if (i < list.size() - 1) {
                    sb.append(" , ");
                }
            }
        }
        sb.append(")");
        return sb.toString();
    }
}
