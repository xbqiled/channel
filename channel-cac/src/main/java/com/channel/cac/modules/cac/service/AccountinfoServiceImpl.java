package com.channel.cac.modules.cac.service;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ArrayUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.channel.api.entity.cac.AccountinfoEntity;
import com.channel.api.entity.chl.ChlUserEntity;
import com.channel.api.service.cac.AccountinfoService;
import com.channel.cac.modules.cac.dao.AccountinfoDao;
import com.channel.common.constant.ConfigConstant;
import com.channel.common.json.GsonResult;
import com.channel.common.json.GsonUserGame;
import com.channel.common.utils.HttpHandler;
import com.channel.common.utils.PageUtils;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.logging.Handler;


@Service("accountinfoService")
@com.alibaba.dubbo.config.annotation.Service(
        interfaceClass = AccountinfoService.class,
        timeout = 30000,
        version = "${api.service.version}"
)
public class AccountinfoServiceImpl extends ServiceImpl<AccountinfoDao, AccountinfoEntity> implements AccountinfoService {

    @Override
    public PageUtils queryUserPhoneList(Map<String, Object> params, ChlUserEntity userEntity) {
        //获取当前账号对应的渠道编码
        String current = (String)params.get(ConfigConstant.LIMIT);
        String size  = (String)params.get(ConfigConstant.PAGE);

        //根据拓展Server获取渠道ID
        String channelId = ObjectUtil.isNotNull(userEntity.getChannelId()) ? userEntity.getChannelId() : "";
        String ip = ObjectUtil.isNotNull(params.get("ip")) ? (String)params.get("ip") : "";
        String userId = ObjectUtil.isNotNull(params.get("userId")) ? (String)params.get("userId") : "";
        String userName = ObjectUtil.isNotNull(params.get("userName")) ? (String)params.get("userName") : "";

        String nickName = ObjectUtil.isNotNull(params.get("nickName")) ? (String)params.get("nickName") : "";
        String promoterId = ObjectUtil.isNotNull(params.get("promoterId")) ? (String)params.get("promoterId") : "";
        String isOnLine = ObjectUtil.isNotNull(params.get("isOnLine")) ? (String)params.get("isOnLine") : "";

        String vipLevel = ObjectUtil.isNotNull(params.get("vipLevel")) ? (String)params.get("vipLevel") : "";
        String source = ObjectUtil.isNotNull(params.get("source")) ? (String)params.get("source") : "";
        String queryType = ObjectUtil.isNotNull(params.get("queryType")) ? (String)params.get("queryType") : "";

        String queryValue = ObjectUtil.isNotNull(params.get("queryValue")) ? (String)params.get("queryValue") : "";
        String regStartTime = ObjectUtil.isNotNull(params.get("regStartTime")) && ((String) params.get("regStartTime")).length() == 13 ? (String)params.get("regStartTime")  : "";
        String regEndTime = ObjectUtil.isNotNull(params.get("regEndTime")) && ((String) params.get("regEndTime")).length() == 13 ?  (String)params.get("regEndTime") : "";

        String isRecharge = ObjectUtil.isNotNull(params.get("isRecharge")) ? (String)params.get("isRecharge") : "";
        String order = ObjectUtil.isNotNull(params.get("order")) ? (String)params.get("order") : "";

        String accountNum = "";
        String accountName = "";
        String phoneNumber = "";

        String sortby = ObjectUtil.isNotNull(params.get("sortby")) ? (String)params.get("sortby") : "";
        Page<Map<String, Object>> page = new Page<>(new Integer(size), new Integer(current));
        String orderStr = packOrderStr(isRecharge, order, sortby);

        if (StrUtil.isNotBlank(regStartTime)) {
            regStartTime = DateUtil.format(new Date(Long.valueOf(regStartTime)), ConfigConstant.FULL_DATE_TIME_FORMAT);
        }

        if (StrUtil.isNotBlank(regEndTime)) {
            regEndTime = DateUtil.format(new Date(Long.valueOf(regEndTime)), ConfigConstant.FULL_DATE_TIME_FORMAT);
        }

        //判断查询条件
        if (StrUtil.isNotBlank(queryType) && StrUtil.isNotBlank(queryValue)) {
            if (queryType.equals("1")) {
                userName = queryValue;
            } else if (queryType.equals("2")) {
                nickName = queryValue;
            } else if (queryType.equals("3")) {
                accountName = queryValue;
            } else if (queryType.equals("4")) {
                accountNum = queryValue;
            } else if (queryType.equals("5")) {
                phoneNumber = queryValue;
            }
        }

        List<Map<String, Object>> list = this.baseMapper.queryUserPhoneList(page, channelId, isOnLine, promoterId, userId, userName, nickName, ip,
                regStartTime, regEndTime, accountName, vipLevel, accountNum, phoneNumber, source, orderStr);
        return new PageUtils(page.setRecords(list));
    }


    @Override
    public PageUtils quereyAccountPage(Map<String, Object> params, ChlUserEntity userEntity) {
        //获取当前账号对应的渠道编码
        String current = (String)params.get(ConfigConstant.LIMIT);
        String size  = (String)params.get(ConfigConstant.PAGE);

        //根据拓展Server获取渠道ID
        String channelId = ObjectUtil.isNotNull(userEntity.getChannelId()) ? userEntity.getChannelId() : "";
        String ip = ObjectUtil.isNotNull(params.get("ip")) ? (String)params.get("ip") : "";
        String userId = ObjectUtil.isNotNull(params.get("userId")) ? (String)params.get("userId") : "";
        String userName = ObjectUtil.isNotNull(params.get("userName")) ? (String)params.get("userName") : "";

        String nickName = ObjectUtil.isNotNull(params.get("nickName")) ? (String)params.get("nickName") : "";
        String promoterId = ObjectUtil.isNotNull(params.get("promoterId")) ? (String)params.get("promoterId") : "";
        String isOnLine = ObjectUtil.isNotNull(params.get("isOnLine")) ? (String)params.get("isOnLine") : "";

        String source = ObjectUtil.isNotNull(params.get("source")) ? (String)params.get("source") : "";
        String queryType = ObjectUtil.isNotNull(params.get("queryType")) ? (String)params.get("queryType") : "";
        String queryValue = ObjectUtil.isNotNull(params.get("queryValue")) ? (String)params.get("queryValue") : "";
        String vipLevel = ObjectUtil.isNotNull(params.get("vipLevel")) ? (String)params.get("vipLevel") : "";

        String regStartTime = ObjectUtil.isNotNull(params.get("regStartTime")) && ((String) params.get("regStartTime")).length() == 13 ? (String)params.get("regStartTime")  : "";
        String regEndTime = ObjectUtil.isNotNull(params.get("regEndTime")) && ((String) params.get("regEndTime")).length() == 13 ?  (String)params.get("regEndTime") : "";
        String isRecharge = ObjectUtil.isNotNull(params.get("isRecharge")) ? (String)params.get("isRecharge") : "";
        String order = ObjectUtil.isNotNull(params.get("order")) ? (String)params.get("order") : "";

        String accountNum = "";
        String accountName = "";
        String phoneNumber = "";

        String sortby = ObjectUtil.isNotNull(params.get("sortby")) ? (String)params.get("sortby") : "";
        Page<Map<String, Object>> page = new Page<>(new Integer(size), new Integer(current));
        String orderStr = packOrderStr(isRecharge, order, sortby);

        if (StrUtil.isNotBlank(regStartTime)) {
            regStartTime = DateUtil.format(new Date(Long.valueOf(regStartTime)), ConfigConstant.FULL_DATE_TIME_FORMAT);
        }

        if (StrUtil.isNotBlank(regEndTime)) {
            regEndTime = DateUtil.format(new Date(Long.valueOf(regEndTime)), ConfigConstant.FULL_DATE_TIME_FORMAT);
        }

        //判断查询条件
        if (StrUtil.isNotBlank(queryType) && StrUtil.isNotBlank(queryValue)) {
            if (queryType.equals("1")) {
                userName = queryValue;
            } else if (queryType.equals("2")) {
                nickName = queryValue;
            } else if (queryType.equals("3")) {
                accountName = queryValue;
            } else if (queryType.equals("4")) {
                accountNum = queryValue;
            } else if (queryType.equals("5")) {
                phoneNumber = queryValue;
            }
        }

        List<Map<String, Object>> list = this.baseMapper.queryUserInfo(page, channelId, isOnLine, promoterId, userId, userName, nickName, ip,
                regStartTime, regEndTime, accountName, vipLevel, accountNum, phoneNumber, source, orderStr);
        return  new PageUtils(page.setRecords(list));
    }


    String packOrderStr(String isRecharge, String order, String sortby) {
        StringBuffer sb = new StringBuffer();
        if (StrUtil.isNotBlank(isRecharge)) {
            if (Integer.valueOf(isRecharge) == 0) {
                sb.append("  and tb.tRechargemoney > 0");
            } else {
                sb.append("  and tb.tRechargemoney = 0");
            }
        }

        if (StrUtil.isBlank(order) || StrUtil.isBlank(sortby)) {
            sb.append("  order by tb.tRegtime desc  ");
            return sb.toString();
        } else {
            sb.append("order by ");
            if (sortby.equals("tTotalgametime")) {
                sb.append(" tb.tTotalgametime  ");
            } else if (sortby.equals("tRechargemoney")) {
                sb.append(" tb.tRechargemoney ");
            } else if (sortby.equals("tTotalgametime")) {
                sb.append(" tb.tTotalgametime ");
            } else if (sortby.equals("tAccountstate")) {
                sb.append(" tb.tAccountstate ");
            } else if (sortby.equals("tBankgoldcoin")) {
                sb.append(" tb.tBankgoldcoin ");
            } else if (sortby.equals("tGoldcoin")) {
                sb.append(" tb.tGoldcoin ");
            } else if (sortby.equals("isOnLine")) {
                sb.append(" tb.isOnLine ");
            }
            sb.append(order);
            return sb.toString();
        }
    }

    @Override
    public Map<String, Object> queryIpAccountPage(Map<String, Object> params, ChlUserEntity userEntity) {
        String current = (String)params.get(ConfigConstant.LIMIT);
        String size  = (String)params.get(ConfigConstant.PAGE);
        String channelId = ObjectUtil.isNotNull(userEntity.getChannelId()) ? userEntity.getChannelId() : "";

        String regStartTime = ObjectUtil.isNotNull(params.get("regStartTime")) && ((String) params.get("regStartTime")).length() == 13 ? (String)params.get("regStartTime")  : "";
        String regEndTime = ObjectUtil.isNotNull(params.get("regEndTime")) && ((String) params.get("regEndTime")).length() == 13 ?  (String)params.get("regEndTime") : "";
        String regIp = ObjectUtil.isNotNull(params.get("regIp"))  ?  (String)params.get("regIp") : "";

        if (StrUtil.isNotBlank(regStartTime)) {
            regStartTime = DateUtil.format(new Date(Long.valueOf(regStartTime)), ConfigConstant.FULL_DATE_TIME_FORMAT);
        }
        if (StrUtil.isNotBlank(regEndTime)) {
            regEndTime = DateUtil.format(new Date(Long.valueOf(regEndTime)), ConfigConstant.FULL_DATE_TIME_FORMAT);
        }

        Page<Map<String, Object>> page = new Page<>(new Integer(size), new Integer(current));
        List<Map<String, Object>> resultList = baseMapper.queryIpUserInfo(page, regIp, channelId, regStartTime, regEndTime);
        List<Map<String, Object>> pieList = new ArrayList<>();

        if (CollUtil.isNotEmpty(resultList)) {
            for (Map<String, Object> map : resultList) {
                Map<String, Object> pieMap = new HashMap<>();
                pieMap.put("name", map.get("regIp"));
                pieMap.put("value", map.get("regNum"));
                pieList.add(pieMap);
            }
        }

        //查询分组汇总信息
        Map<String, Object> resultMap = baseMapper.queryRegUserCount(channelId, regIp, regStartTime, regEndTime);
        Map<String, Object> map = new HashMap<>();
        map.put("page",  new PageUtils(page.setRecords(resultList)));

        map.put("regIp", resultMap);
        map.put("pieChar", pieList);
        return map;
    }


    @Override
    public PageUtils quereyDetailIpListPage(Map<String, Object> params, ChlUserEntity userEntity) {
        String current = (String)params.get(ConfigConstant.LIMIT);
        String size  = (String)params.get(ConfigConstant.PAGE);

        String channelId = ObjectUtil.isNotNull(userEntity.getChannelId()) ? userEntity.getChannelId() : "";
        String regIp = ObjectUtil.isNotNull(params.get("regIp")) ? (String)params.get("regIp")  : "";
        String regStartTime = ObjectUtil.isNotNull(params.get("regStartTime")) && ((String) params.get("regStartTime")).length() == 13 ? (String)params.get("regStartTime")  : "";

        String regEndTime = ObjectUtil.isNotNull(params.get("regEndTime")) && ((String) params.get("regEndTime")).length() == 13 ?  (String)params.get("regEndTime") : "";
        if (StrUtil.isNotBlank(regStartTime)) {
            regStartTime = DateUtil.format(new Date(Long.valueOf(regStartTime)), ConfigConstant.FULL_DATE_TIME_FORMAT);
        }

        if (StrUtil.isNotBlank(regEndTime)) {
            regEndTime = DateUtil.format(new Date(Long.valueOf(regEndTime)), ConfigConstant.FULL_DATE_TIME_FORMAT);
        }
        Page<Map<String, Object>> page = new Page<>(new Integer(size), new Integer(current));
        List<Map<String, Object>> resultList = baseMapper.quereyDetailIpPage(page, channelId, regIp, regStartTime, regEndTime);
        return new PageUtils(page.setRecords(resultList));
    }

    @Override
    public GsonResult bindPhoneNum(Integer userId, String phoneNum) {
        return HttpHandler.bindPhoneNum(userId,  phoneNum);
    }

    @Override
    public GsonResult updateStatus(String tAccountid, String status) {
        return  HttpHandler.operateAccount(Integer.parseInt(tAccountid), Integer.parseInt(status) , null);
    }

    @Override
    public Map<String, Object> getUserInfoById(String accountId) {
        String accountTableName = "";
        String userbaseattrTableName = "";
        Integer mo = Integer.valueOf(accountId) % 4;
        switch (mo) {
            case 0:
                userbaseattrTableName = "xbqp_data1.t_userbaseattr";
                accountTableName = "xbqp_data1.t_accountinfo";
                break;

            case 1:
                userbaseattrTableName = "xbqp_data2.t_userbaseattr";
                accountTableName = "xbqp_data2.t_accountinfo";
                break;

            case 2:
                userbaseattrTableName = "xbqp_data3.t_userbaseattr";
                accountTableName = "xbqp_data3.t_accountinfo";
                break;

            case 3:
                userbaseattrTableName = "xbqp_data4.t_userbaseattr";
                accountTableName = "xbqp_data4.t_accountinfo";
                break;
        }

        return baseMapper.getUserInfoById(accountTableName, userbaseattrTableName, accountId);
    }

    @Override
    public int updateUserNote(String userId, String note, ChlUserEntity userEntity) {
        GsonResult result = HttpHandler.updateUserNote(Integer.valueOf(userId), note);
        if (result != null) {
            return result.getRet();
        }
        return 1;
    }

    @Override
    public GsonResult kickUser(Integer userId) {
        Integer [] accountList = {userId};
        GsonResult result = HttpHandler.kickUser(accountList);
        return  result;
    }

    @Override
    public List<Map<String, Object>> isInGame(Integer userId) {
        List<Map<String, Object>> resultList = new ArrayList<>();
        GsonUserGame userGame = HttpHandler.getUserGameList(userId);

        if (userGame != null && ArrayUtil.isNotEmpty(userGame.getList()) ) {
            GsonUserGame.GameList [] gameList = userGame.getList();
            for ( GsonUserGame.GameList game : gameList) {
                Map<String, Object> resultMap = new HashMap<>();

                resultMap.put("game", game.getGame());
                resultMap.put("name", game.getName());
                resultMap.put("coin", game.getCoin());
                resultList.add(resultMap);
            }
        }
        return resultList;
    }

    @Override
    public int modifyLoginPw(String userId, String password) {
        //String md5Pw = MD5Util.getMD5(password);
        //调用接口修改密码
        GsonResult result = HttpHandler.modifyAccountPassword(Integer.parseInt(userId), "", password, Boolean.FALSE);
        return checkResult(result, userId);
    }

    @Override
    public GsonResult modifyRealName(Long accountId, String realName) {
        return HttpHandler.modifyRealName(accountId, realName);
    }

    @Override
    public GsonResult clearCommonDev(Integer userId) {
        return HttpHandler.clearCommonDev(userId);
    }


    @Override
    public GsonResult unbindPhoneNum(Integer userId) {
        return HttpHandler.unbindPhoneNum(userId);
    }


    @Override
    public GsonResult addRelation(String channel, Long accountId, Long promoterId) {
            return HttpHandler.addRelation(channel,accountId, promoterId);
    }

    private int  checkResult(GsonResult result, String userId) {
        if (result.getRet()  == -3) {
            //表示用户不在线
            //baseMapper.modifyPw(userId, pw);
            return 1;
        } else if (result.getRet()  == 0) {
            return 0;
        } else {
            return 2;
        }
    }
}
