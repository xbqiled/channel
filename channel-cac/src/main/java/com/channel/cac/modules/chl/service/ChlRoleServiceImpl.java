package com.channel.cac.modules.chl.service;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.channel.api.entity.chl.ChlRoleEntity;
import com.channel.api.service.chl.ChlRoleMenuService;
import com.channel.api.service.chl.ChlRoleService;
import com.channel.api.service.chl.ChlUserRoleService;
import com.channel.api.service.chl.ChlUserService;
import com.channel.common.constant.Constant;
import com.channel.common.execption.AppException;
import com.channel.common.utils.PageUtils;
import com.channel.common.utils.Query;
import com.channel.cac.modules.chl.dao.ChlRoleDao;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * 角色
 *
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2018-11-16 15:58:41
 */
@Service("chlRoleService")
@com.alibaba.dubbo.config.annotation.Service(
		interfaceClass = ChlRoleService.class,
		timeout = 10000,
		version = "${api.service.version}"
)
public class ChlRoleServiceImpl extends ServiceImpl<ChlRoleDao, ChlRoleEntity> implements ChlRoleService {

	@Autowired
	private ChlRoleMenuService ChlRoleMenuService;

	@Autowired
	private ChlUserService ChlUserService;

	@Autowired
	private ChlUserRoleService ChlUserRoleService;

	@Override
	public PageUtils queryPage(Map<String, Object> params) {
		String roleName = (String)params.get("roleName");
		Long createUserId = (Long)params.get("createUserId");
		String channelId = params.get("channelId") == null ? null : (String)params.get("channelId") ;

		Page<ChlRoleEntity> page = this.selectPage(
				new Query<ChlRoleEntity>(params).getPage(),
				new EntityWrapper<ChlRoleEntity>()
						.like(StringUtils.isNotBlank(roleName),"role_name", roleName)
						.eq(createUserId != null,"create_user_id", createUserId)
						.eq(StrUtil.isNotBlank(channelId),"channel_id", channelId)
		);

		return new PageUtils(page);
	}


	@Override
	public PageUtils queryAdminPage(Map<String, Object> params) {
		String roleName = (String)params.get("roleName");
		Long createUserId = (Long)params.get("createUserId");

		Page<ChlRoleEntity> page = this.selectPage(
				new Query<ChlRoleEntity>(params).getPage(),
				new EntityWrapper<ChlRoleEntity>()
						.like(StringUtils.isNotBlank(roleName),"role_name", roleName)
						.eq(createUserId != null,"create_user_id", createUserId)
		);

		return new PageUtils(page);
	}



	@Override
	@Transactional(rollbackFor = Exception.class)
	public void save(ChlRoleEntity role) {
		role.setCreateTime(new Date());
		this.insert(role);

		//检查权限是否越权
		checkPrems(role);
		//保存角色与菜单关系
		ChlRoleMenuService.saveOrUpdate(role.getRoleId(), role.getMenuIdList());
	}


	@Override
	@Transactional(rollbackFor = Exception.class)
	public void update(ChlRoleEntity role) {
		this.updateById(role);

		//检查权限是否越权
		checkPrems(role);

		//更新角色与菜单关系
		ChlRoleMenuService.saveOrUpdate(role.getRoleId(), role.getMenuIdList());
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void deleteBatch(Long[] roleIds) {
		//删除角色
		this.deleteBatchIds(Arrays.asList(roleIds));

		//删除角色与菜单关联
		ChlRoleMenuService.deleteBatch(roleIds);

		//删除角色与用户关联
		ChlUserRoleService.deleteBatch(roleIds);
	}


	@Override
	public List<Long> queryRoleIdList(Long createUserId) {
		return baseMapper.queryRoleIdList(createUserId);
	}

	/**
	 * 检查权限是否越权
	 */
	private void checkPrems(ChlRoleEntity role){
		//如果不是超级管理员，则需要判断角色的权限是否超过自己的权限
		if(role.getCreateUserId() == Constant.SUPER_ADMIN){
			return ;
		}

		//查询用户所拥有的菜单列表
		List<Long> menuIdList = ChlUserService.queryAllMenuId(role.getCreateUserId());

		//判断是否越权
		if(!menuIdList.containsAll(role.getMenuIdList())){
			throw new AppException("新增角色的权限，已超出你的权限范围");
		}
	}
}
