package com.channel.cac.modules.cac.service;

import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.channel.api.entity.cac.TipOrderEntity;
import com.channel.api.service.cac.TipOrderService;
import com.channel.cac.modules.cac.dao.TipOrderDao;
import com.channel.common.utils.PageUtils;
import com.channel.common.utils.Query;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;


@Service("tipOrderService")
@com.alibaba.dubbo.config.annotation.Service(
        interfaceClass = TipOrderService.class,
        timeout = 20000,
        version = "${api.service.version}"
)
public class TipOrderServiceImpl extends ServiceImpl<TipOrderDao, TipOrderEntity> implements TipOrderService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        Page<TipOrderEntity> page = this.selectPage(
                new Query<TipOrderEntity>(params).getPage(),
                new EntityWrapper<TipOrderEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public List<Map<String, Object>> queryTipInfoOrder(String channelId) {
        List<Map<String, Object>> resultList = baseMapper.queryTipInfoOrder(channelId);
        if (CollUtil.isNotEmpty(resultList)) {
            List<TipOrderEntity> list = new ArrayList<>();
            for (Map<String, Object> map : resultList) {
                if (map.get("cash_no") != null) {
                    list.add(createTipOrder(map)) ;
                }
            }
            this.insertBatch(list);
        }
        return resultList;
    }

    private TipOrderEntity createTipOrder(Map<String, Object> map) {
        TipOrderEntity order = new TipOrderEntity();
        order.setCreateTime(new Date());
        order.setOrderNo((String)map.get("cash_no"));

        order.setType(2);
        order.setTipCount(1);
        order.setStatus(1);
        return order;
    }
}
