package com.channel.cac.modules.cac.service;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.channel.api.entity.cac.BindMobileEntity;
import com.channel.api.entity.chl.ChlUserEntity;
import com.channel.api.service.cac.BindMobileService;
import com.channel.cac.modules.cac.dao.BindMobileDao;
import com.channel.common.json.GsonResult;
import com.channel.common.utils.HttpHandler;
import com.channel.common.utils.PageUtils;
import com.channel.common.utils.Query;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Map;


@Service("bindMobileService")
@com.alibaba.dubbo.config.annotation.Service(
        interfaceClass = BindMobileService.class,
        timeout = 10000,
        version = "${api.service.version}"
)
public class BindMobileServiceImpl extends ServiceImpl<BindMobileDao, BindMobileEntity> implements BindMobileService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        Page<BindMobileEntity> page = this.selectPage(
                new Query<BindMobileEntity>(params).getPage(),
                new EntityWrapper<BindMobileEntity>()
        );
        return new PageUtils(page);
    }

    @Override
    public BindMobileEntity getByChannelId(ChlUserEntity chlUserEntity) {
        String channel = chlUserEntity.getChannelId();
        BindMobileEntity bindMobileEntity =  baseMapper.findByChannelId(channel);
        return  bindMobileEntity == null ?   new BindMobileEntity() : bindMobileEntity;
    }

    @Override
    public Boolean saveOrUpdateBind(BindMobileEntity bindMobile, ChlUserEntity chlUserEntity) {
        String channel = chlUserEntity.getChannelId();
        BindMobileEntity bindMobileEntity =   baseMapper.findByChannelId(channel);
        bindMobile.setChannelId(chlUserEntity.getChannelId());

        GsonResult result = HttpHandler.bindGive(channel, bindMobile.getGiveValue().intValue(), bindMobile.getRebate(), bindMobile.getMinExchange(),
                bindMobile.getExchange(), bindMobile.getInitCoin(), bindMobile.getDamaMulti(), bindMobile.getRebateDamaMulti(),
                bindMobile.getExchangeCount(), bindMobile.getPromoteType(), bindMobile.getResetDamaLeftCoin());
        if (result != null && result.getRet() == 0) {
            if(bindMobileEntity != null && StrUtil.isNotBlank(bindMobileEntity.getChannelId())) {
                bindMobile.setModifyTime(new Date());
                bindMobile.setChannelId(channel);

                bindMobile.setModifyBy(chlUserEntity.getUsername());
                baseMapper.updateBind(bindMobile.getChannelId(), bindMobile.getModifyTime(), bindMobile.getModifyBy(), bindMobile.getGiveValue().toString(),
                        bindMobile.getRebate().toString(), bindMobile.getExchange().toString(), bindMobile.getInitCoin().toString(),
                        bindMobile.getMinExchange().toString(), bindMobile.getDamaMulti().toString(), bindMobile.getRebateDamaMulti().toString(),
                        bindMobile.getExchangeCount().toString(), bindMobile.getPromoteType().toString(), bindMobile.getResetDamaLeftCoin().toString());
                return Boolean.TRUE;
            } else {
                bindMobile.setCreateTime(new Date());
                bindMobile.setChannelId(channel);
                bindMobile.setCreateBy(chlUserEntity.getUsername());
                return this.insert(bindMobile);
            }
        } else {
            return Boolean.FALSE;
        }
    }
}
