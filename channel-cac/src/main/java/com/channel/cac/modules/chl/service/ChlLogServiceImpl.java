package com.channel.cac.modules.chl.service;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.channel.api.entity.cac.SysPayOrderEntity;
import com.channel.api.entity.chl.ChlLogEntity;
import com.channel.api.entity.chl.ChlUserEntity;
import com.channel.api.service.chl.ChlLogService;
import com.channel.common.constant.ConfigConstant;
import com.channel.common.utils.PageUtils;
import com.channel.common.utils.Query;
import com.channel.cac.modules.chl.dao.ChlLogDao;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service("chlLogService")
@com.alibaba.dubbo.config.annotation.Service(
        interfaceClass = ChlLogService.class,
        timeout = 10000,
        version = "${api.service.version}"
)
public class ChlLogServiceImpl extends ServiceImpl<ChlLogDao, ChlLogEntity> implements ChlLogService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        String key = (String)params.get("key");

        Page<ChlLogEntity> page = this.selectPage(
            new Query<ChlLogEntity>(params).getPage(),
            new EntityWrapper<ChlLogEntity>().like(StringUtils.isNotBlank(key),"username", key)
        );

        return new PageUtils(page);
    }


    @Override
    public PageUtils queryLogPage(Map<String, Object> params, ChlUserEntity userEntity) {
        String size  = (String)params.get(ConfigConstant.PAGE);
        String current = (String)params.get(ConfigConstant.LIMIT);

        //根据拓展Server获取渠道ID
        String channelId = ObjectUtil.isNotNull(userEntity.getChannelId()) ? userEntity.getChannelId(): "";
        String userId = ObjectUtil.isNotNull(params.get("userId")) ? (String)params.get("userId") : "";
        String userName = ObjectUtil.isNotNull(params.get("userName")) ? (String)params.get("userName") : "";
        String operation = ObjectUtil.isNotNull(params.get("operation")) ? (String)params.get("operation") : "";

        String startTime = ObjectUtil.isNotNull(params.get("startTime")) ? (String)params.get("startTime") : "";
        String endTime = ObjectUtil.isNotNull(params.get("endTime")) ? (String)params.get("endTime") : "";
        Page<ChlLogEntity> page = new Page<>(new Integer(size), new Integer(current));

        List<ChlLogEntity> list = this.baseMapper.queryLogPage(page, channelId, userId, userName, operation, startTime, endTime);
        return  new PageUtils(page.setRecords(list));
    }
}
