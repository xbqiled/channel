package com.channel.cac.modules.cac.dao;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.channel.api.entity.cac.SysPayDisplayEntity;

/**
 * 
 * 
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-10-06 17:15:45
 */
public interface SysPayDisplayDao extends BaseMapper<SysPayDisplayEntity> {


	
}
