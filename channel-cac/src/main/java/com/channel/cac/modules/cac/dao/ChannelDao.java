package com.channel.cac.modules.cac.dao;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.channel.api.entity.cac.VideoBalanceVo;
import com.channel.api.entity.cac.ChannelEntity;
import org.apache.ibatis.annotations.Param;

/**
 * 
 * 
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-03-30 14:16:02
 */
public interface ChannelDao extends BaseMapper<ChannelEntity> {

    ChannelEntity  findChannelById(@Param("channelId")String channelId);

    VideoBalanceVo getVideoBalance(@Param("channelId")String channelId);

}
