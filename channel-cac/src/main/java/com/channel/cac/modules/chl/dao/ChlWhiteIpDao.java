package com.channel.cac.modules.chl.dao;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.channel.api.entity.chl.ChlWhiteIpEntity;
import org.apache.ibatis.annotations.Param;

/**
 * 
 * 
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2018-12-26 16:31:59
 */
public interface ChlWhiteIpDao extends BaseMapper<ChlWhiteIpEntity> {

     ChlWhiteIpEntity queryByIp(@Param("ip") String ip, @Param("userId") String userId);

}
