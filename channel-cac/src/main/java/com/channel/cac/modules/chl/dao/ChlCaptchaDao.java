package com.channel.cac.modules.chl.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.channel.api.entity.chl.ChlCaptchaEntity;

/**
 * 验证码
 *
 */
public interface ChlCaptchaDao extends BaseMapper<ChlCaptchaEntity> {

}
