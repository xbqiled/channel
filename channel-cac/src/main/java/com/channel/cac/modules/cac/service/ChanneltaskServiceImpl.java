package com.channel.cac.modules.cac.service;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ArrayUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.channel.api.entity.cac.ChannelVipEntity;
import com.channel.api.entity.cac.ChanneltaskEntity;
import com.channel.api.entity.cac.TaskVo;
import com.channel.api.entity.cac.VipVo;
import com.channel.api.entity.chl.ChlUserEntity;
import com.channel.api.service.cac.ChanneltaskService;
import com.channel.cac.modules.cac.dao.ChanneltaskDao;
import com.channel.common.constant.ConfigConstant;
import com.channel.common.json.GsonChannelTask;
import com.channel.common.json.GsonResult;
import com.channel.common.json.GsonTask;
import com.channel.common.json.RequestVip;
import com.channel.common.utils.HttpHandler;
import com.channel.common.utils.PageUtils;
import com.google.gson.Gson;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Service("channeltaskService")
@com.alibaba.dubbo.config.annotation.Service(
        interfaceClass = ChanneltaskService.class,
        timeout = 20000,
        version = "${api.service.version}"
)
public class ChanneltaskServiceImpl extends ServiceImpl<ChanneltaskDao, ChanneltaskEntity> implements ChanneltaskService {

    @Override
    public PageUtils queryTaskPage(Map<String, Object> params, ChlUserEntity userEntity) {
        String current = (String) params.get(ConfigConstant.LIMIT);
        String size = (String) params.get(ConfigConstant.PAGE);

        String channelId = ObjectUtil.isNotNull(userEntity.getChannelId()) ? userEntity.getChannelId() : "";
        String startTime = ObjectUtil.isNotNull(params.get("startTime")) ? (String) params.get("startTime") : "";
        String endTime = ObjectUtil.isNotNull(params.get("endTime")) ? (String) params.get("endTime") : "";

        Page<TaskVo> page = new Page<>(new Integer(size), new Integer(current));
        List<TaskVo> list = this.baseMapper.queryTaskPage(page, channelId, startTime, endTime);
        return new PageUtils(page.setRecords(list));
    }

    @Override
    public int channelOpenType(String channelId, String openType) {
        ChanneltaskEntity channeltask = baseMapper.getTaskByChannelId(channelId);
        if (channeltask != null && StrUtil.isNotEmpty(channeltask.getTChannel())) {

            Integer type = channeltask.getTActivityopentype() == 1 ? 0 : 1;
            String jsonStr = new String(channeltask.getTTaskinfo());
            if (StrUtil.isNotEmpty(jsonStr)) {

                GsonTask [] gsonTask = new Gson().fromJson(jsonStr, GsonTask[].class);
                GsonResult result = HttpHandler.cfgTask(channelId, type, channeltask.getTActivityopentype(), channeltask.getTBegintime(), channeltask.getTEndtime(), gsonTask);
                if (result != null) {
                    return result.getRet();
                }
            }
        }
        return 1;
    }

    @Override
    public Map<String, Object> findByChannelId(String channelId) {
        ChanneltaskEntity channelTask = baseMapper.getTaskByChannelId(channelId);
        List<Map<String, Object>> taskList = baseMapper.getTaskCfg(null);
        Map<String, Object> resultMap = new HashMap<>();

        List<Map<String, Object>> rsultList = new ArrayList<>();
        GsonTask [] gsonTask = {};
        if (channelTask != null && StrUtil.isNotEmpty(channelTask.getTChannel())) {

            String jsonStr = new String(channelTask.getTTaskinfo());
            gsonTask = new Gson().fromJson(jsonStr, GsonTask[].class);
            resultMap.put("taskInfo", channelTask);
        }

        if (CollUtil.isNotEmpty(taskList)) {
            for (Map<String, Object> map : taskList) {
                Map<String, Object> taskMap = new HashMap<>();

                taskMap.putAll(map);
                for (GsonTask task : gsonTask) {
                    if (map.get("taskId").toString().equals(task.getTaskId().toString())) {
                        taskMap.put("taskOpenType", task.getTaskOpenType());
                        taskMap.put("targetNum", task.getTargetNum());
                        taskMap.put("taskReward", task.getTaskReward());
                        taskMap.put("continueTaskCoinLimit", task.getContinueTaskCoinLimit());
                    }
                }
                rsultList.add(taskMap);
                resultMap.put("taskArr", rsultList);
            }
        }
        return resultMap;
    }


    @Override
    public int saveOrUpdateConfig(ChlUserEntity userEntity, Integer openType, Integer activityTimeType, String beginTime, String endTime, GsonTask[] taskArr) {
        Long beginDate = new Long(0);
        Long endDate = new Long(0);
        if (StrUtil.isNotEmpty(beginTime) && beginTime.length() == 13) {
             beginDate = new Long(beginTime) /1000;
        }
        if (StrUtil.isNotEmpty(endTime) && endTime.length() == 13) {
             endDate = new Long(endTime) /1000;
        }

        String channelId = userEntity.getChannelId();
        List<GsonTask> resultList = new ArrayList<>();
        GsonTask[] submitTaskArr = {};

        if (ArrayUtil.isNotEmpty(taskArr)) {
            for (GsonTask task : taskArr) {
                if (task.getTargetNum() != null && task.getTaskReward() != null) {
                    if ( task.getTaskOpenType() == null) {
                        task.setTaskOpenType(0);
                    }
                    if ( task.getContinueTaskCoinLimit() == null) {
                        task.setContinueTaskCoinLimit(0);
                    }

                    resultList.add(task);
                }
            }
        }

        submitTaskArr = new GsonTask[resultList.size()];
        resultList.toArray(submitTaskArr);
        GsonResult result = HttpHandler.cfgTask(channelId, openType, activityTimeType, beginDate, endDate, submitTaskArr);
        if (result != null) {
            return result.getRet();
        }
        return 1;
    }
}
