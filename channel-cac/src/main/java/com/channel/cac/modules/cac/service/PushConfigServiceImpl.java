package com.channel.cac.modules.cac.service;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.channel.api.entity.cac.PushConfigEntity;
import com.channel.api.service.cac.PushConfigService;
import com.channel.cac.modules.cac.dao.PushConfigDao;
import org.springframework.stereotype.Service;



@Service("pushConfigService")
@com.alibaba.dubbo.config.annotation.Service(
        interfaceClass = PushConfigService.class,
        timeout = 30000,
        version = "${api.service.version}"
)
public class PushConfigServiceImpl extends ServiceImpl<PushConfigDao, PushConfigEntity> implements PushConfigService {



}
