package com.channel.cac.modules.cac.dao;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.channel.api.entity.cac.SysCashOrderEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 
 * 
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-02-24 19:28:16
 */
public interface SysCashOrderDao extends BaseMapper<SysCashOrderEntity> {

    void approval(@Param("approveBy")String approveBy , @Param("id") String id, @Param("status") String status, @Param("note") String note);

    List<SysCashOrderEntity> queryOrderPage (Pagination page, @Param("channelId")String channelId, @Param("userId")String userId, @Param("status")String status, @Param("startTime")String startTime, @Param("endTime")String endTime);

    List<SysCashOrderEntity> queryCashOrderList (@Param("channelId")String channelId);

}
