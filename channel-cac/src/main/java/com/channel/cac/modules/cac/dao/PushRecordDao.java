package com.channel.cac.modules.cac.dao;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.channel.api.entity.cac.PushRecordEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 
 * 
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2020-05-27 20:42:22
 */
public interface PushRecordDao extends BaseMapper<PushRecordEntity> {

    List<Map<String, Object>> queryPushRecordPage(Pagination page, @Param("sendSys") String sendSys,
                                                  @Param("title") String title,
                                                  @Param("channelId") String channelId,
                                                  @Param("startTime") String startTime,
                                                  @Param("endTime") String endTime);

    Map<String, Object> getPushRecordById(@Param("id") Integer id);

}
