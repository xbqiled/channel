package com.channel.cac.modules.cac.dao;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.channel.api.entity.cac.CsInfoEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 
 * 
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-03-29 17:46:03
 */
public interface CsInfoDao extends BaseMapper<CsInfoEntity> {

    List<CsInfoEntity> queryCsInfoList (Pagination page, @Param("channelId")String channelId, @Param("csName")String userId);

}
