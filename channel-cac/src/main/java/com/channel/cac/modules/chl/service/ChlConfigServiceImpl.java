package com.channel.cac.modules.chl.service;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.channel.api.entity.chl.ChlConfigEntity;
import com.channel.api.service.chl.ChlConfigService;
import com.channel.common.execption.AppException;
import com.channel.common.utils.PageUtils;
import com.channel.common.utils.Query;
import com.channel.cac.modules.chl.dao.ChlConfigDao;
import com.google.gson.Gson;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.Map;

@Service("chlConfigService")
@com.alibaba.dubbo.config.annotation.Service(
		interfaceClass = ChlConfigService.class,
		timeout = 10000,
		version = "${api.service.version}"
)
public class ChlConfigServiceImpl extends ServiceImpl<ChlConfigDao, ChlConfigEntity> implements ChlConfigService {

	@Autowired
	private RedisTemplate redisTemplate;

	@Override
	public PageUtils queryPage(Map<String, Object> params) {
		String paramKey = (String)params.get("paramKey");

		Page<ChlConfigEntity> page = this.selectPage(
				new Query<ChlConfigEntity>(params).getPage(),
				new EntityWrapper<ChlConfigEntity>()
					.like(StringUtils.isNotBlank(paramKey),"param_key", paramKey)
					.eq("status", 1)
		);

		return new PageUtils(page);
	}
	
	@Override
	public void save(ChlConfigEntity config) {
		this.insert(config);
		//ChlConfigRedis.saveOrUpdate(config);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void update(ChlConfigEntity config) {
		this.updateAllColumnById(config);
		//redisTemplate.saveOrUpdate(config);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void updateValueByKey(String key, String value) {
		baseMapper.updateValueByKey(key, value);
		//ChlConfigRedis.delete(key);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void deleteBatch(Long[] ids) {
		for(Long id : ids){
			ChlConfigEntity config = this.selectById(id);
			//ChlConfigRedis.delete(config.getParamKey());
		}

		this.deleteBatchIds(Arrays.asList(ids));
	}

	@Override
	public String getValue(String key) {
		//ChlConfigEntity config = ChlConfigRedis.get(key);
//		if(config == null){
//			config = baseMapper.queryByKey(key);
//			//ChlConfigRedis.saveOrUpdate(config);
//		}
		//return config == null ? null : config.getParamValue();
		return null;
	}
	
	@Override
	public <T> T getConfigObject(String key, Class<T> clazz) {
		String value = getValue(key);
		if(StringUtils.isNotBlank(value)){
			return new Gson().fromJson(value, clazz);
		}

		try {
			return clazz.newInstance();
		} catch (Exception e) {
			throw new AppException("获取参数失败");
		}
	}
}
