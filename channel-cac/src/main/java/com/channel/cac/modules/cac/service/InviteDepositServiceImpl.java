package com.channel.cac.modules.cac.service;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.channel.api.entity.cac.InviteDepositChannelVo;
import com.channel.api.entity.cac.InviteDepositEntity;
import com.channel.api.entity.chl.ChlUserEntity;
import com.channel.api.service.cac.InviteDepositService;
import com.channel.cac.modules.cac.dao.InviteDepositDao;
import com.channel.common.constant.ConfigConstant;
import com.channel.common.json.GsonResult;
import com.channel.common.utils.HttpHandler;
import com.channel.common.utils.PageUtils;
import com.channel.common.utils.Query;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;




@Service("inviteDepositService")
@com.alibaba.dubbo.config.annotation.Service(
        interfaceClass = InviteDepositService.class,
        timeout = 10000,
        version = "${api.service.version}"
)
public class InviteDepositServiceImpl extends ServiceImpl<InviteDepositDao, InviteDepositEntity> implements InviteDepositService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        Page<InviteDepositEntity> page = this.selectPage(
                new Query<InviteDepositEntity>(params).getPage(),
                new EntityWrapper<InviteDepositEntity>()
        );
        return new PageUtils(page);
    }

    @Override
    public InviteDepositChannelVo findByChannelId(String channelId) {
        return baseMapper.getInviteDepositByChannelId(channelId);
    }

    @Override
    public int saveInviteDepositCfg(String channelId, Integer openType, Long timeType, Long beginTime, Long endTime,
                                    Long firstTotalDeposit, Long selfReward, Long upperReward) {

        Long start = new Long(0);
        Long finish = new Long(0);

        if (timeType == 1) {
            start = new Long(beginTime) / 1000;
            finish = new Long(endTime) / 1000;
        }

        GsonResult result = HttpHandler.inviteDeposit(channelId, openType, timeType, start, finish, firstTotalDeposit, selfReward, upperReward);
        if (result != null) {
            return result.getRet();
        }
        return 1;
    }

    @Override
    public int channelOpenType(String channelId, String openType) {
        InviteDepositChannelVo model  = baseMapper.getInviteDepositByChannelId(channelId);
        if (model != null && StrUtil.isNotEmpty(model.getChannelId())) {

            Integer type = model.getDepositType() == 1 ? 0 : 1;
            GsonResult result = HttpHandler.inviteDeposit(channelId, type, model.getTimeType(), model.getBeginTime(), model.getEndTime(),
                    model.getFirstTotalDeposit(), model.getSelfReward(), model.getUpperReward());
            if (result != null) {
                return result.getRet();
            }
        }
        return 1;
    }

    @Override
    public PageUtils queryInviteDepositPage(Map<String, Object> params, ChlUserEntity userEntity) {
        String current = (String) params.get(ConfigConstant.LIMIT);
        String size = (String) params.get(ConfigConstant.PAGE);

        String channelId = ObjectUtil.isNotNull(userEntity.getChannelId()) ? userEntity.getChannelId() : "";
        String startTime = ObjectUtil.isNotNull(params.get("startTime")) ? (String) params.get("startTime") : "";
        String endTime = ObjectUtil.isNotNull(params.get("endTime")) ? (String) params.get("endTime") : "";

        Page<InviteDepositChannelVo> page = new Page<>(new Integer(size), new Integer(current));
        List<InviteDepositChannelVo> list = this.baseMapper.queryInviteDepositPage(page, channelId, startTime, endTime);
        return  new PageUtils(page.setRecords(list));
    }
}
