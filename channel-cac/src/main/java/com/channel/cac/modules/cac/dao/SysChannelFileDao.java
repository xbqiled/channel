package com.channel.cac.modules.cac.dao;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.channel.api.entity.cac.SysChannelFileEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 文件上传
 * 
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-04-05 16:04:25
 */
public interface SysChannelFileDao extends BaseMapper<SysChannelFileEntity> {

    List<SysChannelFileEntity> queryFilePage(Pagination page, @Param("channelId") String channelId);
	
}
