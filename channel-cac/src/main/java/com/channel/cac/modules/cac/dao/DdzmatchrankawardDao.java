package com.channel.cac.modules.cac.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.channel.api.entity.cac.DdzMatchAwardVo;
import com.channel.api.entity.cac.DdzmatchrankawardEntity;
import com.channel.api.entity.cac.EditnickawardVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * VIEW
 * 
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-09-17 20:44:19
 */
public interface DdzmatchrankawardDao extends BaseMapper<DdzmatchrankawardEntity> {

    List<DdzMatchAwardVo> queryDdzMatchAwardPage(Pagination page, @Param("channelId") String channelId, @Param("optionType") String optionType , @Param("startTime") String startTime, @Param("endTime") String endTime);

    DdzMatchAwardVo getDdzMatchAwardByChannelId(@Param("channelId") String channelId , @Param("optionType") String optionType);
	
}
