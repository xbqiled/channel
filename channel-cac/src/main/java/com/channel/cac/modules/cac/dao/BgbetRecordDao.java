package com.channel.cac.modules.cac.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.channel.api.entity.cac.BgbetRecordEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 
 * 
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-11-15 15:21:28
 */
public interface BgbetRecordDao extends BaseMapper<BgbetRecordEntity> {


    List<BgbetRecordEntity> queryBgBetRecordPage(Pagination page, @Param("tableSql") String tableSql, @Param("channelId") String channelId,
                                                       @Param("orderId") String orderId, @Param("userId") String userId,
                                                       @Param("issueId") String issueId, @Param("gameName") String gameName,
                                                       @Param("startTime") String startTime, @Param("endTime") String endTime);


    List<Map<String, Object>> queryStaticBgBetRecordPage(Pagination page, @Param("tableSql") String tableSql, @Param("channelId") String channelId,
                                                         @Param("startTime") String startTime, @Param("endTime") String endTime);


    List<String> queryHaveTab(@Param("tableSql") String tableSql);

}
