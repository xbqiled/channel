package com.channel.cac.modules.cac.service;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.channel.api.entity.cac.RelationEntity;
import com.channel.api.entity.chl.ChlUserEntity;
import com.channel.api.service.cac.RelationService;
import com.channel.cac.modules.cac.dao.RelationDao;
import com.channel.common.constant.ConfigConstant;
import com.channel.common.utils.PageUtils;
import com.channel.common.utils.Query;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;


@Service("relationService")
@com.alibaba.dubbo.config.annotation.Service(
        interfaceClass = RelationService.class,
        timeout = 10000,
        version = "${api.service.version}"
)
public class RelationServiceImpl extends ServiceImpl<RelationDao, RelationEntity> implements RelationService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        Page<RelationEntity> page = this.selectPage(
                new Query<RelationEntity>(params).getPage(),
                new EntityWrapper<RelationEntity>()
        );
        return new PageUtils(page);
    }

    @Override
    public PageUtils queryRelationPage(Map<String, Object> params, ChlUserEntity userEntity) {
        String current = (String)params.get(ConfigConstant.LIMIT);
        String size  = (String)params.get(ConfigConstant.PAGE);

        //根据拓展Server获取渠道ID
        String channelId = ObjectUtil.isNotNull(userEntity.getChannelId()) ? userEntity.getChannelId().toString() : "";
        String accountId = ObjectUtil.isNotNull(params.get("accountId")) ? (String)params.get("accountId") : "";
        String promoterId = ObjectUtil.isNotNull(params.get("promoterId")) ? (String)params.get("promoterId") : "";

        Page<Map<String, Object>> page = new Page<>(new Integer(size), new Integer(current));
        List<Map<String, Object>>   list = this.baseMapper.queryRelationPage(page, channelId, accountId, promoterId);
        return  new PageUtils(page.setRecords(list));
    }


}
