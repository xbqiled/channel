package com.channel.cac.modules.chl.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.channel.api.entity.chl.ChlUserRoleEntity;

import java.util.List;

/**
 * 用户与角色对应关系
 *
 */
public interface ChlUserRoleDao extends BaseMapper<ChlUserRoleEntity> {
	
	/**
	 * 根据用户ID，获取角色ID列表
	 */
	List<Long> queryRoleIdList(Long userId);


	/**
	 * 根据角色ID数组，批量删除
	 */
	int deleteBatch(Long[] roleIds);
}
