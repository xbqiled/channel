package com.channel.cac.modules.cac.service;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.channel.api.entity.cac.UserExtEntity;
import com.channel.api.entity.chl.ChlUserEntity;
import com.channel.api.service.cac.UserExtService;
import com.channel.cac.modules.cac.dao.UserExtDao;
import org.springframework.stereotype.Service;


@Service("userExtService")
@com.alibaba.dubbo.config.annotation.Service(
        interfaceClass = UserExtService.class,
        timeout = 10000,
        version = "${api.service.version}"
)
public class UserExtServiceImpl extends ServiceImpl<UserExtDao, UserExtEntity> implements UserExtService {


    @Override
    public Boolean editNote(String note, String userId, ChlUserEntity userEntity) {
        //首先查詢是否有对应的数据信息
        Long count = baseMapper.queryUserByAccountId(userId);
        if (count > 0) {
            baseMapper.updateUserNote(note, userId);
        } else {
            UserExtEntity model = new UserExtEntity();
            model.setAccountid(Integer.valueOf(userId));
            model.setNote(note);
            this.insert(model);
        }
        return Boolean.TRUE;
    }
}
