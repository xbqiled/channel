package com.channel.cac;


import com.alibaba.dubbo.config.spring.context.annotation.EnableDubbo;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
//@ComponentScan("com.channel.adminservice.modules.*.service.impl")
@MapperScan(basePackages = {"com.channel.cac.modules.*.dao"})
@EnableDubbo
public class CacApplication {
    public static void main(String[] args) {
        SpringApplication.run(CacApplication.class, args);
    }
}
