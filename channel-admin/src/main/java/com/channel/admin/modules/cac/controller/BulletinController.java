package com.channel.admin.modules.cac.controller;

import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUtil;
import com.alibaba.dubbo.config.annotation.Reference;
import com.channel.admin.base.BaseController;
import com.channel.api.entity.cac.BulletinEntity;
import com.channel.api.entity.cac.BulletinVo;
import com.channel.api.service.cac.BulletinService;
import com.channel.common.annotation.ChlLog;
import com.channel.common.utils.PageUtils;
import com.channel.common.utils.R;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.*;

import java.util.Map;



/**
 * 
 *
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-03-06 16:05:02
 */
@RestController
@RequestMapping("cac/bulletin")
public class BulletinController extends BaseController {

    @Reference(version = "${api.service.version}", timeout = 10000, check = true)
    private BulletinService bulletinService;


    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("cac:bulletin:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = bulletinService.queryBulletinPage(params, getUser());
        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("cac:bulletin:info")
    public R info(@PathVariable("id") String id){
        try {
            BulletinEntity bulletin = bulletinService.selectById(id);
            String content = new String(bulletin.getTContent(), "UTF-8");

            R res = R.ok().put("bulletin", bulletin);
            res.put("content", content);
            res.put("start",  DatePattern.NORM_DATETIME_FORMAT.format(DateUtil.calendar(bulletin.getTStart())));
            res.put("finish", DatePattern.NORM_DATETIME_FORMAT.format(DateUtil.calendar(bulletin.getTFinish())));
            return res;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return  null;
    }

    /**
     * 保存
     */
    @ChlLog("保存公告信息")
    @RequestMapping("/save")
    @RequiresPermissions("cac:bulletin:save")
    public R save(@RequestBody BulletinVo bulletinVo){
        int result = bulletinService.addBulletin(bulletinVo, getUser());

        if (result == 0) {
            return R.ok();
        } else if (result == 1) {
            return R.error("公告发送失败!");
        } else {
            return R.error("公告发送失败!");
        }
    }

    /**
     * 修改
     */
    @ChlLog("修改公告信息")
    @RequestMapping("/update")
    @RequiresPermissions("cac:bulletin:update")
    public R update(@RequestBody BulletinVo bulletinVo){
        int result = bulletinService.modifyBulletin(bulletinVo, getUser());

        if (result == 0) {
            return R.ok();
        } else if (result == 1) {
            return R.error("公告修改失败!");
        } else {
            return R.error("公告修改失败!");
        }
    }

    /**
     * 删除
     */
    @ChlLog("删除公告信息")
    @RequestMapping("/delete")
    @RequiresPermissions("cac:bulletin:delete")
    public R delete(@RequestBody String [] ids){
        int result = bulletinService.delBulletin(ids);
        if (result == 0) {
            return R.ok();
        } else if (result == 1) {
            return R.error("公告删除失败!");
        } else {
            return R.error("公告删除失败!");
        }
    }
}
