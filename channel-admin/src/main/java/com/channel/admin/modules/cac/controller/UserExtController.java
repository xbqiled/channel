package com.channel.admin.modules.cac.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.channel.admin.base.BaseController;
import com.channel.admin.modules.cac.form.UserExtForm;
import com.channel.api.service.cac.UserExtService;
import com.channel.common.utils.R;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;



/**
 * 
 *
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-09-20 15:21:18
 */
@RestController
@RequestMapping("cac/userext")
public class UserExtController extends BaseController {

    @Reference(version = "${api.service.version}", timeout = 10000, check = true)
    private UserExtService userExtService;


    /**
     * 信息
     */
    @RequestMapping("/editNote")
    @RequiresPermissions("cac:userext:editnote")
    public R editNote(@RequestBody UserExtForm userExtForm){
        Boolean b  = userExtService.editNote(userExtForm.getNote(), userExtForm.getUserId(), getUser());
        if (b) {
            return R.ok();
        } else {
            return R.error();
        }
    }

}
