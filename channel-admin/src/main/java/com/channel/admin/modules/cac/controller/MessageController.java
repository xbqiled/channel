package com.channel.admin.modules.cac.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.channel.admin.base.BaseController;
import com.channel.api.entity.cac.MessageEntity;
import com.channel.api.service.cac.MessageService;
import com.channel.common.annotation.ChlLog;
import com.channel.common.utils.PageUtils;
import com.channel.common.utils.R;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.*;

import java.util.Map;


/**
 * @author Howard
 * @email howardwong2312@gmail.com
 * @date 2020-02-03 16:01:03
 */
@RestController
@RequestMapping("cac/msg")
public class MessageController extends BaseController {

    @Reference(version = "${api.service.version}", timeout = 10000, check = true)
    private MessageService messageService;


    /**
     *<B>邮件信息</B>
     * @param params
     * @return
     */
    @RequestMapping("/list")
    @RequiresPermissions("cac:msg:list")
    public R mailList(@RequestParam Map<String, Object> params) {
        PageUtils page = messageService.queryPage(params, getUser());
        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    public R userInfo(@PathVariable("id") Long Id) {
        return R.ok().put("result", messageService.queryById(Id));
    }


    /**
     * <B>消息推送</B>
     * @return
     */
    @ChlLog("消息推送")
    @RequestMapping("/send")
    @RequiresPermissions("cac:msg:send")
    public R saveMail(@RequestBody MessageEntity messageEntity){
        int result = messageService.send(messageEntity, getUser());
        if (result == 0) {
            return R.ok();
        } else if (result == 1) {
            return R.error("开始时间不可为空");
        } else if (result == 2) {
            return R.error("结束时间不可为空");
        } else if (result == 3) {
            return R.error("开始时间不可大于结束时间");
        } else {
            return R.error("消息推送失败!");
        }
    }

}
