package com.channel.admin.modules.cac.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.channel.admin.base.BaseController;
import com.channel.api.service.cac.RelationService;
import com.channel.common.utils.PageUtils;
import com.channel.common.utils.R;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;



/**
 * VIEW
 *
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-05-07 17:05:21
 */
@RestController
@RequestMapping("cac/relation")
public class RelationController extends BaseController {

    @Reference(version = "${api.service.version}", timeout = 10000, check = true)
    private RelationService relationService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("cac:relation:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = relationService.queryRelationPage(params, getUser());
        return R.ok().put("page", page);
    }
}
