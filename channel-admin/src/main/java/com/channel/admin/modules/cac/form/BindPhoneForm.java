package com.channel.admin.modules.cac.form;

import java.io.Serializable;


public class BindPhoneForm implements Serializable {

    private String userId;
    private String  phoneNum;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPhoneNum() {
        return phoneNum;
    }

    public void setPhoneNum(String phoneNum) {
        this.phoneNum = phoneNum;
    }
}
