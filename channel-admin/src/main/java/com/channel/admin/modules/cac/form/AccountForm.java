package com.channel.admin.modules.cac.form;


import java.io.Serializable;

public class AccountForm implements Serializable {

    private String tAccountid;

    private String status;

    private String password;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String gettAccountid() {
        return tAccountid;
    }

    public void settAccountid(String tAccountid) {
        this.tAccountid = tAccountid;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}



