package com.channel.admin.modules.cac.form;


import java.io.Serializable;
import java.util.List;



public class MailOrderForm implements Serializable {

    private  String batchId;
    private String id;
    private String title;
    private String content;

    private String isGroupMail;
    private String isTemplateMail;
    private String isTextMail;

    private String sendDate;
    private String sender;
    private List<String> items;

    private List<String> templateId;
    private String customParams;
    private String sendType;

    private List<String> productIDList;
    private String toUserList;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBatchId() {
        return batchId;
    }

    public void setBatchId(String batchId) {
        this.batchId = batchId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getIsGroupMail() {
        return isGroupMail;
    }

    public void setIsGroupMail(String isGroupMail) {
        this.isGroupMail = isGroupMail;
    }

    public String getIsTemplateMail() {
        return isTemplateMail;
    }

    public void setIsTemplateMail(String isTemplateMail) {
        this.isTemplateMail = isTemplateMail;
    }

    public String getIsTextMail() {
        return isTextMail;
    }

    public void setIsTextMail(String isTextMail) {
        this.isTextMail = isTextMail;
    }

    public String getSendDate() {
        return sendDate;
    }

    public void setSendDate(String sendDate) {
        this.sendDate = sendDate;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public List<String> getItems() {
        return items;
    }

    public void setItems(List<String> items) {
        this.items = items;
    }

    public List<String> getTemplateId() {
        return templateId;
    }

    public void setTemplateId(List<String> templateId) {
        this.templateId = templateId;
    }

    public String getCustomParams() {
        return customParams;
    }

    public void setCustomParams(String customParams) {
        this.customParams = customParams;
    }

    public String getSendType() {
        return sendType;
    }

    public void setSendType(String sendType) {
        this.sendType = sendType;
    }

    public List<String> getProductIDList() {
        return productIDList;
    }

    public void setProductIDList(List<String> productIDList) {
        this.productIDList = productIDList;
    }

    public String getToUserList() {
        return toUserList;
    }

    public void setToUserList(String toUserList) {
        this.toUserList = toUserList;
    }
}
