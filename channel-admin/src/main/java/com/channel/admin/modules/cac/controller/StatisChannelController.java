package com.channel.admin.modules.cac.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.channel.admin.base.BaseController;
import com.channel.api.service.cac.StatisChannelService;
import com.channel.common.utils.PageUtils;
import com.channel.common.utils.R;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;



/**
 * 
 *
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2020-02-28 14:18:36
 */
@RestController
@RequestMapping("cac/statischannel")
public class  StatisChannelController  extends BaseController {

    @Reference(version = "${api.service.version}", timeout = 50000, check = true)
    private StatisChannelService statisChannelService;


    @RequestMapping("/dayReport")
    @RequiresPermissions("report:dayreport:list")
    public R getStatisDayReportPage(@RequestParam Map<String, Object> params){
        PageUtils page = statisChannelService.getStatisDayReportPage(params, getUser());
        return R.ok().put("page", page);
    }


    @RequestMapping("/monthReport")
    @RequiresPermissions("report:monthreport:list")
    public R getStatisMonthReportPage(@RequestParam Map<String, Object> params){
        PageUtils page = statisChannelService.getStatisMonthReportPage(params, getUser());
        return R.ok().put("page", page);
    }
}
