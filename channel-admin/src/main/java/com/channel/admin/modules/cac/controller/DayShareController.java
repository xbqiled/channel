package com.channel.admin.modules.cac.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.channel.admin.base.BaseController;
import com.channel.admin.modules.cac.form.DayShareForm;
import com.channel.admin.modules.cac.form.OpenTypeForm;
import com.channel.api.entity.cac.ChannelEntity;
import com.channel.api.entity.cac.DayShareEntity;
import com.channel.api.service.cac.ChannelService;
import com.channel.api.service.cac.DayShareService;
import com.channel.common.annotation.ChlLog;
import com.channel.common.json.RequestDayShare;
import com.channel.common.utils.PageUtils;
import com.channel.common.utils.R;
import com.google.gson.Gson;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.*;

import java.util.Map;


/**
 * VIEW
 * @email ismyjuliet@gmail.com
 * @date 2019-12-15 16:02:59
 */
@RestController
@RequestMapping("cac/dayshare")
public class DayShareController extends BaseController {

    @Reference(version = "${api.service.version}", timeout = 10000, check = true)
    private DayShareService dayShareService;

    @Reference(version = "${api.service.version}", timeout = 10000, check = true)
    private ChannelService channelService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("cac:dayshare:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = dayShareService.queryDaySharePage(params, getUser());
        ChannelEntity entity =  channelService.findById(getUser());
        return R.ok().put("page", page).put("channel", entity);
    }


    @RequestMapping("/info/{channelId}")
    @RequiresPermissions("cac:dayshare:info")
    public R info(@PathVariable("channelId") String channelId){
        DayShareEntity dayShareEntity = dayShareService.findByChannelId(channelId);
        String jsonStr = new String(dayShareEntity.getTDayshareinfo());
        RequestDayShare requestDayShare  = new Gson().fromJson(jsonStr, RequestDayShare.class);

        if (requestDayShare != null) {
            return R.ok().put("dayShareCfg", requestDayShare);
        }
        return  null;
    }


    @ChlLog("每日分享修改状态")
    @RequestMapping("/channelOpenType")
    @RequiresPermissions("cac:dayshare:opentype")
    public R channelOpenType(@RequestBody OpenTypeForm openTypeForm){
        int result = dayShareService.channelOpenType(openTypeForm.getChannelId(), openTypeForm.getOpenType());
        if (result == 0) {
            return R.ok();
        } else {
            return R.error("修改每日分享状态失败!");
        }
    }


    @ChlLog("每日分享信息保存")
    @RequestMapping("/doConfig")
    @RequiresPermissions("cac:dayshare:config")
    public R doConfig(@RequestBody DayShareForm dayShareForm){
        int result = dayShareService.saveOrUpdateConfig(getUser(),
                dayShareForm.getOpenType(), dayShareForm.getDayAward(), dayShareForm.getAwardDayShareCnt(),
                dayShareForm.getDayShareList());
        if (result == 0) {
            return R.ok();
        } else {
            return R.error("配置每日分享失败!");
        }
    }

}
