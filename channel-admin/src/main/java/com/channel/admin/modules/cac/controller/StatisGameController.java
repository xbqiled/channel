package com.channel.admin.modules.cac.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.channel.admin.base.BaseController;
import com.channel.api.service.cac.StatisGameService;
import com.channel.common.utils.PageUtils;
import com.channel.common.utils.R;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;



/**
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2020-03-02 21:28:00
 */
@RestController
@RequestMapping("cac/statisgame")
public class StatisGameController extends BaseController {


    @Reference(version = "${api.service.version}", timeout = 50000, check = true)
    private StatisGameService statisGameService;

    /**
     * 列表
     */
    @RequestMapping("/gameList")
    @RequiresPermissions("cac:statisgame:list")
    public R statisGameList(@RequestParam Map<String, Object> params){
        PageUtils page = statisGameService.queryStatisGamePage(params, getUser());
        return R.ok().put("page", page);
    }

    @RequestMapping("/usergameList")
    @RequiresPermissions("cac:statisusergame:list")
    public R statisUserGameList(@RequestParam Map<String, Object> params){
        PageUtils page = statisGameService.queryStatisUserGamePage(params, getUser());
        return R.ok().put("page", page);
    }

    @RequestMapping("/allGame")
    public R gamelist() {
        List<Map<String, String>> list = statisGameService.queryGameList();
        return R.ok().put("gameList", list);
    }
}
