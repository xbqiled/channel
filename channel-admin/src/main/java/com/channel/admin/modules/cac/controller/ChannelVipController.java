package com.channel.admin.modules.cac.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.channel.admin.base.BaseController;
import com.channel.admin.modules.cac.form.OpenTypeForm;
import com.channel.admin.modules.cac.form.VipForm;
import com.channel.api.service.cac.ChannelVipService;
import com.channel.common.annotation.ChlLog;
import com.channel.common.json.RequestVip;
import com.channel.common.utils.PageUtils;
import com.channel.common.utils.R;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.*;

import java.util.Map;



/**
 * VIEW
 *
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-12-17 14:48:34
 */
@RestController
@RequestMapping("cac/channelvip")
public class ChannelVipController extends BaseController {

    @Reference(version = "${api.service.version}", timeout = 20000, check = true)
    private ChannelVipService channelVipService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("cac:vip:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = channelVipService.queryChannelVipPage(params, getUser());
        return R.ok().put("page", page);
    }

    /**
     * 信息
     */
    @RequestMapping("/info/{channelId}")
    @RequiresPermissions("cac:vip:info")
    public R info(@PathVariable("channelId") String channelId){
        RequestVip channelVip = channelVipService.findByChannelId(channelId);
        return R.ok().put("vip", channelVip);
    }

    @ChlLog("VIP修改状态")
    @RequestMapping("/channelOpenType")
    @RequiresPermissions("cac:vip:opentype")
    public R channelOpenType(@RequestBody OpenTypeForm openTypeForm){
        int result = channelVipService.channelOpenType(openTypeForm.getChannelId(), openTypeForm.getOpenType());
        if (result == 0) {
            return R.ok();
        } else {
            return R.error("修改VIP状态失败!");
        }
    }


    @ChlLog("VIP配置信息保存")
    @RequestMapping("/doConfig")
    @RequiresPermissions("cac:vip:config")
    public R doConfig(@RequestBody VipForm vipForm){
        int result = channelVipService.saveOrUpdateConfig(getUser(), vipForm.getOpenType(),
                vipForm.getVip1(), vipForm.getVip2(), vipForm.getVip3(),vipForm.getVip4(),
                vipForm.getVip5(), vipForm.getVip6(), vipForm.getVip7(), vipForm.getVip8(), vipForm.getVip9());
        if (result == 0) {
            return R.ok();
        } else {
            return R.error("配置VIP信息失败!");
        }
    }
}
