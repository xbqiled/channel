package com.channel.admin.modules.cac.controller;

import cn.hutool.core.bean.BeanUtil;
import com.alibaba.dubbo.config.annotation.Reference;
import com.channel.admin.base.BaseController;
import com.channel.api.entity.cac.BindMobileEntity;
import com.channel.api.entity.cac.BindVo;
import com.channel.api.entity.cac.ChannelEntity;
import com.channel.api.service.cac.BindMobileService;
import com.channel.api.service.cac.ChannelService;
import com.channel.common.annotation.ChlLog;
import com.channel.common.utils.PageUtils;
import com.channel.common.utils.R;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;



/**
 * 
 *
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-04-09 13:47:05
 */
@RestController
@RequestMapping("cac/bind")
public class BindController  extends BaseController {


    @Reference(version = "${api.service.version}", timeout = 10000, check = true)
    private BindMobileService bindMobileService;


    @Reference(version = "${api.service.version}", timeout = 10000, check = true)
    private ChannelService channelService;


    /**
     * 信息
     */
    @RequestMapping("/info")
    @RequiresPermissions("cac:bind:info")
    public R info() {
        BindMobileEntity bindMobileEntity = bindMobileService.getByChannelId(getUser());
        ChannelEntity channelEntity = channelService.findById(getUser());
        BindVo vo = new BindVo();

        BeanUtil.copyProperties(channelEntity, vo);
        vo.setGiveValue(bindMobileEntity.getGiveValue());
        vo.setExchange(bindMobileEntity.getExchange());

        vo.setInitCoin(bindMobileEntity.getInitCoin());
        vo.setRebate(bindMobileEntity.getRebate());
        vo.setMinExchange(bindMobileEntity.getMinExchange());

        vo.setVersionNo(bindMobileEntity.getVersionNo());
        vo.setDamaMulti(bindMobileEntity.getDamaMulti());
        vo.setRebateDamaMulti(bindMobileEntity.getRebateDamaMulti());

        vo.setPromoteType(bindMobileEntity.getPromoteType());
        vo.setExchangeCount(bindMobileEntity.getExchangeCount());
        vo.setResetDamaLeftCoin(bindMobileEntity.getResetDamaLeftCoin());
        return R.ok().put("bindMobile", vo);
    }

    /**
     * 修改
     */
    @ChlLog("配置参数信息")
    @RequestMapping("/saveOrUpdate")
    @RequiresPermissions("cac:bind:update")
    public R update(@RequestBody BindMobileEntity bindMobile){
        Boolean result = bindMobileService.saveOrUpdateBind(bindMobile, getUser());//全部更新
        if (result) {
            return R.ok();
        } else {
            return R.error("修改配置信息失败");
        }

    }
}
