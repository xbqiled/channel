package com.channel.admin.modules.cac.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.channel.api.entity.cac.SysPayPlatformEntity;
import com.channel.api.service.cac.SysPayPlatformService;
import com.channel.common.utils.PageUtils;
import com.channel.common.utils.R;
import com.channel.common.validator.ValidatorUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.Map;



/**
 * 
 *
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-02-24 19:27:36
 */
@RestController
@RequestMapping("cac/payplatform")
public class SysPayPlatformController {
    @Reference(version = "${api.service.version}", timeout = 10000, check = true)
    private SysPayPlatformService sysPayPlatformService;

    /**
     * 列表
     */
    @RequestMapping("/getPayPlatFormData")
    public R getPayPlatFormData() {
        List<Map<String, Object>> list = sysPayPlatformService.getPayPlatFormData();
        return R.ok().put("payPlatformList", list);
    }
}
