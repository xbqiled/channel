package com.channel.admin.modules.cac.form;

import java.io.Serializable;



public class AccountRealNameForm implements Serializable {

    private Long accountId;
    private String realName;

    public Long getAccountId() {
        return accountId;
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }
}
