package com.channel.admin.modules.cac.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.channel.admin.base.BaseController;
import com.channel.admin.modules.cac.form.UserCtrlForm;
import com.channel.api.entity.cac.UsergamedataEntity;
import com.channel.api.service.cac.UsergamedataService;
import com.channel.common.annotation.ChlLog;
import com.channel.common.utils.PageUtils;
import com.channel.common.utils.R;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;



/**
 *
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-04-10 12:50:44
 */
@RestController
@RequestMapping("cac/usergamedata")
public class UsergamedataController extends BaseController {

    @Reference(version = "${api.service.version}", timeout = 10000, check = true)
    private UsergamedataService usergamedataService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("cac:usergamedata:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = usergamedataService.queryUserCtrlPage(params, getUser());
        return R.ok().put("page", page);
    }

    /**
     * 查詢在綫用戶信息
     * @param params
     * @return
     */
    @RequestMapping("/onLinelist")
    @RequiresPermissions("cac:usergamedata:onLinelist")
    public R onLineUserlist(@RequestParam Map<String, Object> params){
        PageUtils page = usergamedataService.queryUserOnlinePage(params, getUser());
        return R.ok().put("page", page);
    }

    @ChlLog("取消玩家控制")
    @RequestMapping("/cancelCtrl")
    public R cancelCtrl(@RequestBody UserCtrlForm userCtrlForm) {
        int result = usergamedataService.cancelCtrl(userCtrlForm.getAccountId(), userCtrlForm.getRoomList());
        if (result == 0) {
            return R.ok();
        } else {
            return R.error("设置玩家控制失败!");
        }
    }

    /**
     * <B>获取可以控制的游戏树</B>
     * @return
     */
    @PostMapping("/getRoomTree")
    public R getRoomTree() {
        return R.ok().put("roomList", usergamedataService.getRoomTreeData());
    }

    /**
     * <B>获取全部游戏树</B>
     * @return
     */
    @PostMapping("/getAllRoomTree")
    public R getAllRoomTree() {
        return R.ok().put("roomList", usergamedataService.getAllRoomTreeData());
    }


    @RequestMapping("/roomList")
    public R roomList() {
        List<Map<String, Object>> roomList = usergamedataService.queryGameRoom();
        if (roomList != null && roomList.size() > 0) {
            for (int i = 0; i < roomList.size(); i++) {

                Integer key = (Integer)roomList.get(i).get("gameAtomTypeId");
                String gameKindName = (String)roomList.get(i).get("gameKindName");
                String phoneGameName = (String)roomList.get(i).get("phoneGameName");

                roomList.get(i).put("phoneGameName", gameKindName + "-" + phoneGameName);
                roomList.get(i).put("key", key);
                roomList.get(i).put("label", gameKindName + "-" + phoneGameName);
            }
        }
        return R.ok().put("roomList", roomList);
    }

    /**
     * 信息
     */
    @RequestMapping("/info/{tAccountid}")
    @RequiresPermissions("cac:usergamedata:info")
    public R info(@PathVariable("tAccountid") Integer tAccountid){
        UsergamedataEntity vUsergamedata = usergamedataService.selectById(tAccountid);
        return R.ok().put("vUsergamedata", vUsergamedata);
    }

    /**
     * 保存
     */
    @ChlLog("设置玩家控制")
    @RequestMapping("/set")
    @RequiresPermissions("cac:usergamedata:doSet")
    public R save(@RequestBody UserCtrlForm userCtrlForm){
        int result = usergamedataService.doSet(userCtrlForm.getAccountId(), userCtrlForm.getType(), userCtrlForm.getBetLimit(), userCtrlForm.getRate(), userCtrlForm.getRound(), userCtrlForm.getRoomList(), getUser());
        if (result == 0) {
            return R.ok();
        } else if (result == 1) {
            return R.error("用户ID不正确!");
        } else if (result == 2) {
            return R.error("设置玩家控制失败!");
        } else {
            return R.error("设置玩家控制失败!");
        }
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public R delete(@RequestBody UserCtrlForm userCtrlForm) {
        int result = usergamedataService.delCtrlData(userCtrlForm.getAccountId(), userCtrlForm.getRoomList());
        if (result == 0) {
            return R.ok();
        } else if (result == 2) {
            return R.error("删除玩家控制失败!");
        } else {
            return R.error("删除玩家控制失败!");
        }
    }
}
