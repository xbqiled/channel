package com.channel.admin.modules.cac.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.channel.admin.base.BaseController;
import com.channel.api.service.cac.AgentInfoService;
import com.channel.common.utils.PageUtils;
import com.channel.common.utils.R;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;


/**
 * VIEW
 *
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-05-21 15:14:50
 */
@RestController
@RequestMapping("cac/agentInfo")
public class AgentInfoController extends BaseController {

    @Reference(version = "${api.service.version}", timeout = 10000, check = true)
    private AgentInfoService agentInfoService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("cac:agentInfo:list")
    public R list(@RequestParam Integer accountId) {
        List agentlist = agentInfoService.getAgentInfoPage(accountId, getUser());
        return R.ok().put("agentlist", agentlist);
    }

    /**
     *
     * @param accountId
     * @return
     */
    @RequestMapping("/childNotelist")
    public R getChildNotelist(@RequestParam Integer accountId){
        List noteList = agentInfoService.getChildNoteList(accountId, getUser());
        return R.ok().put("noteList", noteList);
    }

    /**
     *
     * @param params
     * @return
     */
    @RequestMapping("/childPage")
    public R getChildlistPage(@RequestParam Map<String, Object> params) {
        PageUtils page = agentInfoService.getChildlistPage(params, getUser());
        return R.ok().put("page", page);
    }

}
