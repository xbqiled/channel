package com.channel.admin.modules.cac.form;

import com.channel.common.json.GsonVip;

import java.io.Serializable;


public class VipForm implements Serializable {
    private Integer openType;
    private GsonVip vip1;
    private GsonVip vip2;
    private GsonVip vip3;

    private GsonVip vip4;
    private GsonVip vip5;
    private GsonVip vip6;

    private GsonVip vip7;
    private GsonVip vip8;
    private GsonVip vip9;

    public GsonVip getVip1() {
        return vip1;
    }

    public void setVip1(GsonVip vip1) {
        this.vip1 = vip1;
    }

    public GsonVip getVip2() {
        return vip2;
    }

    public void setVip2(GsonVip vip2) {
        this.vip2 = vip2;
    }

    public GsonVip getVip3() {
        return vip3;
    }

    public void setVip3(GsonVip vip3) {
        this.vip3 = vip3;
    }

    public GsonVip getVip4() {
        return vip4;
    }

    public void setVip4(GsonVip vip4) {
        this.vip4 = vip4;
    }

    public GsonVip getVip5() {
        return vip5;
    }

    public void setVip5(GsonVip vip5) {
        this.vip5 = vip5;
    }

    public GsonVip getVip6() {
        return vip6;
    }

    public void setVip6(GsonVip vip6) {
        this.vip6 = vip6;
    }

    public GsonVip getVip7() {
        return vip7;
    }

    public void setVip7(GsonVip vip7) {
        this.vip7 = vip7;
    }

    public GsonVip getVip8() {
        return vip8;
    }

    public void setVip8(GsonVip vip8) {
        this.vip8 = vip8;
    }

    public GsonVip getVip9() {
        return vip9;
    }

    public void setVip9(GsonVip vip9) {
        this.vip9 = vip9;
    }

    public Integer getOpenType() {
        return openType;
    }

    public void setOpenType(Integer openType) {
        this.openType = openType;
    }
}
