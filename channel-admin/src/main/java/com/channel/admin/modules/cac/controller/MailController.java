package com.channel.admin.modules.cac.controller;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.dubbo.config.annotation.Reference;
import com.channel.admin.base.BaseController;
import com.channel.admin.modules.cac.form.MailForm;
import com.channel.api.entity.cac.UserMailEntity;
import com.channel.api.entity.cac.UserMailVo;
import com.channel.api.service.cac.UserMailService;
import com.channel.common.annotation.ChlLog;
import com.channel.common.json.GsonRewardList;
import com.channel.common.utils.PageUtils;
import com.channel.common.utils.R;
import com.google.gson.Gson;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.*;

import java.util.Map;



/**
 * 
 *
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-03-07 13:09:16
 */
@RestController
@RequestMapping("cac/mail")
public class MailController extends BaseController {

    @Reference(version = "${api.service.version}", timeout = 10000, check = true)
    private UserMailService userMailService;


    /**
     *<B>邮件信息</B>
     * @param params
     * @return
     */
    @RequestMapping("/maillist")
    @RequiresPermissions("cac:email:list")
    public R mailList(@RequestParam Map<String, Object> params) {
        PageUtils page = userMailService.queryUserMailPage(params, getUser());
        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/mailinfo")
    @RequiresPermissions("cac:email:info")
    public R userInfo(@RequestBody MailForm form) {
        UserMailEntity usermail = userMailService.findbyId(form.getId(), form.getUserId(), getUser());
        UserMailVo vo = new UserMailVo();
        vo.setContent(new String(usermail.getTContent()));

        vo.setTitle(usermail.getTTitle());
        if (StrUtil.isNotEmpty(usermail.getTRewardlist())) {
            GsonRewardList[] rewardList = new Gson().fromJson(usermail.getTRewardlist(), GsonRewardList[].class);

            if (rewardList != null && rewardList.length > 0 && rewardList[0].getNum() != null) {
                vo.setNum(rewardList[0].getNum() > 100 ? rewardList[0].getNum()/100 : rewardList[0].getNum());
            }
        }

        return R.ok().put("usermail", vo);
    }


    /**
     * <B>保存个人邮件</B>
     * @param userMailVo
     * @return
     */
    @ChlLog("发送邮件信息")
    @RequestMapping("/mailsend")
    @RequiresPermissions("cac:email:send")
    public R saveMail(@RequestBody UserMailVo userMailVo){
        int result = userMailService.sendMail(userMailVo, getUser());
        if (result == 0) {
            return R.ok();
        } else if (result == 1) {
            return R.error("用户ID不正确!");
        } else if (result == 2) {
            return R.error("发送邮件错误!");
        } else if (result == 3) {
            return R.error("用户ID不能为空!");
        } else {
            return R.error("发送邮件错误!");
        }
    }

}
