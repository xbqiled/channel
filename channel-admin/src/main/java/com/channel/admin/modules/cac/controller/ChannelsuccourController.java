package com.channel.admin.modules.cac.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.channel.admin.base.BaseController;
import com.channel.admin.modules.cac.form.OpenTypeForm;
import com.channel.admin.modules.cac.form.SuccourForm;
import com.channel.api.service.cac.ChannelsuccourService;
import com.channel.common.annotation.ChlLog;
import com.channel.common.json.GsonSuccour;
import com.channel.common.utils.PageUtils;
import com.channel.common.utils.R;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.*;

import java.util.Map;



/**
 * VIEW
 *
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-12-31 12:23:45
 */
@RestController
@RequestMapping("cac/channelsuccour")
public class ChannelsuccourController extends BaseController {

    @Reference(version = "${api.service.version}", timeout = 20000, check = true)
    private ChannelsuccourService channelsuccourService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("cac:succour:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = channelsuccourService.querySuccourPage(params, getUser());
        return R.ok().put("page", page);
    }

    /**
     * 信息g
     */
    @RequestMapping("/info/{channelId}")
    @RequiresPermissions("cac:succour:info")
    public R info(@PathVariable("channelId") String channelId){
        GsonSuccour gsonSuccour = channelsuccourService.findByChannelId(channelId);
        return R.ok().put("succour", gsonSuccour);
    }

    @ChlLog("救济金修改状态")
    @RequestMapping("/channelOpenType")
    @RequiresPermissions("cac:succour:opentype")
    public R channelOpenType(@RequestBody OpenTypeForm openTypeForm){
        int result = channelsuccourService.channelOpenType(openTypeForm.getChannelId(), openTypeForm.getOpenType());
        if (result == 0) {
            return R.ok();
        } else {
            return R.error("修改救济金配置状态失败!");
        }
    }

    @ChlLog("救济金配置信息保存")
    @RequestMapping("/doConfig")
    @RequiresPermissions("cac:succour:config")
    public R doConfig(@RequestBody SuccourForm succourForm){
        int result = channelsuccourService.saveOrUpdateConfig(getUser(), succourForm.getOpenType(), succourForm.getCoinLimit(),
                succourForm.getSuccourAward(), succourForm.getTotalSuccourCnt(), succourForm.getShareConditionCnt());
        if (result == 0) {
            return R.ok();
        } else {
            return R.error("配置救济金信息失败!");
        }
    }
}
