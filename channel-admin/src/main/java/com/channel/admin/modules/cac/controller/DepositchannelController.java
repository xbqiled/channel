package com.channel.admin.modules.cac.controller;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.dubbo.config.annotation.Reference;
import com.channel.admin.base.BaseController;
import com.channel.admin.modules.cac.form.OpenTypeForm;
import com.channel.admin.modules.cac.form.RebateConfigForm;
import com.channel.api.entity.cac.ChannelEntity;
import com.channel.api.entity.cac.DepositchannelEntity;
import com.channel.api.service.cac.ChannelService;
import com.channel.api.service.cac.DepositchannelService;
import com.channel.common.annotation.ChlLog;
import com.channel.common.constant.ConfigConstant;
import com.channel.common.json.GsonRechargeRebateCfg;
import com.channel.common.utils.PageUtils;
import com.channel.common.utils.R;
import com.google.gson.Gson;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.*;

import java.util.*;


/**
 * VIEW
 *
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-05-31 11:20:19
 */
@RestController
@RequestMapping("cac/deposit")
public class DepositchannelController extends BaseController {

    @Reference(version = "${api.service.version}", timeout = 10000, check = true)
    private DepositchannelService depositchannelService;

    @Reference(version = "${api.service.version}", timeout = 10000, check = true)
    private ChannelService channelService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("cac:deposit:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = depositchannelService.queryDepositPage(params, getUser());
        ChannelEntity entity =  channelService.findById(getUser());
        return R.ok().put("page", page).put("channel", entity);
    }

    /**
     * 信息
     */
    @RequestMapping("/info/{tChannel}")
    @RequiresPermissions("cac:deposit:info")
    public R info(@PathVariable("tChannel") String tChannel){
        DepositchannelEntity depositchannel = depositchannelService.findByChannelId(tChannel);
        String str = new String(depositchannel.getTCfginfo());
        List<GsonRechargeRebateCfg> rabateCfg = new ArrayList<>();

        if (StrUtil.isNotBlank(str)) {
            GsonRechargeRebateCfg[] array = new Gson().fromJson(str, GsonRechargeRebateCfg[].class);
            rabateCfg = Arrays.asList(array);
        }

        String startTime = DateUtil.format(new Date(depositchannel.getTStarttime() * 1000), ConfigConstant.FULL_DATE_TIME_FORMAT);
        String endTime = DateUtil.format(new Date(depositchannel.getTEndtime() * 1000), ConfigConstant.FULL_DATE_TIME_FORMAT);
        return R.ok().put("depositchannel", depositchannel).put("rabateCfg", rabateCfg).put("startTime", startTime).put("endTime", endTime);
    }


    @ChlLog("充值返利配置信息修改状态")
    @RequestMapping("/channelOpenType")
    @RequiresPermissions("cac:deposit:opentype")
    public R channelOpenType(@RequestBody OpenTypeForm openTypeForm){
        int result = depositchannelService.channelOpenType(openTypeForm.getChannelId(), openTypeForm.getOpenType());
        if (result == 0) {
            return R.ok();
        } else {
            return R.error("修改签到配置信息状态失败!");
        }
    }

    /**
     * 保存
     */
    @ChlLog("充值返利配置信息保存")
    @RequestMapping("/doConfig")
    @RequiresPermissions("cac:deposit:config")
    public R save(@RequestBody RebateConfigForm rebateConfigForm){
        if (StrUtil.isEmpty(rebateConfigForm.getBeginTime())) {
            return R.error("开始时间不能为空!");
        }

        if (StrUtil.isEmpty(rebateConfigForm.getEndTime())) {
            return R.error("结束时间不能为空!");
        }

        if (StrUtil.equals(rebateConfigForm.getBeginTime(), rebateConfigForm.getEndTime())) {
            return R.error("活动开始时间与活动结束时间不能相同!");
        }

        int result = depositchannelService.saveRechargeRebateCfg(getUser().getChannelId(), rebateConfigForm.getBeginTime(), rebateConfigForm.getEndTime(), 1, rebateConfigForm.getRabateCfg());
        if (result == 0) {
            return R.ok();
        } else {
            return R.error("配置充值返利失败!");
        }
    }
}
