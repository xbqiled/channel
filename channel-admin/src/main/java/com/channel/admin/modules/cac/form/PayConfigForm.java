package com.channel.admin.modules.cac.form;

import java.io.Serializable;

public class PayConfigForm implements Serializable {

    private String id;
    private String status;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
