package com.channel.admin.modules.cac.controller;

import cn.hutool.core.util.StrUtil;
import com.alibaba.dubbo.config.annotation.Reference;
import com.channel.admin.base.BaseController;
import com.channel.api.entity.cac.SysChannelFileEntity;
import com.channel.api.service.cac.SysChannelFileService;
import com.channel.api.service.cac.SysConfigService;
import com.channel.common.annotation.ChlLog;
import com.channel.common.constant.ConfigConstant;
import com.channel.common.execption.AppException;
import com.channel.common.oss.CloudStorageConfig;
import com.channel.common.oss.OSSFactory;
import com.channel.common.utils.PageUtils;
import com.channel.common.utils.R;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.Arrays;
import java.util.Date;
import java.util.Map;



/**
 * 文件上传
 *
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-04-05 16:04:25
 */
@RestController
@RequestMapping("cac/channelFile")
public class SysChannelFileController extends BaseController {

    @Reference(version = "${api.service.version}", timeout = 10000, check = true)
    private SysChannelFileService sysChannelFileService;

    @Reference(version = "${api.service.version}", timeout = 10000, check = true)
    private SysConfigService sysConfigService;


    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("cac:channelfile:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = sysChannelFileService.queryFilePage(params, getUser());
        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("cac:channelfile:info")
    public R info(@PathVariable("id") Long id){
        SysChannelFileEntity sysChannelFile = sysChannelFileService.selectById(id);
        return R.ok().put("sysChannelFile", sysChannelFile);
    }



    /**
     * 上传文件
     */
    @ChlLog("上传文件")
    @PostMapping("/upload")
    @RequiresPermissions("cac:channelfile:all")
    public R upload(@RequestParam("file") MultipartFile file) throws Exception {
        if (file.isEmpty()) {
            throw new AppException("上传文件不能为空");
        }

        SysChannelFileEntity sysChannelFile  = new SysChannelFileEntity();
        String url = "";
        try {
            //文件名
            String fileName = file.getOriginalFilename();
            String name=fileName.substring(0, fileName.lastIndexOf("."));
            //文件后缀
            String prefix=fileName.substring(fileName.lastIndexOf(".")+1);

            //上传文件
            String suffix = file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf("."));
            //返回文件的URL
            CloudStorageConfig config = sysConfigService.getConfigObject(ConfigConstant.CLOUD_STORAGE_CONFIG_KEY, CloudStorageConfig.class);
            url = OSSFactory.build(config).uploadSuffix(file.getBytes(), suffix);

            sysChannelFile.setFileName(fileName);
            sysChannelFile.setName(name);
            sysChannelFile.setSuffix(prefix);

            sysChannelFile.setType(prefix);
            sysChannelFile.setFileUrl(url);
            sysChannelFile.setCreateTime(new Date());

            if (getUser() != null && StrUtil.isNotBlank(getUser().getChannelId())) {
                sysChannelFile.setChannelId(getUser().getChannelId());
            }

            sysChannelFileService.insert(sysChannelFile);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return R.ok().put("url", url);
    }



    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("cac:channelfile:save")
    public R save(@RequestBody SysChannelFileEntity sysChannelFile){
        if (getUser() != null && StrUtil.isNotBlank(getUser().getChannelId())) {
            sysChannelFile.setChannelId(getUser().getChannelId());
        }

        sysChannelFileService.insert(sysChannelFile);
        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("cac:channelfile:delete")
    public R delete(@RequestBody Long[] ids){
        sysChannelFileService.deleteBatchIds(Arrays.asList(ids));
        return R.ok();
    }

}
