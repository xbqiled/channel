package com.channel.admin.modules.cac.form;

import com.channel.common.json.GsonDayShare;

import java.io.Serializable;
import java.util.List;


public class DayShareForm implements Serializable {

    private Integer openType;
    private Long dayAward;
    private Long awardDayShareCnt;
    private List<GsonDayShare> dayShareList;

    public Integer getOpenType() {
        return openType;
    }

    public void setOpenType(Integer openType) {
        this.openType = openType;
    }

    public Long getAwardDayShareCnt() {
        return awardDayShareCnt;
    }

    public void setAwardDayShareCnt(Long awardDayShareCnt) {
        this.awardDayShareCnt = awardDayShareCnt;
    }

    public Long getDayAward() {
        return dayAward;
    }

    public void setDayAward(Long dayAward) {
        this.dayAward = dayAward;
    }

    public List<GsonDayShare> getDayShareList() {
        return dayShareList;
    }

    public void setDayShareList(List<GsonDayShare> dayShareList) {
        this.dayShareList = dayShareList;
    }
}
