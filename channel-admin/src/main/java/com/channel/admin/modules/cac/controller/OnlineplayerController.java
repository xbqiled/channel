package com.channel.admin.modules.cac.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.channel.admin.base.BaseController;
import com.channel.api.entity.cac.OnlineplayerEntity;
import com.channel.api.service.cac.OnlineplayerService;
import com.channel.common.utils.PageUtils;
import com.channel.common.utils.R;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;



/**
 * VIEW
 *
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-05-21 15:14:50
 */
@RestController
@RequestMapping("cac/onlineplayer")
public class OnlineplayerController extends BaseController {

    @Reference(version = "${api.service.version}", timeout = 10000, check = true)
    private OnlineplayerService onlineplayerService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("cac:onlineplayer:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = onlineplayerService.queryOnLinePlayerPage(params, getUser());
        Long hallNum = onlineplayerService.queryHallNum(getUser());

        Long onlineNum = onlineplayerService.queryOnlineNum(getUser());
        Long gameNum = onlineNum - hallNum;
        return R.ok().put("page", page).put("hallNum", hallNum).put("onlineNum", onlineNum).put("gameNum", gameNum);
    }


    @RequestMapping("/chart")
    public R chart(@RequestParam Map<String, Object> params) {
        PageUtils page = onlineplayerService.queryLoginByTime(params, getUser());
        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{tAccountid}")
    @RequiresPermissions("cac:onlineplayer:info")
    public R info(@PathVariable("tAccountid") Integer tAccountid){
        OnlineplayerEntity onlineplayer = onlineplayerService.selectById(tAccountid);
        return R.ok().put("onlineplayer", onlineplayer);
    }
}
