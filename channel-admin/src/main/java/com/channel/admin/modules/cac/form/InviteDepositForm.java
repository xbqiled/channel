package com.channel.admin.modules.cac.form;

import java.io.Serializable;


public class InviteDepositForm implements Serializable {

    private String channel;
    private Integer openType;

    private Long timeType;
    private Long beginTime;
    private Long endTime;

    private Long firstTotalDeposit;
    private Long selfReward;
    private Long upperReward;

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public Integer getOpenType() {
        return openType;
    }

    public void setOpenType(Integer openType) {
        this.openType = openType;
    }

    public Long getTimeType() {
        return timeType;
    }

    public void setTimeType(Long timeType) {
        this.timeType = timeType;
    }

    public Long getBeginTime() {
        return beginTime;
    }

    public void setBeginTime(Long beginTime) {
        this.beginTime = beginTime;
    }

    public Long getEndTime() {
        return endTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    public Long getFirstTotalDeposit() {
        return firstTotalDeposit;
    }

    public void setFirstTotalDeposit(Long firstTotalDeposit) {
        this.firstTotalDeposit = firstTotalDeposit;
    }

    public Long getSelfReward() {
        return selfReward;
    }

    public void setSelfReward(Long selfReward) {
        this.selfReward = selfReward;
    }

    public Long getUpperReward() {
        return upperReward;
    }

    public void setUpperReward(Long upperReward) {
        this.upperReward = upperReward;
    }
}
