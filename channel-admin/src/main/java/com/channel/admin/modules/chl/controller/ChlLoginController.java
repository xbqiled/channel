package com.channel.admin.modules.chl.controller;


import com.alibaba.dubbo.config.annotation.Reference;
import com.channel.admin.base.BaseController;
import com.channel.admin.modules.chl.form.ChlLoginForm;
import com.channel.api.entity.chl.ChlUserEntity;
import com.channel.api.service.chl.ChlCaptchaService;
import com.channel.api.service.chl.ChlUserService;
import com.channel.api.service.chl.ChlUserTokenService;
import com.channel.api.service.chl.ChlWhiteIpService;
import com.channel.common.utils.R;
import com.google.code.kaptcha.Producer;
import org.apache.commons.io.IOUtils;
import org.apache.shiro.crypto.hash.Sha256Hash;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Map;

/**
 * <B>系统登录类</B>
 */
@RestController
public class ChlLoginController extends BaseController {

    @Reference(version = "${api.service.version}", timeout =10000, check = true)
    private ChlUserService chlUserService;

    @Reference(version = "${api.service.version}", timeout =10000, check = true)
    private ChlUserTokenService chlUserTokenService;

    @Reference(version = "${api.service.version}", timeout =10000, check = true)
    private ChlCaptchaService chlCaptchaService;

    @Reference(version = "${api.service.version}", timeout =10000, check = true)
    private ChlWhiteIpService chlWhiteIpService;

    @Autowired
    private Producer producer;

    /**
     * 验证码
     */
    @GetMapping("captcha.jpg")
    public void captcha(HttpServletResponse response, String uuid)throws ServletException, IOException {
        response.setHeader("Cache-Control", "no-store, no-cache");
        response.setContentType("image/jpeg");

        //获取code
        String code = producer.createText();
        //获取图片验证码
        boolean result = chlCaptchaService.getCaptcha(uuid, code);
        BufferedImage image = producer.createImage(code);
        if (result) {
            ServletOutputStream out = response.getOutputStream();
            ImageIO.write(image, "jpg", out);
            IOUtils.closeQuietly(out);
        }
    }

    /**
     * 登录
     */
    @RequestMapping(value = "/chl/login",  method = RequestMethod.POST)
    public Map<String, Object> login(@RequestBody ChlLoginForm form, HttpServletRequest request)throws IOException {
        boolean captcha = chlCaptchaService.validate(form.getUuid(), form.getCaptcha());
        if(!captcha){
            return R.error("验证码不正确");
        }

        //用户信息
        ChlUserEntity user = chlUserService.queryByUserName(form.getUsername());
        //账号不存在、密码错误
        if(user == null || !user.getPassword().equals(new Sha256Hash(form.getPassword(), user.getSalt()).toHex())) {
            return R.error("账号或密码不正确");
        }

        //账号锁定
        if(user.getStatus() == 0){
            return R.error("账号已被锁定,请联系管理员");
        }

        //String ip = IPUtils.getIpAddr(request);
        //ChlWhiteIpEntity ipEntity = chlWhiteIpService.queryByIp(ip, user.getUserId().toString());
//        //暂时先打上注释 方便测试
        //if (ipEntity != null) {
            //生成token，并保存到数据库
         R r = chlUserTokenService.createToken(user.getUserId());
         return r;
//        } else {
//            return R.error("ip不在白名单内");
//        }
    }



    /**
     * 退出
     */
    @PostMapping("/chl/logout")
    public R logout() {
        chlUserTokenService.logout(getUserId());
        return R.ok();
    }
}
