package com.channel.admin.modules.cac.form;

import com.channel.common.json.GsonPayValue;

import java.io.Serializable;

public class PayValueConfigForm implements Serializable {

   private Integer payConfigId;

   private GsonPayValue [] payValueCfg;

    public Integer getPayConfigId() {
        return payConfigId;
    }

    public void setPayConfigId(Integer payConfigId) {
        this.payConfigId = payConfigId;
    }


    public GsonPayValue[] getPayValueCfg() {
        return payValueCfg;
    }

    public void setPayValueCfg(GsonPayValue[] payValueCfg) {
        this.payValueCfg = payValueCfg;
    }
}
