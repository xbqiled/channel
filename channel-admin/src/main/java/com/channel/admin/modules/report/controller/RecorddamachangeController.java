package com.channel.admin.modules.report.controller;

import cn.hutool.core.util.StrUtil;
import com.alibaba.dubbo.config.annotation.Reference;
import com.channel.admin.base.BaseController;
import com.channel.admin.modules.report.form.ChangeTradingForm;
import com.channel.api.service.report.RecorddamachangeService;
import com.channel.common.json.GsonChangeTrading;
import com.channel.common.utils.PageUtils;
import com.channel.common.utils.R;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;



/**
 * VIEW
 *
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-08-24 11:49:24
 */
@RestController
@RequestMapping("report/recorddamachange")
public class RecorddamachangeController extends BaseController {


    @Reference(version = "${api.service.version}", timeout = 30000, check = true)
    private RecorddamachangeService recorddamachangeService;

    /**
     * <B>查询会员打码量信息</B>
     * @param params
     * @return
     */
    @RequestMapping("/tradingList")
    @RequiresPermissions("report:recorddamachange:detaillist")
    public R queryTradinglist(@RequestParam Map<String, Object> params) {
        PageUtils page = recorddamachangeService.queryTradinglist(params, getUser());
        return R.ok().put("page", page);
    }



    @RequestMapping("/lossStaticlist")
    @RequiresPermissions("report:lossStatic:list")
    public R lossStaticlist(@RequestParam Map<String, Object> params) {
        PageUtils page = recorddamachangeService.queryStaticLoss(params, getUser());
        return R.ok().put("page", page);
    }


    /**
     * <B>查询会员打码量信息</B>
     * @param params
     * @return
     */
    @RequestMapping("/userTrading")
    @RequiresPermissions("report:recorddamachange:userTrading")
    public R getUserTrading(@RequestParam Map<String, Object> params) {
        PageUtils page = recorddamachangeService.getUserTrading(params, getUser());
        return R.ok().put("page", page);
    }

    /**
     * <B>修改会员打码量</B>
     * @param
     * @return
     */
    @RequestMapping("/changeUserTrading")
    @RequiresPermissions("report:recorddamachange:changeUserTrading")
    public R changeUserTrading(@RequestBody ChangeTradingForm changeTradingForm) {
        if (StrUtil.isEmpty(changeTradingForm.getUserId())) {
            return R.error("用户ID不能为空!");
        }

        if (changeTradingForm.getDamaChange() == 0) {
            return R.error("调整金额不能为空!");
        }

        if (changeTradingForm.getOpType() == 0) {
            return R.error("调整方式不能为空!");
        }

        GsonChangeTrading gsonChangeTrading = recorddamachangeService.changeTrading(changeTradingForm.getUserId(),  changeTradingForm.getOpType(), changeTradingForm.getDamaChange(), getUser());
        if (gsonChangeTrading != null && gsonChangeTrading.getRet() == 0) {
            return R.ok().put("channelTrading", gsonChangeTrading);
        } else {
            return R.error("调整打码量失败!");
        }
    }

}
