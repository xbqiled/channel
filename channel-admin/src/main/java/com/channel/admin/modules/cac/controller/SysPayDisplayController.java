package com.channel.admin.modules.cac.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.channel.api.entity.cac.SysPayDisplayEntity;
import com.channel.api.service.cac.SysPayDisplayService;
import com.channel.common.utils.PageUtils;
import com.channel.common.utils.R;
import com.channel.common.validator.ValidatorUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.Map;



/**
 * 
 *
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-10-06 17:15:45
 */
@RestController
@RequestMapping("cac/syspaydisplay")
public class SysPayDisplayController {

    @Reference(version = "${api.service.version}", timeout = 30000, check = true)
    private SysPayDisplayService sysPayDisplayService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("cac:syspaydisplay:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = sysPayDisplayService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("cac:syspaydisplay:info")
    public R info(@PathVariable("id") Integer id){
        SysPayDisplayEntity sysPayDisplay = sysPayDisplayService.selectById(id);
        return R.ok().put("sysPayDisplay", sysPayDisplay);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("cac:syspaydisplay:save")
    public R save(@RequestBody SysPayDisplayEntity sysPayDisplay){
        sysPayDisplayService.insert(sysPayDisplay);
        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("cac:syspaydisplay:update")
    public R update(@RequestBody SysPayDisplayEntity sysPayDisplay){
        ValidatorUtils.validateEntity(sysPayDisplay);
        sysPayDisplayService.updateAllColumnById(sysPayDisplay);//全部更新
        
        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("cac:syspaydisplay:delete")
    public R delete(@RequestBody Integer[] ids){
        sysPayDisplayService.deleteBatchIds(Arrays.asList(ids));

        return R.ok();
    }

}
