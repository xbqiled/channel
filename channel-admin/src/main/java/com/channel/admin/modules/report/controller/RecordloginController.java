package com.channel.admin.modules.report.controller;

import cn.hutool.core.collection.CollUtil;
import com.alibaba.dubbo.config.annotation.Reference;
import com.channel.admin.base.BaseController;
import com.channel.api.service.report.RecordloginplatformService;
import com.channel.common.utils.PageUtils;
import com.channel.common.utils.R;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



/**
 * VIEW
 *
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-02-25 19:23:00
 */
@RestController
@RequestMapping("report/recordlogin")
public class RecordloginController extends BaseController {

    @Reference(version = "${api.service.version}", timeout =20000, check = true)
    private RecordloginplatformService recordloginplatformService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("report:recordlogin:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = recordloginplatformService.queryLoginReport(params, getUser());
        return R.ok().put("page", page);
    }

    /**
     * <B>留存率统计</B>
     * @param params
     * @return
     */
    @RequestMapping("/retainList")
    @RequiresPermissions("report:retainRecord:list")
    public R retainList(@RequestParam Map<String, Object> params){
        List<Map<String, Object>> resultList = new ArrayList<>();
        List<String> timeList = new ArrayList<>();
        List<String> typeList = new ArrayList<>();
        typeList.add("留存率");

        PageUtils page  = recordloginplatformService.queryUserRetainList(params, getUser());

        if (CollUtil.isNotEmpty(page.getList())) {
            for (Object obj : page.getList()) {
                if (obj instanceof Map) {
                    Map<String, Object> result = (Map<String, Object>)obj;

                    timeList.add((String)result.get("regTime"));

                    Map map = new HashMap();
                    map.put("name", "留存率");
                    map.put("type", "line");
                    map.put("stack", "数量");
                    map.put("data", result.get("statis"));
                    resultList.add(map);
                }
            }
        }
        return R.ok().put("page", page).put("reportList", resultList).put("timeList", timeList).put("typeList", typeList);
    }
}
