package com.channel.admin.modules.report.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.channel.admin.base.BaseController;
import com.channel.api.service.report.RecorduserscorepergameService;
import com.channel.common.utils.PageUtils;
import com.channel.common.utils.R;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;



/**
 * VIEW
 *
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-09-21 11:05:04
 */
@RestController
@RequestMapping("report/recorduserscorepergame")
public class RecorduserscorepergameController extends BaseController {

    @Reference(version = "${api.service.version}", timeout =30000, check = true)
    private RecorduserscorepergameService recorduserscorepergameService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("report:gameRecord:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = recorduserscorepergameService.queryUserScoreperGame(params, getUser());
        return R.ok().put("page", page);
    }

    /**
     * 游戏下注统计列表
     * @param params
     * @return
     */
    @RequestMapping("/gameBetStatisList")
    @RequiresPermissions("report:gameBetStatisList:list")
    public R gameBetStatisList(@RequestParam Map<String, Object> params){
        PageUtils page = recorduserscorepergameService.queryGameBetPage(params, getUser());
        return R.ok().put("page", page);
    }

}
