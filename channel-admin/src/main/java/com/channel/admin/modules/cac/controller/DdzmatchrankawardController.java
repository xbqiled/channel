package com.channel.admin.modules.cac.controller;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.dubbo.config.annotation.Reference;
import com.channel.admin.base.BaseController;
import com.channel.admin.modules.cac.form.DdzMatchAwardForm;
import com.channel.admin.modules.cac.form.OpenTypeForm;
import com.channel.api.entity.cac.ChannelEntity;
import com.channel.api.entity.cac.DdzMatchAwardVo;
import com.channel.api.service.cac.ChannelService;
import com.channel.api.service.cac.DdzmatchrankawardService;
import com.channel.common.annotation.ChlLog;
import com.channel.common.json.GsonMatchRankAward;
import com.channel.common.json.GsonNickRward;
import com.channel.common.json.GsonResult;
import com.channel.common.utils.HttpHandler;
import com.channel.common.utils.PageUtils;
import com.channel.common.utils.R;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;



/**
 * VIEW
 *
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-09-17 20:44:19
 */
@RestController
@RequestMapping("match/ddzmatchrankaward")
public class DdzmatchrankawardController extends BaseController {

    @Reference(version = "${api.service.version}", timeout = 20000, check = true)
    private DdzmatchrankawardService ddzmatchrankawardService;

    @Reference(version = "${api.service.version}", timeout = 20000, check = true)
    private ChannelService channelService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("match:ddzRewardconfig:list")
    public R list(@RequestParam Map<String, Object> params){
        ChannelEntity entity =  channelService.findById(getUser());
        PageUtils page = ddzmatchrankawardService.queryDdzMatchAwardPage(params, getUser());
        return R.ok().put("page", page).put("channel", entity);
    }


    @RequestMapping("/info")
    @RequiresPermissions("match:ddzRewardconfig:info")
    public R info(@RequestParam Map<String, Object> params){
        String channelId = ObjectUtil.isNotNull(params.get("channelId")) ? (String) params.get("channelId") : "";
        String optionType = ObjectUtil.isNotNull(params.get("optionType")) ? (String) params.get("optionType") : "";

        List<GsonMatchRankAward> list = new ArrayList<>();
        DdzMatchAwardVo vo = ddzmatchrankawardService.findByChannelId(channelId, optionType);
        String cfg = new String(vo.getStrjsonaward());

        if (StrUtil.isNotEmpty(cfg)) {
            JsonParser parser = new JsonParser();
            JsonArray jsonArray = parser.parse(cfg).getAsJsonArray();

            for (JsonElement bean : jsonArray) {
                //使用GSON，直接转成Bean对象
                GsonMatchRankAward gsonMatchRankAward = new Gson().fromJson(bean, GsonMatchRankAward.class);
                list.add(gsonMatchRankAward);
            }
        }
        return R.ok().put("ddzMatchRewardVo", vo).put("rewardList", list);
    }



    @ChlLog("代理修改斗地主比赛奖励修改状态")
    @RequestMapping("/channelOpenType")
    @RequiresPermissions("match:ddzRewardconfig:opentype")
    public R channelOpenType(@RequestBody OpenTypeForm openTypeForm){
        int result = ddzmatchrankawardService.channelOpenType(openTypeForm.getChannelId(), openTypeForm.getOptionType(), openTypeForm.getOpenType());
        if (result == 0) {
            return R.ok();
        } else {
            return R.error("修改斗地主比赛奖励状态失败!");
        }
    }


    @ChlLog("配置斗地主比赛奖励信息保存")
    @RequestMapping("/doConfig")
    @RequiresPermissions("match:ddzRewardconfig:config")
    public R doConfig(@RequestBody DdzMatchAwardForm ddzMatchAward) {
        int result = ddzmatchrankawardService.saveDdzMatchAwardCfg(getUser().getChannelId(), ddzMatchAward.getOpenType(), ddzMatchAward.getOptionType(), ddzMatchAward.getAwards());
        if (result == 0) {
            return R.ok();
        } else {
            return R.error("配置斗地主比赛奖励失败!");
        }
    }

}
