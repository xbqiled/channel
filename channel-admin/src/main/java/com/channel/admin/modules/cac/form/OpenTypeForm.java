package com.channel.admin.modules.cac.form;

import java.io.Serializable;

public class OpenTypeForm implements Serializable {

    String channelId;

    String openType;

    String isCirculate;

    String optionType;

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public String getOpenType() {
        return openType;
    }

    public void setOpenType(String openType) {
        this.openType = openType;
    }

    public String getIsCirculate() {
        return isCirculate;
    }

    public void setIsCirculate(String isCirculate) {
        this.isCirculate = isCirculate;
    }


    public String getOptionType() {
        return optionType;
    }

    public void setOptionType(String optionType) {
        this.optionType = optionType;
    }
}
