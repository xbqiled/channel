package com.channel.admin.modules.chl.controller;


import com.alibaba.dubbo.config.annotation.Reference;
import com.channel.admin.base.BaseController;
import com.channel.api.service.chl.ChlLogService;
import com.channel.common.utils.PageUtils;
import com.channel.common.utils.R;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

/**
 * 系统日志
 */
@Controller
@RequestMapping("/chl/log")
public class ChlLogController extends BaseController {

    @Reference(version = "${api.service.version}", timeout =10000, check = true)
    private ChlLogService chlLogService;

    /**
     * 列表
     */
    @ResponseBody
    @GetMapping("/list")
    @RequiresPermissions("chl:log:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = chlLogService.queryLogPage(params, getUser());
        return R.ok().put("page", page);
    }

}
