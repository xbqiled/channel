package com.channel.admin.modules.cac.form;

import com.channel.common.json.GsonSignAwardCfg;

import java.util.List;

public class SignConfigForm {

    private List<GsonSignAwardCfg> signCfg;

    private String beginTime;

    private String endTime;

    private Integer isCirculate;

    private Integer num;

    public List<GsonSignAwardCfg> getSignCfg() {
        return signCfg;
    }

    public void setSignCfg(List<GsonSignAwardCfg> signCfg) {
        this.signCfg = signCfg;
    }

    public String getBeginTime() {
        return beginTime;
    }

    public void setBeginTime(String beginTime) {
        this.beginTime = beginTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public Integer getIsCirculate() {
        return isCirculate;
    }

    public void setIsCirculate(Integer isCirculate) {
        this.isCirculate = isCirculate;
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }
}
