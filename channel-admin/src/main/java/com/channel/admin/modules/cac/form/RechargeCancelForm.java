package com.channel.admin.modules.cac.form;

import java.io.Serializable;

public class RechargeCancelForm implements Serializable {

    private Integer orderId;
    private Integer force;
    private String note;

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public Integer getForce() {
        return force;
    }

    public void setForce(Integer force) {
        this.force = force;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
}
