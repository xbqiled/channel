package com.channel.admin.modules.report.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.channel.admin.base.BaseController;
import com.channel.api.service.report.PlayerenrollmentService;
import com.channel.common.utils.PageUtils;
import com.channel.common.utils.R;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;



/**
 * VIEW
 *
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-09-17 15:10:16
 */
@RestController
@RequestMapping("match/playerenrollment")
public class PlayerenrollmentController extends BaseController {


    @Reference(version = "${api.service.version}", timeout =40000, check = true)
    private PlayerenrollmentService playerenrollmentService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("match:playerenrollment:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = playerenrollmentService.queryPlayerEnrollmentPage(params, getUser());
        return R.ok().put("page", page);
    }

}
