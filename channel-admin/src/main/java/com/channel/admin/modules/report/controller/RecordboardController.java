package com.channel.admin.modules.report.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.channel.admin.base.BaseController;
import com.channel.api.service.report.DayReportService;
import com.channel.common.utils.PageUtils;
import com.channel.common.utils.R;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;



/**
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-02-24 14:04:07
 */
@RestController
@RequestMapping("report/recordboard")
public class RecordboardController extends BaseController {

    @Reference(version = "${api.service.version}", timeout =10000, check = true)
    private DayReportService dayReportService;


    /**
     * <B> 查询龙虎斗牌局</B>
     */
    @GetMapping("/lhboard")
    @RequiresPermissions("report:lhboard:list")
    public R getLhBoardPage(@RequestParam Map<String, Object> params) {
        PageUtils page = dayReportService.queryLhBoard(params, getUser());
        return  R.ok().put("page", page);
    }

    /**
     * <B>查询斗地主牌局</B>
     * @param params
     * @return
     */
    @GetMapping("/ddzboard")
    @RequiresPermissions("report:ddzboard:list")
    public R getDdzBoardPage(@RequestParam Map<String, Object> params) {
        PageUtils page = dayReportService.queryDdzBoard(params, getUser());
        return  R.ok().put("page", page);
    }

    @GetMapping("/ddzboardDetails")
    public R getDdzBoardDetails(@RequestParam Map<String, Object> params) {
        Map<String, Object> resultMap = dayReportService.analysisDdzBoard(params, getUser());
        return  R.ok().put("ddzboardDetails", resultMap);
    }

    /**
     * <B>查询跑得快牌局</B>
     * @param params
     * @return
     */
    @GetMapping("/pdkboard")
    @RequiresPermissions("report:pdkboard:list")
    public R getPdkBoardPage(@RequestParam Map<String, Object> params) {
        PageUtils page = dayReportService.queryPdkBoard(params, getUser());
        return  R.ok().put("page", page);
    }

    @GetMapping("/pdkboardDetails")
    public R getPdkBoardDetails(@RequestParam Map<String, Object> params) {
        Map<String, Object> resultMap = dayReportService.analysisPdkBoard(params, getUser());
        return  R.ok().put("pdkboardDetails", resultMap);
    }

    /**
     * <B>查询捕鱼牌局</B>
     * @param params
     * @return
     */
    @GetMapping("/byboard")
    @RequiresPermissions("report:byboard:list")
    public R getByBoardPage(@RequestParam Map<String, Object> params) {
        PageUtils page = dayReportService.queryByBoard(params, getUser());
        return  R.ok().put("page", page);
    }


    @GetMapping("/byboardDetails")
    public R getByBoardDetails(@RequestParam Map<String, Object> params) {
        Map<String, Object> resultMap = dayReportService.analysisByBoard(params, getUser());
        return  R.ok().put("byboardDetails", resultMap);
    }

    /**
     * <B>查询牛牛牌局</B>
     * @param params
     * @return
     */
    @GetMapping("/nnboard")
    @RequiresPermissions("report:nnboard:list")
    public R getNnBoardPage(@RequestParam Map<String, Object> params) {
        PageUtils page = dayReportService.queryNnBoard(params, getUser());
        return  R.ok().put("page", page);
    }


    @GetMapping("/nnboardDetails")
    public R getNnBoardDetails(@RequestParam Map<String, Object> params) {
        Map<String, Object> resultMap = dayReportService.analysisNnBoard(params, getUser());
        return  R.ok().put("nnboardDetails", resultMap);
    }

    /**
     * <B>查询炸金花牌局</B>
     * @param params
     * @return
     */
    @GetMapping("/zjhboard")
    @RequiresPermissions("report:zjhboard:list")
    public R getZjhBoardPage(@RequestParam Map<String, Object> params) {
        PageUtils page = dayReportService.queryZjhBoard(params, getUser());
        return  R.ok().put("page", page);
    }


    @GetMapping("/zjhboardDetails")
    public R getZjhBoardDetails(@RequestParam Map<String, Object> params) {
        Map<String, Object> resultMap = dayReportService.analysisZjhBoard(params, getUser());
        return  R.ok().put("zjhboardDetails", resultMap);
    }


    /**
     * <B>查询百家乐牌局</B>
     * @param params
     * @return
     */
    @GetMapping("/bjlboard")
    @RequiresPermissions("report:bjlboard:list")
    public R getBjlBoardPage(@RequestParam Map<String, Object> params) {
        PageUtils page = dayReportService.queryBjlBoard(params, getUser());
        return  R.ok().put("page", page);
    }


    @GetMapping("/bjlboardDetails")
    public R getBjlBoardDetails(@RequestParam Map<String, Object> params) {
        Map<String, Object> resultMap = dayReportService.analysisBjlBoard(params, getUser());
        return  R.ok().put("bjlboardDetails", resultMap);
    }


    /**
     * <B>查询比花色牌局</B>
     * @param params
     * @return
     */
    @GetMapping("/bhsboard")
    @RequiresPermissions("report:bhsboard:list")
    public R getBhsBoardPage(@RequestParam Map<String, Object> params) {
        PageUtils page = dayReportService.queryBhsBoard(params, getUser());
        return  R.ok().put("page", page);
    }


    @GetMapping("/bhsboardDetails")
    public R getBhsBoardDetails(@RequestParam Map<String, Object> params) {
        Map<String, Object> resultMap = dayReportService.analysisBhsBoard(params, getUser());
        return  R.ok().put("bhsboardDetails", resultMap);
    }


    /**
     * <B>查询财神到牌局</B>
     * @param params
     * @return
     */
    @GetMapping("/csdboard")
    @RequiresPermissions("report:csdboard:list")
    public R getCsdBoardPage(@RequestParam Map<String, Object> params) {
        PageUtils page = dayReportService.queryCsdBoard(params, getUser());
        return  R.ok().put("page", page);
    }


    @GetMapping("/boardDetail/{id}")
    public R queryCsdboardDetail(@PathVariable String id) {
        List<Map<String, Object>> detailList = dayReportService.analysisCsdBoard(id);
        return R.ok().put("detailList", detailList);
    }

    /**
     * <B>查询红黑大战牌局</B>
     * @param params
     * @return
     */
    @GetMapping("/hhdzboard")
    @RequiresPermissions("report:hhdzboard:list")
    public R getHhdzBoardPage(@RequestParam Map<String, Object> params) {
        PageUtils page = dayReportService.queryHhdzBoard(params, getUser());
        return  R.ok().put("page", page);
    }

    /**
     * <B>红黑大战机器人信息</B>
     * @param params
     * @return
     */
    @GetMapping("/hhboardDetail")
    public R queryHhBoardDetail(@RequestParam Map<String, Object> params) {
        Map<String, Object> detailList = dayReportService.analysisHhdzBoard(params, getUser());
        return R.ok().put("detailList", detailList);
    }

    /**
     * <B>查询红包接龙牌局</B>
     * @param params
     * @return
     */
    @GetMapping("/hbjlboard")
    @RequiresPermissions("report:hbjlboard:list")
    public R getHbjlBoardPage(@RequestParam Map<String, Object> params) {
        PageUtils page = dayReportService.queryHbjlBoard(params, getUser());
        return  R.ok().put("page", page);
    }

    /**
     * <B>查询抢庄牛牛牌局</B>
     * @param params
     * @return
     */
    @GetMapping("/qznnboard")
    @RequiresPermissions("report:qznnboard:list")
    public R getQznnBoardPage(@RequestParam Map<String, Object> params) {
        PageUtils page = dayReportService.queryQznnBoard(params, getUser());
        return  R.ok().put("page", page);
    }

    @GetMapping("/qznnboardDetails")
    public R getQznnBoardDetails(@RequestParam Map<String, Object> params) {
        Map<String, Object> resultMap = dayReportService.analysisQznnBoard(params, getUser());
        return  R.ok().put("qznnboardDetails", resultMap);
    }

    @GetMapping("/fqzsboard")
    @RequiresPermissions("report:fqzsboard:list")
    public R getFqzsBoardPage(@RequestParam Map<String, Object> params) {
        PageUtils page = dayReportService.queryFqzsBoard(params, getUser());
        return  R.ok().put("page", page);
    }

    @GetMapping("/fqzsboardDetails")
    public R getFqzsBoardDetails(@RequestParam Map<String, Object> params) {
        Map<String, Object> resultMap = dayReportService.analysisFqzsBoard(params, getUser());
        return  R.ok().put("fqzsboardDetails", resultMap);
    }

    @GetMapping("/bcbmboard")
    @RequiresPermissions("report:bcbmboard:list")
    public R getBcbmBoardPage(@RequestParam Map<String, Object> params) {
        PageUtils page = dayReportService.queryBcbmBoard(params, getUser());
        return  R.ok().put("page", page);
    }

    @GetMapping("/bcbmboardDetails")
    public R getBcbmBoardDetails(@RequestParam Map<String, Object> params) {
        Map<String, Object> resultMap = dayReportService.analysisBcbmBoard(params, getUser());
        return  R.ok().put("bcbmboardDetails", resultMap);
    }

    @GetMapping("/sgjboard")
    @RequiresPermissions("report:sgjboard:list")
    public R getSgjBoardPage(@RequestParam Map<String, Object> params) {
        PageUtils page = dayReportService.querySgjBoard(params, getUser());
        return  R.ok().put("page", page);
    }

    @GetMapping("/sgjboardDetails")
    public R getSgjBoardDetails(@RequestParam Map<String, Object> params) {
        Map<String, Object> resultMap = dayReportService.analysisSgjBoard(params, getUser());
        return  R.ok().put("sgjboardDetails", resultMap);
    }

    @GetMapping("/esydboard")
    @RequiresPermissions("report:esydboard:list")
    public R getEsydBoardPage(@RequestParam Map<String, Object> params) {
        PageUtils page = dayReportService.queryEsydBoard(params, getUser());
        return  R.ok().put("page", page);
    }

    @GetMapping("/esydboardDetails")
    public R getEsydBoardDetails(@RequestParam Map<String, Object> params) {
        Map<String, Object> resultMap = dayReportService.analysisEsydBoard(params, getUser());
        return  R.ok().put("esydboardDetails", resultMap);
    }
}
