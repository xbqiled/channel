package com.channel.admin.modules.cac.controller;

import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONUtil;
import com.alibaba.dubbo.config.annotation.Reference;
import com.channel.admin.base.BaseController;
import com.channel.admin.modules.cac.form.*;
import com.channel.api.entity.cac.AccountinfoEntity;
import com.channel.api.entity.cac.UserCashInfoVo;
import com.channel.api.service.cac.AccountinfoService;
import com.channel.common.annotation.ChlLog;
import com.channel.common.json.GsonCashAli;
import com.channel.common.json.GsonCashBank;
import com.channel.common.json.GsonResult;
import com.channel.common.utils.PageUtils;
import com.channel.common.utils.R;
import com.channel.common.validator.ValidatorUtils;
import com.google.gson.Gson;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.Map;



/**
 * 
 *
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-02-23 14:34:23
 */
@RestController
@RequestMapping("cac/accountinfo")
public class AccountinfoController extends BaseController {

    @Reference(version = "${api.service.version}", timeout = 30000, check = true)
    private AccountinfoService accountinfoService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("cac:accountinfo:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = accountinfoService.quereyAccountPage(params, getUser());
        return R.ok().put("page", page);
    }


    /**
     * 列表
     */
    @RequestMapping("/adminlist")
    @RequiresPermissions("cac:accountadmin:list")
    public R adminList(@RequestParam Map<String, Object> params){
        PageUtils page = accountinfoService.queryUserPhoneList(params, getUser());
        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{accountid}")
    @RequiresPermissions("cac:accountinfo:info")
    public R info(@PathVariable("accountid") Integer tAccountid){
        Map<String, Object> map = accountinfoService.getUserInfoById(tAccountid.toString());
        return R.ok().put("accountinfo", map);
    }

    /**
     * <B>通过注册IP查询</B>
     * @param params
     * @return
     */
    @RequestMapping("/ipList")
    @RequiresPermissions("cac:accountinfo:iplist")
    public R ipList(@RequestParam Map<String, Object> params){
        Map<String, Object> map = accountinfoService.queryIpAccountPage(params, getUser());
        return R.ok().put("iplist", map);
    }


    @RequestMapping("/ipDetailList")
    public R ipDetailList(@RequestParam Map<String, Object> params){
        PageUtils page = accountinfoService.quereyDetailIpListPage(params, getUser());
        return R.ok().put("ipDetailPage", page);
    }


    @RequestMapping("/getUserCashInfo/{accountid}")
    @RequiresPermissions("cac:accountinfo:info")
    public R getUserCashInfo(@PathVariable("accountid") Integer tAccountid){
        AccountinfoEntity accountinfo = accountinfoService.selectById(tAccountid.toString());
        UserCashInfoVo vo = new UserCashInfoVo();
        if (accountinfo != null && StrUtil.isNotBlank(accountinfo.gettBankinfo()) &&  JSONUtil.isJson(accountinfo.gettBankinfo())) {

            GsonCashBank gsonCashBank = new Gson().fromJson(accountinfo.gettBankinfo(), GsonCashBank.class);
            vo.setBankPayId(gsonCashBank.getPayId());
            vo.setBank(gsonCashBank.getBank());

            vo.setSubBank(gsonCashBank.getSubBank());
        }
        vo.setBankRealName(accountinfo.gettRealname());

        if (accountinfo != null && StrUtil.isNotBlank(accountinfo.gettAlipayinfo()) && JSONUtil.isJson(accountinfo.gettAlipayinfo())) {
            GsonCashAli ali = new Gson().fromJson(accountinfo.gettAlipayinfo(), GsonCashAli.class);

            vo.setAliPayId(ali.getPayId());
        }
        vo.setAliRealName(accountinfo.gettRealname());
        return R.ok().put("accountinfo", vo);
    }

    /**
     * 保存
     */
    @ChlLog("保存游戏账号")
    @RequestMapping("/save")
    @RequiresPermissions("chl:accountinfo:save")
    public R save(@RequestBody AccountinfoEntity tAccountinfo){
        accountinfoService.insert(tAccountinfo);
        return R.ok();
    }

    /**
     * 修改
     */
    @ChlLog("修改游戏账号")
    @RequestMapping("/update")
    @RequiresPermissions("chl:accountinfo:update")
    public R update(@RequestBody AccountinfoEntity tAccountinfo){
        ValidatorUtils.validateEntity(tAccountinfo);
        accountinfoService.updateAllColumnById(tAccountinfo);//全部更新
        return R.ok();
    }

    /**
     * 删除
     */
    @ChlLog("删除游戏账号")
    @RequestMapping("/delete")
    @RequiresPermissions("chl:accountinfo:delete")
    public R delete(@RequestBody Integer[] tAccountids){
        accountinfoService.deleteBatchIds(Arrays.asList(tAccountids));
        return R.ok();
    }

    /**
     * 修改状态信息
     * @param from
     * @return
     */
    @ChlLog("修改游戏账号状态")
    @RequestMapping("/updateStatus")
    @RequiresPermissions("cac:accountinfo:status")
    public R updateStatus(@RequestBody AccountForm from) {
       GsonResult result = accountinfoService.updateStatus(from.gettAccountid(), from.getStatus());
       if (result.getRet() == 0) {
           return R.ok();
       } else if (result.getRet() == -3) {
           return R.error("修改状态失败,用户不在线!");
       } else {
           return R.error("修改状态失败!");
       }
    }


    /**
     * <B>踢人</B>
     * @param userId
     * @return
     */
    @ChlLog("踢人")
    @RequestMapping("/kickUser")
    @RequiresPermissions("cac:accountinfo:kickuser")
    public R kickUser(String userId) {
        GsonResult result = accountinfoService.kickUser(Integer.parseInt(userId));
        if (result.getRet() == 0) {
            return R.ok();
        } else if (result.getRet() == -1) {
            return R.error("当前用户不在线!");
        } else {
            return R.error("踢人失败!");
        }
    }

    /**
     * <B>解绑手机号</B>
     * @param userId
     * @return
     */
    @ChlLog("解绑手机号码")
    @RequestMapping("/unbindPhoneNum")
    public R unbindPhoneNum(String userId) {
        GsonResult result = accountinfoService.unbindPhoneNum(Integer.parseInt(userId));
        if (result.getRet() == 0) {
            return R.ok();
        } else if (result.getRet() == -1) {
            return R.error("解除手机号码绑定失败!");
        } else {
            return R.error("解除手机号码绑定失败!");
        }
    }

    @ChlLog("绑定手机号码")
    @RequestMapping("/bindPhoneNum")
    public R bindPhoneNum(@RequestBody BindPhoneForm from) {
        GsonResult result = accountinfoService.bindPhoneNum(Integer.parseInt(from.getUserId()), from.getPhoneNum());
        if (result.getRet() == 0) {
            return R.ok();
        } else if (result.getRet() == -1) {
            return R.error("绑定手机号码绑定失败!");
        } else {
            return R.error("绑定手机号码绑定失败!");
        }
    }

    /**
     * <B>清空设备信息</B>
     * @param userId
     * @return
     */
    @ChlLog("清空设备信息")
    @RequestMapping("/clearCommonDev")
    public R clearCommonDev(String userId) {
        GsonResult result = accountinfoService.clearCommonDev(Integer.parseInt(userId));
        if (result.getRet() == 0) {
            return R.ok();
        } else if (result.getRet() == -1) {
            return R.error("清空设备信息失败!");
        } else {
            return R.error("清空设备信息失败!");
        }
    }

    /**
     * <B> 修改登录密码</B>
     * @param from
     * @return
     */
    @ChlLog("修改游戏账号登录密码")
    @RequestMapping("/modifyLoginPw")
    @RequiresPermissions("cac:accountinfo:modifyLoginPw")
    public R modifyLoginPw(@RequestBody AccountForm from) {
       int result = accountinfoService.modifyLoginPw(from.gettAccountid(), from.getPassword());
        if (result == 0) {
            return R.ok();
        } else if (result == 1) {
            return R.error("当前用户不在线!");
        } else {
            return R.error("修改登录密码失败!");
        }
    }



    @ChlLog("修改用户真实姓名")
    @RequestMapping("/modifyRealName")
    @RequiresPermissions("cac:accountinfo:modifyRealName")
    public R modifyRealName(@RequestBody AccountRealNameForm from) {
        GsonResult result = accountinfoService.modifyRealName(from.getAccountId(), from.getRealName());
        if (result.getRet() == 0) {
            return R.ok();
        } else {
            return R.error("修改真实姓名失败!");
        }
    }

    /**
     * <B>查看是否在线</B>
     * @param userId
     * @return
     */
    @ChlLog("查看游戏玩家是否在线")
    @RequestMapping("/isInGame")
    @RequiresPermissions("cac:accountinfo:isInGame")
    public R isInGame(String userId) {
        List<Map<String, Object>> inGameList = accountinfoService.isInGame(Integer.parseInt(userId));
        return R.ok().put("inGameList", inGameList);
    }


    @ChlLog("添加玩家上下级关系")
    @RequestMapping("/addRelation")
    public R addRelation(@RequestBody AccountRelationForm from) {
        GsonResult result = accountinfoService.addRelation(getUser().getChannelId(), from.getAccountId(), from.getPromoterId());
        if (result != null &&  result.getRet() == 0) {
            return R.ok();
        } else if (result != null &&  result.getRet() == -44) {
            return R.error("推广账户id不合法!");
        } else if (result != null &&  result.getRet() == -45) {
            return R.error("创建玩家关系节点失败!");
        } else if (result != null &&  result.getRet() == -46) {
            return R.error("玩家已存在推广人!");
        } else if (result != null &&  result.getRet() == -47) {
            return R.error("玩家与推广人渠道不一致!");
        } else if (result != null &&  result.getRet() == -48) {
            return R.error("玩家关系节点不存在!");
        } else if (result != null &&  result.getRet() == -49) {
            return R.error("推广人节点不存在!");
        } else if (result != null &&  result.getRet() == -50) {
            return R.error("参数不合法!");
        } else if (result != null &&  result.getRet() == -52) {
            return R.error("玩家关系节点已存在!");
        } else if (result != null &&  result.getRet() == -53) {
            return R.error("推广人玩家不存在!");
        }  else if (result != null &&  result.getRet() == -200) {
            return R.error("DB 错误!");
        } else {
            return R.error("未知错误!");
        }
    }


    @RequestMapping("/editNote")
    @RequiresPermissions("cac:userext:editnote")
    public R editNote(@RequestBody UserExtForm userExtForm) {
        int result = accountinfoService.updateUserNote(userExtForm.getUserId(), userExtForm.getNote(), getUser());
        if (result == 0) {
            return R.ok();
        } else {
            return R.error();
        }
    }
}
