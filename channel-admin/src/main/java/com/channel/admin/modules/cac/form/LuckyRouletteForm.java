package com.channel.admin.modules.cac.form;

import com.channel.common.json.GsonLuckRoulette;

import java.io.Serializable;
import java.util.List;


public class LuckyRouletteForm implements Serializable {

    private List<GsonLuckRoulette> diamondcfg;
    private List<GsonLuckRoulette> goldcfg;
    private List<GsonLuckRoulette> silvercfg;

    private String startTimeStr;
    private String finishTimeStr;

    private Integer openType;

    private Integer goldcost;
    private Integer diamondcost;
    private Integer silvercost;


    public List<GsonLuckRoulette> getDiamondcfg() {
        return diamondcfg;
    }

    public void setDiamondcfg(List<GsonLuckRoulette> diamondcfg) {
        this.diamondcfg = diamondcfg;
    }

    public List<GsonLuckRoulette> getGoldcfg() {
        return goldcfg;
    }

    public void setGoldcfg(List<GsonLuckRoulette> goldcfg) {
        this.goldcfg = goldcfg;
    }

    public List<GsonLuckRoulette> getSilvercfg() {
        return silvercfg;
    }

    public void setSilvercfg(List<GsonLuckRoulette> silvercfg) {
        this.silvercfg = silvercfg;
    }

    public String getStartTimeStr() {
        return startTimeStr;
    }

    public void setStartTimeStr(String startTimeStr) {
        this.startTimeStr = startTimeStr;
    }

    public String getFinishTimeStr() {
        return finishTimeStr;
    }

    public void setFinishTimeStr(String finishTimeStr) {
        this.finishTimeStr = finishTimeStr;
    }

    public Integer getGoldcost() {
        return goldcost;
    }

    public void setGoldcost(Integer goldcost) {
        this.goldcost = goldcost;
    }

    public Integer getDiamondcost() {
        return diamondcost;
    }

    public void setDiamondcost(Integer diamondcost) {
        this.diamondcost = diamondcost;
    }

    public Integer getSilvercost() {
        return silvercost;
    }

    public void setSilvercost(Integer silvercost) {
        this.silvercost = silvercost;
    }

    public Integer getOpenType() {
        return openType;
    }

    public void setOpenType(Integer openType) {
        this.openType = openType;
    }
}
