package com.channel.admin.modules.report.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.channel.admin.base.BaseController;
import com.channel.api.service.report.GameRebateService;
import com.channel.common.utils.PageUtils;
import com.channel.common.utils.R;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;



/**
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-03-30 19:46:22
 */
@RestController
@RequestMapping("report/gamerebate")
public class RecordgamerebateController extends BaseController {


    @Reference(version = "${api.service.version}", timeout =10000, check = true)
    private GameRebateService gameRebateService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("report:gamerebate:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = gameRebateService.queryGameRebatePage(params,  getUser());
        return R.ok().put("page", page);
    }

}
