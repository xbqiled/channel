package com.channel.admin.modules.cac.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.channel.admin.base.BaseController;
import com.channel.api.service.cac.BgRedPacketExchangeService;
import com.channel.common.utils.PageUtils;
import com.channel.common.utils.R;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;



/**
 * 
 *
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2020-01-14 14:39:56
 */
@RestController
@RequestMapping("cac/bgredpacket")
public class BgRedPacketRecordController extends BaseController {

    @Reference(version = "${api.service.version}", timeout = 20000, check = true)
    private BgRedPacketExchangeService bgRedPacketExchangeService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("cac:bgredpacket:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = bgRedPacketExchangeService.queryRedPacketExchangePage(params, getUser());
        return R.ok().put("page", page);
    }
}
