package com.channel.admin.modules.cac.controller;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.dubbo.config.annotation.Reference;
import com.channel.admin.base.BaseController;
import com.channel.admin.modules.cac.form.LuckyRouletteForm;
import com.channel.admin.modules.cac.form.OpenTypeForm;
import com.channel.api.entity.cac.LuckyRouletteTransmit;
import com.channel.api.entity.cac.LuckyrouletteEntity;
import com.channel.api.service.cac.LuckyrouletteService;
import com.channel.common.annotation.ChlLog;
import com.channel.common.constant.ConfigConstant;
import com.channel.common.json.GsonLuckRouletteForm;
import com.channel.common.utils.PageUtils;
import com.channel.common.utils.R;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.BeanUtils;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * VIEW
 *
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-04-22 16:07:30
 */
@RestController
@RequestMapping("cac/luckyroulette")
public class LuckyRouletteController extends BaseController {

    @Reference(version = "${api.service.version}", timeout = 10000, check = true)
    private LuckyrouletteService luckyrouletteService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("cac:luckyroulette:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = luckyrouletteService.queryLuckyRoulettePage(params, getUser());
        return R.ok().put("page", page);
    }

    @ChlLog("幸运轮盘修改状态")
    @RequestMapping("/channelOpenType")
    @RequiresPermissions("cac:luckyroulette:opentype")
    public R channelOpenType(@RequestBody OpenTypeForm openTypeForm){
        int result = luckyrouletteService.channelOpenType(openTypeForm.getChannelId(), openTypeForm.getOpenType());
        if (result == 0) {
            return R.ok();
        } else {
            return R.error("修改幸运轮盘配置信息状态失败!");
        }
    }

    /**
     * 信息
     */
    @RequestMapping("/getLuckRouletteInfo/{channelId}")
    @RequiresPermissions("cac:luckyroulette:info")
    public R getLuckRouletteInfo(@PathVariable("channelId") String channelId){
        LuckyrouletteEntity luckyroulette = luckyrouletteService.selectById(channelId);
        if (luckyroulette != null && StrUtil.isNotEmpty(luckyroulette.getTChannel())) {

            String diamondcfg = new String(luckyroulette.getTDiamondcfg());
            String goldcfg = new String(luckyroulette.getTGoldcfg());
            String silvercfg = new String(luckyroulette.getTSilvercfg());

            LuckyRouletteTransmit vo = new LuckyRouletteTransmit();
            BeanUtils.copyProperties(luckyroulette, vo);
            Long startTime = luckyroulette.getTStart();
            Long finishTime = luckyroulette.getTFinish();

            vo.setStartTimeStr(DateUtil.format(new Date(startTime * 1000), ConfigConstant.FULL_DATE_TIME_FORMAT));
            vo.setFinishTimeStr(DateUtil.format(new Date(finishTime * 1000), ConfigConstant.FULL_DATE_TIME_FORMAT));

            List<GsonLuckRouletteForm> diamondList = packUpdateLuckRoulette(diamondcfg);
            List<GsonLuckRouletteForm> goldList =  packUpdateLuckRoulette(goldcfg);
            List<GsonLuckRouletteForm> silverList =  packUpdateLuckRoulette(silvercfg);

            return R.ok().put("diamondList", diamondList).put("goldList", goldList).put("silverList", silverList).put("luckyroulette", vo);
        }
        return null;
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{channelId}")
    @RequiresPermissions("cac:luckyroulette:info")
    public R info(@PathVariable("channelId") String channelId){
        LuckyrouletteEntity luckyroulette = luckyrouletteService.selectById(channelId);
        if (luckyroulette != null && StrUtil.isNotEmpty(luckyroulette.getTChannel())) {

            String diamondcfg = new String(luckyroulette.getTDiamondcfg());
            String goldcfg = new String(luckyroulette.getTGoldcfg());
            String silvercfg = new String(luckyroulette.getTSilvercfg());

            LuckyRouletteTransmit vo = new LuckyRouletteTransmit();
            BeanUtils.copyProperties(luckyroulette, vo);
            Long startTime = luckyroulette.getTStart();
            Long finishTime = luckyroulette.getTFinish();

            vo.setStartTimeStr(DateUtil.format(new Date(startTime * 1000), ConfigConstant.FULL_DATE_TIME_FORMAT));
            vo.setFinishTimeStr(DateUtil.format(new Date(finishTime * 1000), ConfigConstant.FULL_DATE_TIME_FORMAT));

            List<GsonLuckRouletteForm> diamondList = packLuckRoulette(diamondcfg);
            List<GsonLuckRouletteForm> goldList =  packLuckRoulette(goldcfg);
            List<GsonLuckRouletteForm> silverList =  packLuckRoulette(silvercfg);

            return R.ok().put("diamondList", diamondList).put("goldList", goldList).put("silverList", silverList).put("luckyroulette", vo);
        }
        return null;
    }


    private List<GsonLuckRouletteForm> packUpdateLuckRoulette(String cfg) {
        List<GsonLuckRouletteForm> list = new ArrayList<>();
        JsonParser parser = new JsonParser();
        Gson gson = new Gson();

        if (StrUtil.isNotEmpty(cfg)) {
            JsonArray jsonArray = parser.parse(cfg).getAsJsonArray();
            for (JsonElement user : jsonArray) {
                //使用GSON，直接转成Bean对象

                GsonLuckRouletteForm gsonLuckRoulette = gson.fromJson(user, GsonLuckRouletteForm.class);
                String str = getResName(gsonLuckRoulette.getRes());
                gsonLuckRoulette.setName(str);

                gsonLuckRoulette.setValue(gsonLuckRoulette.getRate());
                list.add(gsonLuckRoulette);
            }
        }
        return list;
    }


    private List<GsonLuckRouletteForm> packLuckRoulette(String cfg) {
        List<GsonLuckRouletteForm> list = new ArrayList<>();
        JsonParser parser = new JsonParser();
        Gson gson = new Gson();

        if (StrUtil.isNotEmpty(cfg)) {
            JsonArray jsonArray = parser.parse(cfg).getAsJsonArray();
            for (JsonElement user : jsonArray) {
                //使用GSON，直接转成Bean对象

                GsonLuckRouletteForm gsonLuckRoulette = gson.fromJson(user, GsonLuckRouletteForm.class);
                String str = getResName(gsonLuckRoulette.getRes());
                gsonLuckRoulette.setName(str);

                gsonLuckRoulette.setValue(gsonLuckRoulette.getRate());
                list.add(gsonLuckRoulette);
            }
        }
        return list;
    }


    private String getResName (Integer res) {
        String resultStr = "";
        switch(res){
            case 1:
                resultStr = "1金币";
                break;

            case 2:
                resultStr = "2金币";
                break;

            case 3:
                resultStr = "红包";
                break;

            case 4:
                resultStr = "金色红包";
            break;

            case 5:
                resultStr = "钱包";
                break;

            case 6:
                resultStr = "钱袋";
                break;

            case 7:
                resultStr = "宝箱";
            break;
        }
        return  resultStr;
    }


    /**
     * 保存
     */
    @ChlLog("幸运轮盘配置")
    @RequestMapping("/doConfig")
    @RequiresPermissions("cac:luckyroulette:save")
    public R doConfig(@RequestBody LuckyRouletteForm luckyRouletteForm) {

        if (CollUtil.isEmpty(luckyRouletteForm.getDiamondcfg())) {
            return R.error("钻石轮盘配置不能为空!");
        }

        if (CollUtil.isEmpty(luckyRouletteForm.getGoldcfg())) {
            return R.error("黄金轮盘配置不能为空!");
        }

        if (CollUtil.isEmpty(luckyRouletteForm.getSilvercfg())) {
            return R.error("白银轮盘配置不能为空!");
        }

        if (StrUtil.isEmpty(luckyRouletteForm.getStartTimeStr())) {
            return R.error("活动开始时间不能为空!");
        }

        if (StrUtil.isEmpty(luckyRouletteForm.getFinishTimeStr())) {
            return R.error("活动结束时间不能为空!");
        }

        if (StrUtil.equals(luckyRouletteForm.getStartTimeStr(), luckyRouletteForm.getFinishTimeStr())) {
            return R.error("活动开始时间与活动结束时间不能相同!");
        }

        if (DateUtil.parse(luckyRouletteForm.getFinishTimeStr()).before(DateUtil.parse(luckyRouletteForm.getStartTimeStr()))) {
            return R.error("活动开始时间必须大于活动结束时间!");
        }

        //判断活动开始时间不能大于2038
        if (DateUtil.parse(luckyRouletteForm.getStartTimeStr()).after(DateUtil.parse("2037-12-31"))) {
            return R.error("活动开始时间必须小于2038年!");
        }

        if (DateUtil.parse(luckyRouletteForm.getFinishTimeStr()).after(DateUtil.parse("2037-12-31"))) {
            return R.error("活动结束时间必须小于2038年!");
        }

        if (luckyRouletteForm.getSilvercost().intValue() == 0) {
            return R.error("白银轮盘消耗积分不能为空!");
        }

        if (luckyRouletteForm.getGoldcost().intValue() == 0) {
            return R.error("黄金轮盘消耗积分不能为空!");
        }

        if (luckyRouletteForm.getDiamondcost().intValue() == 0) {
            return R.error("钻石轮盘消耗积分不能为空!");
        }

        int result = luckyrouletteService.saveLuckRoulette(getUser(), luckyRouletteForm.getOpenType(), luckyRouletteForm.getDiamondcfg(), luckyRouletteForm.getGoldcfg(),
                luckyRouletteForm.getSilvercfg(), luckyRouletteForm.getStartTimeStr(), luckyRouletteForm.getFinishTimeStr(), luckyRouletteForm.getGoldcost(),
                luckyRouletteForm.getDiamondcost(), luckyRouletteForm.getSilvercost());
        if (result == 0) {
            return R.ok();
        } else {
            return R.error("配置幸运轮盘失败!");
        }
    }
}
