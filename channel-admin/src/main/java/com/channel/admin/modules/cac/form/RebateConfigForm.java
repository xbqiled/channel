package com.channel.admin.modules.cac.form;

import com.channel.common.json.GsonRechargeRebateCfg;

import java.io.Serializable;
import java.util.List;




public class RebateConfigForm implements Serializable {

    private List<GsonRechargeRebateCfg> rabateCfg;

    private String beginTime;

    private String endTime;

    public List<GsonRechargeRebateCfg> getRabateCfg() {
        return rabateCfg;
    }

    public void setRabateCfg(List<GsonRechargeRebateCfg> rabateCfg) {
        this.rabateCfg = rabateCfg;
    }

    public String getBeginTime() {
        return beginTime;
    }

    public void setBeginTime(String beginTime) {
        this.beginTime = beginTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

}
