package com.channel.admin.modules.cac.form;

import java.io.Serializable;


public class NickAwardForm implements Serializable {

    private String channel;
    private Integer openType;
    private Integer reward;

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public Integer getOpenType() {
        return openType;
    }

    public void setOpenType(Integer openType) {
        this.openType = openType;
    }

    public Integer getReward() {
        return reward;
    }

    public void setReward(Integer reward) {
        this.reward = reward;
    }
}
