package com.channel.admin.modules.cac.form;

import java.io.Serializable;

public class RechargeForm implements Serializable {

    private String userId;
    private String payFee;
    private String depositor;

    private Double damaMulti;
    private String account;
    private String remark;
    private String payType;

    private Integer rate;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPayFee() {
        return payFee;
    }

    public void setPayFee(String payFee) {
        this.payFee = payFee;
    }

    public String getDepositor() {
        return depositor;
    }

    public void setDepositor(String depositor) {
        this.depositor = depositor;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getPayType() {
        return payType;
    }

    public void setPayType(String payType) {
        this.payType = payType;
    }

    public Double getDamaMulti() {
        return damaMulti;
    }

    public Integer getRate() {
        return rate;
    }

    public void setRate(Integer rate) {
        this.rate = rate;
    }

    public void setDamaMulti(Double damaMulti) {
        this.damaMulti = damaMulti;
    }
}
