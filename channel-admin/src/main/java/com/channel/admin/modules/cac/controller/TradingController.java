package com.channel.admin.modules.cac.controller;

import com.channel.admin.base.BaseController;
import com.channel.common.utils.R;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * <B>查询用户打码量信息</B>
 */

@RestController
@RequestMapping("cac/trading")
public class TradingController extends BaseController {

    /**
     * <B>查询用户打码量信息</B>
     * @return
     */
    @RequestMapping("/userTrading")
    public R getUserTrading() {
//        List<Map<String, Object>> list = sysPayPlatformService.getPayPlatFormData();
//        return R.ok().put("payPlatformList", list);
        return null;
    }

    /**
     * <B>查询用户打码量明细</B>
     * @return
     */
    @RequestMapping("/tradingRecord")
    public R getTradingRecord() {
//        List<Map<String, Object>> list = sysPayPlatformService.getPayPlatFormData();
//        return R.ok().put("payPlatformList", list);
        return null;
    }

    /**
     * <B>减少打码量</B>
     * @return
     */
    @RequestMapping("/reduceTrading")
    public R reduceTrading() {
//        List<Map<String, Object>> list = sysPayPlatformService.getPayPlatFormData();
//        return R.ok().put("payPlatformList", list);
        return null;
    }

    /**
     * <B>增加打码量</B>
     * @return
     */
    @RequestMapping("/addTrading")
    public R addTrading() {
//        List<Map<String, Object>> list = sysPayPlatformService.getPayPlatFormData();
//        return R.ok().put("payPlatformList", list);
        return null;
    }

}
