package com.channel.admin.modules.cac.form;

import java.io.Serializable;


public class SuccourForm implements Serializable {
    private Integer openType;
    private Long coinLimit;
    private Long succourAward;
    private Integer totalSuccourCnt;
    private Integer shareConditionCnt;

    public Integer getOpenType() {
        return openType;
    }

    public void setOpenType(Integer openType) {
        this.openType = openType;
    }

    public Long getCoinLimit() {
        return coinLimit;
    }

    public void setCoinLimit(Long coinLimit) {
        this.coinLimit = coinLimit;
    }

    public Long getSuccourAward() {
        return succourAward;
    }

    public void setSuccourAward(Long succourAward) {
        this.succourAward = succourAward;
    }

    public Integer getTotalSuccourCnt() {
        return totalSuccourCnt;
    }

    public void setTotalSuccourCnt(Integer totalSuccourCnt) {
        this.totalSuccourCnt = totalSuccourCnt;
    }

    public Integer getShareConditionCnt() {
        return shareConditionCnt;
    }

    public void setShareConditionCnt(Integer shareConditionCnt) {
        this.shareConditionCnt = shareConditionCnt;
    }
}
