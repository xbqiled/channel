package com.channel.admin.modules.cac.form;


import java.io.Serializable;


public class UserCtrlForm implements Serializable {

    private String accountId;

    private String type;

    private String betLimit;

    private String rate;

    private String round;

    private String roomList;

    private String betLimitStart;

    private String betLimitEnd;

    public String getBetLimitStart() {
        return betLimitStart;
    }

    public void setBetLimitStart(String betLimitStart) {
        this.betLimitStart = betLimitStart;
    }

    public String getBetLimitEnd() {
        return betLimitEnd;
    }

    public void setBetLimitEnd(String betLimitEnd) {
        this.betLimitEnd = betLimitEnd;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getBetLimit() {
        return betLimit;
    }

    public void setBetLimit(String betLimit) {
        this.betLimit = betLimit;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getRound() {
        return round;
    }

    public void setRound(String round) {
        this.round = round;
    }

    public String getRoomList() {
        return roomList;
    }

    public void setRoomList(String roomList) {
        this.roomList = roomList;
    }
}
