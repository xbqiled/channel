package com.channel.admin.modules.chl.controller;



import cn.hutool.core.util.StrUtil;
import com.alibaba.dubbo.config.annotation.Reference;
import com.channel.admin.base.BaseController;
import com.channel.api.entity.chl.ChlRoleEntity;
import com.channel.api.service.chl.ChlRoleMenuService;
import com.channel.api.service.chl.ChlRoleService;
import com.channel.common.annotation.ChlLog;
import com.channel.common.constant.Constant;
import com.channel.common.utils.PageUtils;
import com.channel.common.utils.R;
import com.channel.common.validator.ValidatorUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <B> 角色管理</B>
 */
@RestController
@RequestMapping("/chl/role")
public class ChlRoleController extends BaseController {

    @Reference(version = "${api.service.version}", timeout =10000, check = true)
    private ChlRoleService chlRoleService;

    @Reference(version = "${api.service.version}", timeout =10000, check = true)
    private ChlRoleMenuService chlRoleMenuService;

    /**
     * 角色列表
     */
    @GetMapping("/list")
    @RequiresPermissions("chl:role:list")
    public R list(@RequestParam Map<String, Object> params){
        //如果不是超级管理员，则只查询自己创建的角色列表
        if(getUserId() != Constant.SUPER_ADMIN){
            params.put("createUserId", getUserId());
        }

        if (StrUtil.isNotBlank(getUser().getChannelId())) {
            params.put("channelId", getUser().getChannelId());
        }

        PageUtils page = chlRoleService.queryPage(params);
        return R.ok().put("page", page);
    }

    /**
     * 角色列表
     */
    @GetMapping("/select")
    @RequiresPermissions("chl:role:select")
    public R select(){
        Map<String, Object> map = new HashMap<>();

        //如果不是超级管理员，则只查询自己所拥有的角色列表
        if(getUserId() != Constant.SUPER_ADMIN){
            map.put("create_user_id", getUserId());
        }

        if(getUser() != null && StrUtil.isNotBlank(getUser().getChannelId())) {
            map.put("channel_id", getUser().getChannelId());
        }

        List<ChlRoleEntity> list = chlRoleService.selectByMap(map);
        return R.ok().put("list", list);
    }

    /**
     * 角色信息
     */
    @GetMapping("/info/{roleId}")
    @RequiresPermissions("chl:role:info")
    public R info(@PathVariable("roleId") Long roleId){
        ChlRoleEntity role = chlRoleService.selectById(roleId);

        //查询角色对应的菜单
        List<Long> menuIdList = chlRoleMenuService.queryMenuIdList(roleId);
        role.setMenuIdList(menuIdList);
        return R.ok().put("role", role);
    }

    /**
     * 保存角色
     */
    @ChlLog("保存角色")
    @PostMapping("/save")
    @RequiresPermissions("chl:role:save")
    public R save(@RequestBody ChlRoleEntity role){
        ValidatorUtils.validateEntity(role);

        role.setCreateUserId(getUserId());
        //判断是否关联渠道
        if (getUser() != null && StrUtil.isNotBlank(getUser().getChannelId())) {
            role.setChannelId(getUser().getChannelId());
        }

        if (role.getMenuIdList() != null && role.getMenuIdList().size() > 0) {
            role.getMenuIdList().remove(new Long(-666666));
        }
        
        chlRoleService.save(role);
        return R.ok();
    }

    /**
     * 修改角色
     */
    @ChlLog("修改角色")
    @PostMapping("/update")
    @RequiresPermissions("chl:role:update")
    public R update(@RequestBody ChlRoleEntity role){
        ValidatorUtils.validateEntity(role);

        if (role.getMenuIdList() != null && role.getMenuIdList().size() > 0) {
            role.getMenuIdList().remove(new Long(-666666));
        }

        role.setCreateUserId(getUserId());
        //判断是否关联渠道
        if (getUser() != null && StrUtil.isNotBlank(getUser().getChannelId())) {
            role.setChannelId(getUser().getChannelId());
        }
        chlRoleService.update(role);
        return R.ok();
    }

    /**
     * 删除角色
     */
    @ChlLog("删除角色")
    @PostMapping("/delete")
    @RequiresPermissions("chl:role:delete")
    public R delete(@RequestBody Long[] roleIds){
        chlRoleService.deleteBatch(roleIds);
        return R.ok();
    }
}
