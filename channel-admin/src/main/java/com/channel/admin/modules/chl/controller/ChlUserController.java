package com.channel.admin.modules.chl.controller;


import cn.hutool.core.util.StrUtil;
import com.alibaba.dubbo.config.annotation.Reference;
import com.channel.admin.base.BaseController;
import com.channel.admin.modules.chl.form.PasswordForm;
import com.channel.api.entity.chl.ChlUserEntity;
import com.channel.api.service.chl.ChlUserRoleService;
import com.channel.api.service.chl.ChlUserService;
import com.channel.common.annotation.ChlLog;
import com.channel.common.constant.Constant;
import com.channel.common.utils.PageUtils;
import com.channel.common.utils.R;
import com.channel.common.validator.Assert;
import com.channel.common.validator.ValidatorUtils;
import com.channel.common.validator.group.AddGroup;
import org.apache.commons.lang.ArrayUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.crypto.hash.Sha256Hash;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;


@RestController
@RequestMapping("/chl/user")
public class ChlUserController extends BaseController {

    @Reference(version = "${api.service.version}", timeout =10000, check = true)
    private ChlUserService chlUserService;

    @Reference(version = "${api.service.version}", timeout =10000, check = true)
    private ChlUserRoleService chlUserRoleService;

    /**
     * 所有用户列表
     */
    @GetMapping("/list")
    @RequiresPermissions("chl:user:list")
    public R list(@RequestParam Map<String, Object> params){
        //只有超级管理员，才能查看所有管理员列表
        if(getUserId() != Constant.SUPER_ADMIN){
            params.put("createUserId", getUserId());
        }

        if (StrUtil.isNotBlank(getUser().getChannelId())) {
            params.put("channelId", getUser().getChannelId());
        }

        PageUtils page = chlUserService.queryPage(params);
        return R.ok().put("page", page);
    }

    /**
     * 获取登录的用户信息
     */
    @GetMapping("/info")
    public R info(){
        return R.ok().put("user", getUser());
    }

    /**
     * 修改登录用户密码
     */
    @ChlLog("修改密码")
    @PostMapping("/password")
    public R password(@RequestBody PasswordForm form){
        Assert.isBlank(form.getNewPassword(), "新密码不为能空");

        //旧密码与新密码不能相同
        if (form.getPassword().equals(form.getNewPassword())){
            return R.error("原密码与新密码不能相同");
        }

        //sha256加密
        String password = new Sha256Hash(form.getPassword(), getUser().getSalt()).toHex();
        //sha256加密
        String newPassword = new Sha256Hash(form.getNewPassword(), getUser().getSalt()).toHex();

        //更新密码
        boolean flag = chlUserService.updatePassword(getUserId(), password, newPassword);
        if(!flag){
            return R.error("原密码不正确");
        }

        return R.ok();
    }

    /**
     * 用户信息
     */
    @GetMapping("/info/{userId}")
    @RequiresPermissions("chl:user:info")
    public R info(@PathVariable("userId") Long userId){
        ChlUserEntity user = chlUserService.selectById(userId);

        //获取用户所属的角色列表
        List<Long> roleIdList = chlUserRoleService.queryRoleIdList(userId);
        user.setRoleIdList(roleIdList);
        return R.ok().put("user", user);
    }

    /**
     * 保存用户
     */
    @ChlLog("保存用户")
    @PostMapping("/save")
    @RequiresPermissions("chl:user:save")
    public R save(@RequestBody ChlUserEntity user){
        ValidatorUtils.validateEntity(user, AddGroup.class);
        //添加用户前  先判断用户名是否重复
        ChlUserEntity entity = chlUserService.queryByUserName(user.getUsername());
        if (entity == null) {
            user.setCreateUserId(getUserId());

            //判断是否是渠道总账号
            if (getUser() != null && StrUtil.isNotBlank(getUser().getChannelId())) {
                user.setChannelId(getUser().getChannelId());
                user.setType(1);
            }

            chlUserService.save(user);
            return R.ok();
        } else {
            return R.error("用户名已存在");
        }
    }

    /**
     * 修改用户
     */
    @ChlLog("修改用户")
    @PostMapping("/update")
    @RequiresPermissions("chl:user:update")
    public R update(@RequestBody ChlUserEntity user){
        //添加用户前  先判断用户名是否重复
        ChlUserEntity entity = chlUserService.queryByUserName(user.getUsername());
        if (entity == null || entity.getUserId() == user.getUserId()) {
            user.setCreateUserId(getUserId());

            //判断是否是渠道总账号
            if (getUser() != null && StrUtil.isNotBlank(getUser().getChannelId())) {
                user.setChannelId(getUser().getChannelId());
                user.setType(1);
            }
            chlUserService.update(user);
            return R.ok();
        } else {
            return R.error("用户名已存在");
        }
    }

    /**
     * 删除用户
     */
    @ChlLog("删除用户")
    @PostMapping("/delete")
    @RequiresPermissions("chl:user:delete")
    public R delete(@RequestBody Long[] userIds){
        if(ArrayUtils.contains(userIds, 1L)){
            return R.error("系统管理员不能删除");
        }

        if(ArrayUtils.contains(userIds, getUserId())){
            return R.error("当前用户不能删除");
        }

        chlUserService.deleteBatch(userIds);
        return R.ok();
    }
}
