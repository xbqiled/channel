package com.channel.admin.modules.report.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.channel.admin.base.BaseController;
import com.channel.api.service.cac.AccountinfoService;
import com.channel.api.service.report.DayReportService;
import com.channel.api.service.report.UserbetscorechangeService;
import com.channel.common.utils.PageUtils;
import com.channel.common.utils.R;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;



/**
 * VIEW
 *
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-02-24 14:04:07
 */
@RestController
@RequestMapping("report/statis")
public class DayreportController extends BaseController {

    @Reference(version = "${api.service.version}", timeout =10000, check = true)
    private DayReportService dayReportService;

    @Reference(version = "${api.service.version}", timeout = 10000, check = true)
    private AccountinfoService accountinfoService;

    @Reference(version = "${api.service.version}",  timeout =10000, check = true)
    private UserbetscorechangeService userbetscorechangeService;


    /**
     * 查询日报报表
     */
    @RequestMapping("/dayreportlist")
    @RequiresPermissions("report:dayreport:list")
    public R dayReportList(@RequestParam Map<String, Object> params){
        PageUtils page = dayReportService.queryDayReport(params, getUser());
        return R.ok().put("page", page);
    }

    /**
     * 查询月报报表
     */
    @RequestMapping("/monthreportlist")
    @RequiresPermissions("report:monthreport:list")
    public R monthReportList(@RequestParam Map<String, Object> params){
        PageUtils page = dayReportService.queryMonthReport(params, getUser());
        return R.ok().put("page", page);
    }

    /**
     * <B>查询账变信息牌局</B>
     * @param params
     * @return
     */
    @RequestMapping("/useraccountlist")
    @RequiresPermissions("report:useraccount:list")
    public R userAccountList(@RequestParam Map<String, Object> params){
        PageUtils page = dayReportService.queryUserAccount(params, getUser());
        return R.ok().put("page", page);
    }

    /**
     * <B>查询类型信息</B>
     * @return
     */
    @RequestMapping("/operatorlist")
    public R operatorlist() {
        List<Map<String, Object>> list = userbetscorechangeService.queryOperatorList();
        return R.ok().put("operatorlist", list);
    }

    /**
     * <B>财富排行榜信息</B>
     * @param params
     * @return
     */
    @RequestMapping("/wealthrank")
    @RequiresPermissions("report:wealthrank:list")
    public R wealthRankList(@RequestParam Map<String, Object> params){
        PageUtils page = dayReportService.queryWealthRank(params, getUser());
        return R.ok().put("page", page);
    }
}
