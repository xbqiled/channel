package com.channel.admin.modules.report.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.channel.admin.base.BaseController;
import com.channel.api.service.report.UserbetscorechangeService;
import com.channel.common.utils.PageUtils;
import com.channel.common.utils.R;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;



/**
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-04-24 19:09:09
 */
@RestController
@RequestMapping("report/scoreRecord")
public class ScorechangeController extends BaseController {

    @Reference(version = "${api.service.version}", timeout =10000, check = true)
    private UserbetscorechangeService userbetscorechangeService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("report:scorerecord:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = userbetscorechangeService.queryRecordPage(params, getUser());
        return R.ok().put("page", page);
    }
}
