package com.channel.admin.modules.cac.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.channel.admin.base.BaseController;
import com.channel.admin.modules.cac.form.BindUrlForm;
import com.channel.api.entity.cac.BindUrlEntity;
import com.channel.api.service.cac.BindUrlService;
import com.channel.common.annotation.ChlLog;
import com.channel.common.utils.PageUtils;
import com.channel.common.utils.R;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;



/**
 * VIEW
 *
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-12-06 18:42:05
 */
@RestController
@RequestMapping("cac/bindurl")
public class BindUrlController extends BaseController {

    @Reference(version = "${api.service.version}", timeout = 10000, check = true)
    private BindUrlService bindUrlService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("cac:bindurl:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = bindUrlService.queryBindUrlPage(params, getUser());
        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info")
    public R info(@RequestBody BindUrlForm bindUrlForm){
        BindUrlEntity result = bindUrlService.findByChannelId(bindUrlForm.getUrl(), bindUrlForm.getAccountId(), getUser());
        return R.ok().put("bindUrlEntity", result);
    }

    /**
     * 保存
     */
    @ChlLog("添加推广绑定域名配置")
    @RequestMapping("/save")
    @RequiresPermissions("cac:bindurl:add")
    public R save(@RequestBody BindUrlForm bindUrlForm){
        int result =  bindUrlService.saveEntity(bindUrlForm.getUrl(), bindUrlForm.getAccountId(), getUser());
        if (result == 0) {
            return R.ok();
        } else {
            return R.error(result, "配置推广绑定域名失败,检查域名是否重复");
        }
    }

    /**
     * 修改
     */
    @ChlLog("修改推广绑定域名配置")
    @RequestMapping("/update")
    @RequiresPermissions("cac:bindurl:edit")
    public R update(@RequestBody BindUrlForm bindUrlForm){
        int result = bindUrlService.updateEntity(bindUrlForm.getUrl(), bindUrlForm.getAccountId(), getUser());
        if (result == 0) {
            return R.ok();
        } else {
            return R.error(result, "配置推广绑定域名失败,检查域名是否重复");
        }
    }

    /**
     * 删除
     */
    @ChlLog("删除推广绑定域名配置")
    @RequestMapping("/delete")
    @RequiresPermissions("cac:bindurl:del")
    public R delete(@RequestBody BindUrlForm bindUrlForm){
        int result = bindUrlService.delEntity(bindUrlForm.getUrl(), bindUrlForm.getAccountId(), getUser());
        if (result == 0) {
            return R.ok();
        } else {
            return R.error(result, "删除推广绑定域名失败");
        }
    }
}
