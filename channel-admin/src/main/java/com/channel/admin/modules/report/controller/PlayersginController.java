package com.channel.admin.modules.report.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.channel.admin.base.BaseController;
import com.channel.api.service.report.PlayerdepositService;
import com.channel.api.service.report.PlayersigninService;
import com.channel.common.utils.PageUtils;
import com.channel.common.utils.R;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;


/**
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-05-29 13:35:09
 */
@RestController
@RequestMapping("report/playersgin")
public class PlayersginController extends BaseController {

    @Reference(version = "${api.service.version}", timeout =10000, check = true)
    private PlayersigninService playersigninService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("report:playersgin:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = playersigninService.querySginPage(params, getUser());
        return R.ok().put("page", page);
    }

}
