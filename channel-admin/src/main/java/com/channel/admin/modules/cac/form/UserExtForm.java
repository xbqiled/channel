package com.channel.admin.modules.cac.form;

import java.io.Serializable;

public class UserExtForm implements Serializable {

    String note;

    String userId;

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
