package com.channel.admin.modules.cac.controller;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.dubbo.config.annotation.Reference;
import com.channel.admin.base.BaseController;
import com.channel.admin.modules.cac.form.OpenTypeForm;
import com.channel.admin.modules.cac.form.SignConfigForm;
import com.channel.api.entity.cac.SigninchannelEntity;
import com.channel.api.service.cac.ChannelService;
import com.channel.api.service.cac.SignchannelService;
import com.channel.common.annotation.ChlLog;
import com.channel.common.constant.ConfigConstant;
import com.channel.common.json.GsonSignAwardCfg;
import com.channel.common.utils.PageUtils;
import com.channel.common.utils.R;
import com.google.gson.Gson;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.*;

import java.util.*;


/**
 * VIEW
 *
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-05-28 14:35:42
 */
@RestController
@RequestMapping("cac/signconfig")
public class SignConfigController  extends BaseController {

    @Reference(version = "${api.service.version}", timeout = 10000, check = true)
    private SignchannelService signinchannelService;

    @Reference(version = "${api.service.version}", timeout = 10000, check = true)
    private ChannelService channelService;
    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("cac:signconfig:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = signinchannelService.querySignCfgPage(params, getUser());
        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{tChannel}")
    @RequiresPermissions("cac:signinconfig:info")
    public R info(@PathVariable("tChannel") String tChannel){
        SigninchannelEntity signinchannel = signinchannelService.findByChannelId(tChannel);
        List<GsonSignAwardCfg> rabateCfg = new ArrayList<>();

        String str = new String(signinchannel.gettCfginfo());
        if (StrUtil.isNotBlank(str)) {
            GsonSignAwardCfg[] array = new Gson().fromJson(str, GsonSignAwardCfg[].class);
            rabateCfg = Arrays.asList(array);
        }

        String startTime = DateUtil.format(new Date(signinchannel.gettStarttime() * 1000), ConfigConstant.FULL_DATE_TIME_FORMAT);
        String endTime = DateUtil.format(new Date(signinchannel.gettEndtime() * 1000), ConfigConstant.FULL_DATE_TIME_FORMAT);
        return R.ok().put("signinchannel", signinchannel).put("rabateCfg", rabateCfg).put("startTime", startTime).put("endTime", endTime);
    }


    @ChlLog("签到配置信息修改状态")
    @RequestMapping("/channelOpenType")
    @RequiresPermissions("cac:signinconfig:opentype")
    public R channelOpenType(@RequestBody OpenTypeForm openTypeForm){
        int result = signinchannelService.channelOpenType(openTypeForm.getChannelId(), openTypeForm.getOpenType());
        if (result == 0) {
            return R.ok();
        } else {
            return R.error("修改签到配置信息状态失败!");
        }
    }

    @ChlLog("签到配置信息修改是否循环")
    @RequestMapping("/channelLoop")
    @RequiresPermissions("cac:signinconfig:opentype")
    public R channelLoop(@RequestBody OpenTypeForm openTypeForm){
        int result = signinchannelService.channelLoop(openTypeForm.getChannelId(), openTypeForm.getIsCirculate());
        if (result == 0) {
            return R.ok();
        } else {
            return R.error("修改签到配置信息状态失败!");
        }
    }

    /**
     * 保存
     */
    @RequestMapping("/doConfig")
    @RequiresPermissions("cac:signinconfig:config")
    public R save(@RequestBody SignConfigForm signConfigForm){
        if (StrUtil.isEmpty(signConfigForm.getBeginTime())) {
            return R.error("开始时间不能为空!");
        }

        if (StrUtil.isEmpty(signConfigForm.getEndTime())) {
            return R.error("结束时间不能为空!");
        }

        if (StrUtil.equals(signConfigForm.getBeginTime(), signConfigForm.getEndTime())) {
            return R.error("活动开始时间与活动结束时间不能相同!");
        }

        int result = signinchannelService.saveSignCfg(getUser().getChannelId(), signConfigForm.getBeginTime(), signConfigForm.getEndTime(), 1,
                signConfigForm.getIsCirculate(), signConfigForm.getNum(), signConfigForm.getSignCfg());
        if (result == 0) {
            return R.ok();
        } else {
            return R.error("配置充值返利失败!");
        }
    }
}
