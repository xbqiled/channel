package com.channel.admin.modules.cac.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.channel.admin.base.BaseController;
import com.channel.api.service.cac.StatisAccountService;
import com.channel.common.utils.PageUtils;
import com.channel.common.utils.R;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;



/**
 * 
 *
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-06-16 15:38:03
 */
@RestController
@RequestMapping("cac/statisaccount")
public class StatisAccountController extends BaseController {

    @Reference(version = "${api.service.version}", timeout = 50000, check = true)
    private StatisAccountService statisAccountService;

    /**
     * 列表
     */
    @RequestMapping("/realtimeList")
    @RequiresPermissions("cac:realtimeaccount:list")
    public R list(@RequestParam Map<String, Object> params){
        Map<String, Object> page = statisAccountService.queryStatisAccountData(params, getUser());
        return R.ok().put("page", page.get("page")).put("statis", page.get("statis"));
    }


    @RequestMapping("/statisList")
    @RequiresPermissions("cac:statisaccount:list")
    public R statisList(@RequestParam Map<String, Object> params){
        PageUtils page = statisAccountService.queryStatisAccountPage(params, getUser());
        return R.ok().put("page", page);
    }
}
