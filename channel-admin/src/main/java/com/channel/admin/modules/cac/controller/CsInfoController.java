package com.channel.admin.modules.cac.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.channel.admin.base.BaseController;
import com.channel.api.entity.cac.CsInfoEntity;
import com.channel.api.service.cac.CsInfoService;
import com.channel.common.annotation.ChlLog;
import com.channel.common.utils.PageUtils;
import com.channel.common.utils.R;
import com.channel.common.validator.ValidatorUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.Map;


/**
 * 
 *
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-03-29 17:46:03
 */
@RestController
@RequestMapping("cac/csinfo")
public class CsInfoController extends BaseController {

    @Reference(version = "${api.service.version}", timeout = 10000, check = true)
    private CsInfoService csInfoService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("cac:csinfo:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = csInfoService.queryCsInfoList(params, getUser());
        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("cac:csinfo:info")
    public R info(@PathVariable("id") Integer id){
        CsInfoEntity csInfo = csInfoService.selectById(id);
        return R.ok().put("csInfo", csInfo);
    }

    /**
     * 保存
     */
    @ChlLog("添加客服信息")
    @RequestMapping("/save")
    @RequiresPermissions("cac:csinfo:save")
    public R save(@RequestBody CsInfoEntity csInfo){
        csInfoService.saveCsInfo(csInfo, getUser());
        return R.ok();
    }

    /**
     * 修改
     */
    @ChlLog("修改客服信息")
    @RequestMapping("/update")
    @RequiresPermissions("cac:csinfo:update")
    public R update(@RequestBody CsInfoEntity csInfo){
        ValidatorUtils.validateEntity(csInfo);
        csInfoService.modifyCsInfo(csInfo, getUser());//全部更新
        return R.ok();
    }

    /**
     * 删除
     */
    @ChlLog("删除客服信息")
    @RequestMapping("/delete")
    @RequiresPermissions("cac:csinfo:delete")
    public R delete(@RequestBody Integer[] ids){
        csInfoService.deleteBatchIds(Arrays.asList(ids));
        return R.ok();
    }

}
