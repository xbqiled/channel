package com.channel.admin.modules.cac.form;


import java.io.Serializable;
import java.util.List;

/**
 * @author Howard
 * @email howardwong2312@gmail.com
 * @date 2020-02-03 16:01:03
 */
public class MsgForm implements Serializable {

    private Long accountId;
    private String title;
    private String startTime;
    private String endTime;

    public Long getAccountId() {
        return accountId;
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }
}
