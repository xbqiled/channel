package com.channel.admin.modules.cac.form;


import java.io.Serializable;

public class PayOrderApprovalForm implements Serializable {

    private String status;
    private String id;
    private String note;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
}
