package com.channel.admin.modules.report.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.channel.admin.base.BaseController;
import com.channel.api.service.report.RecordlogoutplatformService;
import com.channel.common.utils.PageUtils;
import com.channel.common.utils.R;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;



/**
 * VIEW
 *
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-02-25 19:18:37
 */
@RestController
@RequestMapping("report/recordlogout")
public class RecordlogoutController extends BaseController {

    @Reference(version = "${api.service.version}", timeout = 20000, check = true)
    private RecordlogoutplatformService recordlogoutplatformService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("report:recordlogout:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = recordlogoutplatformService.queryLogoutReport(params, getUser());
        return R.ok().put("page", page);
    }
}
