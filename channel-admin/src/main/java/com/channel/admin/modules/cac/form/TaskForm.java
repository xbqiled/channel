package com.channel.admin.modules.cac.form;

import com.channel.common.json.GsonTask;

import java.io.Serializable;


public class TaskForm implements Serializable {
    private Integer openType;

    private Integer activityTimeType;

    private Integer activityOpenType;

    private String beginTime;

    private String endTime;

    private GsonTask [] taskArr;

    public Integer getOpenType() {
        return openType;
    }

    public void setOpenType(Integer openType) {
        this.openType = openType;
    }

    public Integer getActivityTimeType() {
        return activityTimeType;
    }

    public void setActivityTimeType(Integer activityTimeType) {
        this.activityTimeType = activityTimeType;
    }

    public Integer getActivityOpenType() {
        return activityOpenType;
    }

    public void setActivityOpenType(Integer activityOpenType) {
        this.activityOpenType = activityOpenType;
    }

    public String getBeginTime() {
        return beginTime;
    }

    public void setBeginTime(String beginTime) {
        this.beginTime = beginTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public GsonTask[] getTaskArr() {
        return taskArr;
    }

    public void setTaskArr(GsonTask[] taskArr) {
        this.taskArr = taskArr;
    }
}
