package com.channel.admin.modules.cac.form;


import java.io.Serializable;

public class MailForm implements Serializable {

    private String id;

    private String userId;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
