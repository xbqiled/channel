package com.channel.admin.modules.cac.form;

import com.channel.common.json.GsonMatchRankAward;

import java.io.Serializable;
import java.util.List;



public class DdzMatchAwardForm implements Serializable {


    private List<GsonMatchRankAward> awards;

    private Integer openType;

    private Integer optionType;

    public List<GsonMatchRankAward> getAwards() {
        return awards;
    }

    public void setAwards(List<GsonMatchRankAward> awards) {
        this.awards = awards;
    }

    public Integer getOpenType() {
        return openType;
    }

    public void setOpenType(Integer openType) {
        this.openType = openType;
    }

    public Integer getOptionType() {
        return optionType;
    }

    public void setOptionType(Integer optionType) {
        this.optionType = optionType;
    }
}
