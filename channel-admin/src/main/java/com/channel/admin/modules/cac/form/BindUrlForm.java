package com.channel.admin.modules.cac.form;

import java.io.Serializable;



public class BindUrlForm implements Serializable {

    private String url;

    private String accountId;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }
}
