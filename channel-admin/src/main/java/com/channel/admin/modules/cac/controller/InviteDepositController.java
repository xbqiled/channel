package com.channel.admin.modules.cac.controller;

import cn.hutool.core.util.ObjectUtil;
import com.alibaba.dubbo.config.annotation.Reference;
import com.channel.admin.base.BaseController;
import com.channel.admin.modules.cac.form.InviteDepositForm;
import com.channel.admin.modules.cac.form.OpenTypeForm;
import com.channel.api.entity.cac.InviteDepositChannelVo;
import com.channel.api.service.cac.InviteDepositService;
import com.channel.common.annotation.ChlLog;
import com.channel.common.utils.PageUtils;
import com.channel.common.utils.R;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;



/**
 * VIEW
 *
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-07-31 12:40:41
 */
@RestController
@RequestMapping("cac/invitedeposit")
public class InviteDepositController  extends BaseController {

    @Reference(version = "${api.service.version}", timeout = 10000, check = true)
    private InviteDepositService inviteDepositService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    public R list(@RequestParam Map<String, Object> params) {
        PageUtils page = inviteDepositService.queryInviteDepositPage(params, getUser());
        return R.ok().put("page", page);
    }

    /**
     * 信息
     */
    @RequestMapping("/info")
    @RequiresPermissions("cac:invitedeposit:info")
    public R info(@RequestParam Map<String, Object> params){
        String channelId = ObjectUtil.isNotNull(params.get("channelId")) ? (String) params.get("channelId") : "";
        InviteDepositChannelVo inviteDeposit = inviteDepositService.findByChannelId(channelId);
        return R.ok().put("inviteDeposit", inviteDeposit);
    }


    @ChlLog("代理累计充值配置修改状态")
    @RequestMapping("/channelOpenType")
    @RequiresPermissions("cac:invitedeposit:opentype")
    public R channelOpenType(@RequestBody OpenTypeForm openTypeForm){
        int result = inviteDepositService.channelOpenType(openTypeForm.getChannelId(), openTypeForm.getOpenType());
        if (result == 0) {
            return R.ok();
        } else {
            return R.error("修改签到配置信息状态失败!");
        }
    }

    @ChlLog("代理累计充值配置信息保存")
    @RequestMapping("/doConfig")
    @RequiresPermissions("cac:invitedeposit:config")
    public R doConfig(@RequestBody InviteDepositForm inviteDepositForm) {
        if (inviteDepositForm.getTimeType() == 1) {
            if (inviteDepositForm.getBeginTime() == 0) {
                return R.error("开始时间不能为空!");
            }
            if (inviteDepositForm.getEndTime() == 0) {
                return R.error("结束时间不能为空!");
            }
            if (inviteDepositForm.getBeginTime() == inviteDepositForm.getEndTime()) {
                return R.error("活动开始时间与活动结束时间不能相同!");
            }
        }

        int result = inviteDepositService.saveInviteDepositCfg(getUser().getChannelId(), inviteDepositForm.getOpenType(), inviteDepositForm.getTimeType(), inviteDepositForm.getBeginTime(),
                inviteDepositForm.getEndTime(), inviteDepositForm.getFirstTotalDeposit(), inviteDepositForm.getSelfReward(), inviteDepositForm.getUpperReward());
        if (result == 0) {
            return R.ok();
        } else {
            return R.error("配置充值返利失败!");
        }
    }
}
