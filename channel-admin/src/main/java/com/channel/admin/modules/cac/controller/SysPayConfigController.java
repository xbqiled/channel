package com.channel.admin.modules.cac.controller;

import cn.hutool.core.bean.BeanUtil;
import com.alibaba.dubbo.config.annotation.Reference;
import com.channel.admin.base.BaseController;
import com.channel.admin.modules.cac.form.PayConfigForm;
import com.channel.admin.modules.cac.form.PayValueConfigForm;
import com.channel.api.entity.cac.SysPayConfigEntity;
import com.channel.api.entity.cac.SysPayConfigVo;
import com.channel.api.service.cac.SysPayConfigService;
import com.channel.api.service.cac.SysPayPlatformService;
import com.channel.common.annotation.ChlLog;
import com.channel.common.json.GsonPayValue;
import com.channel.common.utils.PageUtils;
import com.channel.common.utils.R;
import com.channel.common.validator.ValidatorUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.Map;



/**
 * 
 *
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-02-24 19:27:36
 */
@RestController
@RequestMapping("cac/payconfig")
public class SysPayConfigController extends BaseController {

    @Reference(version = "${api.service.version}", timeout = 10000, check = true)
    private SysPayConfigService sysPayConfigService;

    @Reference(version = "${api.service.version}", timeout = 10000, check = true)
    private SysPayPlatformService sysPayPlatformService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("cac:payconfig:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = sysPayConfigService.queryPayConfigPay(params, getUser());
        return R.ok().put("page", page);
    }

    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("cac:payconfig:info")
    public R info(@PathVariable("id") Long id){
        SysPayConfigEntity sysPayConfig = sysPayConfigService.selectById(id);
        SysPayConfigVo payConfigVo = new SysPayConfigVo();

        BeanUtil.copyProperties(sysPayConfig, payConfigVo);
        return R.ok().put("sysPayConfig", payConfigVo);
    }

    @RequestMapping("/payValueInfo/{id}")
    @RequiresPermissions("cac:payValueConfig:info")
    public R payValueConfig(@PathVariable("id") Long id){
        List<GsonPayValue> payValueList = sysPayConfigService.getPayValueConfig(id.toString());
        return R.ok().put("payValueList", payValueList);
    }

    /**
     * 保存
     */
    @ChlLog("保存支付配置信息")
    @RequestMapping("/save")
    @RequiresPermissions("cac:payconfig:save")
    public R save(@RequestBody SysPayConfigVo sysPayConfigVo){
        ValidatorUtils.validateEntity(sysPayConfigVo);
        Boolean result = sysPayConfigService.saveConfig(sysPayConfigVo, getUser());
        if (result) {
            return R.ok();
        } else {
            return R.error("添加入款配置失败!");
        }
    }

    /**
     * 修改
     */
    @ChlLog("修改支付配置信息")
    @RequestMapping("/update")
    @RequiresPermissions("cac:payconfig:update")
    public R update(@RequestBody SysPayConfigVo sysPayConfigVo) {
        ValidatorUtils.validateEntity(sysPayConfigVo);
        Boolean result = sysPayConfigService.updateConfig(sysPayConfigVo, getUser());
        if (result) {
            return R.ok();
        } else {
            return R.error("修改入款配置失败!");
        }
    }

    /**
     * 删除
     */
    @ChlLog("删除支付配置信息")
    @RequestMapping("/delete")
    @RequiresPermissions("cac:payconfig:delete")
    public R delete(@RequestBody Long[] ids){
        sysPayConfigService.deleteBatchIds(Arrays.asList(ids));
        return R.ok();
    }

    @ChlLog("支付配置信息修改状态")
    @RequestMapping("/modifyStatus")
    @RequiresPermissions("cac:payconfig:modifyStatus")
    public R modifyStatus(@RequestBody PayConfigForm payConfigForm){
        Boolean result = sysPayConfigService.updateConfigStatus(payConfigForm.getId(), payConfigForm.getStatus());
        if (result) {
            return R.ok();
        } else {
            return R.error("修改入款配置状态失败!");
        }
    }

    @ChlLog("配置固定支付信息")
    @RequestMapping("/payValueConfig")
    @RequiresPermissions("cac:payconfig:payValueConfig")
    public R payValueConfig(@RequestBody PayValueConfigForm payValueConfigForm){
        Boolean result = sysPayConfigService.updatePayValueConfig(payValueConfigForm.getPayConfigId(), payValueConfigForm.getPayValueCfg());
        if (result) {
            return R.ok();
        } else {
            return R.error("配置固定支付信息失败!");
        }
    }

    /**
     * <B>获取支付通道信息</B>
     * @return
     */
    @RequestMapping("/getPlatform")
    public R getPlatformList(){
        List<Map<String, Object>> platformList = sysPayPlatformService.getPayPlatFormData();
        return R.ok().put("platformList", platformList);
    }
}
