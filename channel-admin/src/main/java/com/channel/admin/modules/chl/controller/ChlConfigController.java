package com.channel.admin.modules.chl.controller;


import com.alibaba.dubbo.config.annotation.Reference;
import com.channel.admin.base.BaseController;
import com.channel.api.entity.chl.ChlConfigEntity;
import com.channel.api.service.chl.ChlConfigService;
import com.channel.common.annotation.ChlLog;
import com.channel.common.utils.PageUtils;
import com.channel.common.utils.R;
import com.channel.common.validator.ValidatorUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * 系统配置信息
 *
 */
@RestController
@RequestMapping("/chl/config")
public class ChlConfigController extends BaseController {

	@Reference(version = "${api.service.version}",  timeout =10000, check = true)
	private ChlConfigService chlConfigService;

	/**
	 * 所有配置列表
	 */
	@GetMapping("/list")
	@RequiresPermissions("chl:config:list")
	public R list(@RequestParam Map<String, Object> params){
		PageUtils page = chlConfigService.queryPage(params);
		return R.ok().put("page", page);
	}
	
	
	/**
	 * 配置信息
	 */
	@GetMapping("/info/{id}")
	@RequiresPermissions("chl:config:info")
	public R info(@PathVariable("id") Long id){
		ChlConfigEntity config = chlConfigService.selectById(id);
		return R.ok().put("config", config);
	}
	
	/**
	 * 保存配置
	 */
	@ChlLog("保存配置")
	@PostMapping("/save")
	@RequiresPermissions("chl:config:save")
	public R save(@RequestBody ChlConfigEntity config){
		ValidatorUtils.validateEntity(config);

		chlConfigService.save(config);
		return R.ok();
	}
	
	/**
	 * 修改配置
	 */
	@ChlLog("修改配置")
	@PostMapping("/update")
	@RequiresPermissions("chl:config:update")
	public R update(@RequestBody ChlConfigEntity config){
		ValidatorUtils.validateEntity(config);

		chlConfigService.update(config);
		return R.ok();
	}
	
	/**
	 * 删除配置
	 */
	@ChlLog("删除配置")
	@PostMapping("/delete")
	@RequiresPermissions("chl:config:delete")
	public R delete(@RequestBody Long[] ids){
		chlConfigService.deleteBatch(ids);
		return R.ok();
	}

}
