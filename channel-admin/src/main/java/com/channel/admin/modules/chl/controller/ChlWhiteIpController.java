package com.channel.admin.modules.chl.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.channel.admin.base.BaseController;
import com.channel.api.entity.chl.ChlWhiteIpEntity;
import com.channel.api.service.chl.ChlWhiteIpService;
import com.channel.common.annotation.ChlLog;
import com.channel.common.utils.PageUtils;
import com.channel.common.utils.R;
import com.channel.common.validator.ValidatorUtils;
import com.channel.common.validator.group.AddGroup;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.Map;



/**
 * 
 *
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2018-12-26 16:31:59
 */
@RestController
@RequestMapping("chl/chlwhiteip")
public class ChlWhiteIpController extends BaseController {

    @Reference(version = "${api.service.version}", timeout =10000, check = true)
    private ChlWhiteIpService chlWhiteIpService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("chl:whiteip:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = chlWhiteIpService.queryPage(params, getUser());
        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("chl:whiteip:info")
    public R info(@PathVariable("id") Long id){
        ChlWhiteIpEntity chlWhiteIp = chlWhiteIpService.selectById(id);
        return R.ok().put("chlWhiteIp", chlWhiteIp);
    }

    /**
     * 保存
     */
    @ChlLog("保存白名单")
    @RequestMapping("/save")
    @RequiresPermissions("chl:whiteip:save")
    public R save(@RequestBody ChlWhiteIpEntity chlWhiteIp){
        ValidatorUtils.validateEntity(chlWhiteIp, AddGroup.class);
        ChlWhiteIpEntity entity = chlWhiteIpService.queryByIp(chlWhiteIp.getIp(), getUserId().toString());
        if (entity == null) {
            chlWhiteIpService.save(chlWhiteIp, getUser());
            return R.ok();
        } else {
            return R.error("ip已存在");
        }
    }

    /**
     * 修改
     */
    @ChlLog("修改白名单")
    @RequestMapping("/update")
    @RequiresPermissions("chl:whiteip:update")
    public R update(@RequestBody ChlWhiteIpEntity chlWhiteIp){
        ValidatorUtils.validateEntity(chlWhiteIp);
        chlWhiteIpService.updateById(chlWhiteIp);//全部更新
        return R.ok();
    }

    /**
     * 删除
     */
    @ChlLog("删除白名单")
    @RequestMapping("/delete")
    @RequiresPermissions("chl:whiteip:delete")
    public R delete(@RequestBody Long[] ids){
        chlWhiteIpService.deleteBatchIds(Arrays.asList(ids));
        return R.ok();
    }

}
