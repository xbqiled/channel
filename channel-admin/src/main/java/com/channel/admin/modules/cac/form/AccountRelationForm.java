package com.channel.admin.modules.cac.form;

import java.io.Serializable;



public class AccountRelationForm implements Serializable {

    private Long accountId;
    private Long promoterId;

    public Long getAccountId() {
        return accountId;
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }

    public Long getPromoterId() {
        return promoterId;
    }

    public void setPromoterId(Long promoterId) {
        this.promoterId = promoterId;
    }
}
