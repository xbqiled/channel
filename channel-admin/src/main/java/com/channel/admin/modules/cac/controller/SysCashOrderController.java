package com.channel.admin.modules.cac.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.channel.admin.base.BaseController;
import com.channel.admin.modules.cac.form.CashOrderApprovalForm;
import com.channel.api.entity.cac.SysCashOrderEntity;
import com.channel.api.service.cac.SysCashOrderService;
import com.channel.common.annotation.ChlLog;
import com.channel.common.utils.PageUtils;
import com.channel.common.utils.R;
import com.channel.common.validator.ValidatorUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.Map;



/**
 * 
 *
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-02-24 19:28:16
 */
@RestController
@RequestMapping("cac/cashorder")
public class SysCashOrderController extends BaseController {
    @Reference(version = "${api.service.version}", timeout = 10000, check = true)
    private SysCashOrderService sysCashOrderService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("cac:cashorder:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = sysCashOrderService.queryOrderPage(params, getUser());
        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("cac:cashorder:info")
    public R info(@PathVariable("id") Integer id){
        SysCashOrderEntity sysCashOrder = sysCashOrderService.selectById(id);
        return R.ok().put("cashOrder", sysCashOrder);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("cac:cashorder:save")
    public R save(@RequestBody SysCashOrderEntity sysCashOrder){
        sysCashOrderService.insert(sysCashOrder);
        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("cac:cashorder:update")
    public R update(@RequestBody SysCashOrderEntity sysCashOrder){
        ValidatorUtils.validateEntity(sysCashOrder);
        sysCashOrderService.updateAllColumnById(sysCashOrder);//全部更新
        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("cac:cashorder:delete")
    public R delete(@RequestBody Integer[] ids){
        sysCashOrderService.deleteBatchIds(Arrays.asList(ids));
        return R.ok();
    }


    /**
     * 审批
     */
    @ChlLog("审批提现信息")
    @RequestMapping("/approval")
    @RequiresPermissions("cac:cashorder:approval")
    public R approval(@RequestBody CashOrderApprovalForm form){
        Boolean result = sysCashOrderService.approval(form.getId(), form.getStatus(), form.getNote(), getUser());
        if (result) {
            return R.ok();
        } else {
            return R.error("审批提现失败");
        }
    }

}
