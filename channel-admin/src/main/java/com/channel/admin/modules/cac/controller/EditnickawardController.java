package com.channel.admin.modules.cac.controller;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.dubbo.config.annotation.Reference;
import com.channel.admin.base.BaseController;
import com.channel.admin.modules.cac.form.NickAwardForm;
import com.channel.admin.modules.cac.form.OpenTypeForm;
import com.channel.api.entity.cac.ChannelEntity;
import com.channel.api.entity.cac.EditnickawardVo;
import com.channel.api.service.cac.ChannelService;
import com.channel.api.service.cac.EditnickawardService;
import com.channel.common.annotation.ChlLog;
import com.channel.common.json.GsonNickRward;
import com.channel.common.json.GsonResult;
import com.channel.common.utils.HttpHandler;
import com.channel.common.utils.PageUtils;
import com.channel.common.utils.R;
import com.google.gson.Gson;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.*;

import java.util.Map;



/**
 * VIEW
 *
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-09-18 18:58:08
 */
@RestController
@RequestMapping("cac/editnickaward")
public class EditnickawardController extends BaseController {

    @Reference(version = "${api.service.version}", timeout = 20000, check = true)
    private EditnickawardService editnickawardService;

    @Reference(version = "${api.service.version}", timeout = 20000, check = true)
    private ChannelService channelService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("cac:editnickaward:list")
    public R list(@RequestParam Map<String, Object> params){
        ChannelEntity entity =  channelService.findById(getUser());
        PageUtils page = editnickawardService.queryNickAwardPage(params, getUser());
        return R.ok().put("page", page).put("channel", entity);
    }


    @RequestMapping("/info/{channelId}")
    @RequiresPermissions("cac:editnickaward:info")
    public R info(@PathVariable("channelId") String channelId){
        EditnickawardVo vo = editnickawardService.findByChannelId(channelId);
        String cfg = new String(vo.getStrJsonAward());
        if (StrUtil.isNotEmpty(cfg)) {
            GsonNickRward nickRward = new Gson().fromJson(cfg, GsonNickRward.class);
            vo.setAward(nickRward.getAward());
        }

        return R.ok().put("nickawardVo", vo);
    }



    @ChlLog("代理修改昵称返利修改状态")
    @RequestMapping("/channelOpenType")
    @RequiresPermissions("cac:editnickaward:opentype")
    public R channelOpenType(@RequestBody OpenTypeForm openTypeForm){
        int result = editnickawardService.channelOpenType(openTypeForm.getChannelId(), openTypeForm.getOpenType());
        if (result == 0) {
            return R.ok();
        } else {
            return R.error("修改昵称返利信息状态失败!");
        }
    }


    @ChlLog("代理修改昵称返利信息保存")
    @RequestMapping("/doConfig")
    @RequiresPermissions("cac:editnickaward:config")
    public R doConfig(@RequestBody NickAwardForm nickAwardForm) {
        int result = editnickawardService.saveNcikAwardCfg(getUser().getChannelId(), nickAwardForm.getOpenType(), nickAwardForm.getReward());
        if (result == 0) {
            return R.ok();
        } else {
            return R.error("配置修改昵称返利失败!");
        }
    }
}
