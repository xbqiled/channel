package com.channel.admin.modules.report.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.channel.admin.base.BaseController;
import com.channel.api.service.report.PlayerDayShareService;
import com.channel.common.utils.PageUtils;
import com.channel.common.utils.R;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;



/**
 * VIEW
 * @email ismyjuliet@gmail.com
 * @date 2019-12-23 15:48:43
 */
@RestController
@RequestMapping("report/playerdayshare")
public class PlayerDayShareController extends BaseController {

    @Reference(version = "${api.service.version}", timeout =10000, check = true)
    private PlayerDayShareService playerDayShareService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("cac:playerdayshare:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = playerDayShareService.queryDaySharePage(params, getUser());
        return R.ok().put("page", page);
    }
}
