package com.channel.admin.modules.cac.controller;


import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.dubbo.config.annotation.Reference;
import com.channel.admin.base.BaseController;
import com.channel.api.entity.cac.*;
import com.channel.api.entity.report.DayReportEntity;
import com.channel.api.service.cac.*;
import com.channel.api.service.report.DayReportService;
import com.channel.common.utils.R;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * <B>首页Con</B>
 */
@RestController
@RequestMapping("cac/home")
public class HomeController extends BaseController {

    @Reference(version = "${api.service.version}", timeout = 20000, check = true)
    private ChannelService channelService;

    @Reference(version = "${api.service.version}", timeout = 50000, check = true)
    private DayReportService  dayReportService;

    @Reference(version = "${api.service.version}", timeout = 20000, check = true)
    private SysPayOrderService sysPayOrderService;

    @Reference(version = "${api.service.version}", timeout = 20000, check = true)
    private SysCashOrderService  sysCashOrderService;

    @Reference(version = "${api.service.version}", timeout = 20000, check = true)
    private TipOrderService tipOrderService;

    @Reference(version = "${api.service.version}", timeout = 20000, check = true)
    private TipSoundTokenService tipSoundTokenService;

    /**
     * <B>获取提示信息</B>
     * @return
     */
    @RequestMapping("/tipInfo")
    public R getCashTipInfo() {
        List<SysCashOrderEntity> cashResultList = sysCashOrderService.queryCashOrderList(getUser().getChannelId());
        List<SysPayOrderEntity> rechargeResultList = sysPayOrderService.queryRechargeOrderList(getUser().getChannelId());
        return R.ok().put("cashTipInfo", cashResultList).put("rechargeTipInfo", rechargeResultList);
    }

    /**
     * <B>获取tonken信息</B>
     * @return
     */
    @RequestMapping("/soundTonken")
    public R getSoundTonken() {
        TipSoundTokenEntity  tonken = tipSoundTokenService.getTonkenByChannelId(getUser().getChannelId());
        return R.ok().put("tonken", tonken.getAccessToken());
    }

    /**
     * <B>渠道信息</B>
     * @return
     */
    @RequestMapping("/channel")
    public R homeChannelInfo() {
        VideoBalanceVo vo = channelService.getVideoBalance(getUser());
        return R.ok().put("channel", vo);
    }


    /**
     * <B>获取图标数据信息</B>
     * @return
     */
    @RequestMapping("/report")
    public R showHomePage() {
        //获取周统计数据
        List<DayReportEntity> reportList = dayReportService.queryDayReportByChannel(getUser());
        //分解出本日信息
        DayReportEntity dayReport = new DayReportEntity();
        if (CollUtil.isNotEmpty(reportList)) {
            dayReport = reportList.get(0);
        }

        //分解出图表信息
        List<String> timeList = new ArrayList<>();
        List<String> typeList = new ArrayList<>();
        List<String> zcrsList = new ArrayList<>();

        List<String> xzyhList = new ArrayList<>();
        List<String> dlrsList = new ArrayList<>();
        List<String> czrsList = new ArrayList<>();

        typeList.add("充值人数");
        typeList.add("登录人数");
        typeList.add("注册人数");
        typeList.add("游戏人数");

        if (reportList != null && reportList.size() > 0) {
            for (DayReportEntity report : reportList) {

                String recordtime = "";
                //统计时间
                if (report.getRecordtime() != null &&  StrUtil.isBlank(report.getRecordtime())) {
                    recordtime = "";
                } else {
                    recordtime = report.getRecordtime();
                }
                timeList.add(recordtime);

                //注册人数
                Long regcount = new Long(0);
                if (report.getRegcount() != null && !report.getRegcount().equals("")) {
                    regcount =  report.getRegcount();
                }
                zcrsList.add(regcount.toString());

                //游戏人数
                Long newgamecount = new Long(0);
                if (report.getLogingamecount() != null && !report.getLogingamecount().equals("")) {
                    newgamecount = report.getLogingamecount();
                }
                xzyhList.add(newgamecount.toString());

                //登录人数
                Long logincount = new Long(0);
                if (report.getLogincount() != null && !report.getLogincount().equals("")) {
                    logincount = report.getLogincount();
                }
                dlrsList.add(logincount.toString());

                //充值人数
                Long rechargecount = new Long(0);
                if (report.getRechargecount() != null && !report.getRechargecount().equals("")) {
                    rechargecount = report.getRechargecount();
                }
                czrsList.add(rechargecount.toString());
            }
        }

        //充值折线统计
        List<Map<String, Object>> lineResultList = new ArrayList<>();
        //柱图统计
        List<Map<String, Object>> barResultList = new ArrayList<>();
        //饼图统计
        List<Map<String, Object>> pieResultList = new ArrayList<>();

        for (String str : typeList) {
            Map<String, Object> seriesMap = new HashMap<>();
            Map<String, Object> seriesBarMap = new HashMap<>();

            seriesMap.put("name", str);
            seriesMap.put("type", "line");
            seriesMap.put("stack", "数量");

            seriesBarMap.put("name", str);
            seriesBarMap.put("type", "bar");
            seriesBarMap.put("stack", "数量");

            if (str.equals("注册人数")) {
                seriesMap.put("data", zcrsList);
                seriesBarMap.put("data", zcrsList);
            } else if (str.equals("游戏人数")) {
                seriesMap.put("data", xzyhList);
                seriesBarMap.put("data", xzyhList);
            } else if (str.equals("登录人数")) {
                seriesMap.put("data", dlrsList);
                seriesBarMap.put("data", dlrsList);
            } else if (str.equals("充值人数")) {
                seriesMap.put("data", czrsList);
                seriesBarMap.put("data", czrsList);
            }
            lineResultList.add(seriesMap);
            barResultList.add(seriesBarMap);
        }

        //画饼图
        List<Map<String, Object>> gameResultList = dayReportService.queryPlayGameCount(DateUtil.today(), DateUtil.today(), getUser());
        if (CollUtil.isNotEmpty(gameResultList)) {
            for (Map<String, Object> map : gameResultList) {
                Map<String, Object> pieMap = new HashMap<>();

                pieMap.put("name", map.get("gameKindName"));
                pieMap.put("value", map.get("num"));
                pieResultList.add(pieMap);
            }
        }

        //加工数据信息
        return R.ok().put("reportList", reportList).put("dayReport", dayReport).put("timeList", timeList).put("typeList", typeList)
        .put("lineResult", lineResultList).put("barResultList", barResultList).put("pieResultList", pieResultList);
    }
}
