package com.channel.admin.modules.cac.controller;

import cn.jpush.api.report.MessagesResult;
import cn.jpush.api.report.ReceivedsResult;
import com.alibaba.dubbo.config.annotation.Reference;
import com.channel.admin.base.BaseController;
import com.channel.admin.modules.cac.form.PushForm;
import com.channel.api.service.cac.PushConfigService;
import com.channel.api.service.cac.PushRecordService;
import com.channel.common.utils.PageUtils;
import com.channel.common.utils.R;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;



/**
 * 
 *
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2020-05-27 20:42:22
 */
@RestController
@RequestMapping("cac/push")
public class PushRecordController extends BaseController {

    @Reference(version = "${api.service.version}", timeout = 30000)
    private PushRecordService pushRecordService;

    @Reference(version = "${api.service.version}", timeout = 30000)
    private PushConfigService pushConfigService;


    @RequestMapping("/list")
    @RequiresPermissions("cac:push:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = pushRecordService.queryPushRecordPage(params, getUser());
        return R.ok().put("page", page);
    }

    @PostMapping("/sendPush")
    public R sendPush(@RequestBody PushForm pushForm) {
        Boolean result = pushRecordService.sendPush(getUser(), pushForm.getDevicePlatform(), pushForm.getTimeType(), pushForm.getRetainTime(), pushForm.getTimeLong(),
                pushForm.getSendType(), pushForm.getAlias(), pushForm.getTitle(), pushForm.getContent());

        if (result) {
            return R.ok();
        } else {
            return R.error();
        }
    }

    @GetMapping("/getById/{id}")
    public R getConfigById(@PathVariable("id") Integer id){
        Map<String, Object> pushRecord = pushRecordService.getObj(id);
        return  R.ok().put("pushRecord", pushRecord);
    }

    @GetMapping("/getReportById/{id}")
    public R getReportById(@PathVariable("id") Integer id){
        ReceivedsResult pushReport = pushRecordService.getReport(id);
        return  R.ok().put("pushReport", pushReport);
    }

}
