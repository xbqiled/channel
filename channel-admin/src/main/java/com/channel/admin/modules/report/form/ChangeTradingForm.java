package com.channel.admin.modules.report.form;


import java.io.Serializable;



public class ChangeTradingForm implements Serializable {

    private String UserId;
    private Integer opType;
    private Long damaChange;

    public String getUserId() {
        return UserId;
    }

    public void setUserId(String userId) {
        UserId = userId;
    }

    public Integer getOpType() {
        return opType;
    }

    public void setOpType(Integer opType) {
        this.opType = opType;
    }

    public Long getDamaChange() {
        return damaChange;
    }

    public void setDamaChange(Long damaChange) {
        this.damaChange = damaChange;
    }
}
