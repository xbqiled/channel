package com.channel.admin.modules.cac.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.channel.admin.base.BaseController;
import com.channel.admin.modules.cac.form.OpenTypeForm;
import com.channel.admin.modules.cac.form.TaskForm;
import com.channel.api.service.cac.ChanneltaskService;
import com.channel.common.annotation.ChlLog;
import com.channel.common.utils.PageUtils;
import com.channel.common.utils.R;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;



/**
 * VIEW
 *
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-12-31 12:30:32
 */
@RestController
@RequestMapping("cac/channeltask")
public class ChanneltaskController extends BaseController {

    @Reference(version = "${api.service.version}", timeout = 20000, check = true)
    private ChanneltaskService channeltaskService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("cac:task:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = channeltaskService.queryTaskPage(params, getUser());
        return R.ok().put("page", page);
    }

    /**
     * 信息
     */
    @RequestMapping("/info")
    @RequiresPermissions("cac:task:info")
    public R info(){
        Map<String, Object> taskMap = channeltaskService.findByChannelId(getUser().getChannelId());
        return R.ok().put("task", taskMap);
    }


    @ChlLog("任务修改状态")
    @RequestMapping("/channelOpenType")
    @RequiresPermissions("cac:task:opentype")
    public R channelOpenType(@RequestBody OpenTypeForm openTypeForm){
        int result = channeltaskService.channelOpenType(openTypeForm.getChannelId(), openTypeForm.getOpenType());
        if (result == 0) {
            return R.ok();
        } else {
            return R.error("修改任务配置状态失败!");
        }
    }

    @ChlLog("任务配置信息保存")
    @RequestMapping("/doConfig")
    @RequiresPermissions("cac:task:config")
    public R doConfig(@RequestBody TaskForm taskForm){
        int result = channeltaskService.saveOrUpdateConfig(getUser(), taskForm.getActivityOpenType(), taskForm.getActivityTimeType(),
                taskForm.getBeginTime(), taskForm.getEndTime(), taskForm.getTaskArr());
        if (result == 0) {
            return R.ok();
        } else {
            return R.error("配置任务信息失败!");
        }
    }
}
