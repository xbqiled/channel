package com.channel.admin.modules.cac.controller;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.dubbo.config.annotation.Reference;
import com.channel.admin.base.BaseController;
import com.channel.admin.modules.cac.form.PayOrderApprovalForm;
import com.channel.admin.modules.cac.form.RechargeCancelForm;
import com.channel.admin.modules.cac.form.RechargeForm;
import com.channel.admin.modules.cac.form.ReduceForm;
import com.channel.api.entity.cac.CancelResultVo;
import com.channel.api.entity.cac.SysPayOrderEntity;
import com.channel.api.entity.cac.SysPayOrderVo;
import com.channel.api.service.cac.SysPayOrderService;
import com.channel.api.service.cac.SysPayPlatformService;
import com.channel.common.annotation.ChlLog;
import com.channel.common.json.GsonReduceResult;
import com.channel.common.utils.PageUtils;
import com.channel.common.utils.R;
import com.channel.common.validator.ValidatorUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.Map;



/**
 * 
 *
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-02-24 19:28:02
 */
@RestController
@RequestMapping("cac/payorder")
public class SysPayOrderController extends BaseController {

    @Reference(version = "${api.service.version}", timeout = 10000, check = true, retries = 0)
    private SysPayOrderService sysPayOrderService;

    @Reference(version = "${api.service.version}", timeout = 10000, check = true, retries = 0)
    private SysPayPlatformService sysPayPlatformService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("cac:payorder:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = sysPayOrderService.queryOrderPage(params, getUser());
        return R.ok().put("page", page);
    }

    @RequestMapping("/statisList")
    public R statisList(@RequestParam Map<String, Object> params){
        PageUtils page = sysPayOrderService.queryRechargeStatisPage(params, getUser());
        return R.ok().put("statispage", page);
    }

    @RequestMapping("/userList")
    public R userList(@RequestParam Map<String, Object> params){
        PageUtils page = sysPayOrderService.queryRechargeUserNumPage(params, getUser());
        return R.ok().put("userpage", page);
    }

    @RequestMapping("/allList")
    public R allList(@RequestParam Map<String, Object> params){
        Map<String, Object> resultMap  = sysPayOrderService.queryAllRechargeData(params, getUser());
        return R.ok().put("resultData", resultMap);
    }

    /**
     * 手动扣款
     * @param reduceForm
     * @return
     */
    @ChlLog("手动扣款")
    @RequestMapping("/reduceFee")
    public R reduceFee(@RequestBody ReduceForm reduceForm) {
        GsonReduceResult resulet = sysPayOrderService.reduceFee(reduceForm.getUserId(), reduceForm.getNote(), reduceForm.getPayFee(), reduceForm.getForce(), getUser());
        if (resulet != null && resulet.getRet() == 0 && resulet.getRealCancel() != null && resulet.getRealCancel() > 0) {
            return R.ok().put("resulet", resulet);
        } else if (resulet == null) {
            return R.error("找不到到对应的用户!");
        } else {
            return R.error("处理撤销失败!");
        }
    }


    /**
     * 撤销充值信息
     * @param cancelForm
     * @return
     */
    @ChlLog("充值订单撤销")
    @RequestMapping("/rechargeCancel")
    public R rechargeCancel(@RequestBody RechargeCancelForm cancelForm){
        CancelResultVo resulet = sysPayOrderService.rechargeCancel(cancelForm.getOrderId(), cancelForm.getNote(), cancelForm.getForce(), getUser());
        if (resulet != null && resulet.getRet() == 0 && resulet.getRealCancel() != null && resulet.getRealCancel() > 0) {
            return R.ok().put("resulet", resulet);
        } else {
            return R.error("处理撤销失败!");
        }
    }

    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("cac:payorder:info")
    public R info(@PathVariable("id") Integer id){
        SysPayOrderEntity sysPayOrder = sysPayOrderService.selectById(id);
        String name = "";
        SysPayOrderVo sysPayOrderVo  = new SysPayOrderVo();

        if (sysPayOrder != null && sysPayOrder.getPayConfigId() != null && sysPayOrder.getPayConfigId() != 0) {
            name = sysPayPlatformService.getPlatformName(sysPayOrder.getPayConfigId());
        } else {
            name = "手工上分";
        }

        BeanUtil.copyProperties(sysPayOrder, sysPayOrderVo);
        if (StrUtil.isNotBlank(name)) {
            sysPayOrderVo.setCompany(name);
        }

        return R.ok().put("payOrder", sysPayOrderVo);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("cac:payorder:save")
    public R save(@RequestBody SysPayOrderEntity sysPayOrder){
        sysPayOrderService.insert(sysPayOrder);
        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("cac:payorder:update")
    public R update(@RequestBody SysPayOrderEntity sysPayOrder){
        ValidatorUtils.validateEntity(sysPayOrder);
        sysPayOrderService.updateAllColumnById(sysPayOrder);//全部更新
        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("cac:payorder:delete")
    public R delete(@RequestBody Integer[] ids){
        sysPayOrderService.deleteBatchIds(Arrays.asList(ids));
        return R.ok();
    }

    /**
     * 审批
     */
    @ChlLog("支付订单审批处理")
    @RequestMapping("/approval")
    @RequiresPermissions("cac:payorder:approval")
    public R Approval(@RequestBody PayOrderApprovalForm from) {
        Boolean result = sysPayOrderService.approval(from.getStatus(), from.getId(), from.getNote(), getUser());
        if(result) {
            return R.ok();
        } else {
            return R.error("手动处理失败!");
        }
    }

    @ChlLog("手动上分")
    @RequestMapping("/recharge")
    @RequiresPermissions("cac:payorder:recharge")
    public R recharge(@RequestBody RechargeForm from) {
        Boolean reuslt = sysPayOrderService.recharge(from.getUserId(), 1, from.getDamaMulti(), from.getPayFee(), from.getDepositor(), from.getAccount(), from.getRemark(), from.getPayType(), from.getRate(), getUser());
        if (reuslt) {
            return R.ok();
        } else {
            return R.error("上分失败!");
        }
    }

}
