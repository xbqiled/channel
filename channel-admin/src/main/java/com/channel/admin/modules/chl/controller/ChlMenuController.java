package com.channel.admin.modules.chl.controller;


import com.alibaba.dubbo.config.annotation.Reference;
import com.channel.admin.base.BaseController;
import com.channel.api.entity.chl.ChlMenuEntity;
import com.channel.api.service.shiro.ShiroService;
import com.channel.api.service.chl.ChlMenuService;
import com.channel.common.annotation.ChlLog;
import com.channel.common.constant.Constant;
import com.channel.common.execption.AppException;
import com.channel.common.utils.R;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

/**
 * 系统菜单
 */
@RestController
@RequestMapping("/chl/menu")
public class ChlMenuController extends BaseController {

    @Reference(version = "${api.service.version}", timeout =10000, check = true)
    private ChlMenuService chlMenuService;

    @Reference(version = "${api.service.version}", timeout =10000, check = true)
    private ShiroService shiroService;


    /**
     * 导航菜单
     */
    @GetMapping("/nav")
    public R nav(){
        List<ChlMenuEntity> menuList = chlMenuService.getUserMenuList(getUserId());
        Set<String> permissions = shiroService.getUserPermissions(getUserId());
        return R.ok().put("menuList", menuList).put("permissions", permissions);
    }

    /**
     * 所有渠道菜单
     */
    @GetMapping("/list")
    @RequiresPermissions("chl:menu:list")
    public List<ChlMenuEntity> list(){
        List<ChlMenuEntity> menuList = chlMenuService.queryChannelList(getUserId());
        for(ChlMenuEntity sysMenuEntity : menuList){
            ChlMenuEntity parentMenuEntity = chlMenuService.selectById(sysMenuEntity.getParentId());
            if(parentMenuEntity != null){
                sysMenuEntity.setParentName(parentMenuEntity.getName());
            }
        }
        return menuList;
    }


    /**
     * 所有菜单列表
     */
    @GetMapping("/adminlist")
    @RequiresPermissions("chl:adminmenu:list")
    public List<ChlMenuEntity> adminList(){
        List<ChlMenuEntity> menuList = chlMenuService.queryAll();
        return menuList;
    }


    /**
     * 选择菜单(添加、修改菜单)
     */
    @GetMapping("/select")
    @RequiresPermissions("chl:menu:select")
    public R select(){
        //查询列表数据
        List<ChlMenuEntity> menuList = chlMenuService.queryNotButtonList();

        //添加顶级菜单
        ChlMenuEntity root = new ChlMenuEntity();
        root.setMenuId(0L);
        root.setName("一级菜单");

        root.setParentId(-1L);
        root.setOpen(true);
        menuList.add(root);
        return R.ok().put("menuList", menuList);
    }

    /**
     * 菜单信息
     */
    @GetMapping("/info/{menuId}")
    @RequiresPermissions("chl:menu:info")
    public R info(@PathVariable("menuId") Long menuId){
        ChlMenuEntity menu = chlMenuService.selectById(menuId);
        return R.ok().put("menu", menu);
    }

    /**
     * 保存
     */
    @ChlLog("保存菜单")
    @PostMapping("/save")
    @RequiresPermissions("chl:menu:save")
    public R save(@RequestBody ChlMenuEntity menu){
        //数据校验
        verifyForm(menu);

        chlMenuService.insert(menu);
        return R.ok();
    }

    /**
     * 修改
     */
    @ChlLog("修改菜单")
    @PostMapping("/update")
    @RequiresPermissions("chl:menu:update")
    public R update(@RequestBody ChlMenuEntity menu){
        //数据校验
        verifyForm(menu);

        chlMenuService.updateById(menu);
        return R.ok();
    }

    /**
     * 删除
     */
    @ChlLog("删除菜单")
    @PostMapping("/delete/{menuId}")
    @RequiresPermissions("chl:menu:delete")
    public R delete(@PathVariable("menuId") long menuId){
        if(menuId <= 31){
            return R.error("系统菜单，不能删除");
        }
        //判断是否有子菜单或按钮
        List<ChlMenuEntity> menuList = chlMenuService.queryListParentId(menuId);
        if(menuList.size() > 0){
            return R.error("请先删除子菜单或按钮");
        }

        chlMenuService.delete(menuId);
        return R.ok();
    }

    /**
     * 验证参数是否正确
     */
    private void verifyForm(ChlMenuEntity menu){
        if(StringUtils.isBlank(menu.getName())){
            throw new AppException("菜单名称不能为空");
        }

        if(menu.getParentId() == null){
            throw new AppException("上级菜单不能为空");
        }

        //菜单
        if(menu.getType() == Constant.MenuType.MENU.getValue()){
            if(StringUtils.isBlank(menu.getUrl())){
                throw new AppException("菜单URL不能为空");
            }
        }

        //上级菜单类型
        int parentType = Constant.MenuType.CATALOG.getValue();
        if(menu.getParentId() != 0){
            ChlMenuEntity parentMenu = chlMenuService.selectById(menu.getParentId());
            parentType = parentMenu.getType();
        }

        //目录、菜单
        if(menu.getType() == Constant.MenuType.CATALOG.getValue() ||
                menu.getType() == Constant.MenuType.MENU.getValue()){
            if(parentType != Constant.MenuType.CATALOG.getValue()){
                throw new AppException("上级菜单只能为目录类型");
            }
            return ;
        }

        //按钮
        if(menu.getType() == Constant.MenuType.BUTTON.getValue()){
            if(parentType != Constant.MenuType.MENU.getValue()){
                throw new AppException("上级菜单只能为菜单类型");
            }
            return ;
        }
    }
}
