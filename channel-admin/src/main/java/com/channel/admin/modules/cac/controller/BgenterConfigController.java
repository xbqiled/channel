package com.channel.admin.modules.cac.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.channel.admin.base.BaseController;
import com.channel.api.entity.cac.BgenterConfigEntity;
import com.channel.api.service.cac.BgenterConfigService;
import com.channel.common.utils.PageUtils;
import com.channel.common.utils.R;
import com.channel.common.validator.ValidatorUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.*;

import java.util.Map;



/**
 * 
 *
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-11-15 13:23:00
 */
@RestController
@RequestMapping("cac/bgenterconfig")
public class BgenterConfigController extends BaseController {

    @Reference(version = "${api.service.version}", timeout = 20000, check = true)
    private BgenterConfigService bgenterConfigService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("cac:bgenterconfig:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = bgenterConfigService.queryBgEnterGamePage(params, getUser());
        return R.ok().put("page", page).put("channelId", getUser().getChannelId());
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{channelId}")
    @RequiresPermissions("cac:bgenterconfig:info")
    public R info(@PathVariable("channelId") String channelId){
        BgenterConfigEntity bgenterConfig = bgenterConfigService.getConfigByChannelId(channelId);
        return R.ok().put("bgenterConfig", bgenterConfig);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("cac:bgenterconfig:save")
    public R save(@RequestBody BgenterConfigEntity bgenterConfigEntity){
        bgenterConfigService.saveEnterConfig(bgenterConfigEntity, getUser());
        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("cac:bgenterconfig:update")
    public R update(@RequestBody BgenterConfigEntity bgenterConfig){
        ValidatorUtils.validateEntity(bgenterConfig);
        bgenterConfigService.modifyEnterConfig(bgenterConfig, getUser());//全部更新
        return R.ok();
    }

}
