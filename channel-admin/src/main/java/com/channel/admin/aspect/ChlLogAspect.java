package com.channel.admin.aspect;

import com.alibaba.dubbo.config.annotation.Reference;
import com.channel.api.entity.chl.ChlLogEntity;
import com.channel.api.entity.chl.ChlUserEntity;
import com.channel.api.service.chl.ChlLogService;
import com.channel.common.annotation.ChlLog;
import com.channel.common.utils.HttpContextUtils;
import com.channel.common.utils.IPUtils;
import com.google.gson.Gson;

import org.apache.shiro.SecurityUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;
import java.util.Date;


/**
 * 系统日志，切面处理类
 *
 * @author Mark sunlightcs@gmail.com
 */
@Aspect
@Component
public class ChlLogAspect {

	@Reference(version = "${api.service.version}", timeout = 10000, check = true)
	private ChlLogService chlLogService;
	
	@Pointcut("@annotation(com.channel.common.annotation.ChlLog)")
	public void logPointCut() { 
		
	}

	@Around("logPointCut()")
	public Object around(ProceedingJoinPoint point) throws Throwable {
		long beginTime = System.currentTimeMillis();
		//执行方法
		Object result = point.proceed();
		//执行时长(毫秒)
		long time = System.currentTimeMillis() - beginTime;

		//保存日志
		saveChlLog(point, time);
		return result;
	}

	private void saveChlLog(ProceedingJoinPoint joinPoint, long time) {
		MethodSignature signature = (MethodSignature) joinPoint.getSignature();
		Method method = signature.getMethod();

		ChlLogEntity chlLogEntity = new ChlLogEntity();
		ChlLog chlLog = method.getAnnotation(ChlLog.class);
		if(chlLog != null){
			//注解上的描述
			chlLogEntity.setOperation(chlLog.value());
		}

		//请求的方法名
		String className = joinPoint.getTarget().getClass().getName();
		String methodName = signature.getName();
		chlLogEntity.setMethod(className + "." + methodName + "()");

		//请求的参数
		Object[] args = joinPoint.getArgs();
		try{
			String params = new Gson().toJson(args);
			chlLogEntity.setParams(params);
		}catch (Exception e){
			e.printStackTrace();
		}

		//获取request
		HttpServletRequest request = HttpContextUtils.getHttpServletRequest();
		//设置IP地址
		chlLogEntity.setIp(IPUtils.getIpAddr(request));

		//用户名
		Long userId =  ((ChlUserEntity) SecurityUtils.getSubject().getPrincipal()).getUserId();
		String userName = ((ChlUserEntity) SecurityUtils.getSubject().getPrincipal()).getUsername();
		String channelId =  ((ChlUserEntity) SecurityUtils.getSubject().getPrincipal()).getChannelId();

		chlLogEntity.setUserId(userId);
		chlLogEntity.setUsername(userName);
		chlLogEntity.setChannelId(channelId);

		chlLogEntity.setTime(time);
		chlLogEntity.setCreateDate(new Date());
		//保存系统日志
		chlLogService.insert(chlLogEntity);
	}
}
