package com.channel.common.push;


import lombok.Data;

@Data
public class PushResponse {
    private String code;
    private String msg;
}
