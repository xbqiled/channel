package com.channel.common.push.jpush.model;


import lombok.Data;

@Data
public class RegistrationIdReq {

    private String registrationId;

}
