package com.channel.common.push.jpush.model;


import com.channel.common.push.PushRequest;
import lombok.Data;

import java.util.List;
import java.util.Map;

@Data
public class JPushRequest  extends PushRequest {


    String devicePlatform;

    boolean apnsProduction;

    long timeToAlive;

    String title;

    String content;

    Map<String, String> extras;

    List<String> targetAlias;
}
