package com.channel.common.push.jpush.model;

import lombok.Data;

import java.util.List;


@Data
public class PushAliasBean extends JPushRequest {

    List<String> targetAlias;

}
