package com.channel.common.utils;




public class PayImageUtil {

    private static String WECHAR_ICON_URL = "https://youwanimage.oss-cn-shanghai.aliyuncs.com/game/20190221/69f725b50bf0453aad3aa850874ed9df.jpg";
    private static String ALIPAY_ICON_URL = "https://youwanimage.oss-cn-shanghai.aliyuncs.com/game/20190221/0ff41da4fa784febb67402c3f2bebeb9.jpg";
    private static String BANK_ICON_URL = "https://youwanimage.oss-cn-shanghai.aliyuncs.com/game/20190324/eb7f2fd095684d3985e7b95a1c99cede.jpg";
    private static String QQ_ICON_URL = "https://youwanimage.oss-cn-shanghai.aliyuncs.com/game/20190511/54a6b80a20b0433d84c92156e6134268.jpg";


    public static String getIconUrl(Integer payType) {
        String imgUrl = "";
        switch (payType) {
            case 0: {
                //微信公众账号
                imgUrl = WECHAR_ICON_URL;
            }
            break;

            case 1: {
                //微信扫码支付
                imgUrl = WECHAR_ICON_URL;
            }
            break;

            case 2: {
                //微信网页支付
                imgUrl = WECHAR_ICON_URL;
            }
            break;

            case 3: {
                //微信app支付
                imgUrl = WECHAR_ICON_URL;
            }
            break;

            case 4: {
                //微信转账支付
                imgUrl = WECHAR_ICON_URL;
            }
            break;

            case 5: {
                //支付宝扫码
                imgUrl = ALIPAY_ICON_URL;
            }
            break;


            case 6: {
                //支付宝APP
                imgUrl = ALIPAY_ICON_URL;
            }
            break;

            case 7: {
                //支付宝网页
                imgUrl = ALIPAY_ICON_URL;
            }
            break;

            case 8: {
                //支付宝转账
                imgUrl = ALIPAY_ICON_URL;
            }
            break;

            case 9: {
                //QQ钱包Wap
                imgUrl = QQ_ICON_URL;
            }
            break;

            case 10: {
                //QQ钱包手机扫码
                imgUrl = QQ_ICON_URL;
            }
            break;

            case 11: {
                //QQ转账支付
                imgUrl = QQ_ICON_URL;
            }
            break;

            case 12: {
                //银联二维码
                imgUrl = BANK_ICON_URL;
            }
            break;

            case 13: {
                //网银B2B支付
                imgUrl = BANK_ICON_URL;
            }
            break;

            case 14: {
                //网银B2C支付
                imgUrl = BANK_ICON_URL;
            }
            break;

            case 15: {
                //无卡快捷支付
                imgUrl = BANK_ICON_URL;
            }
            break;

            case 16: {
                //银联H5快捷
                imgUrl = BANK_ICON_URL;
            }
            break;

            case 17: {
                //银行卡转账
                imgUrl = BANK_ICON_URL;
            }
            break;


            default:{
                imgUrl = QQ_ICON_URL;
            }
        }

        return  imgUrl;
    }

}

