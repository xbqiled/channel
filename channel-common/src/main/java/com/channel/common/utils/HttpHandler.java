package com.channel.common.utils;


import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.http.HttpRequest;
import cn.hutool.http.HttpUtil;
import cn.hutool.json.JSONUtil;
import com.channel.common.constant.UrlConstant;
import com.channel.common.json.*;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.channel.common.constant.UrlConstant.*;



/**
 * <B>请求服务器接口信息</B>
 */
@Slf4j
public class HttpHandler {


    public static GsonResult updateUserNote(Integer accountId, String note) {
        RequestUserNote userNote = new RequestUserNote();
        userNote.setAccountId(accountId);
        userNote.setNote(note);

        String body = new Gson().toJson(userNote);
        log.debug("修改用户备注请求:" + GAME_API_URL + USERNOTE_URL + "Json:" + body);
        String resultStr =  HttpUtil.post(GAME_API_URL + USERNOTE_URL,  body);
        log.debug("修改用户备注返回:" + resultStr);
        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            return new Gson().fromJson(resultStr, GsonResult.class);
        }
        return null;
    }



    public static GsonResult cfgTask(String channelId, Integer openType, Integer activityTimeType, Long beginTime, Long endTime, GsonTask[] taskArr) {
        GsonChannelTask task = new GsonChannelTask();
        task.setChannel(channelId);
        task.setActivityOpenType(openType);

        task.setActivityTimeType(activityTimeType);
        if (activityTimeType == 1) {
            task.setBeginTime(beginTime);
            task.setEndTime(endTime);
        }

        if (taskArr != null && taskArr.length > 0) {
            task.setTaskArr(taskArr);
        }

        String body = new Gson().toJson(task);
        log.debug("配置任务请求:" + GAME_API_URL + TASK_URL + "Json:" + body);
        String resultStr =  HttpUtil.post(GAME_API_URL + TASK_URL,  body);
        log.debug("配置任务返回:" + resultStr);
        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            return new Gson().fromJson(resultStr, GsonResult.class);
        }
        return null;
    }


    public static GsonResult cfgSuccour(String channelId, Integer openType, Long coinLimit, Long succourAward, Integer totalSuccourCnt, Integer shareConditionCnt) {
        GsonSuccour succour = new GsonSuccour();
        succour.setChannel(channelId);
        succour.setOpenType(openType);

        succour.setCoinLimit(coinLimit);
        succour.setSuccourAward(succourAward);
        succour.setTotalSuccourCnt(totalSuccourCnt);

        succour.setShareConditionCnt(shareConditionCnt);
        String body = new Gson().toJson(succour);
        log.debug("配置救济金请求:" + GAME_API_URL + SUCCOUR_URL + "Json:" + body);
        String resultStr =  HttpUtil.post(GAME_API_URL + SUCCOUR_URL,  body);
        log.debug("配置救济金返回:" + resultStr);
        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            return new Gson().fromJson(resultStr, GsonResult.class);
        }
        return null;
    }



    public static GsonResult cfgVip(String channelId, Integer openType, GsonVip vip1, GsonVip vip2, GsonVip vip3, GsonVip vip4, GsonVip vip5,
                                    GsonVip vip6, GsonVip vip7, GsonVip vip8, GsonVip vip9) {

        RequestVip requestVip = new RequestVip();
        requestVip.setChannel(channelId);
        requestVip.setOpenType(openType);

        requestVip.setVip1(vip1);
        requestVip.setVip2(vip2);
        requestVip.setVip3(vip3);

        requestVip.setVip4(vip4);
        requestVip.setVip5(vip5);
        requestVip.setVip6(vip6);

        requestVip.setVip7(vip7);
        requestVip.setVip8(vip8);
        requestVip.setVip9(vip9);

        String body = new Gson().toJson(requestVip);
        log.debug("配置VIP等级请求:" + GAME_API_URL + VIP_URL + "Json:" + body);
        String resultStr =  HttpUtil.post(GAME_API_URL + VIP_URL,  body);
        log.debug("配置VIP等级返回:" + resultStr);
        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            return new Gson().fromJson(resultStr, GsonResult.class);
        }
        return null;
    }


    public static GsonResult cfgDayShare(RequestDayShare requestDayShare) {
        String body = new Gson().toJson(requestDayShare);
        log.debug("配置每日分享请求:" + GAME_API_URL + DAY_SHARE_URL + "Json:" + body);
        String resultStr =  HttpUtil.post(GAME_API_URL + DAY_SHARE_URL,  body);
        log.debug("配置每日分享返回:" + resultStr);
        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            return new Gson().fromJson(resultStr, GsonResult.class);
        }
        return null;
    }

    public static GsonResult cfgDayShare(Long channelId, Integer opType, Long dayAward, Long awardDayShareCnt, List<GsonDayShare> dayShareList) {
        RequestDayShare requestDayShare = new RequestDayShare();
        requestDayShare.setDayAward(dayAward);
        requestDayShare.setAwardDayShareCnt(awardDayShareCnt);

        requestDayShare.setChannel(channelId);
        requestDayShare.setOpenType(opType);
        if (CollUtil.isNotEmpty(dayShareList) && dayShareList.size() > 0) {

            GsonDayShare [] dayShareArr = new GsonDayShare [dayShareList.size()];
            dayShareList.toArray(dayShareArr);
            requestDayShare.setTotalDayArr(dayShareArr);
        }

        String body = new Gson().toJson(requestDayShare);
        log.debug("配置每日分享请求:" + GAME_API_URL + DAY_SHARE_URL + "Json:" + body);
        String resultStr =  HttpUtil.post(GAME_API_URL + DAY_SHARE_URL,  body);
        log.debug("配置每日分享返回:" + resultStr);
        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            return new Gson().fromJson(resultStr, GsonResult.class);
        }
        return null;
    }


    public static GsonResult cfgBindUrl(String url, Integer opType, String channel, Long accountId) {
        RequestBindUrl  requestBindUrl  = new RequestBindUrl();
        requestBindUrl.setAccountID(accountId);
        requestBindUrl.setChannel(channel);
        requestBindUrl.setOpType(opType);
        requestBindUrl.setUrl(url);

        String body = new Gson().toJson(requestBindUrl);
        log.debug("配置推广绑定域名请求:" + GAME_API_URL + BIND_PROMOTER_URL + "Json:" + body);
        String resultStr =  HttpUtil.post(GAME_API_URL + BIND_PROMOTER_URL,  body);
        log.debug("配置推广绑定域名返回:" + resultStr);
        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            return new Gson().fromJson(resultStr, GsonResult.class);
        }
        return null;
    }


    /**
     * @return
     */
    public static GsonResult modifyRealName(Long accountID, String realName) {
        RequestRealName requestRealName  = new RequestRealName();
        requestRealName.setAccountID(accountID);
        requestRealName.setRealName(realName);

        String body = new Gson().toJson(requestRealName);
        log.debug("配置修改真实姓名请求:" + GAME_API_URL + MODIFY_REALNAME_URL + "Json:" + body);
        String resultStr =  HttpUtil.post(GAME_API_URL + MODIFY_REALNAME_URL,  body);
        log.debug("配置修改真实姓名奖励返回:" + resultStr);
        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            return new Gson().fromJson(resultStr, GsonResult.class);
        }
        return null;
    }



    /**
     * <B>配置鬥地主獎勵接口</B>
     * @param
     * @return
     */
    public static GsonResult configDdzMatchRankAward(List<GsonMatchRankAward> cfgList, String channel, Integer option, Integer openType) {
        RequestMatchRankAward requestMatchRankAward = new RequestMatchRankAward();

        if (CollUtil.isNotEmpty(cfgList) && cfgList.size() > 0) {
            GsonMatchRankAward [] gsonMatchRankAward = new GsonMatchRankAward [cfgList.size()];
            cfgList.toArray(gsonMatchRankAward);
            requestMatchRankAward.setAwards(gsonMatchRankAward);
        }

        requestMatchRankAward.setChannel(channel);
        requestMatchRankAward.setOpenType(openType);
        requestMatchRankAward.setOption(option);

        String body = new Gson().toJson(requestMatchRankAward);
        log.debug("配置斗地主奖励请求:" + GAME_API_URL + UPDATE_DDZ_MATCHRANKAWARD_URL + "Json:" + body);
        String resultStr =  HttpUtil.post(GAME_API_URL + UPDATE_DDZ_MATCHRANKAWARD_URL,  body);
        log.debug("配置斗地主奖励返回:" + resultStr);
        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            return new Gson().fromJson(resultStr, GsonResult.class);
        }
        return null;
    }


    /**
     * <B>配置昵稱獎勵</B>
     * @return
     */
    public static GsonResult configNickAward(Integer openType, String channel, Integer award) {
        RequestNick requestNick  = new RequestNick();
        requestNick.setAward(award);
        requestNick.setChannel(channel);
        requestNick.setOpenType(openType);

        String body = new Gson().toJson(requestNick);
        log.debug("配置昵称奖励请求:" + GAME_API_URL + EDIT_NICK_AWARD_URL + "Json:" + body);
        String resultStr =  HttpUtil.post(GAME_API_URL + EDIT_NICK_AWARD_URL,  body);
        log.debug("配置昵称奖励返回:" + resultStr);
        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            return new Gson().fromJson(resultStr, GsonResult.class);
        }
        return null;
    }


    /**
     * <B>查询用户打码量接口</B>
     * @param accountId
     * @return
     */
    public static GsonUserTrading getUserTrading(Integer accountId) {
        RequestUserTrading requestUserTrading  = new RequestUserTrading();
        requestUserTrading.setAccountID(accountId);

        String body = new Gson().toJson(requestUserTrading);
        log.debug("查询用户打码量请求:" + GAME_API_URL + GET_USER_TRADING_URL + "Json:" + body);
        String resultStr =  HttpUtil.post(GAME_API_URL + GET_USER_TRADING_URL,  body);
        log.debug("查询用户打码量返回:" + resultStr);
        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            return new Gson().fromJson(resultStr, GsonUserTrading.class);
        }
        return null;
    }

    /**
     * <B>打码量操作接口</B>
     * @param opType
     * @param accountId
     * @param damaChange
     * @return
     */
    public static GsonChangeTrading changeUserTrading(Integer opType, Integer accountId, Long damaChange) {
        RequestChangeTrading requestChangeTrading  = new RequestChangeTrading();
        requestChangeTrading.setAccountID(accountId);

        requestChangeTrading.setDamaChange(damaChange);
        requestChangeTrading.setOpType(opType);

        String body = new Gson().toJson(requestChangeTrading);
        log.debug("用户打码量操作请求:" + GAME_API_URL + CHANGE_USER_TRADING_URL + "Json:" + body);
        String resultStr =  HttpUtil.post(GAME_API_URL + CHANGE_USER_TRADING_URL,  body);
        log.debug("用户打码量操作返回:" + resultStr);
        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            return new Gson().fromJson(resultStr, GsonChangeTrading.class);
        }
        return null;
    }



    /**
     * <B>手动添加玩家上级</B>
     * @param channel
     * @param accountId
     * @param promoterId
     * @return
     */
    public static GsonResult addRelation(String channel, Long accountId, Long promoterId) {
        RequestAddRelation requestAddRelation = new RequestAddRelation();
        requestAddRelation.setAccountId(accountId);
        requestAddRelation.setChannel(channel);
        requestAddRelation.setPromoterId(promoterId);

        String body = new Gson().toJson(requestAddRelation);
        log.debug("手动添加玩家上级请求:" + GAME_API_URL + ADD_RELATION_URL + "Json:" + body);
        String resultStr =  HttpUtil.post(GAME_API_URL + ADD_RELATION_URL,  body);
        log.debug("手动添加玩家上级返回:" + resultStr);
        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            return new Gson().fromJson(resultStr, GsonResult.class);
        }
        return null;
    }


    /**
     * <B>设置代理累计充值返利</B>
     * @param channel
     * @param openType
     * @param timeType
     * @param beginTime
     * @param endTime
     * @param firstTotalDeposit
     * @param selfReward
     * @param upperReward
     * @return
     */
    public static GsonResult inviteDeposit(String channel, Integer openType, Long timeType, Long beginTime, Long endTime, Long firstTotalDeposit, Long selfReward, Long upperReward) {
        RequestInviteDeposit requestInviteDeposit = new RequestInviteDeposit();
        requestInviteDeposit.setChannel(channel);
        requestInviteDeposit.setOpenType(openType);
        requestInviteDeposit.setTimeType(timeType);

        if (timeType == 1) {
            requestInviteDeposit.setBeginTime(beginTime);
            requestInviteDeposit.setEndTime(endTime);
        }
        requestInviteDeposit.setFirstTotalDeposit(firstTotalDeposit);

        requestInviteDeposit.setSelfReward(selfReward);
        requestInviteDeposit.setUpperReward(upperReward);
        String body = new Gson().toJson(requestInviteDeposit);

        log.debug("设置代理累计充值请求:" + GAME_API_URL + INVITE_DEPOSIT_CHANNEL_URL + "Json:" + body);
        String resultStr =  HttpUtil.post(GAME_API_URL + INVITE_DEPOSIT_CHANNEL_URL,  body);
        log.debug("设置代理累计充值返回:" + resultStr);
        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            return new Gson().fromJson(resultStr, GsonResult.class);
        }
        return null;
    }


    /**
     * <B>充值赠送配置</B>
     * @param channel
     * @param beginTime
     * @param endTime
     * @param openType
     * @param cfgList
     * @return
     */
    public static GsonResult rechargeRebate(String channel, Long beginTime, Long endTime, Integer openType, List<GsonRechargeRebateCfg> cfgList) {
        RequestRechargeRebate requestRechargeRebate = new RequestRechargeRebate();
        requestRechargeRebate.setChannel(channel);
        requestRechargeRebate.setBeginTime(beginTime);

        requestRechargeRebate.setEndTime(endTime);
        requestRechargeRebate.setOpenType(openType);

        if (CollUtil.isNotEmpty(cfgList) && cfgList.size() > 0) {
            GsonRechargeRebateCfg [] rechargeRabateArr = new GsonRechargeRebateCfg [cfgList.size()];
            cfgList.toArray(rechargeRabateArr);
            requestRechargeRebate.setCfgInfo(rechargeRabateArr);
        }

        String body = new Gson().toJson(requestRechargeRebate);
        log.debug("充值赠送配置请求:" + GAME_API_URL + DEPOSIT_CHANNEL_URL + "Json:" + body);
        String resultStr =  HttpUtil.post(GAME_API_URL + DEPOSIT_CHANNEL_URL,  body);
        log.debug("充值赠送配置返回:" + resultStr);

        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            return new Gson().fromJson(resultStr, GsonResult.class);
        }
        return null;
    }

    /**
     * <B>签到配置</B>
     * @param channel
     * @param beginTime
     * @param endTime
     * @param openType
     * @param cfgList
     * @return
     */
    public static GsonResult signConfig(String channel, Long beginTime, Long endTime, Integer openType, Integer isCirculate, List< GsonSignAwardCfg > cfgList) {
        RequestSign requestSign = new RequestSign();
        requestSign.setChannel(channel);
        requestSign.setBeginTime(beginTime);

        requestSign.setEndTime(endTime);
        requestSign.setOpenType(openType);
        requestSign.setIsCirculate(isCirculate);

        if (CollUtil.isNotEmpty(cfgList) && cfgList.size() > 0) {
            GsonSignAwardCfg [] signAwardArr = new GsonSignAwardCfg [cfgList.size()];
            cfgList.toArray(signAwardArr);
            requestSign.setAwardArr(signAwardArr);
        }

        String body = new Gson().toJson(requestSign);
        log.debug("签到配置请求:" + GAME_API_URL + SIGN_CHANNEL_URL + "Json:" + body);
        String resultStr =  HttpUtil.post(GAME_API_URL + SIGN_CHANNEL_URL,  body);
        log.debug("签到配置返回:" + resultStr);
        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            return new Gson().fromJson(resultStr, GsonResult.class);
        }
        return null;
    }

    /**
     * <B>获取下级代理信息</B>
     * @param accountID
     * @return
     */
    public static GsonAgentLowerLevel getAgentLowerLevel(Integer accountID, Integer page, Integer pageSize) {
        RequestAgentLowerLevel requestAgentLowerLevel = new RequestAgentLowerLevel();
        requestAgentLowerLevel.setAccountID(accountID);
        requestAgentLowerLevel.setPage(page);
        requestAgentLowerLevel.setPageSize(pageSize);

        String body = new Gson().toJson(requestAgentLowerLevel);
        log.debug("获取下级代理请求:" + GAME_API_URL + PROMOTE_DETAIL_URL + "Json:" + body);
        String resultStr =  HttpUtil.post(GAME_API_URL + PROMOTE_DETAIL_URL,  body);
        log.debug("获取下级代理返回:" + resultStr);
        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            return new Gson().fromJson(resultStr, GsonAgentLowerLevel.class);
        }
        return null;
    }


    /**
     * <B>获取当前代理的信息</B>
     * @param accountID
     * @return
     */
    public static GsonAgent getAgentInfo(Integer accountID) {
        Map<String ,Object> paramMap = new HashMap<>();
        paramMap.put("accountID", accountID);

        log.debug("获取当前代理请求:" + GAME_API_URL + PROMOTE_INFO_URL + "Json:" + paramMap.toString());
        String resultStr =  HttpUtil.post(GAME_API_URL + PROMOTE_INFO_URL,  new Gson().toJson(paramMap));
        log.debug("获取当前代理返回:" + resultStr);
        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            return new Gson().fromJson(resultStr, GsonAgent.class);
        }
        return null;
    }

    /**
     * <B>获取百度token</B>
     * @param grant_type
     * @param client_id
     * @param client_secret
     * @return
     */
    public static GsonSound getBaiduSoundTonken(String grant_type, String client_id, String client_secret) {
        Map<String ,Object> paramMap = new HashMap<>();

        paramMap.put("grant_type", grant_type);
        paramMap.put("client_id", client_id);
        paramMap.put("client_secret", client_secret);

        log.debug("获取百度token请求:" + BAIDU_SOUND_URL + "Json:" + paramMap.toString());
        String resultStr =  HttpUtil.post(BAIDU_SOUND_URL, paramMap);
        log.debug("获取百度token返回:" + resultStr);
        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            return new Gson().fromJson(resultStr, GsonSound.class);
        }
        return null;
    }


    /**
     * <B>扣款</B>
     * @return
     */
    public static GsonReduceResult reduceFee(Integer accountID, BigDecimal amount, Integer force) {
        RequestReduceFee reduceFee = new RequestReduceFee();
        reduceFee.setAccountID(accountID);

        reduceFee.setAmount(amount);
        reduceFee.setForce(force);
        String body = new Gson().toJson(reduceFee);

        log.debug("用户扣款请求:" + BAIDU_SOUND_URL + "Json:" + body);
        String resultStr = HttpUtil.post(GAME_API_URL + DIRECTSUB_URL, body);
        log.debug("用户扣款返回:" + resultStr);
        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            return new Gson().fromJson(resultStr, GsonReduceResult.class);
        }
        return null;
    }



    /**
     * <B>获取推广人信息</B>
     * @return
     */
    public static  GsonPromoteInfo getPromoteInfo(Integer accountID) {
        RequestPromoteInfo promoteInfo = new RequestPromoteInfo();
        promoteInfo.setAccountID(accountID);

        String body = new Gson().toJson(promoteInfo);
        log.debug("获取推广人信息请求:" + GAME_API_URL + RECHARGE_CANCEL_URL + "Json:" + body);
        String resultStr = HttpUtil.post(GAME_API_URL + RECHARGE_CANCEL_URL, body);
        log.debug("获取推广人信息返回:" + resultStr);

        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            return new Gson().fromJson(resultStr, GsonPromoteInfo.class);
        }
        return null;
    }

    /**
     * <B>充值取消</B>
     * @return
     */
    public static GsonCancelResult rechargeCancel(Integer accountID, BigDecimal amount, BigDecimal rebateCoin, Integer type, String orderId, Integer force) {
        RequestRechargeCancel cancel = new RequestRechargeCancel();
        cancel.setAccountID(accountID);
        cancel.setAmount(amount);

        cancel.setType(type);
        cancel.setOrderId(orderId);
        cancel.setForce(force);

        cancel.setRebateCoin(rebateCoin);
        String body = new Gson().toJson(cancel);

        log.debug("充值取消请求:" + GAME_API_URL + RECHARGE_CANCEL_URL + "Json:" + body);
        String resultStr = HttpUtil.post(GAME_API_URL + RECHARGE_CANCEL_URL, body);
        log.debug("充值返回:" + resultStr);

        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            return new Gson().fromJson(resultStr, GsonCancelResult.class);
        }
        return null;
    }

    /**
     * <B>幸运转盘</B>
     * @param channelId
     * @param diamondcfg
     * @param goldcfg
     * @param silvercfg
     * @param start
     * @param finish
     * @param diamondCost
     * @param goldCost
     * @param silverCost
     * @return
     */
    public static GsonResult luckyRoulette(Integer openType, Long channelId, List<GsonLuckRoulette> diamondcfg, List<GsonLuckRoulette> goldcfg, List<GsonLuckRoulette> silvercfg,
                                           Long start, Long finish, Integer diamondCost, Integer goldCost, Integer silverCost) {

        RequestLuckyRoulette  luckyRoulette = new RequestLuckyRoulette();
        luckyRoulette.setChannel(channelId);
        luckyRoulette.setStart(start);
        luckyRoulette.setFinish(finish);

        luckyRoulette.setOpenType(openType);
        luckyRoulette.setDiamondCost(diamondCost);
        luckyRoulette.setGoldCost(goldCost);

        luckyRoulette.setSilverCost(silverCost);
        if (CollUtil.isNotEmpty(diamondcfg) && diamondcfg.size() > 0) {
            GsonLuckRoulette [] diamondArr = new GsonLuckRoulette [diamondcfg.size()];
            diamondcfg.toArray(diamondArr);
            luckyRoulette.setDiamond(diamondArr);
        }

        if (CollUtil.isNotEmpty(goldcfg) && goldcfg.size() > 0) {
            GsonLuckRoulette [] goldArr = new GsonLuckRoulette [goldcfg.size()];
            goldcfg.toArray(goldArr);
            luckyRoulette.setGold(goldArr);
        }

        if (CollUtil.isNotEmpty(silvercfg) && silvercfg.size() > 0) {
            GsonLuckRoulette [] silverArr = new GsonLuckRoulette [silvercfg.size()];
            silvercfg.toArray(silverArr);
            luckyRoulette.setSilver(silverArr);
        }

        String body = new Gson().toJson(luckyRoulette);
        log.debug("配置幸运轮盘请求:" + GAME_API_URL + LUCKY_ROULETTE_URL + "Json:" + body);
        String resultStr = HttpUtil.post(GAME_API_URL + LUCKY_ROULETTE_URL, body);
        log.debug("配置幸运轮盘返回:" + resultStr);

        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            return new Gson().fromJson(resultStr, GsonResult.class);
        }
        return null;
    }


    /**
     * <B>手机号码解绑</B>
     * @param accountID
     * @return
     */
    public static GsonResult unbindPhoneNum(Integer accountID) {
        RequestAccount account = new RequestAccount();
        account.setAccountID(accountID);

        String body = new Gson().toJson(account);
        log.debug("手机号码解绑请求:" + GAME_API_URL + UNBIND_PHONE_URL + "Json:" + body);
        String resultStr = HttpUtil.post(GAME_API_URL + UNBIND_PHONE_URL, body);
        log.debug("手机号码解绑返回:" + resultStr);

        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            return new Gson().fromJson(resultStr, GsonResult.class);
        }
        return null;
    }

    /**
     * <B>手机号绑定</B>
     * @param accountID
     * @return
     */
    public static GsonResult bindPhoneNum(Integer accountID, String phoneNum) {
        RequestBindPhoneNum account = new RequestBindPhoneNum();
        account.setAccountID(accountID);
        account.setPhone(phoneNum);

        String body = new Gson().toJson(account);
        log.debug("手机号码绑定请求:" + GAME_API_URL + BIND_PHONE_URL + "Json:" + body);
        String resultStr = HttpUtil.post(GAME_API_URL + BIND_PHONE_URL, body);
        log.debug("手机号码解绑请求:" + resultStr);

        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            return new Gson().fromJson(resultStr, GsonResult.class);
        }
        return null;
    }

    /**
     *  <B>清除常用设备号</B>
     * @param accountID
     * @return
     */
    public static GsonResult clearCommonDev(Integer accountID) {
        RequestAccount account = new RequestAccount();
        account.setAccountID(accountID);

        String body = new Gson().toJson(account);
        log.debug("清除常用设备请求:" + GAME_API_URL + CLEAR_DEV_URL + "Json:" + body);
        String resultStr = HttpUtil.post(GAME_API_URL + CLEAR_DEV_URL, body);
        log.debug("清除常用设备返回:" + resultStr);

        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            return new Gson().fromJson(resultStr, GsonResult.class);
        }
        return null;
    }


    /**
     * <B>处理玩家单控</B>
     * @param accountId
     * @param type
     * @param gameIds
     * @param betLimit
     * @param rate
     * @param round
     * @return
     */
    public static GsonResult userCtrl(Integer accountId, Integer type, Integer [] gameIds, Integer betLimit, Integer rate,  Integer round) {
        RequestUserCtrl userCtrl = new RequestUserCtrl();
        userCtrl.setAccountID(accountId);
        userCtrl.setType(type);

        userCtrl.setGameIds(gameIds);
        userCtrl.setBetLimit(betLimit);
        userCtrl.setRate(rate);

        userCtrl.setRound(round);
        String body = new Gson().toJson(userCtrl);
        log.debug("用户控制请求:" + GAME_API_URL + USER_CTRL_URL + "Json:" + body);

        String resultStr = HttpUtil.post(GAME_API_URL + USER_CTRL_URL, body );
        log.debug("取消用户控制请求:" + resultStr);
        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            return new Gson().fromJson(resultStr,  GsonResult.class);
        }
        return  null;
    }


    public static GsonResult delCtrlDate(Integer accountId,  Integer [] gameIds) {
        RequestUserCtrl userCtrl = new RequestUserCtrl();
        userCtrl.setAccountID(accountId);
        userCtrl.setType(0);
        userCtrl.setGameIds(gameIds);

        String body = new Gson().toJson(userCtrl);
        log.debug("取消控制请求:" + GAME_API_URL + USER_CTRL_URL + "Json:" + body);
        String resultStr = HttpUtil.post(GAME_API_URL + USER_CTRL_URL, body );

        log.debug("取消控制返回:" + resultStr);
        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            return new Gson().fromJson(resultStr,  GsonResult.class);
        }
        return  null;
    }


    public static GsonResult cancelUserCtrl(Integer accountId,  Integer [] gameIds) {
        RequestUserCtrl userCtrl = new RequestUserCtrl();
        userCtrl.setAccountID(accountId);
        userCtrl.setType(0);

        userCtrl.setGameIds(gameIds);
        String body = new Gson().toJson(userCtrl);

        log.debug("取消控制请求:" + GAME_API_URL + USER_CTRL_URL + "Json:" + body);
        String resultStr = HttpUtil.post(GAME_API_URL + USER_CTRL_URL, body );
        log.debug("取消控制返回:" + resultStr);

        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            return new Gson().fromJson(resultStr,  GsonResult.class);
        }
        return  null;
    }


    /**
     * <B>修改绑定手机送金</B>
     * @param regularAward
     * @param channelKey
     * @return
     */
    public static GsonResult bindGive(String channelKey, Integer regularAward, Integer rebate, Integer minExchange,
                                      Integer exchange, Integer initCoin, Integer damaMulti, Integer rebateDamaMulti,
                                      Integer exchangeCount, Integer promoteType, Integer resetDamaLeftCoin) {

        RequestBindGive bindGive = new RequestBindGive();
        bindGive.setChannelKey(channelKey);
        bindGive.setRegularAward(regularAward);

        bindGive.setRebate(rebate);
        bindGive.setMinExchange(minExchange);
        bindGive.setExchange(exchange);

        bindGive.setInitCoin(initCoin);
        bindGive.setDamaMulti(damaMulti);
        bindGive.setRebateDamaMulti(rebateDamaMulti);

        bindGive.setExchangeCount(exchangeCount);
        bindGive.setPromoteType(promoteType);
        bindGive.setResetDamaLeftCoin(resetDamaLeftCoin);
        String body = new Gson().toJson(bindGive);

        log.debug("修改绑定手机送金请求:" + GAME_API_URL + BIND_GIVE_URL + "Json:" + body);
        String resultStr = HttpUtil.post(GAME_API_URL + BIND_GIVE_URL, body );
        log.debug("修改绑定手机送金返回:" + resultStr);

        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            return new Gson().fromJson(resultStr,  GsonResult.class);
        }
        return  null;
    }


    /**optType:
     1-冻结
     2-解冻
     3-解绑第三方账号(暂时不做), 有参数thirdType, thirdType为1表示微信登录
     * <B>玩家状态修改</B>
     * @param accountId
     * @param operatorType
     * @return
     */
    public static GsonResult operateAccount(Integer accountId, Integer operatorType, Integer thirdType) {
        RequestOperateAccount requestOperateAccount = new RequestOperateAccount();
        requestOperateAccount.setAccountID(accountId);

        requestOperateAccount.setOptType(operatorType);
        if (thirdType != null && thirdType != 0) {
            RequestOperateAccount.OptParam opt = requestOperateAccount.new OptParam();
            opt.setThirdType(thirdType);
            requestOperateAccount.setOptType(operatorType);
        }

        String body = new Gson().toJson(requestOperateAccount);
        log.debug("账号冻结解冻请求:" + GAME_API_URL + OPERATE_ACCOUNT_URL + "Json:" + body);
        String resultStr = HttpUtil.post(GAME_API_URL + OPERATE_ACCOUNT_URL, body );
        log.debug("账号冻结解冻返回:" + resultStr);

        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            return new Gson().fromJson(resultStr,  GsonResult.class);
        }
        return  null;
    }

    /**
     * <B> 获取IP 地址信息</B>
     * @param ip
     * @return
     */
    public static String getIpaddress(String ip, String type) {
        HashMap<String, Object> paramMap = new HashMap<>();
        paramMap.put("ip", ip);

        //判断是淘宝还是新浪
        if (StrUtil.isNotBlank(type) && type.equalsIgnoreCase(UrlConstant.IpAddress.IP_TAOBAO.getName())) {
            return HttpUtil.post(UrlConstant.IpAddress.IP_TAOBAO.getValue(), paramMap);
        } else if  (StrUtil.isNotBlank(type) && type.equalsIgnoreCase(UrlConstant.IpAddress.IP_SINA.getName())) {
            return HttpUtil.post(UrlConstant.IpAddress.IP_SINA.getValue(), paramMap);
        } else {
            return HttpUtil.post(UrlConstant.IpAddress.IP_126.getValue(), paramMap);
        }
    }

    /**
     * <B>同步白名单信息</B>
     * @param
     * @param accountList
     * @param operatorType
     * @return
     */
    public static GsonResult synWhiteList(int operatorType, Integer [] accountList) {
        //OperatorType 操作类型 0-添加 1-取消
        //target 0-添加 1-取消
        RequestWhite requestWhite = new RequestWhite();
        requestWhite.setAccountList(accountList);

        requestWhite.setOperatorType(operatorType);
        String body = new Gson().toJson(requestWhite);

        log.debug("账号冻结解冻请求:" + GAME_API_URL + WITELIST_URL + "Json:" + body);
        String resultStr = HttpUtil.post(GAME_API_URL + WITELIST_URL, body );
        log.debug("账号冻结解冻返回:" + resultStr);
        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            return new Gson().fromJson(resultStr,  GsonResult.class);
        }
        return  null;
    }

    /**
     * <B>平台维护</B>
     * @param operatorType
     * @param start
     * @param finish
     * @return
     */
    public static GsonResult doPlatfromMaintenance(int operatorType , int start, int finish) {
        //operatorType 操作类型  1-维护  2-取消维护
        //start 开始时间， 取消维护 start为0
        //finish 结束时间， 取消维护 finish为0
        RequestPlatformMaint request = new RequestPlatformMaint();
        request.setFinish(finish);
        request.setStart(start);

        request.setOperatorType(operatorType);
        String body = new Gson().toJson(request);

        log.debug("平台维护请求:" + GAME_API_URL + PLATFORM_MAINTENANCE_URL + "Json:" + body);
        String resultStr = HttpUtil.post(GAME_API_URL + PLATFORM_MAINTENANCE_URL, body );
        log.debug("平台维护返回:" + resultStr);
        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            return new Gson().fromJson(resultStr,  GsonResult.class);
        }
        return  null;
    }


    /**
     * <B>游戏维护</B>
     * @param operatorType
     * @param start
     * @param finish
     * @return
     */
    public static GsonResult doGameMaintenance(int operatorType , int start, int finish, Integer [] gameList) {
        RequestGameMaint request = new RequestGameMaint();
        request.setStart(start);
        request.setFinish(finish);

        request.setOperatorType(operatorType);
        request.setGameList(gameList);
        String body = new Gson().toJson(request);

        log.debug("游戏维护请求:" + GAME_API_URL + GAME_MAINTENANCE_URL + "Json:" + body);
        String resultStr = HttpUtil.post(GAME_API_URL + GAME_MAINTENANCE_URL, body );
        log.debug("游戏维护返回:" + resultStr);
        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            return new Gson().fromJson(resultStr,  GsonResult.class);
        }
        return  null;
    }



    /**
     * <B>修改机器人信息</B>OK
     * @return
     */
    public static GsonResult modifyBotInfo(Integer id , String nickName) {
        RequestBot request = new RequestBot();
        request.setId(id);
        request.setNick(nickName);
        String body = "{["  + new Gson().toJson(request) +"]}";

        log.debug("修改机器人信息请求:" + GAME_API_URL + MODIFY_AI_URL + "Json:" + body);
        String resultStr = HttpUtil.post(GAME_API_URL + MODIFY_AI_URL, body);
        log.debug("修改机器人信息返回:" + resultStr);

        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            return new Gson().fromJson(resultStr,  GsonResult.class);
        }
        return  null;
    }


    /**
     * <B>添加公告信息</B>
     * @param guid
     * @param start
     * @param finish
     * @return
     */
    public static GsonResult addBulletin(String guid, Integer start, Integer finish, String title, String content, String hyperlink, String [] channleList) {
        // guid 公告Id， 唯一id
        // start 开始时间
        // finish 结束时间
        // displayType 显示方式(*必填 1:静态显示 2 : 弹窗显示 3 : 下拉显示 4 : 滚动显示)
        // displayFrequence 显示频率(*必填 1：每填只弹一次 2：登陆显示一次 3：只显示一次 4：指定频率显示 5：一次性公告)
        // playFrequence 公告播放频率，只针对displayFrequence为4的情况下，指定显示频率
        // Title 公告标题
        // Hyperlink 公告链接
        // productList 公告发送的渠道(1020000001等，数组）json数组
        // gameList 公告显示的游戏（暂时填空），json数组

        String bulletinStr = packBulletin(guid, start, finish, title, content, hyperlink, channleList);
        log.debug("添加公告信息请求:" + GAME_API_URL + ADD_BULLETIN_URL + "Json:" + bulletinStr);
        String resultStr = HttpUtil.post(GAME_API_URL + ADD_BULLETIN_URL, bulletinStr);
        log.debug("添加公告信息返回:" + resultStr);

        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            return new Gson().fromJson(resultStr, GsonResult.class);
        }
        return  null;
    }



    private static String packBulletin(String guid, Integer start, Integer finish,  String title, String content, String hyperlink, String [] channleList) {
        RequestBulletin request = new RequestBulletin();
        request.setGuid(guid);
        request.setStart(start);
        request.setFinish(finish);

        request.setDisplayType(1);
        request.setTitle(title);
        request.setContent(content);

        request.setHyperLink(hyperlink);
        request.setChannelList(channleList);
        String body = new Gson().toJson(request);
        return body;
    }



    public static GsonResult modifyBulletin(String guid, Integer start, Integer finish,  String title, String content, String hyperlink, String [] channleList) {
        // guid 公告Id， 唯一id
        // start 开始时间
        // finish 结束时间
        // displayType 显示方式(*必填 1:静态显示 2 : 弹窗显示 3 : 下拉显示 4 : 滚动显示)
        // displayFrequence 显示频率(*必填 1：每填只弹一次 2：登陆显示一次 3：只显示一次 4：指定频率显示 5：一次性公告)
        // playFrequence 公告播放频率，只针对displayFrequence为4的情况下，指定显示频率
        // Title 公告标题
        // Hyperlink 公告链接
        // productList 公告发送的渠道(1020000001等，数组）json数组
        // gameList 公告显示的游戏（暂时填空），json数组
        String resultStr = HttpUtil.post(GAME_API_URL + MODIFY_BULLETIN_URL, packBulletin(guid, start, finish, title, content, hyperlink, channleList));
        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            return new Gson().fromJson(resultStr, GsonResult.class);
        }
        return  null;
    }


    public static GsonResult delBulletin(String guid) {
        // guid 公告Id， 唯一id
        // start 开始时间
        // finish 结束时间
        // displayType 显示方式(*必填 1:静态显示 2 : 弹窗显示 3 : 下拉显示 4 : 滚动显示)
        // displayFrequence 显示频率(*必填 1：每填只弹一次 2：登陆显示一次 3：只显示一次 4：指定频率显示 5：一次性公告)
        // playFrequence 公告播放频率，只针对displayFrequence为4的情况下，指定显示频率
        // Title 公告标题
        // Hyperlink 公告链接
        // productList 公告发送的渠道(1020000001等，数组）json数组
        // gameList 公告显示的游戏（暂时填空），json数组
        StringBuffer sb = new StringBuffer();
        sb.append("{");
        sb.append('"');
        sb.append("list");
        sb.append('"');
        sb.append("[");
        sb.append('"');
        sb.append(guid);
        sb.append('"');
        sb.append("]");
        sb.append("}");

        String resultStr = HttpUtil.post(GAME_API_URL + DELETE_BULLETIN_URL, sb.toString());
        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            return new Gson().fromJson(resultStr, GsonResult.class);
        }
        return  null;
    }


    public static GsonResult batchDelBulletin(String [] guid) {
        // guid 公告Id， 唯一id
        // start 开始时间
        // finish 结束时间
        // displayType 显示方式(*必填 1:静态显示 2 : 弹窗显示 3 : 下拉显示 4 : 滚动显示)
        // displayFrequence 显示频率(*必填 1：每填只弹一次 2：登陆显示一次 3：只显示一次 4：指定频率显示 5：一次性公告)
        // playFrequence 公告播放频率，只针对displayFrequence为4的情况下，指定显示频率
        // Title 公告标题
        // Hyperlink 公告链接
        // productList 公告发送的渠道(1020000001等，数组）json数组
        // gameList 公告显示的游戏（暂时填空），json数组
        StringBuffer sb = new StringBuffer();
        sb.append("{");
        sb.append('"');
        sb.append("list");
        sb.append('"');
        sb.append(":[");
        for (int i = 0 ; i < guid.length ; i ++) {
            sb.append('"');
            sb.append(guid[i]);
            sb.append('"');
            if (i < guid.length - 1) {
                sb.append(",");
            }
        }
        sb.append("]}");

        log.debug("批量删除公告信息请求:" + GAME_API_URL + DELETE_BULLETIN_URL + "Json:" + sb.toString());
        String resultStr = HttpUtil.post(GAME_API_URL + DELETE_BULLETIN_URL, sb.toString());
        log.debug("批量删除公告信息返回:" + sb.toString());

        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            return new Gson().fromJson(resultStr, GsonResult.class);
        }
        return  null;
    }



    /**
     * <B>更新渠道配置文件</B>
     * @param channelKey
     * @param name
     * @param versionNo
     * @param channelId
     */
    public String modifyChannelVersion(String channelKey, String name, String versionNo, List<String> channelId) {
        //channelKey 渠道号
        // name 渠道名称
        // versionNo 更新版本号
        // nodeIds 渠道开放功能，依据platform_openFunction.csv配置文件id字段，用逗号分隔

        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("channelKey", channelKey);
        paramMap.put("name", name);

        paramMap.put("versionNo", versionNo);
        paramMap.put("channelId", channelId);
        return HttpUtil.post(CHANNEL_URL, paramMap);
    }


    /**
     * <B>获取游戏状态信息</B>
     */
    public static List<GsonGameState>  getGameState() {
        String resultStr =  HttpUtil.post(GAME_API_URL + GAME_STATE_URL, "");
        List<GsonGameState> list = new ArrayList<GsonGameState>();

        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            JsonParser parser = new JsonParser();
            JsonArray jsonArray = parser.parse(resultStr).getAsJsonArray();
            for (JsonElement elment : jsonArray) {

                //使用GSON，直接转成Bean对象
                GsonGameState gsonGameState = new Gson().fromJson(elment, GsonGameState.class);
                list.add(gsonGameState);
            }
        }
        return list;
    }


    /**
     * <B>获取平台状态信息</B>
     */
    public static GsonPlatformState getPlatformState() {
        String resultStr = HttpUtil.post(GAME_API_URL + PLATFORM_STATE_URL, "");
        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            return new Gson().fromJson(resultStr, GsonPlatformState.class);
        }
        return  null;
    }


    /**
     * <B>踢人</B>
     */
    public static GsonResult kickUser(Integer [] accountList){
        // accounts 指定玩家踢人
        RequestKickUser request = new RequestKickUser();
        request.setAccounts(accountList);
        String body = new Gson().toJson(request);

        log.debug("踢人请求:" + GAME_API_URL + KICK_USER_URL + "Json:" + body);
        String resultStr = HttpUtil.post(GAME_API_URL + KICK_USER_URL, body);
        log.debug("踢人返回:" + resultStr);

        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            return new Gson().fromJson(resultStr,  GsonResult.class);
        }
        return  null;
    }


    /**
     * <B>发送邮件</B>
     */
    public static GsonResult sendMail(String title, String content, String rewardId, String rewardNum, int [] channelList, int [] toUserList , Integer sendType){
        // batchId 批次号，作为业务关联标识
        // id 邮件ID
        // title 邮件标题
        // content 邮件内容
        // isGroupMail 是否群邮件
        // isTemplateMail 是否模板邮件
        // isTextMail 是否纯文本邮件，纯文本邮件没有附件
        // sendDate 发送时间 时间戳
        // sender 发送者
        // items 邮件附件
        // templateID 如果是模板邮件，这里是模板邮件ID, 0标识不是
        // customParams 如果是模板邮件，这里是模板邮件需要填充的参数，json数据
        // productIDList 发送给那些渠道
        // toUserList 发送给那些玩家, 玩家间用逗号隔开,如："1000001,1000002,1000003"
        RequestMail request = new RequestMail();
        RequestMail.Mail mail = request.new Mail();
        RequestMail.SendCondition sendCondition = request.new SendCondition();

        //复制信息
        mail.setTitle(title);
        mail.setContent(content);
        mail.setSendDate(DateUtil.currentSeconds());
        mail.setSender("STSTEM");

        //判断是否存在附件
        if (StrUtil.isNotBlank(rewardId) && StrUtil.isNotBlank(rewardNum)) {
            RequestMail.Mail.RewardList rewardList = mail.new RewardList();
            rewardList.setId(Integer.parseInt(rewardId));

            rewardList.setNum(Integer.parseInt(rewardNum) * 100);
            RequestMail.Mail.RewardList [] array = new RequestMail.Mail.RewardList[1];
            array[0] = rewardList;
            mail.setRewardList(array);
        }

        sendCondition.setSendType(sendType);
        if (channelList != null && channelList.length > 0) {
            sendCondition.setChannelList(channelList);
        }

        if (toUserList != null && toUserList.length > 0) {
            sendCondition.setToUserList(toUserList);
        }

        request.setSendCondition(sendCondition);
        request.setMail(mail);
        String body = new Gson().toJson(request);

        log.debug("发送邮件请求:" + GAME_API_URL + SEND_EMAIL_URL + "Json:" + body);
        String resultStr = HttpUtil.post(GAME_API_URL + SEND_EMAIL_URL, body);
        log.debug("发送邮件返回:" + resultStr);

        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            return new Gson().fromJson(resultStr,  GsonResult.class);
        }
        return  null;
    }


    /**
     * <B> 查询用户信息</B>
     * @param userId
     * @return
     */
    public static  GsonUserInfo queryAccountInfo(String userId) {
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("accountID", userId);

        log.debug("查询用户信息请求:" + GAME_API_URL + USER_URL + "Json:" + paramMap.toString());
        String resultStr = HttpUtil.post(GAME_API_URL + USER_URL, paramMap);
        log.debug("查询用户信息返回:" + resultStr);

        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            return new Gson().fromJson(resultStr,  GsonUserInfo.class);
        }
        return  null;
    }


    /**
     * <B>查询玩家操作</B>
     * @param userId
     * @param optType
     * @param thirdType
     * @return
     */
    public static String operateAccount(String userId, String optType, String thirdType) {
        //accountID 玩家ID
        //optType 操作类型 1-冻结 2-解冻 3-第三方账号解绑 4-账号重命名（账号名改为:原账号名-玩家ID) 5-绑定账号为手机号（手机账号转正）
        //thirdType 操作扩展内容 如果操作类型是3，有效， 这个值表示第三方账号类型： 1-微信
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("accountID", userId);
        paramMap.put("optType", optType);
        paramMap.put("thirdType", thirdType);
        return HttpUtil.post(GAME_API_URL + USER_OPERATION_URL, paramMap);
    }


    /**
     *<B>GM命令</B>OK
     * @return
     */
    public static GsonResult gmAction(String domainCode, String cmd) {
        String resultStr = HttpRequest.post(GAME_API_URL + GM_URL + domainCode).contentType("Text").timeout(10).body(cmd).execute().body();

        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            return new Gson().fromJson(resultStr,  GsonResult.class);
        }
        return  null;
    }


    /**
     * <B>上分</B>
     * @param accountID
     * @param orderId
     * @return
     */
    public static GsonResult rechargeOrder(Integer accountID, String orderId, Integer amount, Integer code, Integer type, Integer rate) {
        //accountID 玩家ID
        // order 订单ID
        RequestRecharge request = new RequestRecharge();
        request.setAccountID(accountID);
        request.setOrderId(orderId);

        request.setAmount(amount);
        request.setCode(code);
        request.setType(type);
        //设置赠送率
        if (rate != null && rate > 0) {
            double d = (double) rate/100;
            request.setRate(d);
        }

        String body = new Gson().toJson(request);
        log.debug("用户上分请求:" + GAME_API_URL + RECHARGE_RESULT_URL + "Json:" + body);
        String resultStr = HttpUtil.post(GAME_API_URL + RECHARGE_RESULT_URL, body);
        log.debug("用户上分返回:" + resultStr);
        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            return new Gson().fromJson(resultStr,  GsonResult.class);
        }
        return  null;
    }


    public static GsonResult handerRechargeOrder(Integer accountID, String orderId, Integer amount, Integer code, Integer type, Integer isManual, Double damaMulti, Integer rate) {
        //accountID 玩家ID
        // order 订单ID
        RequesthandRecharge request = new RequesthandRecharge();
        request.setAccountID(accountID);
        request.setOrderId(orderId);

        request.setAmount(amount);
        request.setCode(code);
        request.setType(type);

        //1表示手动
        request.setIsManual(isManual);
        //设置赠送率
        if (rate != null && rate > 0) {
            double d = (double) rate/100;
            request.setRate(d);
        }

        request.setDamaMulti(damaMulti);
        String body = new Gson().toJson(request);
        System.out.println("用户手动上分返回:" + GAME_API_URL + RECHARGE_RESULT_URL + "Json:" + body);
        log.debug("用户手动上分请求:" + GAME_API_URL + RECHARGE_RESULT_URL + "Json:" + body);
        String resultStr = HttpUtil.post(GAME_API_URL + RECHARGE_RESULT_URL, body);
        log.debug("用户手动上分返回:" + GAME_API_URL + RECHARGE_RESULT_URL + "Json:" + resultStr);
        System.out.println("用户手动上分返回:" + GAME_API_URL + RECHARGE_RESULT_URL + "Json:" + resultStr);
        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            return new Gson().fromJson(resultStr,  GsonResult.class);
        }
        return  null;
    }


    /**
     * <B>下分</B>
     * @param userId
     * @param orderId
     * @return
     */
    public static GsonResult cashOrder(Integer userId, String orderId, Integer cashFee) {
        //accountID 玩家ID
        // order 订单ID
        RequestCash request = new RequestCash();
        request.setAccountID(userId);
        request.setCashNo(orderId);
        request.setCashFee(cashFee);
        String body = new Gson().toJson(request);

        log.debug("用户提现请求:" + GAME_API_URL + CASH_RESULT_URL + "Json:" + body);
        String resultStr = HttpUtil.post(GAME_API_URL + CASH_RESULT_URL, body);
        log.debug("用户提现返回:" + resultStr);

        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            return new Gson().fromJson(resultStr,  GsonResult.class);
        }
        return  null;
    }

    /**
     * <B>修改用户密码</B>OK
     * @param userId
     * @param oldPassword
     * @param newPassword
     * @param check
     * @return
     */
    public static GsonResult modifyAccountPassword(int userId, String oldPassword, String newPassword, Boolean check) {
        // accountID 玩家ID
        // oldPassword 旧密码
        // newPassword 新密码
        // check 是否检查旧密码是否一致 true表示要检查
        RequestModifyAccountPw request = new RequestModifyAccountPw();
        request.setAccountID(userId);
        request.setCheck(check);

        request.setOldPassword(oldPassword);
        request.setNewPassword(newPassword);
        String body = new Gson().toJson(request);

        log.debug("用户修改密码请求:" + GAME_API_URL + MODIFY_ACCOUNT_PASSWORD_URL + "Json:" + body);
        String resultStr = HttpUtil.post(GAME_API_URL + MODIFY_ACCOUNT_PASSWORD_URL, body);
        log.debug("用户修改密码返回:" + resultStr);

        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            return new Gson().fromJson(resultStr,  GsonResult.class);
        }
        return  null;
    }


    /**
     * <B>获取用户所在游戏</B>OK
     * @param
     * @param
     * @return
     */
    public static GsonUserGame getUserGameList(Integer userId) {
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("accountID", userId);

        String json = new Gson().toJson(paramMap);
        log.debug("查询用户所在游戏请求:" + GAME_API_URL + ON_LINE_USER_URL + "Json:" + json);
        String resultStr = HttpUtil.post(GAME_API_URL + ON_LINE_USER_URL, json);
        log.debug("查询用户所在游戏返回:" + resultStr);

        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            return new Gson().fromJson(resultStr, GsonUserGame.class);
        }
        return  null;
    }


    /**
     * <B>请求提现订单结果</B>
     * @param userId
     * @return
     */
    public static GsonResult cashOrderResult(Integer userId, String cashNo, Integer code, String msg) {
        RequestCashAol request = new RequestCashAol();
        request.setAccountID(userId);
        request.setCashNo(cashNo);

        request.setCode(code);
        request.setMsg(msg);
        String body = new Gson().toJson(request);

        log.debug("提现订单结果请求:" + GAME_API_URL + CASH_ORDER_RESULT_URL + "Json:" + body);
        String resultStr = HttpUtil.post(GAME_API_URL + CASH_ORDER_RESULT_URL, body);
        log.debug("提现订单结果返回:" + resultStr);

        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            return new Gson().fromJson(resultStr,  GsonResult.class);
        }
        return  null;
    }


    /**
     * <B>获取验证码信息</B>
     * @param accountID
     * @param phone
     * @param reqType
     * @return
     */
    public static String getVerifyCode(String accountID, String phone, String reqType) {
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("accountID", accountID);
        paramMap.put("phone", phone);
        paramMap.put("reqType", reqType);

        String resultStr = HttpUtil.get(GAME_API_URL + RECHARGE_RESULT_URL, paramMap);
        return "";
    }


    /**
     * <B>验证验证码信息</B>
     * @param accountID
     * @param verifyCode
     * @param phone
     * @param reqType
     * @return
     */
    public static String checkVerifyCode(String accountID, String verifyCode, String phone, String reqType) {
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("accountID", accountID);
        paramMap.put("verifyCode", verifyCode);

        paramMap.put("phone", phone);
        paramMap.put("reqType", reqType);
        String resultStr = HttpUtil.get(GAME_API_URL + RECHARGE_RESULT_URL, paramMap);
        return "";
    }



    /**
     * <B>解除绑定手机号码</B>
     * @param userId
     * @return
     */
    public static String unbindPhoneNum(String userId) {
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("accountID", userId);
        return HttpUtil.post(GAME_API_URL + UNBIND_PHONENUM_URL, paramMap);
    }


    /**
     * <B>清除常用设备号</B>
     * @param userId
     * @return
     */
    public static String clearCommonDev(String userId) {
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("accountID", userId);
        return HttpUtil.post(GAME_API_URL + CLEAR_COMMON_DEV_URL, paramMap);
    }

    /**
     * <B>清除绑定身份证号码</B>
     * @param userId
     * @return
     */
    public static String clearIdCard(String userId) {
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("accountID", userId);
        return HttpUtil.post(GAME_API_URL + CLEAR_ID_CARD_URL, paramMap);
    }

}
