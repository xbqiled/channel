package com.channel.common.utils;

/**
 * Redis所有Keys
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @since 2018-11-16
 */
public class RedisKeys {

    public static String getSysConfigKey(String key){
        return "sys:config:" + key;
    }

    public static String getShiroSessionKey(String key){
        return "sessionid:" + key;
    }
}
