package com.channel.common.utils;

import java.util.HashMap;


/**
 * Map工具类
 *
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @since 2018-11-16
 */
public class MapUtils extends HashMap<String, Object> {

    @Override
    public MapUtils put(String key, Object value) {
        super.put(key, value);
        return this;
    }
}
