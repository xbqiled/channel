package com.channel.common.constant;

/**
 * 常量
 *
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @since 2018-11-16
 */
public class Constant {
	/** 超级管理员ID */
	public static final int SUPER_ADMIN = 1;
    /** 数据权限过滤 */
	public static final String SQL_FILTER = "sql_filter";




    public enum PAYORDR_STATUS {
        /**
         * 目录
         */
        WAIT("0", "等待支付"),

        SUCCESS("1", "支付成功"),

        FAIL("2", "支付失败");

        PAYORDR_STATUS(String value, String description) {
            this.value = value;
        }

        /**
         * 类型
         */
        private String value;
        /**
         * 描述
         */
        private String description;

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }
    }


    public enum CASHORDR_STATUS {
        /**
         * 目录
         */
        APPLY("0", "提现申请"),

        PASS("1", "审批通过"),

        FAIL("2", "审批失败");

        CASHORDR_STATUS(String value, String description) {
            this.value = value;
        }

        /**
         * 类型
         */
        private String value;
        /**
         * 描述
         */
        private String description;

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }
    }



    /**
	 * 菜单类型
	 */
    public enum MenuType {
        /**
         * 目录
         */
    	CATALOG(0),
        /**
         * 菜单
         */
        MENU(1),
        /**
         * 按钮
         */
        BUTTON(2);

        private int value;

        MenuType(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }
    
    /**
     * 定时任务状态
     */
    public enum ScheduleStatus {
        /**
         * 正常
         */
    	NORMAL(0),
        /**
         * 暂停
         */
    	PAUSE(1);

        private int value;

        ScheduleStatus(int value) {
            this.value = value;
        }
        
        public int getValue() {
            return value;
        }
    }

    /**
     * 云服务商
     */
    public enum CloudService {
        /**
         * 七牛云
         */
        QINIU(1),
        /**
         * 阿里云
         */
        ALIYUN(2),
        /**
         * 腾讯云
         */
        QCLOUD(3);

        private int value;

        CloudService(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }

}
