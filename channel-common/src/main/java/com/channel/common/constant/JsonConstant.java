package com.channel.common.constant;


public class JsonConstant {

    public enum JSON_RESULT {
        /**
         * 目录
         */
        SUCCESS("0", "成功"),

        FAIL("-2", "失败");

        JSON_RESULT(String value, String description) {
            this.value = value;
            this.description = description;
        }

        /**
         * 类型
         */
        private String value;
        /**
         * 描述
         */
        private String description;

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }
    }
}
