package com.channel.common.constant;




public class UrlConstant {

    public static String CHANNEL_SYSTM_URL  = "http://channel.xbqpthe.com";

    public static String MANAGER_SYSTM_URL  = "http://manager.xbqpthe.com";

    public static String API_SYSTM_URL  = "http://api.xbqpthe.com";

    public static String FEONT_SYSTM_URL  = "http://front.xbqpthe.com";

    public static String BULLETIN_URL = "/bulletin/show/";

    //public static String GAME_API_URL =  "http://43.249.30.51:8080/xinbo.cgi?";
    //测试
    //public static String GAME_API_URL =  "http://182.16.19.226:8080/xinbo.cgi?";

    //正式
//    public static String GAME_API_URL =  "http://10.10.40.4:8080/xinbo.cgi?";
     public static String GAME_API_URL =  "http://192.168.10.11:8080/xinbo.cgi?";

    //更新配置信息
    public static String  UPDATE_CONFIG_URL = "target=hall&function=reloadCfg";

    //同步白名单
    public static String  WITELIST_URL = "target=list&function=updateWhiteList";

    //平台维护
    public static String  PLATFORM_MAINTENANCE_URL = "target=list&function=platformMaintenance";

    //游戏维护
    public static String  GAME_MAINTENANCE_URL = "target=list&function=gameMaintenance";

    //修改机器人信息
    public static String MODIFY_AI_URL = "target=AI&function=NtyAINickModify";

    //添加公告
    public static String ADD_BULLETIN_URL = "target=bulletin&function=addBulletin";

    //修改公告
    public static String MODIFY_BULLETIN_URL = "target=bulletin&function=modBulletin";

    //删除公告
    public static String DELETE_BULLETIN_URL = "target=bulletin&function=delBulletin";

    //更新渠道版本信息
    public static String CHANNEL_URL = "target=list&function=switchCtrl";

    //系统维护
    public static String  MAINTAIN_URL = "target=list&function=gameMaintenance";

    //获取各个游戏的游戏状态信息
    public static String  GAME_STATE_URL = "target=list&function=gameState";

    //获取平台的状态信息
    public static String  PLATFORM_STATE_URL = "target=list&function=platformState";

    //踢人
    public static String  KICK_USER_URL = "target=list&function=kickAccount";

    //修改机器人信息
    public static String  MODIFY_BOT_URL = "target=list&function=kickAccount";

    //发送邮件
    public static String  SEND_EMAIL_URL = "target=mail&function=sendMail";

    //查询玩家信息
    public static String  USER_URL = "target=hall&function=queryAccountInfoById";

    //玩家操作
    public static String  USER_OPERATION_URL = "target=hall&function=operateAccount";

    //GM操作
    public static String  GM_URL = "target=GM&serviceID=";

    //手机号码解绑
    public static String  UNBIND_PHONENUM_URL = "target=hall&function=unbindPhoneNum";

    //清除常用设备号
    public static String  CLEAR_COMMON_DEV_URL = "target=hall&function=clearCommonDev";

    //清除绑定身份号
    public static String  CLEAR_ID_CARD_URL = "target=hall&function=clearIDCard";

    //插入充值订单
    public static String  RECHARGE_ORDER_URL = "target=hall&function=insertRechargeOrder";

    //后台给玩家充值gm命令
    public static String RECHARGE_GM_URL = "target=GM&serviceID=0x00000000";

    //充值回调
    public static String RECHARGE_CALLBACK_URL = "target=hall&function=rechargeCallNotify";

    //修改账户密码
    public static String MODIFY_ACCOUNT_PASSWORD_URL = "target=hall&function=modifyAccountPasswd";

    //查询在线玩家
    public static String ON_LINE_USER_URL = "target=hall&function=getGameViewList";

    //后台获取验证码
    public static String GET_VERIFY_URL = "target=hall&function=getVerifyCode";

    //后台获取验证码
    public static String CHECK_VERIFY_URL = "target=hall&function=checkVerifyCode";

    //提现结果返回接口
    public static String CASH_ORDER_RESULT_URL = "target=hall&function=auditExchangeResult";

    //上分接口
    public static String RECHARGE_RESULT_URL = "target=hall&function=rechargeResult";

    //下分接口
    public static String CASH_RESULT_URL = "target=hall&function=directExchangeResult";

    //修改玩家状态接口
    public static String OPERATE_ACCOUNT_URL = "target=hall&function=operateAccount";

    //修改绑定送金
    public static String BIND_GIVE_URL = "target=list&function=channelCtrl";

    //处理
    public static String USER_CTRL_URL = "target=hall&function=userGameCtrl";

    //手机绑定
    public static String BIND_PHONE_URL = "target=hall&function=bindPhone";

    //手机解绑
    public static String UNBIND_PHONE_URL = "target=hall&function=unbindPhoneNum";

    //清除常用设备号
    public static String CLEAR_DEV_URL = "target=hall&function=clearCommonDev";

    //幸运转盘
    public static String LUCKY_ROULETTE_URL = "target=hall&function=luckyRoulette";

    //支付撤销
    public static String RECHARGE_CANCEL_URL = "target=hall&function=rechargeCancel";

    //扣款
    public static String DIRECTSUB_URL = "target=hall&function=directSub";

    //查看代理对应的信息
    public static String PROMOTE_INFO_URL = "target=relation&function=getPromoteInfo";

    //签到配置
    public static String SIGN_CHANNEL_URL = "target=hall&function=signInChannel";

    //充值返利配置
    public static String DEPOSIT_CHANNEL_URL = "target=hall&function=depositChannel";

    //代理充值返利
    public static String INVITE_DEPOSIT_CHANNEL_URL = "target=hall&function=inviteFirstTotalDeposit";

    //手动设置玩家上级
    public static String ADD_RELATION_URL = "target=hall&function=handAddRelation";

    //获取用户打码量信息
    public static String GET_USER_TRADING_URL = "target=hall&function=queryDamaInfo";

    //修改用户打码量信息
    public static String CHANGE_USER_TRADING_URL = "target=hall&function=opDamaInfo";

    //配置鬥地主獎勵接口
    public static String UPDATE_DDZ_MATCHRANKAWARD_URL = "target=rank&function=updateDddzMatchRankAward";

    //配置修改真实姓名接口
    public static String MODIFY_REALNAME_URL = "target=hall&function=modifyRealName";

    //配置暱稱獎勵接口
    public static String EDIT_NICK_AWARD_URL = "target=hall&function=editNickAward";

    //查询汇总信息
    //public static String PROMOTE_GATHER_URL = "target=relation&function=getPromoteInfo";

    //查询明细信息
    public static String PROMOTE_DETAIL_URL = "target=relation&function=getRefferal";

    //绑定推广域名
    public static String BIND_PROMOTER_URL = "target=list&function=bindPromoterUrl";

    //每日分享
    public static String DAY_SHARE_URL = "target=hall&function=channelDayShare";

    //配置vip
    public static String VIP_URL = "target=hall&function=channelVip";

    //配置救济金
    public static String SUCCOUR_URL = "target=hall&function=channelSuccour";

    //配置
    public static String TASK_URL = "target=hall&function=channelTask";

    //消息推送
    public static String MESSAGE_URL = "target=push&function=addPush";

    //更新用户备注
    public static String USERNOTE_URL = "target=hall&function=userNote";

    //淘宝免费请求接口(http://ip.taobao.com/service/getIpInfo.php?ip=116.212.151.133)
    public static String  IP_TAOBAO =  "http://ip.taobao.com/service/getIpInfo.php";

    //新浪免费请求接口(http://int.dpool.sina.com.cn/iplookup/iplookup.php?format=json&ip=218.4.255.255)
    public static String  IP_SINA =  "http://ip.taobao.com/service/getIpInfo.php";

    //126免费请求接口(http://ip.ws.126.net/ipquery?ip=116.212.151.133)
    public static String  IP_126 = " http://ip.ws.126.net/ipquery";

    //百度语言请求
    public static String BAIDU_SOUND_URL = "https://openapi.baidu.com/oauth/2.0/token";

    /**
     * IP地址
     */
    public enum IpAddress {
        IP_TAOBAO(UrlConstant.IP_TAOBAO, "taobao"),

        IP_SINA(UrlConstant.IP_SINA, "sina"),

        IP_126(UrlConstant.IP_126, "126");

        private String value;
        private String name;

        IpAddress(String value, String name) {
            this.value = value;
            this.name = name;
        }

        public String  getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }


    /**
     * IP白名单
     */
    public enum WiterListType {
        LOGIN("login"),

        HALL("hall");

        private String value;

        WiterListType(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }
    }


    /**
     * 短信服务商
     */
    public enum ApproveStatus {
        APPLY_STATUS(0),

        SUCCESS_STATUS(1),

        FAIL_STATUS(2);

        private int value;

        ApproveStatus(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }
}
