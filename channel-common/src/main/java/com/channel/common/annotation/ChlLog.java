package com.channel.common.annotation;

import java.lang.annotation.*;

/**
 * 系统日志注解
 *
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @since  2018-11-17
 *
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface ChlLog {
	String value() default "";
}
