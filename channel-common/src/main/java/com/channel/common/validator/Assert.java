package com.channel.common.validator;

import com.channel.common.execption.AppException;
import org.apache.commons.lang.StringUtils;

/**
 * 数据校验
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @since 2018-11-16
 */
public abstract class Assert {

    public static void isBlank(String str, String message) {
        if (StringUtils.isBlank(str)) {
            throw new AppException(message);
        }
    }

    public static void isNull(Object object, String message) {
        if (object == null) {
            throw new AppException(message);
        }
    }
}
