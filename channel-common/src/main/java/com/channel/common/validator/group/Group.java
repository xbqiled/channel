package com.channel.common.validator.group;

import javax.validation.GroupSequence;

/**
 * 定义校验顺序，如果AddGroup组失败，则UpdateGroup组不会再校验
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @since 2018-11-16
 */
@GroupSequence({AddGroup.class, UpdateGroup.class})
public interface Group {

}
