package com.channel.common.validator;



import com.channel.common.execption.AppException;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import java.util.Set;

/**
 * hibernate-validator校验工具类
 *
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @since 2018-11-16
 */
public class ValidatorUtils {
    private static Validator validator;

    static {
        validator = Validation.buildDefaultValidatorFactory().getValidator();
    }

    /**
     * 校验对象
     * @param object 待校验对象
     * @param groups 待校验的组
     * @throws AppException 校验不通过，则报RRException异常
     */
    public static void validateEntity(Object object, Class<?>... groups) throws AppException {
        Set<ConstraintViolation<Object>> constraintViolations = validator.validate(object, groups);
        if (!constraintViolations.isEmpty()) {
            ConstraintViolation<Object> constraint = (ConstraintViolation<Object>) constraintViolations.iterator().next();
            throw new AppException(constraint.getMessage());
        }
    }
}
