package com.channel.common.board;


import lombok.Data;

import java.io.Serializable;


@Data
public class EsydCardRecord implements Serializable {

     Integer area;
     Long bet;
     Integer [] cards;

     Integer dim;
     Integer point;
     Integer tax;

     Integer insure;
     Integer win;

}
