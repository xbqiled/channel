package com.channel.common.json;

import lombok.Data;

import java.io.Serializable;


@Data
public class RequestVip implements Serializable {

    private String channel;

    private Integer openType;

    private GsonVip vip1;

    private GsonVip vip2;

    private GsonVip vip3;

    private GsonVip vip4;

    private GsonVip vip5;

    private GsonVip vip6;

    private GsonVip vip7;

    private GsonVip vip8;

    private GsonVip vip9;

}
