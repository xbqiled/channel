package com.channel.common.json;


import lombok.Data;

import java.io.Serializable;


@Data
public class GsonPlatformState implements Serializable {

    private  int start;
    private  int finish;
    private  int status;

}
