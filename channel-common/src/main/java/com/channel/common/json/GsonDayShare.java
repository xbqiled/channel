package com.channel.common.json;

import lombok.Data;

import java.io.Serializable;



@Data
public class GsonDayShare  implements Serializable {

    private Integer totalDay;
    private Long additionAward;

}
