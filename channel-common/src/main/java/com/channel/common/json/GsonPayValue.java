package com.channel.common.json;

import lombok.Data;

import java.io.Serializable;

@Data
public class GsonPayValue implements Serializable {

   private Long payValue;

   private Long showValue;

}
