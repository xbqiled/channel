package com.channel.common.json;

import lombok.Data;

import java.io.Serializable;


@Data
public class GsonUserTrading implements Serializable {

    Integer ret;
    String msg;
    Integer accountID;
    Long damaNeed;
    Long damaAmount;
}
