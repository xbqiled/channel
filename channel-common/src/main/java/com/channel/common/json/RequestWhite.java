package com.channel.common.json;


import lombok.Data;

import java.io.Serializable;


@Data
public class RequestWhite implements Serializable {
    private int operatorType;
    private Integer accountList[];
}
