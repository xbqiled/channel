package com.channel.common.json;

import lombok.Data;

import java.io.Serializable;

@Data
public class GsonPromoteInfo implements Serializable {

    private Integer ret;
    private String msg;
    private Integer accountID;

    private Long promoterId;
    private Long totalNum;
    private Long nextNum;
    private Long histWin;

    private Long canTakeAward;
    private Long nextContribute;
    private Long otherContribute;

    private Integer[] list;

}
