package com.channel.common.json;

import lombok.Data;

import java.io.Serializable;


@Data
public class RequestAddRelation implements Serializable {

    private String channel;
    private Long accountId;
    private Long promoterId;

}
