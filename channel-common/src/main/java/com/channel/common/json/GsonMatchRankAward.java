package com.channel.common.json;


import lombok.Data;

import java.io.Serializable;


@Data
public class GsonMatchRankAward implements Serializable {

    Long rank1;
    Long rank2;
    Long award;
}
