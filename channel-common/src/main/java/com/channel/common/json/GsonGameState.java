package com.channel.common.json;


import lombok.Data;

import java.io.Serializable;


@Data
public class GsonGameState implements Serializable {

    private int status;
    private int finish;
    private int id;
    private int start;
    private int num;

}
