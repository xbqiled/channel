package com.channel.common.json;

import lombok.Data;

import java.io.Serializable;


@Data
public class RequestRealName implements Serializable {

    private Long accountID;
    private String realName;

}
