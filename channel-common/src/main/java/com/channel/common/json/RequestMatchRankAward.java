package com.channel.common.json;

import lombok.Data;

import java.io.Serializable;


@Data
public class RequestMatchRankAward implements Serializable {

    private String channel;
    private Integer option;
    private Integer openType;

    private GsonMatchRankAward [] awards;
}
