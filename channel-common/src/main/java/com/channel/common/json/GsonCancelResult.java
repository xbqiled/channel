package com.channel.common.json;

import lombok.Data;

import java.io.Serializable;



@Data
public class GsonCancelResult implements Serializable {
    private int ret;
    private String msg;
    private Long realCancel;
    private Long rebateCoin;
}
