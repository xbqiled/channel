package com.channel.common.json;

import lombok.Data;

import java.io.Serializable;


@Data
public class GsonCashAli implements Serializable {

    private String payId;
    private String realName;

}
