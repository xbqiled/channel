package com.channel.common.json;

import lombok.Data;

import java.io.Serializable;


@Data
public class RequestPromoteInfo implements Serializable {
    private Integer accountID;
}
