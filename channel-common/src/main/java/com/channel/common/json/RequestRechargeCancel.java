package com.channel.common.json;


import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;


@Data
public class RequestRechargeCancel implements Serializable {

    private Integer accountID;
    private BigDecimal amount;
    private Integer type;
    private String orderId;
    private Integer force;
    private BigDecimal rebateCoin;

}
