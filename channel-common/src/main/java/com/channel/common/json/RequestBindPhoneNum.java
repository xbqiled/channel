package com.channel.common.json;

import lombok.Data;

import java.io.Serializable;


@Data
public class RequestBindPhoneNum implements Serializable {
    private Integer accountID;
    private String  phone;
}
