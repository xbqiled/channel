package com.channel.common.json;

import lombok.Data;

import java.io.Serializable;


@Data
public class GsonRechargeRebateCfg implements Serializable {

    private Long minCoin;
    private Long maxCoin;
    private Integer rebateType;
    private Long rebateContent;

}

