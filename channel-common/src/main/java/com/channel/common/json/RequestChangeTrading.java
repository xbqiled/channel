package com.channel.common.json;

import lombok.Data;

import java.io.Serializable;


@Data
public class RequestChangeTrading implements Serializable {
    Integer accountID;
    Integer opType;
    Long damaChange;
}
