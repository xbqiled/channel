package com.channel.common.json;

import lombok.Data;

import java.io.Serializable;


@Data
public class RequestModifyAccountPw implements Serializable {

    int accountID;
    String oldPassword;
    String newPassword;
    boolean check;

}
