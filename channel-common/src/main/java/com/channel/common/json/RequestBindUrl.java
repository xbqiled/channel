package com.channel.common.json;


import lombok.Data;

import java.io.Serializable;

@Data
public class RequestBindUrl implements Serializable {
    private Integer opType;
    private String url;
    private String channel;
    private Long accountID;

}
