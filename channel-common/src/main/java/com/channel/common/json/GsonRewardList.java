package com.channel.common.json;

import lombok.Data;

import java.io.Serializable;


@Data
public class GsonRewardList implements Serializable {
    private String id;
    private Long num;
}
