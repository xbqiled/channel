package com.channel.common.json;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class GsonChannelTask implements Serializable {

    private String channel;

    private Integer activityTimeType;

    private Integer activityOpenType;

    private Long beginTime;

    private Long endTime;

    private GsonTask [] taskArr;

}
