package com.channel.common.json;

import lombok.Data;

import java.io.Serializable;


@Data
public class GsonResult implements Serializable {

    private int ret;
    private String msg;
    private Long rebateCoin;
}
