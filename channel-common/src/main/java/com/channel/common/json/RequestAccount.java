package com.channel.common.json;

import lombok.Data;

import java.io.Serializable;


@Data
public class RequestAccount implements Serializable {

   private Integer accountID;

}
