package com.channel.common.json;


import lombok.Data;

import java.io.Serializable;

@Data
public class RequestKickUser implements Serializable {
    private Integer accounts[];
}
