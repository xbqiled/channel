package com.channel.common.json;


import lombok.Data;

import java.io.Serializable;

@Data
public class RequestAgentLowerLevel implements Serializable {

    Integer accountID;
    Integer page;
    Integer pageSize;

}
