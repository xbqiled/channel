package com.channel.common.json;

import lombok.Data;

import java.io.Serializable;


@Data
public class GsonVip implements Serializable {

    private Long totalDeposit;

    private Long upgradeAward;

    private Integer addSignInPercent;

    private Long weekAward;

    private Long monthAward;

    private Long succourCnt;
}
