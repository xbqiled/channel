package com.channel.common.json;


import lombok.Data;

import java.io.Serializable;


@Data
public class RequestDayShare implements Serializable {

    private Long channel;
    private Integer openType;
    private Long dayAward;

    private Long awardDayShareCnt;
    private GsonDayShare[] totalDayArr;

}
