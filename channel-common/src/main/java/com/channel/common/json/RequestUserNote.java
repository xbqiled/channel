package com.channel.common.json;

import lombok.Data;

import java.io.Serializable;


@Data
public class RequestUserNote  implements Serializable {
    Integer accountId;
    String note;
}
