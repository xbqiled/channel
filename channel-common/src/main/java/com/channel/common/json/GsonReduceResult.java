package com.channel.common.json;

import lombok.Data;

import java.io.Serializable;


/**
 *
 */
@Data
public class GsonReduceResult implements Serializable {

    private int ret;
    private String msg;
    private Long realCancel;

    public GsonReduceResult(int ret) {
        this.ret = ret;
    }


}
