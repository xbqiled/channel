package com.channel.common.json;


import lombok.Data;

import java.io.Serializable;


@Data
public class GsonUserGame implements Serializable {

    private int ret;

    private String msg;

    private GameList[] list;

    @Data
    public class GameList {
        Integer game;
        String name;
        Long coin;
    }

}
