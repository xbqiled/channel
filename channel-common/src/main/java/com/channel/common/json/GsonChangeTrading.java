package com.channel.common.json;

import lombok.Data;

import java.io.Serializable;


@Data
public class GsonChangeTrading implements Serializable {

    private Integer ret;
    private Integer accountID;
    private Long damaNeed;
    private Long damaAmount;
    private String  msg;
}
