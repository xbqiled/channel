package com.channel.common.json;

import lombok.Data;

import java.io.Serializable;


@Data
public class RequestNick implements Serializable {

    Integer openType;
    String channel;
    Integer award;
}
