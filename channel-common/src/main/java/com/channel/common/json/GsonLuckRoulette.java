package com.channel.common.json;

import lombok.Data;

import java.io.Serializable;


@Data
public class GsonLuckRoulette implements Serializable {

    private Integer award;
    private Integer pos;
    private Integer rate;
    private Integer res;

}
