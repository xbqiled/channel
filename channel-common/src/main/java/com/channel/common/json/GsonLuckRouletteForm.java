package com.channel.common.json;

import lombok.Data;

import java.io.Serializable;


@Data
public class GsonLuckRouletteForm implements Serializable {

    private Integer award;
    private Integer pos;
    private Integer rate;
    private Integer res;
    private String name;
    private Integer value;

}
