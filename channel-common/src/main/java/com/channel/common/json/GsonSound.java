package com.channel.common.json;

import lombok.Data;

import java.io.Serializable;


@Data
public class GsonSound implements Serializable {

    private String access_token;
    private String session_key;
    private String scope;

    private String refresh_token;
    private String session_secret;
    private String expires_in;

}
