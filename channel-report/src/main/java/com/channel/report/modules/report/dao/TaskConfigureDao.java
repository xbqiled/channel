package com.channel.report.modules.report.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.channel.api.entity.report.TaskConfigureEntity;

/**
 * 
 * 
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-12-30 13:46:48
 */
public interface TaskConfigureDao extends BaseMapper<TaskConfigureEntity> {
	
}
