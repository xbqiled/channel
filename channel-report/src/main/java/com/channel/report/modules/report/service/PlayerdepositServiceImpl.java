package com.channel.report.modules.report.service;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import com.channel.api.entity.chl.ChlUserEntity;
import com.channel.api.entity.report.PlayerdepositEntity;
import com.channel.api.service.report.PlayerdepositService;
import com.channel.api.service.report.RecordlogingameService;
import com.channel.common.constant.ConfigConstant;
import com.channel.common.constant.TableNameConstant;
import com.channel.report.modules.report.dao.PlayerdepositDao;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.channel.common.utils.PageUtils;
import com.channel.common.utils.Query;

import static com.channel.common.constant.ConfigConstant.FULL_DATE_TIME_FORMAT;
import static com.channel.common.constant.ConfigConstant.THE_DATE_FORMAT;


@Service("playerdepositService")
@com.alibaba.dubbo.config.annotation.Service(
        interfaceClass = PlayerdepositService.class,
        timeout = 10000,
        version = "${api.service.version}"
)
public class PlayerdepositServiceImpl extends ServiceImpl<PlayerdepositDao, PlayerdepositEntity> implements PlayerdepositService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        Page<PlayerdepositEntity> page = this.selectPage(
                new Query<PlayerdepositEntity>(params).getPage(),
                new EntityWrapper<PlayerdepositEntity>()
        );

        return new PageUtils(page);
    }


    @Override
    public PageUtils queryDepositPage(Map<String, Object> params, ChlUserEntity userEntity) {
        String current = (String) params.get(ConfigConstant.LIMIT);
        String size = (String) params.get(ConfigConstant.PAGE);
        List<Map<String, Object>> list = new ArrayList<>();

        //根据拓展Server获取渠道ID
        String channelId = ObjectUtil.isNotNull(userEntity.getChannelId()) ? userEntity.getChannelId() : "";
        String userId = ObjectUtil.isNotNull(params.get("userId")) ? (String) params.get("userId") : "";
        String orderId = ObjectUtil.isNotNull(params.get("orderId")) ? (String) params.get("orderId") : "";

        String startTime = ObjectUtil.isNotNull(params.get("startTime")) ? (String) params.get("startTime") : "";
        String endTime = ObjectUtil.isNotNull(params.get("endTime")) ? (String) params.get("endTime") : "";
        Page<Map<String, Object>> page = new Page<>(new Integer(size), new Integer(current));

        String tableName = TableNameConstant.PLAYDEPOSIT_VIEW;
        Date startDate = DateUtil.parse(startTime, FULL_DATE_TIME_FORMAT);
        Date endDate = DateUtil.parse(endTime, FULL_DATE_TIME_FORMAT);

        try {
            Long intervalTime = DateUtil.betweenDay(startDate, endDate, Boolean.TRUE);
            if (intervalTime <= 0 && (startTime.substring(0, 10).equals(endTime.substring(0, 10)))) {
                tableName = TableNameConstant.PLAYDEPOSIT_TABLENAME + DateUtil.format(startDate, THE_DATE_FORMAT);
            }

            list = this.baseMapper.queryDepositPage(page, tableName, channelId, orderId, userId, startTime, endTime);
            return new PageUtils(page.setRecords(list));
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            return  new PageUtils(page.setRecords(list));
        }
    }



}
