package com.channel.report.modules.report.service;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.channel.api.entity.chl.ChlUserEntity;
import com.channel.api.entity.report.UserbetscorechangeEntity;
import com.channel.api.service.report.UserbetscorechangeService;
import com.channel.common.constant.ConfigConstant;
import com.channel.common.utils.PageUtils;
import com.channel.common.utils.Query;
import com.channel.report.modules.report.dao.UserbetscorechangeDao;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


@Service("userbetscorechangeService")
@com.alibaba.dubbo.config.annotation.Service(
        interfaceClass = UserbetscorechangeService.class,
        timeout = 10000,
        version = "${api.service.version}"
)
public class UserbetscorechangeServiceImpl extends ServiceImpl<UserbetscorechangeDao, UserbetscorechangeEntity> implements UserbetscorechangeService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        Page<UserbetscorechangeEntity> page = this.selectPage(
                new Query<UserbetscorechangeEntity>(params).getPage(),
                new EntityWrapper<UserbetscorechangeEntity>()
        );

        return new PageUtils(page);
    }


    @Override
    public PageUtils queryRecordPage(Map<String, Object> params, ChlUserEntity userEntity) {
        String current = (String) params.get(ConfigConstant.LIMIT);
        String size = (String) params.get(ConfigConstant.PAGE);
        Page<Map<String, Object>> page = new Page<>(new Integer(size), new Integer(current));

        //根据拓展Server获取渠道ID
        String channelId = ObjectUtil.isNotNull(userEntity.getChannelId()) ? userEntity.getChannelId().toString() : "";
        String userId = ObjectUtil.isNotNull(params.get("userId")) ? (String) params.get("userId") : "";
        String startTime = ObjectUtil.isNotNull(params.get("startTime")) ? (String) params.get("startTime") : "";

        String type = ObjectUtil.isNotNull(params.get("type")) ? (String) params.get("type") : "";
        String endTime = ObjectUtil.isNotNull(params.get("endTime")) ? (String) params.get("endTime") : "";
        List<Map<String, Object>> list = this.baseMapper.queryRecordPage(page, channelId, type, userId, startTime, endTime);
        return new PageUtils(page.setRecords(list));
    }

    @Override
    public List<Map<String, Object>> queryOperatorList() {
        return baseMapper.queryOperatorList();
    }
}
