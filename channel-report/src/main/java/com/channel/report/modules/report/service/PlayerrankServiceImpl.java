package com.channel.report.modules.report.service;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.channel.api.entity.chl.ChlUserEntity;
import com.channel.api.entity.report.PlayerrankEntity;
import com.channel.api.service.report.PlayerrankService;
import com.channel.common.constant.ConfigConstant;
import com.channel.common.utils.PageUtils;
import com.channel.report.modules.report.dao.PlayerrankDao;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import static com.channel.common.constant.ConfigConstant.FULL_DATE_TIME_FORMAT;
import static com.channel.common.constant.ConfigConstant.THE_DATE_FORMAT;


@Service("playerrankService")
@com.alibaba.dubbo.config.annotation.Service(
        interfaceClass = PlayerrankService.class,
        timeout = 40000,
        version = "${api.service.version}"
)
public class PlayerrankServiceImpl extends ServiceImpl<PlayerrankDao, PlayerrankEntity> implements PlayerrankService {

    @Override
    public PageUtils queryPlayerRankPage(Map<String, Object> params, ChlUserEntity userEntity) {
        String current = (String) params.get(ConfigConstant.LIMIT);
        String size = (String) params.get(ConfigConstant.PAGE);
        Page<Map<String, Object>> page = new Page<>(new Integer(size), new Integer(current));

        List<Map<String, Object>> list = new ArrayList<>();
        //根据拓展Server获取渠道ID
        String channelId = ObjectUtil.isNotNull(userEntity.getChannelId()) ? userEntity.getChannelId() : "";
        String userId = ObjectUtil.isNotNull(params.get("userId")) ? (String) params.get("userId") : "";
        String startTime = ObjectUtil.isNotNull(params.get("startTime")) ? (String) params.get("startTime") : "";
        String endTime = ObjectUtil.isNotNull(params.get("endTime")) ? (String) params.get("endTime") : "";

        String gameTypeId = ObjectUtil.isNotNull(params.get("gameTypeId")) ? (String) params.get("gameTypeId") : "";
        String matches = ObjectUtil.isNotNull(params.get("matches")) ? (String) params.get("matches") : "";
        String matchRank = ObjectUtil.isNotNull(params.get("matchRank")) ? (String) params.get("matchRank") : "";

        String tableName = "v_matchplayerrank";
        Date startDate = DateUtil.parse(startTime, FULL_DATE_TIME_FORMAT);
        Date endDate = DateUtil.parse(endTime, FULL_DATE_TIME_FORMAT);

        try {
        //计算相隔天数
        Long intervalTime = DateUtil.betweenDay(startDate, endDate, Boolean.TRUE);
        if (intervalTime <= 0 && (startTime.substring(0, 10).equals(endTime.substring(0, 10)))) {
            tableName = "matchplayerrank" + DateUtil.format(startDate, THE_DATE_FORMAT);
        }

        list = this.baseMapper.queryPlayerRankPage(page, tableName, channelId, gameTypeId, matches, matchRank, userId, startTime, endTime);
        return  new PageUtils(page.setRecords(list));
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            return  new PageUtils(page.setRecords(list));
        }
    }
}
