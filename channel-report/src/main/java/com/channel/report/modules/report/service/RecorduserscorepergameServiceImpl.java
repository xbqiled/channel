package com.channel.report.modules.report.service;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.channel.api.entity.chl.ChlUserEntity;
import com.channel.api.entity.report.RecorduserscorepergameEntity;
import com.channel.api.service.report.RecordloginplatformService;
import com.channel.api.service.report.RecorduserscorepergameService;
import com.channel.common.constant.ConfigConstant;
import com.channel.report.modules.report.dao.RecorduserscorepergameDao;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.channel.common.utils.PageUtils;
import com.channel.common.utils.Query;

import static com.channel.common.constant.ConfigConstant.DATE_FORMAT;
import static com.channel.common.constant.ConfigConstant.FULL_DATE_TIME_FORMAT;
import static com.channel.common.constant.ConfigConstant.THE_DATE_FORMAT;


@Service("recorduserscorepergameService")
@com.alibaba.dubbo.config.annotation.Service(
        interfaceClass = RecorduserscorepergameService.class,
        timeout = 30000,
        version = "${api.service.version}"
)
public class RecorduserscorepergameServiceImpl extends ServiceImpl<RecorduserscorepergameDao, RecorduserscorepergameEntity> implements RecorduserscorepergameService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        Page<RecorduserscorepergameEntity> page = this.selectPage(
                new Query<RecorduserscorepergameEntity>(params).getPage(),
                new EntityWrapper<RecorduserscorepergameEntity>()
        );

        return new PageUtils(page);
    }


    @Override
    public PageUtils queryGameBetPage(Map<String, Object> params, ChlUserEntity userEntity) {
        String current = (String)params.get(ConfigConstant.LIMIT);
        String size  = (String)params.get(ConfigConstant.PAGE);

        //根据拓展Server获取渠道ID
        String channelId = ObjectUtil.isNotNull(userEntity.getChannelId()) ? userEntity.getChannelId() : "";
        String gameRoomId = ObjectUtil.isNotNull(params.get("gameRoomId")) ? (String)params.get("gameRoomId") : "";
        String statisTime = ObjectUtil.isNotNull(params.get("statisTime")) ? (String)params.get("statisTime") : "";

        String tableName = "recorduserscorepergame";
        Date statisDate = DateUtil.parse(statisTime, DATE_FORMAT);
        if (statisDate  != null) {
            tableName = "recorduserscorepergame" + DateUtil.format(statisDate, THE_DATE_FORMAT);
        }

        List<String> tableList = this.baseMapper.isHaveTab(tableName);
        Page<Map<String, Object>> page = new Page<>(new Integer(size), new Integer(current));
        if (tableList != null && tableList.size() >0) {
            List<Map<String, Object>> list = this.baseMapper.queryGameBetPage(page, tableName, channelId,  gameRoomId);
            return  new PageUtils(page.setRecords(list));
        } else {
            List<Map<String, Object>> list = new ArrayList<>();
            return  new PageUtils(page.setRecords(list));
        }
    }


    @Override
    public PageUtils queryUserScoreperGame(Map<String, Object> params, ChlUserEntity userEntity) {

        String current = (String)params.get(ConfigConstant.LIMIT);
        String size  = (String)params.get(ConfigConstant.PAGE);

        //根据拓展Server获取渠道ID
        String channelId = ObjectUtil.isNotNull(userEntity.getChannelId()) ? userEntity.getChannelId() : "";
        String userId = ObjectUtil.isNotNull(params.get("userId")) ? (String)params.get("userId") : "";
        String recordId = ObjectUtil.isNotNull(params.get("recordId")) ? (String)params.get("recordId") : "";

        String gameRoomId = ObjectUtil.isNotNull(params.get("gameRoomId")) ? (String)params.get("gameRoomId") : "";
        String startTime = ObjectUtil.isNotNull(params.get("startTime")) ? (String)params.get("startTime") : "";
        String endTime = ObjectUtil.isNotNull(params.get("endTime")) ? (String)params.get("endTime") : "";

        String tableName = "v_recorduserscorepergame";
        Date startDate = DateUtil.parse(startTime, FULL_DATE_TIME_FORMAT);
        Date endDate = DateUtil.parse(endTime, FULL_DATE_TIME_FORMAT);

        //计算相隔天数
        Long intervalTime = DateUtil.betweenDay(startDate, endDate, Boolean.TRUE);
        if (intervalTime <= 0 && (startTime.substring(0, 10).equals(endTime.substring(0, 10)))) {
            tableName = "recorduserscorepergame" + DateUtil.format(startDate, THE_DATE_FORMAT);
        }

        String order = ObjectUtil.isNotNull(params.get("order")) ? (String)params.get("order") : "";
        String sortby = ObjectUtil.isNotNull(params.get("sortby")) ? (String)params.get("sortby") : "";

        //生成order by 语句
        String orderStr = packOrderStr(order, sortby);
        Page<Map<String, Object>> page = new Page<>(new Integer(size), new Integer(current));
        List<Map<String, Object>> list = this.baseMapper.queryUserScorePerGamePage(page, tableName,
                channelId, recordId, userId, gameRoomId, startTime, endTime, orderStr);
        return  new PageUtils(page.setRecords(list));
    }

    String packOrderStr(String order, String sortby) {
        StringBuffer sb = new StringBuffer();
        if (StrUtil.isBlank(order) || StrUtil.isBlank(sortby)) {
            return "  order by  a.endTime  desc  ";
        } else {
            sb.append("order by ");
            if (sortby.equals("BeforeCount")) {
                sb.append(" a.BeforeCount  ");
            } else if (sortby.equals("Score")) {
                sb.append(" a.Score ");
            } else if (sortby.equals("AfterCount")) {
                sb.append(" a.AfterCount ");
            } else if (sortby.equals("GainCount")) {
                sb.append(" a.GainCount ");
            } else if (sortby.equals("Revenue")) {
                sb.append(" a.Revenue ");
            } else if (sortby.equals("Stake")) {
                sb.append(" a.Stake ");
            } else if (sortby.equals("EndTime")) {
                sb.append(" a.EndTime ");
            } else if (sortby.equals("StartTime")) {
                sb.append(" a.StartTime ");
            }
            sb.append(order);
            return sb.toString();
        }
    }
}
