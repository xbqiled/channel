package com.channel.report.modules.report.dao;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.channel.api.entity.report.RecordlogingameEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * VIEW
 * 
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-05-29 12:12:03
 */
public interface RecordlogingameDao extends BaseMapper<RecordlogingameEntity> {

    /**
     * <B>查询登录游戏信息</B>
     * @param page
     * @param channelId
     * @param userId
     * @param startTime
     * @param endTime
     * @return
     */
     List<Map<String, Object>> queryLoginGameReport(Pagination page, @Param("tableName") String tableName, @Param("channelId") String channelId, @Param("userId") String userId, @Param("startTime") String startTime, @Param("endTime") String endTime);
}
