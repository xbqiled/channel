package com.channel.report.modules.report.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.channel.api.entity.report.PlayerrankEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * VIEW
 * 
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-09-17 15:12:03
 */
public interface PlayerrankDao extends BaseMapper<PlayerrankEntity> {

    List<Map<String, Object>> queryPlayerRankPage(Pagination page, @Param("tableName") String tableName,  @Param("channelId") String channelId, @Param("gameTypeId") String gameTypeId,
                                                  @Param("matches") String matches, @Param("matchRank") String matchRank,
                                                  @Param("userId") String userId, @Param("startTime") String startTime, @Param("endTime") String endTime);
	
}
