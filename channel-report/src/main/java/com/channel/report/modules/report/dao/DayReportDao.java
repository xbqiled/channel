package com.channel.report.modules.report.dao;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.channel.api.entity.report.DayReportEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-02-24 14:04:07
 */
public interface DayReportDao extends BaseMapper<DayReportEntity> {

    //查询游戏次数
    List<Map<String, Object>> queryPlayGameCount(@Param("channelId") String channelId, @Param("startTime") String startTime, @Param("endTime") String endTime);

    //查询echar信息
    List<DayReportEntity> queryEcharReport(Pagination page, @Param("channelId") String channelId);

    //日报信息
    List<Map<String, Object>> queryDayReport(Pagination page, @Param("channelId") String channelId, @Param("startTime") String startTime, @Param("endTime") String endTime);

    //月报信息
    List<Map<String, Object>> queryMonthReport(Pagination page, @Param("channelId")String channelId,  @Param("monthTime")String monthTime);


    /**
     * <B>获取龙虎大战牌局信息</B>
     * @param page
     * @param channelId
     * @param userId
     * @param recordId
     * @param startTime
     * @param endTime
     * @return
     */
    List<Map<String, Object>>  queryLhBoard(Pagination page, @Param("tableName")String tableName, @Param("channelId")String channelId, @Param("userId")String userId, @Param("recordId")String recordId,  @Param("startTime")String startTime, @Param("endTime")String endTime);


    /**
     * <B>获取斗地主牌局信息</B>
     * @param page
     * @param recordId
     * @return
     */
    List<Map<String, Object>>  queryDdzBoard(Pagination page, @Param("boardTableName")String boardTableName,  @Param("extTableName")String extTableName,
                                             @Param("channelId")String channelId, @Param("userId")String userId,  @Param("recordId")String recordId,
                                             @Param("startTime")String startTime, @Param("endTime")String endTime);


    /**
     * <B>通过ID获取斗地主牌局</B>
     * @param recordId
     * @return
     */
    Map<String, Object> getDdzBoardById(@Param("boardTableName")String boardTableName,  @Param("extTableName")String extTableName, @Param("recordId") String recordId , @Param("userId") String userId);


    /**
     * <B>获取斗地主牌局信息</B>
     * @param page
     * @param recordId
     * @return
     */
    List<Map<String, Object>>  queryPdkBoard(Pagination page, @Param("boardTableName")String boardTableName,  @Param("extTableName")String extTableName,
                                             @Param("channelId")String channelId, @Param("userId")String userId,  @Param("recordId")String recordId,
                                             @Param("startTime")String startTime, @Param("endTime")String endTime);


    /**
     * <B>通过ID获取斗地主牌局</B>
     * @param recordId
     * @return
     */
    Map<String, Object> getPdkBoardById(@Param("boardTableName")String boardTableName,  @Param("extTableName")String extTableName, @Param("recordId") String recordId , @Param("userId") String userId);


    /**
     * <B>获取捕鱼牌局信息</B>
     * @param page
     * @param boardTableName
     * @param recordId
     * @param startTime
     * @param endTime
     * @return
     */
    List<Map<String, Object>>  queryByBoard(Pagination page, @Param("boardTableName")String boardTableName, @Param("channelId")String channelId, @Param("userId")String userId,  @Param("recordId")String recordId,  @Param("startTime")String startTime, @Param("endTime")String endTime);


    /**
     * <B>通过ID获取捕鱼牌局</B>
     * @param boardTableName
     * @param recordId
     * @param userId
     * @return
     */
    Map<String, Object> getByBoardById(@Param("boardTableName")String boardTableName,  @Param("recordId") String recordId , @Param("userId") String userId);



    /**
     * <B>获取牛牛牌局信息</B>
     * @param page
     * @param channelId
     * @param userId
     * @param recordId
     * @param startTime
     * @param endTime
     * @return
     */
    List<Map<String, Object>>  queryNnBoard(Pagination page, @Param("boardTableName")String boardTableName, @Param("channelId")String channelId, @Param("userId")String userId, @Param("recordId")String recordId,  @Param("startTime")String startTime, @Param("endTime")String endTime);


    /**
     * <B>通过ID获取牛牛牌局信息</B>
     * @param boardTableName
     * @param recordId
     * @param userId
     * @return
     */
    Map<String, Object> getNnBoardById(@Param("boardTableName")String boardTableName,  @Param("recordId") String recordId , @Param("userId") String userId);



    /**
     * <B>获取比花色牌局</B>
     * @param page
     * @param channelId
     * @param userId
     * @param recordId
     * @param startTime
     * @param endTime
     * @return
     */
    List<Map<String, Object>>  queryBhsBoard(Pagination page, @Param("boardTableName")String boardTableName, @Param("channelId")String channelId, @Param("userId")String userId, @Param("recordId")String recordId,  @Param("startTime")String startTime, @Param("endTime")String endTime);

    /**
     * <B>通过ID获取比花色牌局信息</B>
     * @param boardTableName
     * @param recordId
     * @param userId
     * @return
     */
    Map<String, Object> getBhsBoardById(@Param("boardTableName")String boardTableName,  @Param("recordId") String recordId , @Param("userId") String userId);


    /**
     * <B>获取财神到牌局信息</B>
     * @param page
     * @param recordId
     * @return
     */
    List<Map<String, Object>>  queryCsdBoard(Pagination page, @Param("boardTableName")String boardTableName, @Param("channelId")String channelId, @Param("userId")String userId,  @Param("recordId")String recordId,  @Param("startTime")String startTime, @Param("endTime")String endTime);


    /**
     * <B>财神到牌局</B>
     * @param recordId
     * @return
     */
    String queryCsdboardDetail(@Param("recordId") String recordId);

    /**
     * <B>获取炸金花局信息</B>
     * @param page
     * @param channelId
     * @param userId
     * @param recordId
     * @return
     */
    List<Map<String, Object>>  queryZjhBoard(Pagination page, @Param("boardTableName")String boardTableName, @Param("extTableName")String extTableName,  @Param("channelId")String channelId, @Param("userId")String userId, @Param("recordId")String recordId,   @Param("startTime")String startTime, @Param("endTime")String endTime);


    /**
     * <B>通过ID获取炸金花牌局</B>
     * @param boardTableName
     * @param recordId
     * @return
     */
    Map<String, Object> getZjhBoardById(@Param("boardTableName")String boardTableName,  @Param("recordId") String recordId);

    /**
     * <B>获取百家乐牌局</B>
     * @param page
     * @param channelId
     * @param userId
     * @param recordId
     * @return
     */
    List<Map<String, Object>>  queryBjlBoard(Pagination page, @Param("boardTableName")String boardTableName,  @Param("channelId")String channelId, @Param("userId")String userId, @Param("recordId")String recordId,  @Param("startTime")String startTime, @Param("endTime")String endTime);



    /**
     * <B>通过ID获取百家乐牌局</B>
     * @param boardTableName
     * @param recordId
     * @param userId
     * @return
     */
    Map<String, Object> getBjlBoardById(@Param("boardTableName")String boardTableName,  @Param("recordId") String recordId , @Param("userId") String userId);



    /**
     * <B>获取红黑大战牌局</B>
     * @param page
     * @param channelId
     * @param userId
     * @param recordId
     * @return
     */
    List<Map<String, Object>>  queryHhdzBoard(Pagination page, @Param("boardTableName")String boardTableName, @Param("channelId")String channelId, @Param("userId")String userId, @Param("recordId")String recordId,   @Param("startTime")String startTime, @Param("endTime")String endTime);


    /**
     * <B>获取红黑大战的牌局</B>
     * @param recordId
     * @return
     */
    Map<String, Object> queryHhBoardDetail(@Param("boardTableName")String boardTableName, @Param("recordId") String recordId, @Param("userId")String userId);



    /**
     * <B>获取红包接龙牌局</B>
     * @param page
     * @param channelId
     * @param userId
     * @param recordId
     * @return
     */
    List<Map<String, Object>> queryHbjlBoard(Pagination page, @Param("boardTableName")String boardTableName, @Param("channelId") String channelId, @Param("userId") String userId, @Param("recordId") String recordId,  @Param("startTime")String startTime, @Param("endTime")String endTime);




    /**
     * <B>获取抢庄牛牛牌局</B>
     * @param page
     * @param channelId
     * @param userId
     * @param recordId
     * @return
     */
    List<Map<String, Object>> queryQznnBoard(Pagination page, @Param("boardTableName")String boardTableName, @Param("channelId") String channelId, @Param("userId") String userId, @Param("recordId") String recordId, @Param("startTime") String startTime, @Param("endTime") String endTime);



    /**
     * <B>通过ID获取抢庄牛牛牌局</B>
     * @param recordId
     * @return
     */
    Map<String, Object> getQznnBoardById(@Param("boardTableName") String boardTableName, @Param("recordId") String recordId , @Param("userId") String userId);



    /**
     * <B>通过飞禽走兽获取牌局</B>
     * @param page
     * @param channelId
     * @param userId
     * @param recordId
     * @param startTime
     * @param endTime
     * @return
     */
    List<Map<String, Object>> queryFqzsBoard(Pagination page, @Param("boardTableName")String boardTableName,  @Param("channelId") String channelId, @Param("userId") String userId, @Param("recordId") String recordId, @Param("startTime") String startTime, @Param("endTime") String endTime);



    /**
     * <B>通过ID获取飞禽走兽牌局</B>
     * @param recordId
     * @return
     */
    Map<String, Object> getFqzsBoardById( @Param("boardTableName")String boardTableName, @Param("recordId") String recordId , @Param("userId") String userId);


    /**
     * <B>获取奔驰宝马牌局</B>
     * @param page
     * @param channelId
     * @param userId
     * @param recordId
     * @param startTime
     * @param endTime
     * @return
     */
    List<Map<String, Object>> queryBcbmBoard(Pagination page, @Param("boardTableName")String boardTableName, @Param("channelId") String channelId, @Param("userId") String userId, @Param("recordId") String recordId, @Param("startTime") String startTime, @Param("endTime") String endTime);


    /**
     * <B>根据ID 获取奔驰宝马牌局</B>
     * @param recordId
     * @param userId
     * @return
     */
    Map<String, Object> getBcbmBoardById(@Param("boardTableName")String boardTableName, @Param("recordId") String recordId , @Param("userId") String userId);



    /**
     * <B>获取水果机牌局</B>
     * @param page
     * @param channelId
     * @param userId
     * @param recordId
     * @param startTime
     * @param endTime
     * @return
     */
    List<Map<String, Object>> querySgjBoard(Pagination page, @Param("boardTableName")String boardTableName, @Param("channelId") String channelId, @Param("userId") String userId, @Param("recordId") String recordId, @Param("startTime") String startTime, @Param("endTime") String endTime);



    /**
     * <B>根据ID 获取水果机牌局</B>
     * @param recordId
     * @param userId
     * @return
     */
    Map<String, Object> getSgjboardDetail(@Param("boardTableName")String boardTableName, @Param("recordId") String recordId , @Param("userId") String userId);



    /**
     * <B>获取二十一点牌局</B>
     * @param page
     * @param channelId
     * @param userId
     * @param recordId
     * @param startTime
     * @param endTime
     * @return
     */
    List<Map<String, Object>> queryEsydBoard(Pagination page, @Param("boardTableName")String boardTableName, @Param("channelId") String channelId, @Param("userId") String userId, @Param("recordId") String recordId, @Param("startTime") String startTime, @Param("endTime") String endTime);



    /**
     * <B>根据ID 获取二十一点牌局</B>
     * @param recordId
     * @param userId
     * @return
     */
    Map<String, Object> getEsydboardDetail(@Param("boardTableName")String boardTableName, @Param("recordId") String recordId , @Param("userId") String userId);



    /**
     * <B>获取用户账变信息</B>
     * @param page
     * @param userId
     * @return
     */
    List<Map<String, Object>> queryUserAccount(Pagination page,  @Param("tableName")String tableName, @Param("channelId")String channelId, @Param("userId")String userId,  @Param("businessType")String businessType, @Param("gameRoomId")String gameRoomId,
                                               @Param("itemId")String itemId, @Param("startTime")String startTime,  @Param("endTime")String endTime, @Param("doby") String doby);


    /**
     * <B>获取用户财富榜信息</B>
     * @param page
     * @param userId
     * @return
     */
    List<Map<String, Object>>  queryWealthRank(Pagination page, @Param("channelId")String channelId, @Param("userId")String userId,  @Param("startTime")String startTime,  @Param("endTime")String endTime);


}
