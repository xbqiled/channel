package com.channel.report.modules.report.dao;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.channel.api.entity.report.RecordlogoutplatformEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * VIEW
 * 
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-02-25 19:18:37
 */
public interface RecordlogoutplatformDao extends BaseMapper<RecordlogoutplatformEntity> {

    List<Map<String, Object>> queryLogoutReport(Pagination page, @Param("tableName") String tableName, @Param("channelId")String channelId, @Param("userId")String userId,  @Param("startTime")String startTime, @Param("endTime")String endTime);
}
