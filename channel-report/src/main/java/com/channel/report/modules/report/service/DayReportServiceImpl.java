package com.channel.report.modules.report.service;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.channel.api.entity.chl.ChlUserEntity;
import com.channel.api.entity.report.DayReportEntity;
import com.channel.api.service.report.DayReportService;
import com.channel.common.constant.ConfigConstant;
import com.channel.common.constant.TableNameConstant;
import com.channel.common.utils.BoardHandler;
import com.channel.common.utils.PageUtils;
import com.channel.common.utils.Query;
import com.channel.report.modules.report.dao.DayReportDao;
import org.springframework.stereotype.Service;

import java.util.*;

import static com.channel.common.constant.ConfigConstant.FULL_DATE_TIME_FORMAT;
import static com.channel.common.constant.ConfigConstant.THE_DATE_FORMAT;


@Service("dayReportService")
@com.alibaba.dubbo.config.annotation.Service(
        interfaceClass = DayReportService.class,
        timeout = 50000,
        version = "${api.service.version}"
)
public class DayReportServiceImpl extends ServiceImpl<DayReportDao, DayReportEntity> implements DayReportService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        Page<DayReportEntity> page = this.selectPage(
                new Query<DayReportEntity>(params).getPage(),
                new EntityWrapper<DayReportEntity>()
        );
        return new PageUtils(page);
    }

    @Override
    public List<DayReportEntity> queryDayReportByChannel(ChlUserEntity userEntity) {
        Integer current = 7;
        Integer size  = 1;

        //根据拓展Server获取渠道ID
        String channelId = ObjectUtil.isNotNull(userEntity.getChannelId()) ? userEntity.getChannelId().toString() : "";
        Page<DayReportEntity> page = new Page<>(size, current);
        List<DayReportEntity> list = this.baseMapper.queryEcharReport(page, channelId);
        return list;
    }

    @Override
    public List<Map<String, Object>> queryPlayGameCount(String startTime, String endTime, ChlUserEntity userEntity) {
        String channelId = ObjectUtil.isNotNull(userEntity.getChannelId()) ? userEntity.getChannelId() : "";
        return this.baseMapper.queryPlayGameCount(channelId, startTime, endTime);
    }

    /**
     * <B>日报表查询</B>
     * @param params
     * @param userEntity
     * @return
     */
    @Override
    public PageUtils queryDayReport(Map<String, Object> params, ChlUserEntity userEntity) {
        String current = (String)params.get(ConfigConstant.LIMIT);
        String size  = (String)params.get(ConfigConstant.PAGE);

        //根据拓展Server获取渠道ID
        String channelId = ObjectUtil.isNotNull(userEntity.getChannelId()) ? userEntity.getChannelId().toString() : "";
        String startTime = ObjectUtil.isNotNull(params.get("startTime")) ? (String)params.get("startTime") : "";
        String endTime = ObjectUtil.isNotNull(params.get("endTime")) ? (String)params.get("endTime") : "";

        Page<Map<String, Object>> page = new Page<>(new Integer(size), new Integer(current));
        List<Map<String, Object>> list = this.baseMapper.queryDayReport(page, channelId, startTime, endTime);
        return  new PageUtils(page.setRecords(list));
    }


    /**
     * <B>查询月报报表</B>
     * @param params
     * @param userEntity
     * @return
     */
    @Override
    public PageUtils queryMonthReport(Map<String, Object> params, ChlUserEntity userEntity) {
        String current = (String)params.get(ConfigConstant.LIMIT);
        String size  = (String)params.get(ConfigConstant.PAGE);

        //根据拓展Server获取渠道ID
        String channelId = ObjectUtil.isNotNull(userEntity.getChannelId()) ? userEntity.getChannelId().toString() : "";
        String monthTime = ObjectUtil.isNotNull(params.get("monthTime")) ? (String)params.get("monthTime") : "";

        Page<Map<String, Object>> page = new Page<>(new Integer(size), new Integer(current));
        List<Map<String, Object>> list = this.baseMapper.queryMonthReport(page, channelId, monthTime);
        return  new PageUtils(page.setRecords(list));
    }


    /**
     * <B>查询龙虎斗牌局</B>
     * @param params
     * @param userEntity
     * @return
     */
    @Override
    public PageUtils queryLhBoard(Map<String, Object> params, ChlUserEntity userEntity) {
        String current = (String)params.get(ConfigConstant.LIMIT);
        String size  = (String)params.get(ConfigConstant.PAGE);

        //根据拓展Server获取渠道ID
        String channelId = ObjectUtil.isNotNull(userEntity.getChannelId()) ? userEntity.getChannelId() : "";
        String userId = ObjectUtil.isNotNull(params.get("userId")) ? (String)params.get("userId") : "";
        String recordId = ObjectUtil.isNotNull(params.get("recordId")) ? (String)params.get("recordId") : "";

        String startTime = ObjectUtil.isNotNull(params.get("startTime")) ? (String)params.get("startTime") : "";
        String endTime = ObjectUtil.isNotNull(params.get("endTime")) ? (String)params.get("endTime") : "";
        Page<Map<String, Object>> page = new Page<>(new Integer(size), new Integer(current));

        String tableName = TableNameConstant.LBDBOARD_VIEWNAME;
        Date startDate = DateUtil.parse(startTime, FULL_DATE_TIME_FORMAT);
        Date endDate = DateUtil.parse(endTime, FULL_DATE_TIME_FORMAT);

        //计算相隔天数
        Long intervalTime = DateUtil.betweenDay(startDate, endDate, Boolean.TRUE);
        if (intervalTime <= 0 && (startTime.substring(0, 10).equals(endTime.substring(0, 10)))) {
            tableName = TableNameConstant.LBDBOARD_TABLENAME + DateUtil.format(startDate, THE_DATE_FORMAT);
        }

        List<Map<String, Object>> list = this.baseMapper.queryLhBoard(page, tableName, channelId, userId, recordId, startTime, endTime);
        BoardHandler boardHandler = new BoardHandler();
        List<Map<String, Object>> boardList = boardHandler.handlerLhBoard(list);
        return  new PageUtils(page.setRecords(boardList));
    }


    /**
     * <B>查询斗地主牌局</B>
     * @param params
     * @param userEntity
     * @return
     */
    @Override
    public PageUtils queryDdzBoard(Map<String, Object> params, ChlUserEntity userEntity) {
        long start = System.currentTimeMillis();
        String current = (String)params.get(ConfigConstant.LIMIT);
        String size  = (String)params.get(ConfigConstant.PAGE);

        //根据拓展Server获取渠道ID
        String channelId = ObjectUtil.isNotNull(userEntity.getChannelId()) ? userEntity.getChannelId(): "";
        String userId = ObjectUtil.isNotNull(params.get("userId")) ? (String)params.get("userId") : "";
        String recordId = ObjectUtil.isNotNull(params.get("recordId")) ? (String)params.get("recordId") : "";

        String startTime = ObjectUtil.isNotNull(params.get("startTime")) ? (String)params.get("startTime") : "";
        String endTime = ObjectUtil.isNotNull(params.get("endTime")) ? (String)params.get("endTime") : "";
        Page<Map<String, Object>> page = new Page<>(new Integer(size), new Integer(current));

        String boardTableName = TableNameConstant.DDZBOARD_VIEWNAME;
        String extTableName = TableNameConstant.DDZEXTINFO_VIEWNAME;
        Date startDate = DateUtil.parse(startTime, FULL_DATE_TIME_FORMAT);
        Date endDate = DateUtil.parse(endTime, FULL_DATE_TIME_FORMAT);

        //计算相隔天数
        Long intervalTime = DateUtil.betweenDay(startDate, endDate, Boolean.TRUE);
        if (intervalTime <= 0 && (startTime.substring(0, 10).equals(endTime.substring(0, 10)))) {
            boardTableName = TableNameConstant.DDZBOARD_TABLENAME + DateUtil.format(startDate, THE_DATE_FORMAT);
            extTableName = TableNameConstant.DDZEXTINFO_TABLENAME + DateUtil.format(startDate, THE_DATE_FORMAT);
        }

        List<Map<String, Object>> list = this.baseMapper.queryDdzBoard(page, boardTableName, extTableName, channelId, userId, recordId, startTime, endTime);
        long end = System.currentTimeMillis();
        System.out.println((end-start)/1000);
        return  new PageUtils(page.setRecords(list));
    }

    /**
     * <B>解析斗地主牌局</B>
     * @param params
     * @param userEntity
     * @return
     */
    @Override
    public Map<String, Object> analysisDdzBoard(Map<String, Object> params, ChlUserEntity userEntity) {
        String recordId = ObjectUtil.isNotNull(params.get("recordId")) ? (String)params.get("recordId") : "";
        String userId = ObjectUtil.isNotNull(params.get("userId")) ? (String)params.get("userId") : "";
        String queryTime = ObjectUtil.isNotNull(params.get("queryTime")) ? (String)params.get("queryTime") : "";

        Date queryDate = DateUtil.parse(queryTime, FULL_DATE_TIME_FORMAT);
        String boardTableName = TableNameConstant.DDZBOARD_TABLENAME + DateUtil.format(queryDate, THE_DATE_FORMAT);
        String extTableName = TableNameConstant.DDZEXTINFO_TABLENAME + DateUtil.format(queryDate, THE_DATE_FORMAT);

        Map<String, Object> boardMap = baseMapper.getDdzBoardById( boardTableName, extTableName, recordId, userId);
        String loseOrWinIntegal =   ObjectUtil.isNotNull(boardMap.get("LoseOrWinIntegal")) ? (String)boardMap.get("LoseOrWinIntegal") : "";
        String endIntegral = ObjectUtil.isNotNull(boardMap.get("EndIntegral")) ? (String)boardMap.get("EndIntegral") : "";
        String runRecord = ObjectUtil.isNotNull(boardMap.get("RunRecord")) ? (String)boardMap.get("RunRecord") : "";

        String hosteCount = ObjectUtil.isNotNull(boardMap.get("HosteCount")) ? (String)boardMap.get("HosteCount") : "";
        String hosteTrustDate = ObjectUtil.isNotNull(boardMap.get("HosteTrustDate")) ? (String)boardMap.get("HosteTrustDate") : "";

        String userInfo = ObjectUtil.isNotNull(boardMap.get("UserInfo")) ? (String)boardMap.get("UserInfo") : "";
        String recordInfo = ObjectUtil.isNotNull(boardMap.get("RecordInfo")) ? (String)boardMap.get("RecordInfo") : "";
        String outCard = ObjectUtil.isNotNull(boardMap.get("OutCard")) ? (String)boardMap.get("OutCard") : "";

        BoardHandler boardHandler = new BoardHandler();
        Map<String, Object> resultMap = new HashMap<>();
        //解析手牌信息
        List<Map<String, Object>> boardList = boardHandler.analysisDdzRecordInfo(recordInfo);
        //解析出牌信息
        List<Map<String, Object>> outCardList = boardHandler.analysisDdzOutCard(outCard);
        //解析用户信息
        String userInfoStr = boardHandler.analysisDdzUserInfo(userInfo);
        //该局输赢积分
        String endIntegralStr = boardHandler.analysisDdzIntegral(endIntegral);
        //结算信息
        String loseOrWinIntegalStr = boardHandler.analysisDdzLoseOrWin(loseOrWinIntegal);
        //逃跑记录
        String runRecordStr = boardHandler.analysisDdzRunRecord(runRecord);
        //托管次数
        String hosteCountStr = boardHandler.analysisDdzHosteCount(hosteCount);
        //托管时长
        String hosteTrustDateStr = boardHandler.analysisDdzHosteTrustDate(hosteTrustDate);

        resultMap.put("boardList", boardList);
        resultMap.put("outCardList", outCardList);
        resultMap.put("userInfoStr", userInfoStr);

        resultMap.put("endIntegralStr", endIntegralStr);
        resultMap.put("loseOrWinIntegalStr", loseOrWinIntegalStr);
        resultMap.put("runRecordStr", runRecordStr);

        resultMap.put("hosteCountStr", hosteCountStr);
        resultMap.put("hosteTrustDateStr", hosteTrustDateStr);
        return resultMap;
    }

    /**
     * <B>查询跑得快牌局</B>
     * @param params
     * @param userEntity
     * @return
     */
    @Override
    public PageUtils queryPdkBoard(Map<String, Object> params, ChlUserEntity userEntity) {
        String current = (String)params.get(ConfigConstant.LIMIT);
        String size  = (String)params.get(ConfigConstant.PAGE);

        //根据拓展Server获取渠道ID
        String channelId = ObjectUtil.isNotNull(userEntity.getChannelId()) ? userEntity.getChannelId(): "";
        String userId = ObjectUtil.isNotNull(params.get("userId")) ? (String)params.get("userId") : "";
        String recordId = ObjectUtil.isNotNull(params.get("recordId")) ? (String)params.get("recordId") : "";

        String startTime = ObjectUtil.isNotNull(params.get("startTime")) ? (String)params.get("startTime") : "";
        String endTime = ObjectUtil.isNotNull(params.get("endTime")) ? (String)params.get("endTime") : "";
        Page<Map<String, Object>> page = new Page<>(new Integer(size), new Integer(current));

        String boardTableName = TableNameConstant.PDKBOARD_VIEWNAME;
        String extTableName = TableNameConstant.PDKEXTINFO_VIEWNAME;
        Date startDate = DateUtil.parse(startTime, FULL_DATE_TIME_FORMAT);
        Date endDate = DateUtil.parse(endTime, FULL_DATE_TIME_FORMAT);

        //计算相隔天数
        Long intervalTime = DateUtil.betweenDay(startDate, endDate, Boolean.TRUE);
        if (intervalTime <= 0 && (startTime.substring(0, 10).equals(endTime.substring(0, 10)))) {
            boardTableName = TableNameConstant.PDKBOARD_TABLENAME + DateUtil.format(startDate, THE_DATE_FORMAT);
            extTableName = TableNameConstant.PDKEXTINFO_TABLENAME + DateUtil.format(startDate, THE_DATE_FORMAT);
        }

        List<Map<String, Object>> list = this.baseMapper.queryPdkBoard(page, boardTableName, extTableName, channelId, userId, recordId, startTime, endTime);
        return  new PageUtils(page.setRecords(list));
    }

    /**
     * <B>解析跑得快牌局</B>
     * @param params
     * @param userEntity
     * @return
     */
    @Override
    public Map<String, Object> analysisPdkBoard(Map<String, Object> params, ChlUserEntity userEntity) {
        String recordId = ObjectUtil.isNotNull(params.get("recordId")) ? (String)params.get("recordId") : "";
        String userId = ObjectUtil.isNotNull(params.get("userId")) ? (String)params.get("userId") : "";
        String queryTime = ObjectUtil.isNotNull(params.get("queryTime")) ? (String)params.get("queryTime") : "";

        Date queryDate = DateUtil.parse(queryTime, FULL_DATE_TIME_FORMAT);
        String boardTableName = TableNameConstant.PDKBOARD_TABLENAME + DateUtil.format(queryDate, THE_DATE_FORMAT);
        String extTableName = TableNameConstant.PDKEXTINFO_TABLENAME + DateUtil.format(queryDate, THE_DATE_FORMAT);

        Map<String, Object> boardMap = baseMapper.getPdkBoardById( boardTableName, extTableName, recordId, userId);
        String loseOrWinIntegal =   ObjectUtil.isNotNull(boardMap.get("LoseOrWinIntegal")) ? (String)boardMap.get("LoseOrWinIntegal") : "";
        String endIntegral = ObjectUtil.isNotNull(boardMap.get("EndIntegral")) ? (String)boardMap.get("EndIntegral") : "";
        String runRecord = ObjectUtil.isNotNull(boardMap.get("RunRecord")) ? (String)boardMap.get("RunRecord") : "";

        String hosteCount = ObjectUtil.isNotNull(boardMap.get("HosteCount")) ? (String)boardMap.get("HosteCount") : "";
        String hosteTrustDate = ObjectUtil.isNotNull(boardMap.get("HosteTrustDate")) ? (String)boardMap.get("HosteTrustDate") : "";

        String userInfo = ObjectUtil.isNotNull(boardMap.get("UserInfo")) ? (String)boardMap.get("UserInfo") : "";
        String recordInfo = ObjectUtil.isNotNull(boardMap.get("RecordInfo")) ? (String)boardMap.get("RecordInfo") : "";
        String outCard = ObjectUtil.isNotNull(boardMap.get("OutCard")) ? (String)boardMap.get("OutCard") : "";

        BoardHandler boardHandler = new BoardHandler();
        Map<String, Object> resultMap = new HashMap<>();
        //解析手牌信息
        List<Map<String, Object>> boardList = boardHandler.analysisPdkRecordInfo(recordInfo);
        //解析出牌信息
        List<Map<String, Object>> outCardList = boardHandler.analysisPdkOutCard(outCard);
        //解析用户信息
        String userInfoStr = boardHandler.analysisPdkUserInfo(userInfo);
        //该局输赢积分
        String endIntegralStr = boardHandler.analysisPdkIntegral(endIntegral);
        //结算信息
        String loseOrWinIntegalStr = boardHandler.analysisPdkLoseOrWin(loseOrWinIntegal);
        //逃跑记录
        String runRecordStr = boardHandler.analysisPdkRunRecord(runRecord);
        //托管次数
        String hosteCountStr = boardHandler.analysisPdkHosteCount(hosteCount);
        //托管时长
        String hosteTrustDateStr = boardHandler.analysisPdkHosteTrustDate(hosteTrustDate);

        resultMap.put("boardList", boardList);
        resultMap.put("outCardList", outCardList);
        resultMap.put("userInfoStr", userInfoStr);

        resultMap.put("endIntegralStr", endIntegralStr);
        resultMap.put("loseOrWinIntegalStr", loseOrWinIntegalStr);
        resultMap.put("runRecordStr", runRecordStr);

        resultMap.put("hosteCountStr", hosteCountStr);
        resultMap.put("hosteTrustDateStr", hosteTrustDateStr);
        return resultMap;
    }

    /**
     * <B>查询捕鱼牌局</B>
     * @param params
     * @param userEntity
     * @return
     */
    @Override
    public PageUtils queryByBoard(Map<String, Object> params, ChlUserEntity userEntity) {
        String current = (String)params.get(ConfigConstant.LIMIT);
        String size  = (String)params.get(ConfigConstant.PAGE);

        //根据拓展Server获取渠道ID
        String channelId = ObjectUtil.isNotNull(userEntity.getChannelId()) ? userEntity.getChannelId().toString() : "";
        String recordId = ObjectUtil.isNotNull(params.get("recordId")) ? (String)params.get("recordId") : "";
        String userId = ObjectUtil.isNotNull(params.get("userId")) ? (String)params.get("userId") : "";

        String startTime = ObjectUtil.isNotNull(params.get("startTime")) ? (String)params.get("startTime") : "";
        String endTime = ObjectUtil.isNotNull(params.get("endTime")) ? (String)params.get("endTime") : "";
        Page<Map<String, Object>> page = new Page<>(new Integer(size), new Integer(current));

        String boardTableName = TableNameConstant.BYBOARD_VIEWNAME;
        Date startDate = DateUtil.parse(startTime, FULL_DATE_TIME_FORMAT);
        Date endDate = DateUtil.parse(endTime, FULL_DATE_TIME_FORMAT);

        //计算相隔天数
        Long intervalTime = DateUtil.betweenDay(startDate, endDate, Boolean.TRUE);
        if (intervalTime <= 0 && (startTime.substring(0, 10).equals(endTime.substring(0, 10)))) {
            boardTableName = TableNameConstant.BYBOARD_TABLENAME + DateUtil.format(startDate, THE_DATE_FORMAT);
        }

        List<Map<String, Object>> list = this.baseMapper.queryByBoard(page, boardTableName, channelId, userId, recordId, startTime, endTime);
        return  new PageUtils(page.setRecords(list));
    }

    /**
     * <B>解析捕鱼牌局</B>
     * @param params
     * @param userEntity
     * @return
     */
    @Override
    public Map<String, Object> analysisByBoard(Map<String, Object> params, ChlUserEntity userEntity) {
        Map<String, Object> resultMap = new HashMap<>();

        String recordId = ObjectUtil.isNotNull(params.get("recordId")) ? (String)params.get("recordId") : "";
        String userId = ObjectUtil.isNotNull(params.get("userId")) ? (String)params.get("userId") : "";
        String queryTime = ObjectUtil.isNotNull(params.get("queryTime")) ? (String)params.get("queryTime") : "";

        Date queryDate = DateUtil.parse(queryTime, FULL_DATE_TIME_FORMAT);
        String boardTableName = TableNameConstant.BYBOARD_TABLENAME + DateUtil.format(queryDate, THE_DATE_FORMAT);
        Map<String, Object> boardMap = baseMapper.getByBoardById( boardTableName, recordId, userId);

        String recordInfo = ObjectUtil.isNotNull(boardMap.get("RecordInfo")) ? (String)boardMap.get("RecordInfo") : "";
        BoardHandler boardHandler = new BoardHandler();
        List<Map<String, Object>> recordInfoList = boardHandler.analysisByCardRecord(recordInfo);

        resultMap.put("recordInfo", recordInfoList);
        return resultMap;
    }


    /**
     * <B>查询牛牛牌局</B>
     * @param params
     * @param userEntity
     * @return
     */
    @Override
    public PageUtils queryNnBoard(Map<String, Object> params, ChlUserEntity userEntity) {
        String current = (String)params.get(ConfigConstant.LIMIT);
        String size  = (String)params.get(ConfigConstant.PAGE);

        Page<Map<String, Object>> page = new Page<>(new Integer(size), new Integer(current));
        List<Map<String, Object>> list = new ArrayList<>();

        try {
            //根据拓展Server获取渠道ID
            String channelId = ObjectUtil.isNotNull(userEntity.getChannelId()) ? userEntity.getChannelId().toString() : "";
            String recordId = ObjectUtil.isNotNull(params.get("recordId")) ? (String)params.get("recordId") : "";
            String userId = ObjectUtil.isNotNull(params.get("userId")) ? (String)params.get("userId") : "";
            String startTime = ObjectUtil.isNotNull(params.get("startTime")) ? (String)params.get("startTime") : "";
            String endTime = ObjectUtil.isNotNull(params.get("endTime")) ? (String)params.get("endTime") : "";

            String boardTableName = TableNameConstant.NNBOARD_VIEWNAME;
            Date startDate = DateUtil.parse(startTime, FULL_DATE_TIME_FORMAT);
            Date endDate = DateUtil.parse(endTime, FULL_DATE_TIME_FORMAT);

            Long intervalTime = DateUtil.betweenDay(startDate, endDate, Boolean.TRUE);
            if (intervalTime <= 0 && (startTime.substring(0, 10).equals(endTime.substring(0, 10)))) {
                boardTableName = TableNameConstant.NNBOARD_TABLENAME + DateUtil.format(startDate, THE_DATE_FORMAT);
            }

            list = this.baseMapper.queryNnBoard(page, boardTableName, channelId, userId, recordId, startTime, endTime);
            return  new PageUtils(page.setRecords(list));
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            return  new PageUtils(page.setRecords(list));
        }
    }


    /**
     * <B>获取单条牛牛记录信息</B>
     * @param params
     * @param userEntity
     * @return
     */
    @Override
    public Map<String, Object> analysisNnBoard(Map<String, Object> params, ChlUserEntity userEntity) {
        Map<String, Object> resultMap = new HashMap<>();

        String recordId = ObjectUtil.isNotNull(params.get("recordId")) ? (String)params.get("recordId") : "";
        String userId = ObjectUtil.isNotNull(params.get("userId")) ? (String)params.get("userId") : "";
        String queryTime = ObjectUtil.isNotNull(params.get("queryTime")) ? (String)params.get("queryTime") : "";

        Date queryDate = DateUtil.parse(queryTime, FULL_DATE_TIME_FORMAT);
        String boardTableName = TableNameConstant.NNBOARD_TABLENAME + DateUtil.format(queryDate, THE_DATE_FORMAT);
        Map<String, Object> boardMap = baseMapper.getNnBoardById( boardTableName, recordId, userId);

        String cardRecord = ObjectUtil.isNotNull(boardMap.get("CardRecord")) ? (String)boardMap.get("CardRecord") : "";
        String betDetail = ObjectUtil.isNotNull(boardMap.get("BetDetail")) ? (String)boardMap.get("BetDetail") : "";

        BoardHandler boardHandler = new BoardHandler();
        List<Map<String, Object>> cardRecordList = boardHandler.analysisNnCardRecord(cardRecord);
        String betDetailStr =  boardHandler.analysisNnBetDetail(betDetail);

        resultMap.put("betDetail", betDetailStr);
        resultMap.put("cardRecord", cardRecordList);
        return resultMap;
    }


    /**
     * <B>查询花色嘉联华牌局</B>
     * @param params
     * @param userEntity
     * @return
     */
    @Override
    public PageUtils queryBhsBoard(Map<String, Object> params, ChlUserEntity userEntity) {
        String current = (String)params.get(ConfigConstant.LIMIT);
        String size  = (String)params.get(ConfigConstant.PAGE);

        Page<Map<String, Object>> page = new Page<>(new Integer(size), new Integer(current));
        List<Map<String, Object>> list = new ArrayList<>();

        try {
            //根据拓展Server获取渠道ID
            String channelId = ObjectUtil.isNotNull(userEntity.getChannelId()) ? userEntity.getChannelId() : "";
            String recordId = ObjectUtil.isNotNull(params.get("recordId")) ? (String)params.get("recordId") : "";
            String userId = ObjectUtil.isNotNull(params.get("userId")) ? (String)params.get("userId") : "";

            String startTime = ObjectUtil.isNotNull(params.get("startTime")) ? (String)params.get("startTime") : "";
            String endTime = ObjectUtil.isNotNull(params.get("endTime")) ? (String)params.get("endTime") : "";
            String boardTableName = TableNameConstant.HSJLHBOARD_VIEWNAME;

            Date startDate = DateUtil.parse(startTime, FULL_DATE_TIME_FORMAT);
            Date endDate = DateUtil.parse(endTime, FULL_DATE_TIME_FORMAT);
            Long intervalTime = DateUtil.betweenDay(startDate, endDate, Boolean.TRUE);

            if (intervalTime <= 0 && (startTime.substring(0, 10).equals(endTime.substring(0, 10)))) {
                boardTableName = TableNameConstant.HSJLHBOARD_TABLENAME + DateUtil.format(startDate, THE_DATE_FORMAT);
            }

            list = this.baseMapper.queryBhsBoard(page, boardTableName, channelId, userId, recordId, startTime, endTime);
            return  new PageUtils(page.setRecords(list));
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            return  new PageUtils(page.setRecords(list));
        }
    }


    @Override
    public Map<String, Object> analysisBhsBoard(Map<String, Object> params, ChlUserEntity userEntity) {
        Map<String, Object> resultMap = new HashMap<>();

        String recordId = ObjectUtil.isNotNull(params.get("recordId")) ? (String)params.get("recordId") : "";
        String userId = ObjectUtil.isNotNull(params.get("userId")) ? (String)params.get("userId") : "";
        String queryTime = ObjectUtil.isNotNull(params.get("queryTime")) ? (String)params.get("queryTime") : "";

        Date queryDate = DateUtil.parse(queryTime, FULL_DATE_TIME_FORMAT);
        String boardTableName = TableNameConstant.HSJLHBOARD_TABLENAME + DateUtil.format(queryDate, THE_DATE_FORMAT);
        Map<String, Object> boardMap = baseMapper.getBhsBoardById( boardTableName, recordId, userId);

        String cardRecord = ObjectUtil.isNotNull(boardMap.get("CardRecord")) ? (String)boardMap.get("CardRecord") : "";
        String betDetail = ObjectUtil.isNotNull(boardMap.get("BetDetail")) ? (String)boardMap.get("BetDetail") : "";

        BoardHandler boardHandler = new BoardHandler();
        String cardRecordStr = boardHandler.analysisBhsCardRecord(cardRecord);
        Map<String,Object> betDetailMap =  boardHandler.analysisBhsBetDetail(betDetail);

        resultMap.put("betDetail", betDetailMap);
        resultMap.put("cardRecord", cardRecordStr);
        return resultMap;
    }


    /**
     * <B>查询财神到牌局</B>
     * @param params
     * @param userEntity
     * @return
     */
    @Override
    public PageUtils queryCsdBoard(Map<String, Object> params, ChlUserEntity userEntity) {
        String current = (String)params.get(ConfigConstant.LIMIT);
        String size  = (String)params.get(ConfigConstant.PAGE);

        Page<Map<String, Object>> page = new Page<>(new Integer(size), new Integer(current));
        List<Map<String, Object>> list = new ArrayList<>();
        try {
            //根据拓展Server获取渠道ID
            String channelId = ObjectUtil.isNotNull(userEntity.getChannelId()) ? userEntity.getChannelId() : "";
            String userId = ObjectUtil.isNotNull(params.get("userId")) ? (String)params.get("userId") : "";
            String recordId = ObjectUtil.isNotNull(params.get("recordId")) ? (String)params.get("recordId") : "";

            String startTime = ObjectUtil.isNotNull(params.get("startTime")) ? (String)params.get("startTime") : "";
            String endTime = ObjectUtil.isNotNull(params.get("endTime")) ? (String)params.get("endTime") : "";
            String boardTableName = TableNameConstant.CSDBOARD_VIEWNAME;

            Date startDate = DateUtil.parse(startTime, FULL_DATE_TIME_FORMAT);
            Date endDate = DateUtil.parse(endTime, FULL_DATE_TIME_FORMAT);
            Long intervalTime = DateUtil.betweenDay(startDate, endDate, Boolean.TRUE);

            if (intervalTime <= 0 && (startTime.substring(0, 10).equals(endTime.substring(0, 10)))) {
                boardTableName = TableNameConstant.CSDBOARD_TABLENAME + DateUtil.format(startDate, THE_DATE_FORMAT);
            }

            list = this.baseMapper.queryCsdBoard(page, boardTableName, channelId, userId, recordId, startTime, endTime);
            BoardHandler boardHandler = new BoardHandler();

            List<Map<String, Object>> boardList = boardHandler.handlerCsdBoard(list);
            return new PageUtils(page.setRecords(boardList));
        } catch (Exception e) {
            e.printStackTrace();
        }  finally {
            return  new PageUtils(page.setRecords(list));
        }
    }



    @Override
    public List<Map<String, Object>> analysisCsdBoard(String id) {
        String boardDetail = baseMapper.queryCsdboardDetail(id);
        BoardHandler boardHandler = new BoardHandler();
        return boardHandler.handlerCsdBoardDetail(boardDetail);
    }


    /**
     * <B>查询炸金花牌局</B>
     * @param params
     * @param userEntity
     * @return
     */
    @Override
    public PageUtils queryZjhBoard(Map<String, Object> params, ChlUserEntity userEntity) {
        String current = (String)params.get(ConfigConstant.LIMIT);
        String size  = (String)params.get(ConfigConstant.PAGE);

        Page<Map<String, Object>> page = new Page<>(new Integer(size), new Integer(current));
        List<Map<String, Object>> list = new ArrayList<>();

        try {
            //根据拓展Server获取渠道ID
            String channelId = ObjectUtil.isNotNull(userEntity.getChannelId()) ? userEntity.getChannelId() : "";
            String userId = ObjectUtil.isNotNull(params.get("userId")) ? (String)params.get("userId") : "";
            String recordId = ObjectUtil.isNotNull(params.get("recordId")) ? (String)params.get("recordId") : "";
            String startTime = ObjectUtil.isNotNull(params.get("startTime")) ? (String)params.get("startTime") : "";
            String endTime = ObjectUtil.isNotNull(params.get("endTime")) ? (String)params.get("endTime") : "";

            String boardTableName = TableNameConstant.ZJHBOARD_VIEWNAME;
            String extTableName = TableNameConstant.ZJHEXTINFO_VIEWNAME;

            Date startDate = DateUtil.parse(startTime, FULL_DATE_TIME_FORMAT);
            Date endDate = DateUtil.parse(endTime, FULL_DATE_TIME_FORMAT);
            Long intervalTime = DateUtil.betweenDay(startDate, endDate, Boolean.TRUE);

            if (intervalTime <= 0 && (startTime.substring(0, 10).equals(endTime.substring(0, 10)))) {
                boardTableName = TableNameConstant.ZJHBOARD_TABLENAME + DateUtil.format(startDate, THE_DATE_FORMAT);
                extTableName = TableNameConstant.ZJHEXTINFO_TABLENAME + DateUtil.format(startDate, THE_DATE_FORMAT);
            }

            list = this.baseMapper.queryZjhBoard(page, boardTableName, extTableName, channelId, userId, recordId, startTime, endTime);
            return new PageUtils(page.setRecords(list));
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            return  new PageUtils(page.setRecords(list));
        }
    }


    @Override
    public Map<String, Object> analysisZjhBoard(Map<String, Object> params, ChlUserEntity userEntity) {
        Map<String, Object> resultMap = new HashMap<>();

        String recordId = ObjectUtil.isNotNull(params.get("recordId")) ? (String)params.get("recordId") : "";
        String queryTime = ObjectUtil.isNotNull(params.get("queryTime")) ? (String)params.get("queryTime") : "";

        Date queryDate = DateUtil.parse(queryTime, FULL_DATE_TIME_FORMAT);
        String boardTableName = TableNameConstant.ZJHBOARD_TABLENAME + DateUtil.format(queryDate, THE_DATE_FORMAT);
        Map<String, Object> boardMap = baseMapper.getZjhBoardById( boardTableName, recordId);

        String endIntegral = ObjectUtil.isNotNull(boardMap.get("EndIntegral")) ? (String)boardMap.get("EndIntegral") : "";
        String recordInfo = ObjectUtil.isNotNull(boardMap.get("RecordInfo")) ? (String)boardMap.get("RecordInfo") : "";
        String userInfo = ObjectUtil.isNotNull(boardMap.get("UserInfo")) ? (String)boardMap.get("UserInfo") : "";
        String outCard =  ObjectUtil.isNotNull(boardMap.get("OutCard")) ? (String)boardMap.get("OutCard") : "";

        BoardHandler boardHandler = new BoardHandler();
        String userInfoStr = boardHandler.analysisZjhUserInfo(userInfo);
        String endIntegralStr = boardHandler.analysisZjhEndIntegral(endIntegral);

        List<Map<String, Object>> recordInfoList = boardHandler.analysisZjhRecordInfo(recordInfo);
        List<String> outCardList = boardHandler.analysisZjhOutCard(outCard);
        resultMap.put("endIntegral", endIntegralStr);

        resultMap.put("userInfo", userInfoStr);
        resultMap.put("recordInfo", recordInfoList);
        resultMap.put("outCard", outCardList);
        return resultMap;
    }


    /**
     * <B>查询百家乐牌局</B>
     * @param params
     * @param userEntity
     * @return
     */
    @Override
    public PageUtils queryBjlBoard(Map<String, Object> params, ChlUserEntity userEntity) {
        String current = (String)params.get(ConfigConstant.LIMIT);
        String size  = (String)params.get(ConfigConstant.PAGE);

        Page<Map<String, Object>> page = new Page<>(new Integer(size), new Integer(current));
        List<Map<String, Object>> list = new ArrayList<>();

        try {

            String channelId = ObjectUtil.isNotNull(userEntity.getChannelId()) ? userEntity.getChannelId() : "";
            String userId = ObjectUtil.isNotNull(params.get("userId")) ? (String)params.get("userId") : "";
            String recordId = ObjectUtil.isNotNull(params.get("recordId")) ? (String)params.get("recordId") : "";

            String startTime = ObjectUtil.isNotNull(params.get("startTime")) ? (String)params.get("startTime") : "";
            String endTime = ObjectUtil.isNotNull(params.get("endTime")) ? (String)params.get("endTime") : "";
            String boardTableName = TableNameConstant.BJLBOARD_VIEWNAME;

            Date startDate = DateUtil.parse(startTime, FULL_DATE_TIME_FORMAT);
            Date endDate = DateUtil.parse(endTime, FULL_DATE_TIME_FORMAT);
            Long intervalTime = DateUtil.betweenDay(startDate, endDate, Boolean.TRUE);

            if (intervalTime <= 0 && (startTime.substring(0, 10).equals(endTime.substring(0, 10)))) {
                boardTableName = TableNameConstant.BJLBOARD_TABLENAME + DateUtil.format(startDate, THE_DATE_FORMAT);
            }
            list = this.baseMapper.queryBjlBoard(page, boardTableName, channelId, userId, recordId, startTime, endTime);
            return  new PageUtils(page.setRecords(list));
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            return  new PageUtils(page.setRecords(list));
        }
    }


    @Override
    public Map<String, Object> analysisBjlBoard(Map<String, Object> params, ChlUserEntity userEntity) {
        Map<String, Object> resultMap = new HashMap<>();

        String recordId = ObjectUtil.isNotNull(params.get("recordId")) ? (String)params.get("recordId") : "";
        String userId = ObjectUtil.isNotNull(params.get("userId")) ? (String)params.get("userId") : "";
        String queryTime = ObjectUtil.isNotNull(params.get("queryTime")) ? (String)params.get("queryTime") : "";

        Date queryDate = DateUtil.parse(queryTime, FULL_DATE_TIME_FORMAT);
        String boardTableName = TableNameConstant.BJLBOARD_TABLENAME + DateUtil.format(queryDate, THE_DATE_FORMAT);
        Map<String, Object> boardMap = baseMapper.getBjlBoardById( boardTableName, recordId, userId);

        String cardRecord = ObjectUtil.isNotNull(boardMap.get("CardRecord")) ? (String)boardMap.get("CardRecord") : "";
        String betDetail = ObjectUtil.isNotNull(boardMap.get("BetDetail")) ? (String)boardMap.get("BetDetail") : "";
        BoardHandler boardHandler = new BoardHandler();

        List<Map<String, Object>> recordInfoList = boardHandler.analysisBjlCardRecord(cardRecord);
        Map<String, String> betDetailMap = boardHandler.analysisBjlBetDetail(betDetail);
        resultMap.put("cardRecord", recordInfoList);
        resultMap.put("betDetail", betDetailMap);
        return resultMap;
    }


    @Override
    public PageUtils queryHhdzBoard(Map<String, Object> params, ChlUserEntity userEntity) {
        String current = (String)params.get(ConfigConstant.LIMIT);
        String size  = (String)params.get(ConfigConstant.PAGE);

        Page<Map<String, Object>> page = new Page<>(new Integer(size), new Integer(current));
        List<Map<String, Object>> list = new ArrayList<>();

        try {
            String channelId = ObjectUtil.isNotNull(userEntity.getChannelId()) ? userEntity.getChannelId() : "";
            String userId = ObjectUtil.isNotNull(params.get("userId")) ? (String)params.get("userId") : "";
            String recordId = ObjectUtil.isNotNull(params.get("recordId")) ? (String)params.get("recordId") : "";

            String startTime = ObjectUtil.isNotNull(params.get("startTime")) ? (String)params.get("startTime") : "";
            String endTime = ObjectUtil.isNotNull(params.get("endTime")) ? (String)params.get("endTime") : "";
            String boardTableName = TableNameConstant.HHDZBOARD_VIEWNAME;

            Date startDate = DateUtil.parse(startTime, FULL_DATE_TIME_FORMAT);
            Date endDate = DateUtil.parse(endTime, FULL_DATE_TIME_FORMAT);
            Long intervalTime = DateUtil.betweenDay(startDate, endDate, Boolean.TRUE);

            if (intervalTime <= 0 && (startTime.substring(0, 10).equals(endTime.substring(0, 10)))) {
                boardTableName = TableNameConstant.HHDZBOARD_TABLENAME + DateUtil.format(startDate, THE_DATE_FORMAT);
            }
            list = this.baseMapper.queryHhdzBoard(page, boardTableName, channelId, userId, recordId, startTime, endTime);
            BoardHandler boardHandler = new BoardHandler();

            List<Map<String, Object>> boardList = boardHandler.handlerHhdzBoard(list);
            return new PageUtils(page.setRecords(boardList));
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            return  new PageUtils(page.setRecords(list));
        }
    }


    @Override
    public Map<String, Object> analysisHhdzBoard(Map<String, Object> params, ChlUserEntity userEntity) {
        String recordId = ObjectUtil.isNotNull(params.get("recordId")) ? (String)params.get("recordId") : "";
        String userId = ObjectUtil.isNotNull(params.get("userId")) ? (String)params.get("userId") : "";
        String queryTime = ObjectUtil.isNotNull(params.get("queryTime")) ? (String)params.get("queryTime") : "";

        Date queryDate = DateUtil.parse(queryTime, FULL_DATE_TIME_FORMAT);
        String boardTableName = TableNameConstant.HHDZBOARD_TABLENAME + DateUtil.format(queryDate, THE_DATE_FORMAT);
        Map<String, Object> boardMap = baseMapper.queryHhBoardDetail(boardTableName, recordId, userId);

        String betDetail = ObjectUtil.isNotNull(boardMap.get("BetDetail")) ? (String)boardMap.get("BetDetail") : "";
        String cardRecord =   ObjectUtil.isNotNull(boardMap.get("CardRecord")) ? (String)boardMap.get("CardRecord") : "";

        //解析代码
        BoardHandler boardHandler = new BoardHandler();
        Map<String, Object> resultMap = new HashMap<>();

        List<Map<String, Object>> cardRecordList = boardHandler.analysisHhdzCardRecord(cardRecord);
        String betDetailStr = boardHandler.analysisHhdzbetDetail(betDetail);

        resultMap.put("betDetail", betDetailStr);
        resultMap.put("cardRecord", cardRecordList);
        return resultMap;
    }


    @Override
    public PageUtils queryHbjlBoard(Map<String, Object> params, ChlUserEntity userEntity) {
        String current = (String)params.get(ConfigConstant.LIMIT);
        String size  = (String)params.get(ConfigConstant.PAGE);

        Page<Map<String, Object>> page = new Page<>(new Integer(size), new Integer(current));
        List<Map<String, Object>> list = new ArrayList<>();

        try {
            String channelId = ObjectUtil.isNotNull(userEntity.getChannelId()) ? userEntity.getChannelId() : "";
            String userId = ObjectUtil.isNotNull(params.get("userId")) ? (String)params.get("userId") : "";
            String recordId = ObjectUtil.isNotNull(params.get("recordId")) ? (String)params.get("recordId") : "";

            String startTime = ObjectUtil.isNotNull(params.get("startTime")) ? (String)params.get("startTime") : "";
            String endTime = ObjectUtil.isNotNull(params.get("endTime")) ? (String)params.get("endTime") : "";
            String boardTableName = TableNameConstant.HBJLBOARD_VIEWNAME;

            Date startDate = DateUtil.parse(startTime, FULL_DATE_TIME_FORMAT);
            Date endDate = DateUtil.parse(endTime, FULL_DATE_TIME_FORMAT);
            Long intervalTime = DateUtil.betweenDay(startDate, endDate, Boolean.TRUE);

            if (intervalTime <= 0 && (startTime.substring(0, 10).equals(endTime.substring(0, 10)))) {
                boardTableName = TableNameConstant.HBJLBOARD_TABLENAME + DateUtil.format(startDate, THE_DATE_FORMAT);
            }
            list = this.baseMapper.queryHbjlBoard(page, boardTableName, channelId, userId, recordId, startTime, endTime);
            BoardHandler boardHandler = new BoardHandler();

            List<Map<String, Object>> boardList = boardHandler.handlerHhdzBoard(list);
            return new PageUtils(page.setRecords(boardList));
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            return  new PageUtils(page.setRecords(list));
        }
    }


    @Override
    public PageUtils queryQznnBoard(Map<String, Object> params, ChlUserEntity userEntity) {
        String current = (String)params.get(ConfigConstant.LIMIT);
        String size  = (String)params.get(ConfigConstant.PAGE);

        Page<Map<String, Object>> page = new Page<>(new Integer(size), new Integer(current));
        List<Map<String, Object>> list = new ArrayList<>();

        try {
            String channelId = ObjectUtil.isNotNull(userEntity.getChannelId()) ? userEntity.getChannelId() : "";
            String userId = ObjectUtil.isNotNull(params.get("userId")) ? (String)params.get("userId") : "";
            String recordId = ObjectUtil.isNotNull(params.get("recordId")) ? (String)params.get("recordId") : "";

            String startTime = ObjectUtil.isNotNull(params.get("startTime")) ? (String)params.get("startTime") : "";
            String endTime = ObjectUtil.isNotNull(params.get("endTime")) ? (String)params.get("endTime") : "";
            String boardTableName = TableNameConstant.QZNNBOARD_VIEWNAME;

            Date startDate = DateUtil.parse(startTime, FULL_DATE_TIME_FORMAT);
            Date endDate = DateUtil.parse(endTime, FULL_DATE_TIME_FORMAT);
            Long intervalTime = DateUtil.betweenDay(startDate, endDate, Boolean.TRUE);

            if (intervalTime <= 0 && (startTime.substring(0, 10).equals(endTime.substring(0, 10)))) {
                boardTableName = TableNameConstant.QZNNBOARD_TABLENAME + DateUtil.format(startDate, THE_DATE_FORMAT);
            }
            list = this.baseMapper.queryQznnBoard(page, boardTableName, channelId, userId, recordId, startTime, endTime);
            return new PageUtils(page.setRecords(list));
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            return  new PageUtils(page.setRecords(list));
        }
    }


    @Override
    public Map<String, Object> analysisQznnBoard(Map<String, Object> params, ChlUserEntity userEntity) {
        String recordId = ObjectUtil.isNotNull(params.get("recordId")) ? (String)params.get("recordId") : "";
        String userId = ObjectUtil.isNotNull(params.get("userId")) ? (String)params.get("userId") : "";
        String queryTime = ObjectUtil.isNotNull(params.get("queryTime")) ? (String)params.get("queryTime") : "";

        Date queryDate = DateUtil.parse(queryTime, FULL_DATE_TIME_FORMAT);
        String boardTableName = TableNameConstant.QZNNBOARD_TABLENAME + DateUtil.format(queryDate, THE_DATE_FORMAT);
        Map<String, Object> boardMap = baseMapper.getQznnBoardById(boardTableName, recordId, userId);

        String betMultiple = ObjectUtil.isNotNull(boardMap.get("betMultiple")) ? (String)boardMap.get("betMultiple") : "";
        String CardRecord =   ObjectUtil.isNotNull(boardMap.get("CardRecord")) ? (String)boardMap.get("CardRecord") : "";
        String cardType = ObjectUtil.isNotNull(boardMap.get("cardType")) ? (String)boardMap.get("cardType") : "";

        BoardHandler boardHandler = new BoardHandler();
        Map<String, Object> resultMap = new HashMap<>();
        String betMultipleStr = boardHandler.analysisQznnBetMultiple(betMultiple);
        //解析出牌信息
        List<Map<String, Object>> cardRecordList = boardHandler.analysisQznnCardRecord(CardRecord);
        //解析用户信息
        String cardTypeStr = boardHandler.analysisQznnCardType(cardType);

        resultMap.put("betMultiple", betMultipleStr);
        resultMap.put("cardRecord", cardRecordList);
        resultMap.put("cardType", cardTypeStr);
        return resultMap;
    }


    @Override
    public PageUtils queryFqzsBoard(Map<String, Object> params, ChlUserEntity userEntity) {
        String current = (String)params.get(ConfigConstant.LIMIT);
        String size  = (String)params.get(ConfigConstant.PAGE);

        Page<Map<String, Object>> page = new Page<>(new Integer(size), new Integer(current));
        List<Map<String, Object>> list = new ArrayList<>();

        try {
            String channelId = ObjectUtil.isNotNull(userEntity.getChannelId()) ? userEntity.getChannelId() : "";
            String userId = ObjectUtil.isNotNull(params.get("userId")) ? (String)params.get("userId") : "";
            String recordId = ObjectUtil.isNotNull(params.get("recordId")) ? (String)params.get("recordId") : "";

            String startTime = ObjectUtil.isNotNull(params.get("startTime")) ? (String)params.get("startTime") : "";
            String endTime = ObjectUtil.isNotNull(params.get("endTime")) ? (String)params.get("endTime") : "";
            String boardTableName = TableNameConstant.FQZSBOARD_VIEWNAME;

            Date startDate = DateUtil.parse(startTime, FULL_DATE_TIME_FORMAT);
            Date endDate = DateUtil.parse(endTime, FULL_DATE_TIME_FORMAT);
            Long intervalTime = DateUtil.betweenDay(startDate, endDate, Boolean.TRUE);

            if (intervalTime <= 0 && (startTime.substring(0, 10).equals(endTime.substring(0, 10)))) {
                boardTableName = TableNameConstant.FQZSBOARD_TABLENAME + DateUtil.format(startDate, THE_DATE_FORMAT);
            }
            list = this.baseMapper.queryFqzsBoard(page, boardTableName, channelId, userId, recordId, startTime, endTime);
            return new PageUtils(page.setRecords(list));
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            return  new PageUtils(page.setRecords(list));
        }
    }


    @Override
    public Map<String, Object> analysisFqzsBoard(Map<String, Object> params, ChlUserEntity userEntity) {
        String recordId = ObjectUtil.isNotNull(params.get("recordId")) ? (String)params.get("recordId") : "";
        String userId = ObjectUtil.isNotNull(params.get("userId")) ? (String)params.get("userId") : "";
        String queryTime = ObjectUtil.isNotNull(params.get("queryTime")) ? (String)params.get("queryTime") : "";

        Date queryDate = DateUtil.parse(queryTime, FULL_DATE_TIME_FORMAT);
        String boardTableName = TableNameConstant.FQZSBOARD_TABLENAME + DateUtil.format(queryDate, THE_DATE_FORMAT);
        Map<String, Object> boardMap = baseMapper.getFqzsBoardById(boardTableName, recordId, userId);

        Integer rewardType = ObjectUtil.isNotNull(boardMap.get("rewardType")) ? (Integer)boardMap.get("rewardType") : 0;
        String awardBetArea =   ObjectUtil.isNotNull(boardMap.get("awardBetArea")) ? (String)boardMap.get("awardBetArea") : "";
        String areaTimes = ObjectUtil.isNotNull(boardMap.get("areaTimes")) ? (String)boardMap.get("areaTimes") : "";
        String playerBet = ObjectUtil.isNotNull(boardMap.get("playerBet")) ? (String)boardMap.get("playerBet") : "";

        BoardHandler boardHandler = new BoardHandler();
        Map<String, Object> resultMap = new HashMap<>();

        String rewardTypeStr = boardHandler.analysisFqzsRewardType(rewardType);
        List<String> awardBetAreaList = boardHandler.analysisFqzsAwardBetArea(awardBetArea);
        List<Map<String, Object>> areaTimesList = boardHandler.analysisFqzsAwardAreaTimes(areaTimes);
        List<Map<String, Object>> playerBetList = boardHandler.analysisFqzsPlayerBet(playerBet);

        resultMap.put("rewardType", rewardTypeStr);
        resultMap.put("awardBetArea", awardBetAreaList);
        resultMap.put("areaTimes", areaTimesList);
        resultMap.put("playerBet", playerBetList);
        return resultMap;
    }


    @Override
    public PageUtils queryBcbmBoard(Map<String, Object> params, ChlUserEntity userEntity) {
        String current = (String)params.get(ConfigConstant.LIMIT);
        String size  = (String)params.get(ConfigConstant.PAGE);

        Page<Map<String, Object>> page = new Page<>(new Integer(size), new Integer(current));
        List<Map<String, Object>> list = new ArrayList<>();

        try {
            String channelId = ObjectUtil.isNotNull(userEntity.getChannelId()) ? userEntity.getChannelId() : "";
            String userId = ObjectUtil.isNotNull(params.get("userId")) ? (String)params.get("userId") : "";
            String recordId = ObjectUtil.isNotNull(params.get("recordId")) ? (String)params.get("recordId") : "";

            String startTime = ObjectUtil.isNotNull(params.get("startTime")) ? (String)params.get("startTime") : "";
            String endTime = ObjectUtil.isNotNull(params.get("endTime")) ? (String)params.get("endTime") : "";
            String boardTableName = TableNameConstant.BCBMBOARD_VIEWNAME;

            Date startDate = DateUtil.parse(startTime, FULL_DATE_TIME_FORMAT);
            Date endDate = DateUtil.parse(endTime, FULL_DATE_TIME_FORMAT);
            Long intervalTime = DateUtil.betweenDay(startDate, endDate, Boolean.TRUE);

            if (intervalTime <= 0 && (startTime.substring(0, 10).equals(endTime.substring(0, 10)))) {
                boardTableName = TableNameConstant.BCBMBOARD_TABLENAME + DateUtil.format(startDate, THE_DATE_FORMAT);
            }
            list = this.baseMapper.queryBcbmBoard(page, boardTableName, channelId, userId, recordId, startTime, endTime);
            return new PageUtils(page.setRecords(list));
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            return  new PageUtils(page.setRecords(list));
        }
    }


    @Override
    public Map<String, Object> analysisBcbmBoard(Map<String, Object> params, ChlUserEntity userEntity) {
        String recordId = ObjectUtil.isNotNull(params.get("recordId")) ? (String)params.get("recordId") : "";
        String userId = ObjectUtil.isNotNull(params.get("userId")) ? (String)params.get("userId") : "";
        String queryTime = ObjectUtil.isNotNull(params.get("queryTime")) ? (String)params.get("queryTime") : "";

        Date queryDate = DateUtil.parse(queryTime, FULL_DATE_TIME_FORMAT);
        String boardTableName = TableNameConstant.BCBMBOARD_TABLENAME + DateUtil.format(queryDate, THE_DATE_FORMAT);
        Map<String, Object> boardMap = baseMapper.getBcbmBoardById(boardTableName, recordId, userId);

        Integer rewardType = ObjectUtil.isNotNull(boardMap.get("rewardType")) ? (Integer)boardMap.get("rewardType") : 0;
        String awardBetArea =   ObjectUtil.isNotNull(boardMap.get("awardBetArea")) ? (String)boardMap.get("awardBetArea") : "";
        String areaTimes = ObjectUtil.isNotNull(boardMap.get("areaTimes")) ? (String)boardMap.get("areaTimes") : "";
        String playerBet = ObjectUtil.isNotNull(boardMap.get("playerBet")) ? (String)boardMap.get("playerBet") : "";

        BoardHandler boardHandler = new BoardHandler();
        Map<String, Object> resultMap = new HashMap<>();

        String rewardTypeStr = boardHandler.analysisBcbmRewardType(rewardType);
        List<String> list = boardHandler.analysisBcbmAwardBetArea(awardBetArea);
        List<Map<String, Object>> awardList = boardHandler.analysisBcbmAwardAreaTimes(areaTimes);
        List<Map<String, Object>> betList = boardHandler.analysisBcbmPlayerBet(playerBet);

        resultMap.put("rewardType", rewardTypeStr);
        resultMap.put("awardBetArea", list);
        resultMap.put("areaTimes", awardList);
        resultMap.put("playerBet", betList);
        return resultMap;
    }


    @Override
    public PageUtils querySgjBoard(Map<String, Object> params, ChlUserEntity userEntity) {
        String current = (String)params.get(ConfigConstant.LIMIT);
        String size  = (String)params.get(ConfigConstant.PAGE);

        Page<Map<String, Object>> page = new Page<>(new Integer(size), new Integer(current));
        List<Map<String, Object>> list = new ArrayList<>();

        try {
            String channelId = ObjectUtil.isNotNull(userEntity.getChannelId()) ? userEntity.getChannelId() : "";
            String userId = ObjectUtil.isNotNull(params.get("userId")) ? (String)params.get("userId") : "";
            String recordId = ObjectUtil.isNotNull(params.get("recordId")) ? (String)params.get("recordId") : "";

            String startTime = ObjectUtil.isNotNull(params.get("startTime")) ? (String)params.get("startTime") : "";
            String endTime = ObjectUtil.isNotNull(params.get("endTime")) ? (String)params.get("endTime") : "";
            String boardTableName = TableNameConstant.SGJBOARD_VIEWNAME;

            Date startDate = DateUtil.parse(startTime, FULL_DATE_TIME_FORMAT);
            Date endDate = DateUtil.parse(endTime, FULL_DATE_TIME_FORMAT);
            Long intervalTime = DateUtil.betweenDay(startDate, endDate, Boolean.TRUE);

            if (intervalTime <= 0 && (startTime.substring(0, 10).equals(endTime.substring(0, 10)))) {
                boardTableName = TableNameConstant.SGJBOARD_TABLENAME + DateUtil.format(startDate, THE_DATE_FORMAT);
            }
            list = this.baseMapper.querySgjBoard(page, boardTableName, channelId, userId, recordId, startTime, endTime);
            return new PageUtils(page.setRecords(list));
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            return  new PageUtils(page.setRecords(list));
        }
    }


    @Override
    public Map<String, Object> analysisSgjBoard(Map<String, Object> params, ChlUserEntity userEntity) {
        BoardHandler boardHandler = new BoardHandler();
        Map<String, Object> resultMap = new HashMap<>();

        String userId = ObjectUtil.isNotNull(params.get("userId")) ? (String)params.get("userId") : "";
        String recordId = ObjectUtil.isNotNull(params.get("recordId")) ? (String)params.get("recordId") : "";
        String queryTime = ObjectUtil.isNotNull(params.get("queryTime")) ? (String)params.get("queryTime") : "";

        Date queryDate = DateUtil.parse(queryTime, FULL_DATE_TIME_FORMAT);
        String boardTableName = TableNameConstant.ESYDBOARD_TABLENAME + DateUtil.format(queryDate, THE_DATE_FORMAT);
        Map<String, Object> boardMap = baseMapper.getSgjboardDetail(boardTableName, recordId, userId);

        String endIntegral = ObjectUtil.isNotNull(boardMap.get("EndIntegral")) ? (String)boardMap.get("EndIntegral") : "";
        String recordInfo = ObjectUtil.isNotNull(boardMap.get("RecordInfo")) ? (String)boardMap.get("RecordInfo") : "";
        String endIntegralStr = boardHandler.splitSgjJsjfStr(endIntegral);

        List<Map<String, Object>> recordInfoList = boardHandler.splitSgjCspDetail(recordInfo);
        resultMap.put("endIntegral", endIntegralStr);
        resultMap.put("recordInfo", recordInfoList);
        return resultMap;
    }


    @Override
    public PageUtils queryEsydBoard(Map<String, Object> params, ChlUserEntity userEntity) {
        String current = (String)params.get(ConfigConstant.LIMIT);
        String size  = (String)params.get(ConfigConstant.PAGE);

        Page<Map<String, Object>> page = new Page<>(new Integer(size), new Integer(current));
        List<Map<String, Object>> list = new ArrayList<>();

        try {
            String channelId = ObjectUtil.isNotNull(userEntity.getChannelId()) ? userEntity.getChannelId() : "";
            String userId = ObjectUtil.isNotNull(params.get("userId")) ? (String)params.get("userId") : "";
            String recordId = ObjectUtil.isNotNull(params.get("recordId")) ? (String)params.get("recordId") : "";

            String startTime = ObjectUtil.isNotNull(params.get("startTime")) ? (String)params.get("startTime") : "";
            String endTime = ObjectUtil.isNotNull(params.get("endTime")) ? (String)params.get("endTime") : "";
            String boardTableName = TableNameConstant.ESYDBOARD_VIEWNAME;

            Date startDate = DateUtil.parse(startTime, FULL_DATE_TIME_FORMAT);
            Date endDate = DateUtil.parse(endTime, FULL_DATE_TIME_FORMAT);
            Long intervalTime = DateUtil.betweenDay(startDate, endDate, Boolean.TRUE);

            if (intervalTime <= 0 && (startTime.substring(0, 10).equals(endTime.substring(0, 10)))) {
                boardTableName = TableNameConstant.ESYDBOARD_TABLENAME + DateUtil.format(startDate, THE_DATE_FORMAT);
            }
            list = this.baseMapper.queryEsydBoard(page, boardTableName, channelId, userId, recordId, startTime, endTime);
            return new PageUtils(page.setRecords(list));
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            return  new PageUtils(page.setRecords(list));
        }
    }


    @Override
    public Map<String, Object> analysisEsydBoard(Map<String, Object> params, ChlUserEntity userEntity) {
        BoardHandler boardHandler = new BoardHandler();
        Map<String, Object> resultMap = new HashMap<>();

        String userId = ObjectUtil.isNotNull(params.get("userId")) ? (String)params.get("userId") : "";
        String recordId = ObjectUtil.isNotNull(params.get("recordId")) ? (String)params.get("recordId") : "";
        String queryTime = ObjectUtil.isNotNull(params.get("queryTime")) ? (String)params.get("queryTime") : "";

        Date queryDate = DateUtil.parse(queryTime, FULL_DATE_TIME_FORMAT);
        String boardTableName = TableNameConstant.ESYDBOARD_TABLENAME + DateUtil.format(queryDate, THE_DATE_FORMAT);
        Map<String, Object> boardMap = baseMapper.getEsydboardDetail( boardTableName, recordId, userId);

        //庄家牌型
        String cardRecord = ObjectUtil.isNotNull(boardMap.get("CardRecord")) ? (String)boardMap.get("CardRecord") : "";
        //自己牌型
        String bankCardRecord = ObjectUtil.isNotNull(boardMap.get("BankCardRecord")) ? (String)boardMap.get("BankCardRecord") : "";

        List<String> cardRecordList = boardHandler.analysisEsydCardRecord(bankCardRecord);
        List<Map<String, Object>> bankCardRecordList = boardHandler.analysisEsydBankCardRecord(cardRecord);
        resultMap.put("cardRecord", cardRecordList);
        resultMap.put("bankCardRecord", bankCardRecordList);
        return resultMap;
    }


    /**
     * <B>查询用户账变记录</B>
     * @param params
     * @param userEntity
     * @return
     */
    @Override
    public PageUtils queryUserAccount(Map<String, Object> params, ChlUserEntity userEntity) {
        String current = (String)params.get(ConfigConstant.LIMIT);
        String size  = (String)params.get(ConfigConstant.PAGE);

        //根据拓展Server获取渠道ID
        String channelId = ObjectUtil.isNotNull(userEntity.getChannelId()) ? userEntity.getChannelId() : "";
        String userId = ObjectUtil.isNotNull(params.get("userId")) ? (String)params.get("userId") : "";
        String itemId = ObjectUtil.isNotNull(params.get("itemId")) ? (String)params.get("itemId") : "";

        String businessType = ObjectUtil.isNotNull(params.get("businessType")) ? (String)params.get("businessType") : "";
        String gameRoomId = ObjectUtil.isNotNull(params.get("gameRoomId")) ? (String)params.get("gameRoomId") : "";

        String startTime = ObjectUtil.isNotNull(params.get("startTime")) ? (String)params.get("startTime") : "";
        String endTime = ObjectUtil.isNotNull(params.get("endTime")) ? (String)params.get("endTime") : "";

        String tableName = "V_UserTreasureChange";
        Date startDate = DateUtil.parse(startTime, FULL_DATE_TIME_FORMAT);
        Date endDate = DateUtil.parse(endTime, FULL_DATE_TIME_FORMAT);

        //计算相隔天数
        Long intervalTime = DateUtil.betweenDay(startDate, endDate, Boolean.TRUE);
        if (intervalTime <= 0 && (startTime.substring(0, 10).equals(endTime.substring(0, 10)))) {
            tableName = "usertreasurechange" + DateUtil.format(startDate, THE_DATE_FORMAT);
        }

        String order = ObjectUtil.isNotNull(params.get("order")) ? (String)params.get("order") : "";
        String sortby = ObjectUtil.isNotNull(params.get("sortby")) ? (String)params.get("sortby") : "";

        //生成order by 语句
        String orderStr = packOrderStr(order, sortby);
        Page<Map<String, Object>> page = new Page<>(new Integer(size), new Integer(current));
        List<Map<String, Object>> list = this.baseMapper.queryUserAccount(page, tableName, channelId, userId, businessType, gameRoomId, itemId, startTime, endTime, orderStr);
        return  new PageUtils(page.setRecords(list));
    }


    String packOrderStr(String order, String sortby) {
        StringBuffer sb = new StringBuffer();
        if (StrUtil.isBlank(order) || StrUtil.isBlank(sortby)) {
            return "  order by  a.RecordTime  desc  ";
        } else {
            sb.append("order by ");
            if (sortby.equals("RecordTime")) {
                sb.append(" a.RecordTime  ");
            } else if (sortby.equals("comment")) {
                sb.append(" a.BusinessType ");
            } else if (sortby.equals("BeforeCount")) {
                sb.append(" a.BeforeCount ");
            } else if (sortby.equals("GainCount")) {
                sb.append(" a.GainCount ");
            } else if (sortby.equals("AfterCount")) {
                sb.append(" a.AfterCount ");
            }
            sb.append(order);
            return sb.toString();
        }
    }

    /**
     * <B>查询用户财富排行榜</B>
     * @param params
     * @param userEntity
     * @return
     */
    @Override
    public PageUtils queryWealthRank(Map<String, Object> params, ChlUserEntity userEntity) {
        String current = (String)params.get(ConfigConstant.LIMIT);
        String size  = (String)params.get(ConfigConstant.PAGE);

        //根据拓展Server获取渠道ID
        String channelId = ObjectUtil.isNotNull(userEntity.getChannelId()) ? userEntity.getChannelId().toString() : "";
        String userId = ObjectUtil.isNotNull(params.get("userId")) ? (String)params.get("userId") : "";
        String startTime = ObjectUtil.isNotNull(params.get("startTime")) ? (String)params.get("startTime") : "";
        String endTime = ObjectUtil.isNotNull(params.get("endTime")) ? (String)params.get("endTime") : "";

        Page<Map<String, Object>> page = new Page<>(new Integer(size), new Integer(current));
        List<Map<String, Object>> list = this.baseMapper.queryWealthRank(page, channelId, userId, startTime, endTime);
        return  new PageUtils(page.setRecords(list));
    }

}
