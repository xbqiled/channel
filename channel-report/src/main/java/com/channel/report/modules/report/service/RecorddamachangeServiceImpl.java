package com.channel.report.modules.report.service;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.channel.api.entity.chl.ChlUserEntity;
import com.channel.api.entity.report.RecorddamachangeEntity;
import com.channel.api.service.report.RecorddamachangeService;
import com.channel.common.constant.ConfigConstant;
import com.channel.common.json.GsonChangeTrading;
import com.channel.common.json.GsonUserTrading;
import com.channel.common.utils.HttpHandler;
import com.channel.common.utils.PageUtils;
import com.channel.report.modules.report.dao.RecorddamachangeDao;
import org.springframework.stereotype.Service;

import java.util.*;

import static com.channel.common.constant.ConfigConstant.FULL_DATE_TIME_FORMAT;
import static com.channel.common.constant.ConfigConstant.THE_DATE_FORMAT;


@Service("recorddamachangeService")
@com.alibaba.dubbo.config.annotation.Service(
        interfaceClass = RecorddamachangeService.class,
        timeout = 30000,
        version = "${api.service.version}"
)
public class RecorddamachangeServiceImpl extends ServiceImpl<RecorddamachangeDao, RecorddamachangeEntity> implements RecorddamachangeService {


    @Override
    public PageUtils queryStaticLoss(Map<String, Object> params, ChlUserEntity userEntity) {
        String current = (String) params.get(ConfigConstant.LIMIT);
        String size = (String) params.get(ConfigConstant.PAGE);
        Page<Map<String, Object>> page = new Page<>(new Integer(size), new Integer(current));

        String channelId = ObjectUtil.isNotNull(userEntity.getChannelId()) ? userEntity.getChannelId() : "";
        String userId = ObjectUtil.isNotNull(params.get("userId")) ? (String) params.get("userId") : "";

        String startTime = ObjectUtil.isNotNull(params.get("startTime")) ? (String) params.get("startTime") : "";
        String endTime = ObjectUtil.isNotNull(params.get("endTime")) ? (String) params.get("endTime") : "";
        List<Map<String, Object>> list = this.baseMapper.queryStaticLosslist(page, channelId, startTime, endTime);
        return new PageUtils(page.setRecords(list));
    }


    @Override
    public PageUtils queryStaticTradinglist(Map<String, Object> params, ChlUserEntity userEntity) {
        String current = (String) params.get(ConfigConstant.LIMIT);
        String size = (String) params.get(ConfigConstant.PAGE);
        Page<Map<String, Object>> page = new Page<>(new Integer(size), new Integer(current));

        List<Map<String, Object>> list = new ArrayList<>();
        String channelId = ObjectUtil.isNotNull(userEntity.getChannelId()) ? userEntity.getChannelId() : "";
        String userId = ObjectUtil.isNotNull(params.get("userId")) ? (String) params.get("userId") : "";

        String startTime = ObjectUtil.isNotNull(params.get("startTime")) ? (String) params.get("startTime") : "";
        String endTime = ObjectUtil.isNotNull(params.get("endTime")) ? (String) params.get("endTime") : "";
        String tableName = "V_recorddamachange";

        Date startDate = DateUtil.parse(startTime, FULL_DATE_TIME_FORMAT);
        Date endDate = DateUtil.parse(startTime, FULL_DATE_TIME_FORMAT);

        try {
            //计算相隔天数
            Long intervalTime = DateUtil.betweenDay(startDate, endDate, Boolean.TRUE);
            if (intervalTime <= 0 && (startTime.substring(0, 10).equals(endTime.substring(0, 10)))) {
                tableName = "recorddamachange" + DateUtil.format(startDate, THE_DATE_FORMAT);
            }

            list = this.baseMapper.queryTradinglist(page, channelId, startTime, endTime, userId, tableName);
            return new PageUtils(page.setRecords(list));
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            return  new PageUtils(page.setRecords(list));
        }


    }


    @Override
    public PageUtils queryTradinglist(Map<String, Object> params, ChlUserEntity userEntity) {
        String current = (String) params.get(ConfigConstant.LIMIT);
        String size = (String) params.get(ConfigConstant.PAGE);
        Page<Map<String, Object>> page = new Page<>(new Integer(size), new Integer(current));

        List<Map<String, Object>> list = new ArrayList<>();
        //根据拓展Server获取渠道ID
        String channelId = ObjectUtil.isNotNull(userEntity.getChannelId()) ? userEntity.getChannelId() : "";
        String userId = ObjectUtil.isNotNull(params.get("userId")) ? (String) params.get("userId") : "";

        String startTime = ObjectUtil.isNotNull(params.get("startTime")) ? (String) params.get("startTime") : "";
        String endTime = ObjectUtil.isNotNull(params.get("endTime")) ? (String) params.get("endTime") : "";
        String tableName = "V_recorddamachange";

        Date startDate = DateUtil.parse(startTime, FULL_DATE_TIME_FORMAT);
        Date endDate = DateUtil.parse(startTime, FULL_DATE_TIME_FORMAT);

        try {
           //计算相隔天数
            Long intervalTime = DateUtil.betweenDay(startDate, endDate, Boolean.TRUE);
            if (intervalTime <= 0 && (startTime.substring(0, 10).equals(endTime.substring(0, 10)))) {
                tableName = "recorddamachange" + DateUtil.format(startDate, THE_DATE_FORMAT);
            }

            list = this.baseMapper.queryTradinglist(page, channelId, startTime, endTime, userId, tableName);
            return new PageUtils(page.setRecords(list));
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            return  new PageUtils(page.setRecords(list));
        }
    }


    @Override
    public PageUtils getUserTrading(Map<String, Object> params, ChlUserEntity userEntity) {
        String current = (String) params.get(ConfigConstant.LIMIT);
        String size = (String) params.get(ConfigConstant.PAGE);

        String userId = ObjectUtil.isNotNull(params.get("userId")) ? (String) params.get("userId") : "";
        Map<String, Object> userMap =  baseMapper.queryUserInfoById(userId, userEntity.getChannelId());
        if (userMap == null) {
            return  null;
        }

        Page<Map<String, Object>> page = new Page<>(new Integer(size), new Integer(current));
        List<Map<String, Object>> list = new ArrayList<>();
        Map<String, Object> map = new HashMap<>();
        GsonUserTrading result = HttpHandler.getUserTrading(Integer.parseInt(userId));

        if (result != null && result.getRet() == 0) {
            map.put("damaNeed", result.getDamaNeed());
            map.put("damaAmount", result.getDamaAmount());
            map.put("userId", userId);
            map.put("userName", userMap.get("userName"));
            map.put("nickName", userMap.get("nickName"));
            map.put("regTime", userMap.get("regTime"));
            map.put("regIp", userMap.get("regIp"));

            list.add(map);
            return new PageUtils(page.setRecords(list));
        } else {
            return  null;
        }
    }

    @Override
    public GsonChangeTrading changeTrading(String userId, Integer opType, Long damaChange, ChlUserEntity userEntity) {
        Map<String, Object> userMap = baseMapper.queryUserInfoById(userId, userEntity.getChannelId());
        if (userMap == null) {
            return null;
        }
        GsonChangeTrading result = HttpHandler.changeUserTrading(opType, Integer.parseInt(userId), damaChange);
        return result;
    }
}
