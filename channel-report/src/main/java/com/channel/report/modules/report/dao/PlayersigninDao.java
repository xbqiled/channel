package com.channel.report.modules.report.dao;

import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.channel.api.entity.report.PlayersigninEntity;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * VIEW
 * 
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-05-28 14:18:49
 */
public interface PlayersigninDao extends BaseMapper<PlayersigninEntity> {


    List<Map<String, Object>> querySginPage(Pagination page, @Param("tableName") String tableName, @Param("channelId") String channelId, @Param("userId") String userId, @Param("startTime") String startTime, @Param("endTime") String endTime);

}
