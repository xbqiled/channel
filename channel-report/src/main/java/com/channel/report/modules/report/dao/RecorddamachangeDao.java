package com.channel.report.modules.report.dao;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.channel.api.entity.report.RecorddamachangeEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * VIEW
 *
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-08-24 11:49:24
 */
public interface RecorddamachangeDao extends BaseMapper<RecorddamachangeEntity> {

    List<Map<String, Object>> queryTradinglist(Pagination page, @Param("channelId") String channelId, @Param("startTime") String regStartTime, @Param("endTime") String regEndTime, @Param("userId") String userId, @Param("tableName") String tableName);

    List<Map<String, Object>> queryStaticTradinglist(Pagination page, @Param("channelId") String channelId, @Param("startTime") String regStartTime, @Param("endTime") String regEndTime, @Param("userId") String userId, @Param("tableName") String tableName);

    List<Map<String, Object>> queryStaticLosslist(Pagination page, @Param("channelId") String channelId, @Param("startTime") String regStartTime, @Param("endTime") String regEndTime);

    Map<String, Object> queryUserInfoById(@Param("userId") String userId, @Param("channelId") String channelId);

}
