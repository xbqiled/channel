package com.channel.report.modules.report.service;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.channel.api.entity.chl.ChlUserEntity;
import com.channel.api.entity.report.RecordloginplatformEntity;
import com.channel.api.entity.report.RetainVo;
import com.channel.api.service.report.RecordloginplatformService;
import com.channel.common.constant.ConfigConstant;
import com.channel.common.constant.TableNameConstant;
import com.channel.common.utils.PageUtils;
import com.channel.common.utils.Query;
import com.channel.report.modules.report.dao.RecordloginplatformDao;
import org.springframework.stereotype.Service;

import java.text.DecimalFormat;
import java.util.*;

import static com.channel.common.constant.ConfigConstant.FULL_DATE_TIME_FORMAT;
import static com.channel.common.constant.ConfigConstant.THE_DATE_FORMAT;


@Service("recordloginplatformService")
@com.alibaba.dubbo.config.annotation.Service(
        interfaceClass = RecordloginplatformService.class,
        timeout = 20000,
        version = "${api.service.version}"
)
public class RecordloginplatformServiceImpl extends ServiceImpl<RecordloginplatformDao, RecordloginplatformEntity> implements RecordloginplatformService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        Page<RecordloginplatformEntity> page = this.selectPage(
                new Query<RecordloginplatformEntity>(params).getPage(),
                new EntityWrapper<RecordloginplatformEntity>()
        );

        return new PageUtils(page);
    }


    public PageUtils queryLoginReport(Map<String, Object> params, ChlUserEntity userEntity) {
        String current = (String)params.get(ConfigConstant.LIMIT);
        String size  = (String)params.get(ConfigConstant.PAGE);
        Page<Map<String, Object>> page = new Page<>(new Integer(size), new Integer(current));

        List<Map<String, Object>> list = new ArrayList<>();
        //根据拓展Server获取渠道ID
        String channelId = ObjectUtil.isNotNull(userEntity.getChannelId()) ? userEntity.getChannelId(): "";
        String userId = ObjectUtil.isNotNull(params.get("userId")) ? (String)params.get("userId") : "";
        String startTime = ObjectUtil.isNotNull(params.get("startTime")) ? (String)params.get("startTime") : "";

        String endTime = ObjectUtil.isNotNull(params.get("endTime")) ? (String)params.get("endTime") : "";
        String tableName = TableNameConstant.USERLOGINPLATFORM_VIEW;
        Date startDate = DateUtil.parse(startTime, FULL_DATE_TIME_FORMAT);

        Date endDate = DateUtil.parse(endTime, FULL_DATE_TIME_FORMAT);
        try {
            Long intervalTime = DateUtil.betweenDay(startDate, endDate, Boolean.TRUE);
            if (intervalTime <= 0 && (startTime.substring(0, 10).equals(endTime.substring(0, 10)))) {
                tableName = TableNameConstant.USERLOGINPLATFORM_TABLENAME + DateUtil.format(startDate, THE_DATE_FORMAT);
            }

            list = this.baseMapper.queryLoginReport(page, tableName, channelId, userId, startTime, endTime);
            return  new PageUtils(page.setRecords(list));
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            return  new PageUtils(page.setRecords(list));
        }
    }


    @Override
    public PageUtils queryUserRetainList(Map<String, Object> params, ChlUserEntity userEntity) {
        String current = (String)params.get(ConfigConstant.LIMIT);
        String size  = (String)params.get(ConfigConstant.PAGE);

        //首先获取登录统计
        String channelId = ObjectUtil.isNotNull(userEntity.getChannelId()) ? userEntity.getChannelId() : "";
        String registerStartTime = ObjectUtil.isNotNull(params.get("registerStartTime")) ? (String) params.get("registerStartTime") : "";
        String registerEndTime = ObjectUtil.isNotNull(params.get("registerEndTime")) ? (String) params.get("registerEndTime") : "";

        String loginStartTime = ObjectUtil.isNotNull(params.get("loginStartTime")) ? (String) params.get("loginStartTime") : "";
        String loginEndTime = ObjectUtil.isNotNull(params.get("registerEndTime")) ? (String) params.get("loginEndTime") : "";
        String countTime = ObjectUtil.isNotNull(params.get("countTime")) ? (String) params.get("countTime") : "";

        String origin = ObjectUtil.isNotNull(params.get("origin")) ? (String) params.get("origin") : "";
        String promoterId = ObjectUtil.isNotNull(params.get("promoterId")) ? (String) params.get("promoterId") : "";

        if (StrUtil.isNotBlank(registerStartTime)) {
            Date registerStartDate = new Date(Long.valueOf(registerStartTime));
            registerStartTime = DateUtil.format(registerStartDate, ConfigConstant.DATE_FORMAT);
        }

        if (StrUtil.isNotBlank(registerEndTime)) {
            Date registerEndDate = new Date(Long.valueOf(registerEndTime));
            registerEndTime = DateUtil.format(registerEndDate, ConfigConstant.DATE_FORMAT);
        }

        if (StrUtil.isNotBlank(loginStartTime)) {
            Date loginStartDate = new Date(Long.valueOf(loginStartTime));
            loginStartTime = DateUtil.format(loginStartDate, ConfigConstant.DATE_FORMAT);
        }

        if (StrUtil.isNotBlank(loginEndTime)) {
            Date loginEndDate = new Date(Long.valueOf(loginEndTime));
            loginEndTime = DateUtil.format(loginEndDate, ConfigConstant.DATE_FORMAT);
        }

        //首先查询所有的数据信息
        Page<RetainVo> page = new Page<>(new Integer(size), new Integer(current));
        List<RetainVo> registerList = baseMapper.queryAgentRegisterCountReport(page, channelId, registerStartTime, registerEndTime, origin, promoterId);

        //循环信息
        if (CollUtil.isNotEmpty(registerList)) {
            for (RetainVo vo : registerList) {
                //获取间隔时间
                String regTime = vo.getRegTime();
                //添加间隔时间
                Date loginTime = DateUtil.parse(regTime);
                Date loginDate = DateUtil.offsetDay( loginTime, Integer.valueOf(countTime));
                String logTime = DateUtil.format(loginDate, ConfigConstant.DATE_FORMAT);

                Long regNum = vo.getRegNum();
                Long loginNum = baseMapper.queryAgentLoginCountReportByTime(channelId, regTime, logTime, vo.getOrigin(), vo.getPromoterId());
                double d = Double.valueOf(loginNum.toString()) / Double.valueOf(regNum.toString()) * 100;

                String statis = new DecimalFormat("0.00").format(d);
                vo.setLoginNum(loginNum.toString());
                vo.setStatis(statis);
            }
        }
        return new PageUtils(page.setRecords(registerList));
    }


    private int filterLoginData(String loginTime, List<Map<String, Object>> registerList) {
        Set<String> set = new HashSet<>();
        for (Map<String, Object> map : registerList) {
            String time = (String)map.get("LoginTime");
            Integer userId = (Integer)map.get("UserID");
            if (time.equals(loginTime)) {
                set.add(userId.toString());
            }
        }
        return  set.size();
    }


}
