package com.channel.report.modules.report.dao;

import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.channel.api.entity.report.PlayerenrollmentEntity;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * VIEW
 * 
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-09-17 15:10:16
 */
public interface PlayerenrollmentDao extends BaseMapper<PlayerenrollmentEntity> {

    List<Map<String, Object>> queryPlayerEnrollmentPage(Pagination page, @Param("tableName") String tableName, @Param("channelId") String channelId, @Param("operateType") String operateType,
                                                           @Param("gameTypeId") String gameTypeId,   @Param("userId") String userId,   @Param("startTime") String startTime, @Param("endTime") String endTime);


}
