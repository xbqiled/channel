package com.channel.report.modules.report.service;


import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.channel.api.entity.chl.ChlUserEntity;
import com.channel.api.entity.report.PlayerDayShareEntity;
import com.channel.api.service.report.PlayerDayShareService;
import com.channel.common.constant.ConfigConstant;
import com.channel.common.constant.TableNameConstant;
import com.channel.common.utils.PageUtils;
import com.channel.report.modules.report.dao.PlayerDayShareDao;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import static com.channel.common.constant.ConfigConstant.FULL_DATE_TIME_FORMAT;
import static com.channel.common.constant.ConfigConstant.THE_DATE_FORMAT;


@Service("playerdayshareService")
@com.alibaba.dubbo.config.annotation.Service(
        interfaceClass = PlayerDayShareService.class,
        timeout = 10000,
        version = "${api.service.version}"
)
public class PlayerDayShareServiceImpl extends ServiceImpl<PlayerDayShareDao, PlayerDayShareEntity> implements PlayerDayShareService {

    @Override
    public PageUtils queryDaySharePage(Map<String, Object> params, ChlUserEntity userEntity) {
        String current = (String) params.get(ConfigConstant.LIMIT);
        String size = (String) params.get(ConfigConstant.PAGE);

        Page<Map<String, Object>> page = new Page<>(new Integer(size), new Integer(current));
        List<Map<String, Object>> list = new ArrayList<>();

        String channelId = ObjectUtil.isNotNull(userEntity.getChannelId()) ? userEntity.getChannelId() : "";
        String userId = ObjectUtil.isNotNull(params.get("userId")) ? (String) params.get("userId") : "";
        String startTime = ObjectUtil.isNotNull(params.get("startTime")) ? (String) params.get("startTime") : "";
        String endTime = ObjectUtil.isNotNull(params.get("endTime")) ? (String) params.get("endTime") : "";

        String tableName = TableNameConstant.DAYSHARE_VIEW;
        Date startDate = DateUtil.parse(startTime, FULL_DATE_TIME_FORMAT);
        Date endDate = DateUtil.parse(endTime, FULL_DATE_TIME_FORMAT);

        try {
            Long intervalTime = DateUtil.betweenDay(startDate, endDate, Boolean.TRUE);

            if (intervalTime <= 0 && (startTime.substring(0, 10).equals(endTime.substring(0, 10)))) {
                tableName = TableNameConstant.DAYSHARE_TABLENAME + DateUtil.format(startDate, THE_DATE_FORMAT);
            }

            list = this.baseMapper.queryDaySharePage(page, tableName, channelId, userId, startTime, endTime);
            return new PageUtils(page.setRecords(list));
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            return new PageUtils(page.setRecords(list));
        }

    }

}
