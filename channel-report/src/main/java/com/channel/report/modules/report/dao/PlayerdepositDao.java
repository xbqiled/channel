package com.channel.report.modules.report.dao;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.channel.api.entity.report.PlayerdepositEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * VIEW
 * 
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-05-29 13:35:09
 */
public interface PlayerdepositDao extends BaseMapper<PlayerdepositEntity> {

    List<Map<String, Object>> queryDepositPage(Pagination page, @Param("tableName") String tableName, @Param("channelId") String channelId, @Param("orderId") String orderId,  @Param("userId") String userId, @Param("startTime") String startTime, @Param("endTime") String endTime);
}
