package com.channel.report.modules.report.service;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.channel.api.entity.report.TaskConfigureEntity;
import com.channel.api.service.report.TaskConfigureService;
import com.channel.common.utils.PageUtils;
import com.channel.report.modules.report.dao.TaskConfigureDao;
import org.springframework.stereotype.Service;

import java.util.Map;


@Service("taskConfigureService")
public class TaskConfigureServiceImpl extends ServiceImpl<TaskConfigureDao, TaskConfigureEntity> implements TaskConfigureService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        return null;
    }
}
