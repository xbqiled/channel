package com.channel.report.modules.report.service;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.channel.api.entity.chl.ChlUserEntity;
import com.channel.api.entity.report.RedblackpoolEntity;
import com.channel.api.service.cac.RedblackpoolService;

import com.channel.common.constant.ConfigConstant;
import com.channel.common.constant.TableNameConstant;
import com.channel.common.utils.BoardHandler;
import com.channel.common.utils.PageUtils;
import com.channel.common.utils.Query;
import com.channel.report.modules.report.dao.RedblackpoolDao;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import static com.channel.common.constant.ConfigConstant.FULL_DATE_TIME_FORMAT;
import static com.channel.common.constant.ConfigConstant.THE_DATE_FORMAT;


@Service("redblackpoolService")
@com.alibaba.dubbo.config.annotation.Service(
        interfaceClass = RedblackpoolService.class,
        timeout = 10000,
        version = "${api.service.version}"
)
public class RedblackpoolServiceImpl extends ServiceImpl<RedblackpoolDao, RedblackpoolEntity> implements RedblackpoolService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        Page<RedblackpoolEntity> page = this.selectPage(
                new Query<RedblackpoolEntity>(params).getPage(),
                new EntityWrapper<RedblackpoolEntity>()
        );

        return new PageUtils(page);
    }


    @Override
    public PageUtils queryHhdzPoolPage(Map<String, Object> params,  ChlUserEntity userEntity) {
        String current = (String) params.get(ConfigConstant.LIMIT);
        String size = (String) params.get(ConfigConstant.PAGE);

        Page<Map<String, Object>> page = new Page<>(new Integer(size), new Integer(current));
        List<Map<String, Object>> list = new ArrayList<>();

        try {
            String channelId = ObjectUtil.isNotNull(userEntity.getChannelId()) ? userEntity.getChannelId() : "";
            String periodical = ObjectUtil.isNotNull(params.get("periodical")) ? (String) params.get("periodical") : "";
            String userId = ObjectUtil.isNotNull(params.get("userId")) ? (String) params.get("userId") : "";

            String recordId = ObjectUtil.isNotNull(params.get("recordId")) ? (String) params.get("recordId") : "";
            String startTime = ObjectUtil.isNotNull(params.get("startTime")) ? (String)params.get("startTime") : "";
            String endTime = ObjectUtil.isNotNull(params.get("endTime")) ? (String)params.get("endTime") : "";

            String boardTableName = TableNameConstant.REDBLACKPOOL_VIEWNAME;
            Date startDate = DateUtil.parse(startTime, FULL_DATE_TIME_FORMAT);
            Date endDate = DateUtil.parse(endTime, FULL_DATE_TIME_FORMAT);

            Long intervalTime = DateUtil.betweenDay(startDate, endDate, Boolean.TRUE);
            if (intervalTime <= 0 && (startTime.substring(0, 10).equals(endTime.substring(0, 10)))) {
                boardTableName = TableNameConstant.REDBLACKPOOL_TABLENAME + DateUtil.format(startDate, THE_DATE_FORMAT);
            }

            list = this.baseMapper.queryHhdzPoolPage(page, boardTableName, channelId, periodical, userId, recordId, startTime, endTime);
            BoardHandler boardHandler = new BoardHandler();
            List<Map<String, Object>> boardList = boardHandler.handlerHhdzBoard(list);
            return new PageUtils(page.setRecords(boardList));
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            return  new PageUtils(page.setRecords(list));
        }
    }
}
