package com.channel.report.modules.report.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.channel.api.entity.report.RecordloginplatformEntity;
import com.channel.api.entity.report.RetainVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * VIEW
 * 
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-02-25 19:23:00
 */
public interface RecordloginplatformDao extends BaseMapper<RecordloginplatformEntity> {

    List<Map<String, Object>> queryLoginReport(Pagination page, @Param("tableName") String tableName, @Param("channelId")String channelId, @Param("userId")String userId, @Param("startTime")String startTime, @Param("endTime")String endTime);

    List<RetainVo> queryRegisterCountReport(Pagination page, @Param("channelId")String channelId, @Param("startTime")String startTime, @Param("endTime")String endTime);

    Long queryLoginCountReportByTime(@Param("channelId")String channelId, @Param("registerTime")String registerTime, @Param("loginTime")String loginTime);

    List<RetainVo> queryAgentRegisterCountReport(Pagination page, @Param("channelId")String channelId, @Param("startTime")String startTime, @Param("endTime")String endTime,
                                                 @Param("origin")String origin , @Param("promoterId")String promoterId);

    Long queryAgentLoginCountReportByTime(@Param("channelId")String channelId, @Param("registerTime")String registerTime, @Param("loginTime")String loginTime,
                                          @Param("origin")String origin , @Param("promoterId")String promoterId);

    List<Map<String, Object>> queryAllLoginCountReport(@Param("channelId")String channelId, @Param("loginStartTime")String registerTime, @Param("loginEndTime")String loginTime);

    List<Map<String, Object>> queryAllRegisterCountReport(@Param("channelId")String channelId, @Param("registerStartTime")String registerStartTime, @Param("registerEndTime")String registerEndTime);

    List<Map<String, Object>> queryLoginCountReport(Pagination page, @Param("channelId") String channelId, @Param("loginStartTime") String loginStartTime, @Param("loginEndTime") String loginEndTime, @Param("registerStartTime") String registerStartTime, @Param("registerEndTime") String registerEndTime);

}
