package com.channel.report.modules.report.dao;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.channel.api.entity.report.RecorduserscorepergameEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * VIEW
 * 
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-09-21 11:05:04
 */
public interface RecorduserscorepergameDao extends BaseMapper<RecorduserscorepergameEntity> {


    List<Map<String, Object>> queryUserScorePerGamePage(Pagination page, @Param("tableName")String tableName, @Param("channelId")String channelId,  @Param("recordId")String recordId,  @Param("userId")String userId, @Param("gameRoomId")String gameRoomId,
                                                @Param("startTime")String startTime, @Param("endTime")String endTime, @Param("doby") String doby);



    List<Map<String, Object>> queryGameBetPage(Pagination page, @Param("tableName")String tableName, @Param("channelId")String channelId, @Param("gameRoomId")String gameRoomId);


    List<String> isHaveTab(@Param("tableName") String tableName);

}
