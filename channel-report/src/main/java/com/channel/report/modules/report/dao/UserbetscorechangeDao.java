package com.channel.report.modules.report.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.channel.api.entity.report.UserbetscorechangeEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * VIEW
 * 
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-04-24 19:09:09
 */
public interface UserbetscorechangeDao extends BaseMapper<UserbetscorechangeEntity> {

    List<Map<String, Object>> queryRecordPage(Pagination page, @Param("channelId") String channelId, @Param("type") String type, @Param("userId") String userId, @Param("startTime") String startTime, @Param("endTime") String endTime);

    List<Map<String, Object>> queryRecordRankPage(Pagination page, @Param("channelId") String channelId, @Param("userId") String userId, @Param("startTime") String startTime, @Param("endTime") String endTime);

    List<Map<String, Object>> queryOperatorList();

}
