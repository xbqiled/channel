package com.channel.report.modules.report.dao;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.channel.api.entity.report.RedblackpoolEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * VIEW
 *
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-06-02 13:08:43
 */
public interface RedblackpoolDao extends BaseMapper<RedblackpoolEntity> {

    List<Map<String, Object>> queryHhdzPoolPage(Pagination page, @Param("boardTableName")String boardTableName, @Param("channelId") String channelId, @Param("periodical") String periodical,
                                                @Param("userId") String userId, @Param("recordId") String recordId,  @Param("startTime")String startTime, @Param("endTime")String endTime);

}
