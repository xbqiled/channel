package com.channel.report.modules.report.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.channel.api.entity.report.GameRebateEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * VIEW
 * 
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-03-30 19:46:22
 */
public interface GameRebateDao extends BaseMapper<GameRebateEntity> {

    List<GameRebateEntity> queryGameRebatePage(Pagination page, @Param("tableName") String tableName, @Param("channelId") String channelId, @Param("userId") String userId, @Param("startTime") String startTime, @Param("endTime") String endTime);

}
