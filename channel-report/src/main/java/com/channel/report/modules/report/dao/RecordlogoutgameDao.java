package com.channel.report.modules.report.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.channel.api.entity.report.RecordlogoutgameEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * VIEW
 * 
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-05-29 12:18:47
 */
public interface RecordlogoutgameDao extends BaseMapper<RecordlogoutgameEntity> {

    List<Map<String, Object>> queryLogoutGameReport(Pagination page,  @Param("tableName") String tableName, @Param("channelId") String channelId, @Param("userId") String userId, @Param("startTime") String startTime, @Param("endTime") String endTime);

}
