package com.channel.api.kryo;

import com.alibaba.dubbo.common.serialize.support.SerializationOptimizer;
import com.baomidou.mybatisplus.plugins.Page;
import com.channel.common.utils.PageUtils;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

public class SerializationOptimizerImpl implements SerializationOptimizer {
    public Collection<Class> getSerializableClasses() {
        List<Class> classes = new LinkedList<Class>();
        classes.add(PageUtils.class);
        classes.add(Page.class);
        //classes.add(com.github.pagehelper.Page.class);
        //classes.add(BidResponse.class);

        return classes;
    }
}