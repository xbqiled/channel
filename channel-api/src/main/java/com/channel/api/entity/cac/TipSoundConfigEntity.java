package com.channel.api.entity.cac;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * 
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-05-19 14:18:20
 */
@TableName("tip_sound_config")
public class TipSoundConfigEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键Id
	 */
	@TableId
	private Integer id;
	/**
	 * 类型: 1,百度
	 */
	private Integer type;
	/**
	 * client_id
	 */
	private String clientId;
	/**
	 * client_secret
	 */
	private String clientSecret;
	/**
	 * grant_type
	 */
	private String grantType;
	/**
	 * 渠道编码
	 */
	private String channleId;
	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 创建人
	 */
	private String createBy;

	/**
	 * 设置：主键Id
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * 获取：主键Id
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * 设置：类型: 1,百度
	 */
	public void setType(Integer type) {
		this.type = type;
	}
	/**
	 * 获取：类型: 1,百度
	 */
	public Integer getType() {
		return type;
	}
	/**
	 * 设置：client_id
	 */
	public void setClientId(String clientId) {
		this.clientId = clientId;
	}
	/**
	 * 获取：client_id
	 */
	public String getClientId() {
		return clientId;
	}
	/**
	 * 设置：client_secret
	 */
	public void setClientSecret(String clientSecret) {
		this.clientSecret = clientSecret;
	}
	/**
	 * 获取：client_secret
	 */
	public String getClientSecret() {
		return clientSecret;
	}
	/**
	 * 设置：grant_type
	 */
	public void setGrantType(String grantType) {
		this.grantType = grantType;
	}
	/**
	 * 获取：grant_type
	 */
	public String getGrantType() {
		return grantType;
	}
	/**
	 * 设置：渠道编码
	 */
	public void setChannleId(String channleId) {
		this.channleId = channleId;
	}
	/**
	 * 获取：渠道编码
	 */
	public String getChannleId() {
		return channleId;
	}
	/**
	 * 设置：创建时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：创建时间
	 */
	public Date getCreateTime() {
		return createTime;
	}
	/**
	 * 设置：创建人
	 */
	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}
	/**
	 * 获取：创建人
	 */
	public String getCreateBy() {
		return createBy;
	}
}
