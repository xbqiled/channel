package com.channel.api.entity.cac;



import java.io.Serializable;


public class GameRoomVo implements Serializable {

    private  Integer gameAtomTypeId;

    private  Integer gameKindType;

    private  String gameKindName;

    private  String phoneGameName;


    public Integer getGameAtomTypeId() {
        return gameAtomTypeId;
    }

    public void setGameAtomTypeId(Integer gameAtomTypeId) {
        this.gameAtomTypeId = gameAtomTypeId;
    }

    public Integer getGameKindType() {
        return gameKindType;
    }

    public void setGameKindType(Integer gameKindType) {
        this.gameKindType = gameKindType;
    }

    public String getGameKindName() {
        return gameKindName;
    }

    public void setGameKindName(String gameKindName) {
        this.gameKindName = gameKindName;
    }

    public String getPhoneGameName() {
        return phoneGameName;
    }

    public void setPhoneGameName(String phoneGameName) {
        this.phoneGameName = phoneGameName;
    }
}
