package com.channel.api.entity.cac;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

import java.math.BigDecimal;
import java.io.Serializable;
import java.util.Date;

/**
 * 
 * 
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-06-16 15:38:03
 */
@TableName("tmp_statis_account")
public class StatisAccountEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 用户编码
	 */
	@TableId
	private String userId;
	/**
	 * 用户名
	 */
	private String userName;
	/**
	 * 用户昵称
	 */
	private String nickName;
	/**
	 * 渠道编码
	 */
	private String channelId;
	/**
	 * 统计时间
	 */
	private Date statisTime;
	/**
	 * 充值金额
	 */
	private BigDecimal rechargeFee;
	/**
	 * 充值次数
	 */
	private Long rechargeCount;
	/**
	 * 提现金额
	 */
	private BigDecimal cashFee;
	/**
	 * 提现次数
	 */
	private Long cashCount;
	/**
	 * 下注金额
	 */
	private BigDecimal betFee;
	/**
	 * 下注次数
	 */
	private Long betCount;
	/**
	 * 赢取金额
	 */
	private BigDecimal winFee;
	/**
	 * 游戏时长
	 */
	private Long gameTimeLangth;
	/**
	 * 总进账
	 */
	private BigDecimal allFee;
	/**
	 * 赠送金额
	 */
	private BigDecimal giveFee;
	/**
	 * 当日盈亏
	 */
	private BigDecimal lossFee;
	/**
	 * 是否比赛
	 */
	private Integer match;
	/**
	 * 步骤
	 */
	private Integer step;

	/**
	 * 设置：用户编码
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}
	/**
	 * 获取：用户编码
	 */
	public String getUserId() {
		return userId;
	}
	/**
	 * 设置：用户名
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}
	/**
	 * 获取：用户名
	 */
	public String getUserName() {
		return userName;
	}
	/**
	 * 设置：用户昵称
	 */
	public void setNickName(String nickName) {
		this.nickName = nickName;
	}
	/**
	 * 获取：用户昵称
	 */
	public String getNickName() {
		return nickName;
	}
	/**
	 * 设置：渠道编码
	 */
	public void setChannelId(String channelId) {
		this.channelId = channelId;
	}
	/**
	 * 获取：渠道编码
	 */
	public String getChannelId() {
		return channelId;
	}
	/**
	 * 设置：统计时间
	 */
	public void setStatisTime(Date statisTime) {
		this.statisTime = statisTime;
	}
	/**
	 * 获取：统计时间
	 */
	public Date getStatisTime() {
		return statisTime;
	}
	/**
	 * 设置：充值金额
	 */
	public void setRechargeFee(BigDecimal rechargeFee) {
		this.rechargeFee = rechargeFee;
	}
	/**
	 * 获取：充值金额
	 */
	public BigDecimal getRechargeFee() {
		return rechargeFee;
	}
	/**
	 * 设置：充值次数
	 */
	public void setRechargeCount(Long rechargeCount) {
		this.rechargeCount = rechargeCount;
	}
	/**
	 * 获取：充值次数
	 */
	public Long getRechargeCount() {
		return rechargeCount;
	}
	/**
	 * 设置：提现金额
	 */
	public void setCashFee(BigDecimal cashFee) {
		this.cashFee = cashFee;
	}
	/**
	 * 获取：提现金额
	 */
	public BigDecimal getCashFee() {
		return cashFee;
	}
	/**
	 * 设置：提现次数
	 */
	public void setCashCount(Long cashCount) {
		this.cashCount = cashCount;
	}
	/**
	 * 获取：提现次数
	 */
	public Long getCashCount() {
		return cashCount;
	}
	/**
	 * 设置：下注金额
	 */
	public void setBetFee(BigDecimal betFee) {
		this.betFee = betFee;
	}
	/**
	 * 获取：下注金额
	 */
	public BigDecimal getBetFee() {
		return betFee;
	}
	/**
	 * 设置：下注次数
	 */
	public void setBetCount(Long betCount) {
		this.betCount = betCount;
	}
	/**
	 * 获取：下注次数
	 */
	public Long getBetCount() {
		return betCount;
	}
	/**
	 * 设置：赢取金额
	 */
	public void setWinFee(BigDecimal winFee) {
		this.winFee = winFee;
	}
	/**
	 * 获取：赢取金额
	 */
	public BigDecimal getWinFee() {
		return winFee;
	}
	/**
	 * 设置：游戏时长
	 */
	public void setGameTimeLangth(Long gameTimeLangth) {
		this.gameTimeLangth = gameTimeLangth;
	}
	/**
	 * 获取：游戏时长
	 */
	public Long getGameTimeLangth() {
		return gameTimeLangth;
	}
	/**
	 * 设置：总进账
	 */
	public void setAllFee(BigDecimal allFee) {
		this.allFee = allFee;
	}
	/**
	 * 获取：总进账
	 */
	public BigDecimal getAllFee() {
		return allFee;
	}
	/**
	 * 设置：赠送金额
	 */
	public void setGiveFee(BigDecimal giveFee) {
		this.giveFee = giveFee;
	}
	/**
	 * 获取：赠送金额
	 */
	public BigDecimal getGiveFee() {
		return giveFee;
	}
	/**
	 * 设置：当日盈亏
	 */
	public void setLossFee(BigDecimal lossFee) {
		this.lossFee = lossFee;
	}
	/**
	 * 获取：当日盈亏
	 */
	public BigDecimal getLossFee() {
		return lossFee;
	}
	/**
	 * 设置：是否比赛
	 */
	public void setMatch(Integer match) {
		this.match = match;
	}
	/**
	 * 获取：是否比赛
	 */
	public Integer getMatch() {
		return match;
	}
	/**
	 * 设置：步骤
	 */
	public void setStep(Integer step) {
		this.step = step;
	}
	/**
	 * 获取：步骤
	 */
	public Integer getStep() {
		return step;
	}
}
