package com.channel.api.entity.report;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

import java.math.BigDecimal;
import java.io.Serializable;
import java.util.Date;

/**
 * VIEW
 * 
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-05-29 12:12:03
 */
@TableName("V_RecordUserLoginGame")
public class RecordlogingameEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private Integer id;
	/**
	 * 
	 */
	private Integer userid;
	/**
	 * 
	 */
	private BigDecimal beforescorecount;
	/**
	 * 
	 */
	private Integer gametypeid;
	/**
	 * 
	 */
	private Integer terminaltype;
	/**
	 * 
	 */
	private Integer recordtime;
	/**
	 * 
	 */
	private String matchguid;
	/**
	 * 
	 */
	private String keyguid;
	/**
	 * 
	 */
	private String channelno;

	/**
	 * 设置：
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * 设置：
	 */
	public void setUserid(Integer userid) {
		this.userid = userid;
	}
	/**
	 * 获取：
	 */
	public Integer getUserid() {
		return userid;
	}
	/**
	 * 设置：
	 */
	public void setBeforescorecount(BigDecimal beforescorecount) {
		this.beforescorecount = beforescorecount;
	}
	/**
	 * 获取：
	 */
	public BigDecimal getBeforescorecount() {
		return beforescorecount;
	}
	/**
	 * 设置：
	 */
	public void setGametypeid(Integer gametypeid) {
		this.gametypeid = gametypeid;
	}
	/**
	 * 获取：
	 */
	public Integer getGametypeid() {
		return gametypeid;
	}
	/**
	 * 设置：
	 */
	public void setTerminaltype(Integer terminaltype) {
		this.terminaltype = terminaltype;
	}
	/**
	 * 获取：
	 */
	public Integer getTerminaltype() {
		return terminaltype;
	}
	/**
	 * 设置：
	 */
	public void setRecordtime(Integer recordtime) {
		this.recordtime = recordtime;
	}
	/**
	 * 获取：
	 */
	public Integer getRecordtime() {
		return recordtime;
	}
	/**
	 * 设置：
	 */
	public void setMatchguid(String matchguid) {
		this.matchguid = matchguid;
	}
	/**
	 * 获取：
	 */
	public String getMatchguid() {
		return matchguid;
	}
	/**
	 * 设置：
	 */
	public void setKeyguid(String keyguid) {
		this.keyguid = keyguid;
	}
	/**
	 * 获取：
	 */
	public String getKeyguid() {
		return keyguid;
	}
	/**
	 * 设置：
	 */
	public void setChannelno(String channelno) {
		this.channelno = channelno;
	}
	/**
	 * 获取：
	 */
	public String getChannelno() {
		return channelno;
	}
}
