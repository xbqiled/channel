package com.channel.api.entity.report;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;

/**
 * VIEW
 * 
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-05-28 14:18:49
 */
@TableName("V_PlayerSignIn")
public class PlayersigninEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private Integer id;
	/**
	 * 
	 */
	private Integer accountid;
	/**
	 * 
	 */
	private Integer todaybet;
	/**
	 * 
	 */
	private Integer dayindex;
	/**
	 * 
	 */
	private Integer award;
	/**
	 * 
	 */
	private Integer additionaward;
	/**
	 * 
	 */
	private Integer beforecoin;
	/**
	 * 
	 */
	private Integer changecoin;
	/**
	 * 
	 */
	private Integer aftercoin;
	/**
	 * 
	 */
	private Byte[] dayarr;
	/**
	 * 
	 */
	private String channel;
	/**
	 * 
	 */
	private String loginguid;
	/**
	 * 
	 */
	private String businessguid;
	/**
	 * 
	 */
	private Integer recordtime;

	/**
	 * 设置：
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * 设置：
	 */
	public void setAccountid(Integer accountid) {
		this.accountid = accountid;
	}
	/**
	 * 获取：
	 */
	public Integer getAccountid() {
		return accountid;
	}
	/**
	 * 设置：
	 */
	public void setTodaybet(Integer todaybet) {
		this.todaybet = todaybet;
	}
	/**
	 * 获取：
	 */
	public Integer getTodaybet() {
		return todaybet;
	}
	/**
	 * 设置：
	 */
	public void setDayindex(Integer dayindex) {
		this.dayindex = dayindex;
	}
	/**
	 * 获取：
	 */
	public Integer getDayindex() {
		return dayindex;
	}
	/**
	 * 设置：
	 */
	public void setAward(Integer award) {
		this.award = award;
	}
	/**
	 * 获取：
	 */
	public Integer getAward() {
		return award;
	}
	/**
	 * 设置：
	 */
	public void setAdditionaward(Integer additionaward) {
		this.additionaward = additionaward;
	}
	/**
	 * 获取：
	 */
	public Integer getAdditionaward() {
		return additionaward;
	}
	/**
	 * 设置：
	 */
	public void setBeforecoin(Integer beforecoin) {
		this.beforecoin = beforecoin;
	}
	/**
	 * 获取：
	 */
	public Integer getBeforecoin() {
		return beforecoin;
	}
	/**
	 * 设置：
	 */
	public void setChangecoin(Integer changecoin) {
		this.changecoin = changecoin;
	}
	/**
	 * 获取：
	 */
	public Integer getChangecoin() {
		return changecoin;
	}
	/**
	 * 设置：
	 */
	public void setAftercoin(Integer aftercoin) {
		this.aftercoin = aftercoin;
	}
	/**
	 * 获取：
	 */
	public Integer getAftercoin() {
		return aftercoin;
	}
	/**
	 * 设置：
	 */
	public void setDayarr(Byte[] dayarr) {
		this.dayarr = dayarr;
	}
	/**
	 * 获取：
	 */
	public Byte[] getDayarr() {
		return dayarr;
	}
	/**
	 * 设置：
	 */
	public void setChannel(String channel) {
		this.channel = channel;
	}
	/**
	 * 获取：
	 */
	public String getChannel() {
		return channel;
	}
	/**
	 * 设置：
	 */
	public void setLoginguid(String loginguid) {
		this.loginguid = loginguid;
	}
	/**
	 * 获取：
	 */
	public String getLoginguid() {
		return loginguid;
	}
	/**
	 * 设置：
	 */
	public void setBusinessguid(String businessguid) {
		this.businessguid = businessguid;
	}
	/**
	 * 获取：
	 */
	public String getBusinessguid() {
		return businessguid;
	}
	/**
	 * 设置：
	 */
	public void setRecordtime(Integer recordtime) {
		this.recordtime = recordtime;
	}
	/**
	 * 获取：
	 */
	public Integer getRecordtime() {
		return recordtime;
	}
}
