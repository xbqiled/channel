package com.channel.api.entity.report;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;
import java.util.Date;

/**
 * VIEW
 *
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-12-23 15:48:43
 */
@TableName("v_playerdayshare")
public class PlayerDayShareEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    @TableId
    private Integer id;
    /**
     *
     */
    private Integer accountid;
    /**
     *
     */
    private Integer daysharecnt;
    /**
     *
     */
    private Integer shareaward;
    /**
     *
     */
    private String channel;
    /**
     *
     */
    private String loginguid;
    /**
     *
     */
    private String businessguid;
    /**
     *
     */
    private Integer recordtime;

    /**
     * 设置：
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取：
     */
    public Integer getId() {
        return id;
    }

    /**
     * 设置：
     */
    public void setAccountid(Integer accountid) {
        this.accountid = accountid;
    }

    /**
     * 获取：
     */
    public Integer getAccountid() {
        return accountid;
    }

    /**
     * 设置：
     */
    public void setDaysharecnt(Integer daysharecnt) {
        this.daysharecnt = daysharecnt;
    }

    /**
     * 获取：
     */
    public Integer getDaysharecnt() {
        return daysharecnt;
    }

    /**
     * 设置：
     */
    public void setShareaward(Integer shareaward) {
        this.shareaward = shareaward;
    }

    /**
     * 获取：
     */
    public Integer getShareaward() {
        return shareaward;
    }

    /**
     * 设置：
     */
    public void setChannel(String channel) {
        this.channel = channel;
    }

    /**
     * 获取：
     */
    public String getChannel() {
        return channel;
    }

    /**
     * 设置：
     */
    public void setLoginguid(String loginguid) {
        this.loginguid = loginguid;
    }

    /**
     * 获取：
     */
    public String getLoginguid() {
        return loginguid;
    }

    /**
     * 设置：
     */
    public void setBusinessguid(String businessguid) {
        this.businessguid = businessguid;
    }

    /**
     * 获取：
     */
    public String getBusinessguid() {
        return businessguid;
    }

    /**
     * 设置：
     */
    public void setRecordtime(Integer recordtime) {
        this.recordtime = recordtime;
    }

    /**
     * 获取：
     */
    public Integer getRecordtime() {
        return recordtime;
    }
}
