package com.channel.api.entity.cac;

import java.io.Serializable;


public class VipVo implements Serializable {

    private String channelId;

    private byte[] vipInfo;

    private Boolean openType;

    private Integer depositType;

    private String iconUrl;

    private String channelName;

    private String cfgTime;

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public Boolean getOpenType() {
        return openType;
    }

    public void setOpenType(Boolean openType) {
        this.openType = openType;
    }

    public Integer getDepositType() {
        return depositType;
    }

    public void setDepositType(Integer depositType) {
        this.depositType = depositType;
    }

    public String getIconUrl() {
        return iconUrl;
    }

    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    public String getCfgTime() {
        return cfgTime;
    }

    public void setCfgTime(String cfgTime) {
        this.cfgTime = cfgTime;
    }

    public byte[] getVipInfo() {
        return vipInfo;
    }

    public void setVipInfo(byte[] vipInfo) {
        this.vipInfo = vipInfo;
    }
}
