package com.channel.api.entity.cac;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

import java.math.BigDecimal;
import java.io.Serializable;
import java.util.Date;

/**
 * 
 * 
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-11-15 13:23:00
 */
@TableName("other_bg_transfer")
public class BgTransferEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键
	 */
	@TableId
	private Integer id;
	/**
	 * 业务单据id
	 */
	private Long bizId;
	/**
	 * 厅号码
	 */
	private String sn;
	/**
	 * 用户ID
	 */
	private Long userId;
	/**
	 * 会计项目
	 */
	private Integer accountItem;
	/**
	 * 状态信息 1成功
	 */
	private Integer status;
	/**
	 * 金额
	 */
	private BigDecimal amount;
	/**
	 * 系统余额
	 */
	private BigDecimal sysBalance;
	/**
	 * bg余额
	 */
	private BigDecimal bgBalance;
	/**
	 * 操作时间
	 */
	private Date operateTime;

	/**
	 * 设置：主键
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * 获取：主键
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * 设置：业务单据id
	 */
	public void setBizId(Long bizId) {
		this.bizId = bizId;
	}
	/**
	 * 获取：业务单据id
	 */
	public Long getBizId() {
		return bizId;
	}
	/**
	 * 设置：厅号码
	 */
	public void setSn(String sn) {
		this.sn = sn;
	}
	/**
	 * 获取：厅号码
	 */
	public String getSn() {
		return sn;
	}
	/**
	 * 设置：用户ID
	 */
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	/**
	 * 获取：用户ID
	 */
	public Long getUserId() {
		return userId;
	}
	/**
	 * 设置：会计项目
	 */
	public void setAccountItem(Integer accountItem) {
		this.accountItem = accountItem;
	}
	/**
	 * 获取：会计项目
	 */
	public Integer getAccountItem() {
		return accountItem;
	}
	/**
	 * 设置：状态信息 1成功
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}
	/**
	 * 获取：状态信息 1成功
	 */
	public Integer getStatus() {
		return status;
	}
	/**
	 * 设置：金额
	 */
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	/**
	 * 获取：金额
	 */
	public BigDecimal getAmount() {
		return amount;
	}
	/**
	 * 设置：系统余额
	 */
	public void setSysBalance(BigDecimal sysBalance) {
		this.sysBalance = sysBalance;
	}
	/**
	 * 获取：系统余额
	 */
	public BigDecimal getSysBalance() {
		return sysBalance;
	}
	/**
	 * 设置：bg余额
	 */
	public void setBgBalance(BigDecimal bgBalance) {
		this.bgBalance = bgBalance;
	}
	/**
	 * 获取：bg余额
	 */
	public BigDecimal getBgBalance() {
		return bgBalance;
	}
	/**
	 * 设置：操作时间
	 */
	public void setOperateTime(Date operateTime) {
		this.operateTime = operateTime;
	}
	/**
	 * 获取：操作时间
	 */
	public Date getOperateTime() {
		return operateTime;
	}
}
