package com.channel.api.entity.cac;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

import java.math.BigDecimal;
import java.io.Serializable;
import java.util.Date;

/**
 * 
 * 
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2020-01-14 14:39:56
 */
@TableName("tmp_bgredpacket_exchange")
public class BgRedPacketExchangeEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 记录ID
	 */
	@TableId
	private Long pid;
	/**
	 * 厅代码
	 */
	private String sn;
	/**
	 * 用户ID
	 */
	private Long uid;
	/**
	 * 兑换时间
	 */
	private Date time;
	/**
	 * 用户登录ID
	 */
	private String account;
	/**
	 * 红包余额
	 */
	private BigDecimal balance;
	/**
	 * 兑换金额
	 */
	private BigDecimal amount;

	/**
	 * 设置：记录ID
	 */
	public void setPid(Long pid) {
		this.pid = pid;
	}
	/**
	 * 获取：记录ID
	 */
	public Long getPid() {
		return pid;
	}
	/**
	 * 设置：厅代码
	 */
	public void setSn(String sn) {
		this.sn = sn;
	}
	/**
	 * 获取：厅代码
	 */
	public String getSn() {
		return sn;
	}
	/**
	 * 设置：用户ID
	 */
	public void setUid(Long uid) {
		this.uid = uid;
	}
	/**
	 * 获取：用户ID
	 */
	public Long getUid() {
		return uid;
	}
	/**
	 * 设置：兑换时间
	 */
	public void setTime(Date time) {
		this.time = time;
	}
	/**
	 * 获取：兑换时间
	 */
	public Date getTime() {
		return time;
	}
	/**
	 * 设置：用户登录ID
	 */
	public void setAccount(String account) {
		this.account = account;
	}
	/**
	 * 获取：用户登录ID
	 */
	public String getAccount() {
		return account;
	}
	/**
	 * 设置：红包余额
	 */
	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}
	/**
	 * 获取：红包余额
	 */
	public BigDecimal getBalance() {
		return balance;
	}
	/**
	 * 设置：兑换金额
	 */
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	/**
	 * 获取：兑换金额
	 */
	public BigDecimal getAmount() {
		return amount;
	}
}
