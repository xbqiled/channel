package com.channel.api.entity.cac;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * 
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2020-05-27 20:42:22
 */
@TableName("sys_push_config")
public class PushConfigEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * ID
	 */
	@TableId
	private Integer id;
	/**
	 * 名称
	 */
	private String name;
	/**
	 * 通道编码
	 */
	private Integer platformId;
	/**
	 * 配置
	 */
	private String config;
	/**
	 * 有效性标识,只有一个有效
	 */
	private Integer isActivate;
	/**
	 * 描述
	 */
	private String description;
	/**
	 * 渠道编码
	 */
	private String channelId;
	/**
	 * 当日推送次数
	 */
	private Long dayCount;
	/**
	 * 当日最大推送次数
	 */
	private Long dayMaxCount;
	/**
	 * 总计推送次数
	 */
	private Long totalCount;
	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 创建人
	 */
	private String createBy;
	/**
	 * 最后一次更新时间
	 */
	private Date lastUpdateTime;

	/**
	 * 设置：ID
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * 获取：ID
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * 设置：名称
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * 获取：名称
	 */
	public String getName() {
		return name;
	}
	/**
	 * 设置：通道编码
	 */
	public void setPlatformId(Integer platformId) {
		this.platformId = platformId;
	}
	/**
	 * 获取：通道编码
	 */
	public Integer getPlatformId() {
		return platformId;
	}
	/**
	 * 设置：配置
	 */
	public void setConfig(String config) {
		this.config = config;
	}
	/**
	 * 获取：配置
	 */
	public String getConfig() {
		return config;
	}
	/**
	 * 设置：有效性标识,只有一个有效
	 */
	public void setIsActivate(Integer isActivate) {
		this.isActivate = isActivate;
	}
	/**
	 * 获取：有效性标识,只有一个有效
	 */
	public Integer getIsActivate() {
		return isActivate;
	}
	/**
	 * 设置：描述
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	/**
	 * 获取：描述
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * 设置：渠道编码
	 */
	public void setChannelId(String channelId) {
		this.channelId = channelId;
	}
	/**
	 * 获取：渠道编码
	 */
	public String getChannelId() {
		return channelId;
	}
	/**
	 * 设置：当日推送次数
	 */
	public void setDayCount(Long dayCount) {
		this.dayCount = dayCount;
	}
	/**
	 * 获取：当日推送次数
	 */
	public Long getDayCount() {
		return dayCount;
	}
	/**
	 * 设置：当日最大推送次数
	 */
	public void setDayMaxCount(Long dayMaxCount) {
		this.dayMaxCount = dayMaxCount;
	}
	/**
	 * 获取：当日最大推送次数
	 */
	public Long getDayMaxCount() {
		return dayMaxCount;
	}
	/**
	 * 设置：总计推送次数
	 */
	public void setTotalCount(Long totalCount) {
		this.totalCount = totalCount;
	}
	/**
	 * 获取：总计推送次数
	 */
	public Long getTotalCount() {
		return totalCount;
	}
	/**
	 * 设置：创建时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：创建时间
	 */
	public Date getCreateTime() {
		return createTime;
	}
	/**
	 * 设置：创建人
	 */
	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}
	/**
	 * 获取：创建人
	 */
	public String getCreateBy() {
		return createBy;
	}
	/**
	 * 设置：最后一次更新时间
	 */
	public void setLastUpdateTime(Date lastUpdateTime) {
		this.lastUpdateTime = lastUpdateTime;
	}
	/**
	 * 获取：最后一次更新时间
	 */
	public Date getLastUpdateTime() {
		return lastUpdateTime;
	}
}
