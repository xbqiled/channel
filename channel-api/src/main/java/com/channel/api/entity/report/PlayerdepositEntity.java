package com.channel.api.entity.report;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;

/**
 * VIEW
 * 
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-05-29 13:35:09
 */
@TableName("V_PlayerDeposit")
public class PlayerdepositEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private Integer id;
	/**
	 * 
	 */
	private Integer accountid;
	/**
	 * 
	 */
	private String tOrderid;
	/**
	 * 
	 */
	private Integer rechargecoin;
	/**
	 * 
	 */
	private Integer optype;
	/**
	 * 
	 */
	private Integer rebatecoin;
	/**
	 * 
	 */
	private Integer beforecoin;
	/**
	 * 
	 */
	private Integer changecoin;
	/**
	 * 
	 */
	private Integer aftercoin;
	/**
	 * 
	 */
	private String channel;
	/**
	 * 
	 */
	private String loginguid;
	/**
	 * 
	 */
	private String businessguid;
	/**
	 * 
	 */
	private Integer recordtime;

	/**
	 * 设置：
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * 设置：
	 */
	public void setAccountid(Integer accountid) {
		this.accountid = accountid;
	}
	/**
	 * 获取：
	 */
	public Integer getAccountid() {
		return accountid;
	}
	/**
	 * 设置：
	 */
	public void setTOrderid(String tOrderid) {
		this.tOrderid = tOrderid;
	}
	/**
	 * 获取：
	 */
	public String getTOrderid() {
		return tOrderid;
	}
	/**
	 * 设置：
	 */
	public void setRechargecoin(Integer rechargecoin) {
		this.rechargecoin = rechargecoin;
	}
	/**
	 * 获取：
	 */
	public Integer getRechargecoin() {
		return rechargecoin;
	}
	/**
	 * 设置：
	 */
	public void setOptype(Integer optype) {
		this.optype = optype;
	}
	/**
	 * 获取：
	 */
	public Integer getOptype() {
		return optype;
	}
	/**
	 * 设置：
	 */
	public void setRebatecoin(Integer rebatecoin) {
		this.rebatecoin = rebatecoin;
	}
	/**
	 * 获取：
	 */
	public Integer getRebatecoin() {
		return rebatecoin;
	}
	/**
	 * 设置：
	 */
	public void setBeforecoin(Integer beforecoin) {
		this.beforecoin = beforecoin;
	}
	/**
	 * 获取：
	 */
	public Integer getBeforecoin() {
		return beforecoin;
	}
	/**
	 * 设置：
	 */
	public void setChangecoin(Integer changecoin) {
		this.changecoin = changecoin;
	}
	/**
	 * 获取：
	 */
	public Integer getChangecoin() {
		return changecoin;
	}
	/**
	 * 设置：
	 */
	public void setAftercoin(Integer aftercoin) {
		this.aftercoin = aftercoin;
	}
	/**
	 * 获取：
	 */
	public Integer getAftercoin() {
		return aftercoin;
	}
	/**
	 * 设置：
	 */
	public void setChannel(String channel) {
		this.channel = channel;
	}
	/**
	 * 获取：
	 */
	public String getChannel() {
		return channel;
	}
	/**
	 * 设置：
	 */
	public void setLoginguid(String loginguid) {
		this.loginguid = loginguid;
	}
	/**
	 * 获取：
	 */
	public String getLoginguid() {
		return loginguid;
	}
	/**
	 * 设置：
	 */
	public void setBusinessguid(String businessguid) {
		this.businessguid = businessguid;
	}
	/**
	 * 获取：
	 */
	public String getBusinessguid() {
		return businessguid;
	}
	/**
	 * 设置：
	 */
	public void setRecordtime(Integer recordtime) {
		this.recordtime = recordtime;
	}
	/**
	 * 获取：
	 */
	public Integer getRecordtime() {
		return recordtime;
	}
}
