package com.channel.api.entity.cac;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

import java.math.BigDecimal;
import java.io.Serializable;
import java.util.Date;

/**
 * 
 * 
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2020-02-28 14:18:36
 */
@TableName("tmp_statis_channel")
public class StatisChannelEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键
	 */
	@TableId
	private Long id;
	/**
	 * 统计时间
	 */
	private Date statisTime;
	/**
	 * 渠道编码
	 */
	private String channelId;
	/**
	 * 充值金额
	 */
	private BigDecimal rechargeFee;
	/**
	 * 充值人数
	 */
	private Long rechargeUserCount;
	/**
	 * 充值笔数
	 */
	private Long rechargeCount;
	/**
	 * 提现金额
	 */
	private BigDecimal cashFee;
	/**
	 * 提现人数
	 */
	private Long cashUserCount;
	/**
	 * 提现笔数
	 */
	private Long cashCount;
	/**
	 * 盈亏统计
	 */
	private BigDecimal loss;
	/**
	 * 新增注册人数
	 */
	private Long regUserCount;
	/**
	 * 新增游戏人数
	 */
	private Long newGameUserCount;
	/**
	 * 游戏次数
	 */
	private Long gameCount;
	/**
	 * 游戏人数
	 */
	private Long gameUserCount;
	/**
	 * 登录次数
	 */
	private Long loginCount;
	/**
	 * 登录人数
	 */
	private Long loginUserCount;
	/**
	 * 日活跃用户
	 */
	private Long dailyActiveCount;
	/**
	 * 首冲金额
	 */
	private BigDecimal firstRechargeFee;
	/**
	 * 首次充值人数
	 */
	private Long firstRechargeCount;
	/**
	 * 用户游戏总时长
	 */
	private Long timeLength;
	/**
	 * 用户平均时长
	 */
	private Long userAvgGametime;
	/**
	 * 充值率
	 */
	private Long rechargeRate;
	/**
	 * 步骤
	 */
	private Integer step;

	/**
	 * 设置：主键
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * 获取：主键
	 */
	public Long getId() {
		return id;
	}
	/**
	 * 设置：统计时间
	 */
	public void setStatisTime(Date statisTime) {
		this.statisTime = statisTime;
	}
	/**
	 * 获取：统计时间
	 */
	public Date getStatisTime() {
		return statisTime;
	}
	/**
	 * 设置：渠道编码
	 */
	public void setChannelId(String channelId) {
		this.channelId = channelId;
	}
	/**
	 * 获取：渠道编码
	 */
	public String getChannelId() {
		return channelId;
	}
	/**
	 * 设置：充值金额
	 */
	public void setRechargeFee(BigDecimal rechargeFee) {
		this.rechargeFee = rechargeFee;
	}
	/**
	 * 获取：充值金额
	 */
	public BigDecimal getRechargeFee() {
		return rechargeFee;
	}
	/**
	 * 设置：充值人数
	 */
	public void setRechargeUserCount(Long rechargeUserCount) {
		this.rechargeUserCount = rechargeUserCount;
	}
	/**
	 * 获取：充值人数
	 */
	public Long getRechargeUserCount() {
		return rechargeUserCount;
	}
	/**
	 * 设置：充值笔数
	 */
	public void setRechargeCount(Long rechargeCount) {
		this.rechargeCount = rechargeCount;
	}
	/**
	 * 获取：充值笔数
	 */
	public Long getRechargeCount() {
		return rechargeCount;
	}
	/**
	 * 设置：提现金额
	 */
	public void setCashFee(BigDecimal cashFee) {
		this.cashFee = cashFee;
	}
	/**
	 * 获取：提现金额
	 */
	public BigDecimal getCashFee() {
		return cashFee;
	}
	/**
	 * 设置：提现人数
	 */
	public void setCashUserCount(Long cashUserCount) {
		this.cashUserCount = cashUserCount;
	}
	/**
	 * 获取：提现人数
	 */
	public Long getCashUserCount() {
		return cashUserCount;
	}
	/**
	 * 设置：提现笔数
	 */
	public void setCashCount(Long cashCount) {
		this.cashCount = cashCount;
	}
	/**
	 * 获取：提现笔数
	 */
	public Long getCashCount() {
		return cashCount;
	}
	/**
	 * 设置：盈亏统计
	 */
	public void setLoss(BigDecimal loss) {
		this.loss = loss;
	}
	/**
	 * 获取：盈亏统计
	 */
	public BigDecimal getLoss() {
		return loss;
	}
	/**
	 * 设置：新增注册人数
	 */
	public void setRegUserCount(Long regUserCount) {
		this.regUserCount = regUserCount;
	}
	/**
	 * 获取：新增注册人数
	 */
	public Long getRegUserCount() {
		return regUserCount;
	}
	/**
	 * 设置：新增游戏人数
	 */
	public void setNewGameUserCount(Long newGameUserCount) {
		this.newGameUserCount = newGameUserCount;
	}
	/**
	 * 获取：新增游戏人数
	 */
	public Long getNewGameUserCount() {
		return newGameUserCount;
	}
	/**
	 * 设置：游戏次数
	 */
	public void setGameCount(Long gameCount) {
		this.gameCount = gameCount;
	}
	/**
	 * 获取：游戏次数
	 */
	public Long getGameCount() {
		return gameCount;
	}
	/**
	 * 设置：游戏人数
	 */
	public void setGameUserCount(Long gameUserCount) {
		this.gameUserCount = gameUserCount;
	}
	/**
	 * 获取：游戏人数
	 */
	public Long getGameUserCount() {
		return gameUserCount;
	}
	/**
	 * 设置：登录次数
	 */
	public void setLoginCount(Long loginCount) {
		this.loginCount = loginCount;
	}
	/**
	 * 获取：登录次数
	 */
	public Long getLoginCount() {
		return loginCount;
	}
	/**
	 * 设置：登录人数
	 */
	public void setLoginUserCount(Long loginUserCount) {
		this.loginUserCount = loginUserCount;
	}
	/**
	 * 获取：登录人数
	 */
	public Long getLoginUserCount() {
		return loginUserCount;
	}
	/**
	 * 设置：日活跃用户
	 */
	public void setDailyActiveCount(Long dailyActiveCount) {
		this.dailyActiveCount = dailyActiveCount;
	}
	/**
	 * 获取：日活跃用户
	 */
	public Long getDailyActiveCount() {
		return dailyActiveCount;
	}
	/**
	 * 设置：首冲金额
	 */
	public void setFirstRechargeFee(BigDecimal firstRechargeFee) {
		this.firstRechargeFee = firstRechargeFee;
	}
	/**
	 * 获取：首冲金额
	 */
	public BigDecimal getFirstRechargeFee() {
		return firstRechargeFee;
	}
	/**
	 * 设置：首次充值人数
	 */
	public void setFirstRechargeCount(Long firstRechargeCount) {
		this.firstRechargeCount = firstRechargeCount;
	}
	/**
	 * 获取：首次充值人数
	 */
	public Long getFirstRechargeCount() {
		return firstRechargeCount;
	}
	/**
	 * 设置：用户游戏总时长
	 */
	public void setTimeLength(Long timeLength) {
		this.timeLength = timeLength;
	}
	/**
	 * 获取：用户游戏总时长
	 */
	public Long getTimeLength() {
		return timeLength;
	}
	/**
	 * 设置：用户平均时长
	 */
	public void setUserAvgGametime(Long userAvgGametime) {
		this.userAvgGametime = userAvgGametime;
	}
	/**
	 * 获取：用户平均时长
	 */
	public Long getUserAvgGametime() {
		return userAvgGametime;
	}
	/**
	 * 设置：充值率
	 */
	public void setRechargeRate(Long rechargeRate) {
		this.rechargeRate = rechargeRate;
	}
	/**
	 * 获取：充值率
	 */
	public Long getRechargeRate() {
		return rechargeRate;
	}
	/**
	 * 设置：步骤
	 */
	public void setStep(Integer step) {
		this.step = step;
	}
	/**
	 * 获取：步骤
	 */
	public Integer getStep() {
		return step;
	}
}
