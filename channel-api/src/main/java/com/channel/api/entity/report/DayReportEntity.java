package com.channel.api.entity.report;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * VIEW
 * 
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-02-24 14:04:07
 */
@TableName("V_Day_Report")
public class DayReportEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private Long regcount;
	/**
	 * 
	 */
	private Long newgamecount;
	/**
	 * 
	 */
	private Long logincount;
	/**
	 * 
	 */
	private Long halfhourusercount;
	/**
	 * 
	 */
	private BigDecimal gametimecount;
	/**
	 * 
	 */
	private BigDecimal timelength;
	/**
	 * 
	 */
	private BigDecimal newuseravggametime;
	/**
	 * 
	 */
	private Long loginusercount;
	/**
	 * 
	 */
	private Long dailyactivecount;
	/**
	 * 
	 */
	private Long ordercount;
	/**
	 * 
	 */
	private Long rechargecount;
	/**
	 *
	 */
	private Long logingamecount;
	/**
	 * 
	 */
	private BigDecimal totalrechargemoney;
	/**
	 * 
	 */
	private Double arpu;
	/**
	 * 
	 */
	private BigDecimal rechargerate;
	/**
	 *
	 */
	private BigDecimal gaincount;
	/**
	 * 
	 */
	private String recordtime;
	/**
	 * 
	 */
	private String channelid;

	/**
	 * 设置：
	 */
	public void setRegcount(Long regcount) {
		this.regcount = regcount;
	}
	/**
	 * 获取：
	 */
	public Long getRegcount() {
		return regcount;
	}
	/**
	 * 设置：
	 */
	public void setNewgamecount(Long newgamecount) {
		this.newgamecount = newgamecount;
	}
	/**
	 * 获取：
	 */
	public Long getNewgamecount() {
		return newgamecount;
	}
	/**
	 * 设置：
	 */
	public void setLogincount(Long logincount) {
		this.logincount = logincount;
	}
	/**
	 * 获取：
	 */
	public Long getLogincount() {
		return logincount;
	}
	/**
	 * 设置：
	 */
	public void setHalfhourusercount(Long halfhourusercount) {
		this.halfhourusercount = halfhourusercount;
	}
	/**
	 * 获取：
	 */
	public Long getHalfhourusercount() {
		return halfhourusercount;
	}
	/**
	 * 设置：
	 */
	public void setGametimecount(BigDecimal gametimecount) {
		this.gametimecount = gametimecount;
	}
	/**
	 * 获取：
	 */
	public BigDecimal getGametimecount() {
		return gametimecount;
	}
	/**
	 * 设置：
	 */
	public void setTimelength(BigDecimal timelength) {
		this.timelength = timelength;
	}
	/**
	 * 获取：
	 */
	public BigDecimal getTimelength() {
		return timelength;
	}
	/**
	 * 设置：
	 */
	public void setNewuseravggametime(BigDecimal newuseravggametime) {
		this.newuseravggametime = newuseravggametime;
	}
	/**
	 * 获取：
	 */
	public BigDecimal getNewuseravggametime() {
		return newuseravggametime;
	}
	/**
	 * 设置：
	 */
	public void setLoginusercount(Long loginusercount) {
		this.loginusercount = loginusercount;
	}
	/**
	 * 获取：
	 */
	public Long getLoginusercount() {
		return loginusercount;
	}
	/**
	 * 设置：
	 */
	public void setDailyactivecount(Long dailyactivecount) {
		this.dailyactivecount = dailyactivecount;
	}
	/**
	 * 获取：
	 */
	public Long getDailyactivecount() {
		return dailyactivecount;
	}
	/**
	 * 设置：
	 */
	public void setOrdercount(Long ordercount) {
		this.ordercount = ordercount;
	}
	/**
	 * 获取：
	 */
	public Long getOrdercount() {
		return ordercount;
	}
	/**
	 * 设置：
	 */
	public void setRechargecount(Long rechargecount) {
		this.rechargecount = rechargecount;
	}
	/**
	 * 获取：
	 */
	public Long getRechargecount() {
		return rechargecount;
	}
	/**
	 * 设置：
	 */
	public void setTotalrechargemoney(BigDecimal totalrechargemoney) {
		this.totalrechargemoney = totalrechargemoney;
	}
	/**
	 * 获取：
	 */
	public BigDecimal getTotalrechargemoney() {
		return totalrechargemoney;
	}
	/**
	 * 设置：
	 */
	public void setArpu(Double arpu) {
		this.arpu = arpu;
	}
	/**
	 * 获取：
	 */
	public Double getArpu() {
		return arpu;
	}
	/**
	 * 设置：
	 */
	public void setRechargerate(BigDecimal rechargerate) {
		this.rechargerate = rechargerate;
	}
	/**
	 * 获取：
	 */
	public BigDecimal getRechargerate() {
		return rechargerate;
	}
	/**
	 * 设置：
	 */
	public void setRecordtime(String recordtime) {
		this.recordtime = recordtime;
	}
	/**
	 * 获取：
	 */
	public String getRecordtime() {
		return recordtime;
	}

	public BigDecimal getGaincount() {
		return gaincount;
	}

	public void setGaincount(BigDecimal gaincount) {
		this.gaincount = gaincount;
	}

	public String getChannelid() {
		return channelid;
	}

	public void setChannelid(String channelid) {
		this.channelid = channelid;
	}

	public Long getLogingamecount() {
		return logingamecount;
	}

	public void setLogingamecount(Long logingamecount) {
		this.logingamecount = logingamecount;
	}
}
