package com.channel.api.entity.report;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;

/**
 * VIEW
 * 
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-05-29 12:18:47
 */
@TableName("V_RecordUserLogoutGame")
public class RecordlogoutgameEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private Integer id;
	/**
	 * 
	 */
	private Integer userid;
	/**
	 * 
	 */
	private Integer experience;
	/**
	 * 
	 */
	private Integer gametypeid;
	/**
	 * 
	 */
	private Integer terminaltype;
	/**
	 * 
	 */
	private Integer gametimecount;
	/**
	 * 
	 */
	private Integer recordtime;
	/**
	 * 
	 */
	private String matchguid;
	/**
	 * 
	 */
	private String keyguid;
	/**
	 * 
	 */
	private String channelno;

	/**
	 * 设置：
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * 设置：
	 */
	public void setUserid(Integer userid) {
		this.userid = userid;
	}
	/**
	 * 获取：
	 */
	public Integer getUserid() {
		return userid;
	}
	/**
	 * 设置：
	 */
	public void setExperience(Integer experience) {
		this.experience = experience;
	}
	/**
	 * 获取：
	 */
	public Integer getExperience() {
		return experience;
	}
	/**
	 * 设置：
	 */
	public void setGametypeid(Integer gametypeid) {
		this.gametypeid = gametypeid;
	}
	/**
	 * 获取：
	 */
	public Integer getGametypeid() {
		return gametypeid;
	}
	/**
	 * 设置：
	 */
	public void setTerminaltype(Integer terminaltype) {
		this.terminaltype = terminaltype;
	}
	/**
	 * 获取：
	 */
	public Integer getTerminaltype() {
		return terminaltype;
	}
	/**
	 * 设置：
	 */
	public void setGametimecount(Integer gametimecount) {
		this.gametimecount = gametimecount;
	}
	/**
	 * 获取：
	 */
	public Integer getGametimecount() {
		return gametimecount;
	}
	/**
	 * 设置：
	 */
	public void setRecordtime(Integer recordtime) {
		this.recordtime = recordtime;
	}
	/**
	 * 获取：
	 */
	public Integer getRecordtime() {
		return recordtime;
	}
	/**
	 * 设置：
	 */
	public void setMatchguid(String matchguid) {
		this.matchguid = matchguid;
	}
	/**
	 * 获取：
	 */
	public String getMatchguid() {
		return matchguid;
	}
	/**
	 * 设置：
	 */
	public void setKeyguid(String keyguid) {
		this.keyguid = keyguid;
	}
	/**
	 * 获取：
	 */
	public String getKeyguid() {
		return keyguid;
	}
	/**
	 * 设置：
	 */
	public void setChannelno(String channelno) {
		this.channelno = channelno;
	}
	/**
	 * 获取：
	 */
	public String getChannelno() {
		return channelno;
	}
}
