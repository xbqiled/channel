package com.channel.api.entity.report;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;


/**
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-12-30 13:46:48
 */
@TableName("configure_channeltask")
public class TaskConfigureEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 任务id
	 */
	@TableId
	private Integer tTaskid;
	/**
	 * 是否对目标数量进行转换
	 */
	private Integer tIsconver;
	/**
	 * 游戏类型
	 */
	private Integer tGametype;
	/**
	 * 游戏名称
	 */
	private String tGamename;
	/**
	 * 任务标题
	 */
	private String tTasktitle;
	/**
	 * 任务描述
	 */
	private String tTaskdesc;

	/**
	 * 设置：任务id
	 */
	public void setTTaskid(Integer tTaskid) {
		this.tTaskid = tTaskid;
	}
	/**
	 * 获取：任务id
	 */
	public Integer getTTaskid() {
		return tTaskid;
	}
	/**
	 * 设置：是否对目标数量进行转换
	 */
	public void setTIsconver(Integer tIsconver) {
		this.tIsconver = tIsconver;
	}
	/**
	 * 获取：是否对目标数量进行转换
	 */
	public Integer getTIsconver() {
		return tIsconver;
	}
	/**
	 * 设置：游戏类型
	 */
	public void setTGametype(Integer tGametype) {
		this.tGametype = tGametype;
	}
	/**
	 * 获取：游戏类型
	 */
	public Integer getTGametype() {
		return tGametype;
	}
	/**
	 * 设置：游戏名称
	 */
	public void setTGamename(String tGamename) {
		this.tGamename = tGamename;
	}
	/**
	 * 获取：游戏名称
	 */
	public String getTGamename() {
		return tGamename;
	}
	/**
	 * 设置：任务标题
	 */
	public void setTTasktitle(String tTasktitle) {
		this.tTasktitle = tTasktitle;
	}
	/**
	 * 获取：任务标题
	 */
	public String getTTasktitle() {
		return tTasktitle;
	}
	/**
	 * 设置：任务描述
	 */
	public void setTTaskdesc(String tTaskdesc) {
		this.tTaskdesc = tTaskdesc;
	}
	/**
	 * 获取：任务描述
	 */
	public String getTTaskdesc() {
		return tTaskdesc;
	}
}
