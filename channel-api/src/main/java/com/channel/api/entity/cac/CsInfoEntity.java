package com.channel.api.entity.cac;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * 
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-03-29 17:46:03
 */
@TableName("t_cs_info")
public class CsInfoEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private Integer id;
	/**
	 * 客服名称
	 */
	private String csName;
	/**
	 * 渠道编码
	 */
	private String channelId;
	/**
	 * 类型: 1,微信， 2，QQ， 3,URL
	 */
	private Integer connetType;
	/**
	 * 客服类型: 1,推广客服 2,游戏客服
	 */
	private Integer type;
	/**
	 * 账号或URL
	 */
	private String accountOrUrl;
	/**
	 * 状态 1, 正常， 0，失效
	 */
	private Integer status;
	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 创建人
	 */
	private String createBy;

	/**
	 * 设置：
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * 设置：客服名称
	 */
	public void setCsName(String csName) {
		this.csName = csName;
	}
	/**
	 * 获取：客服名称
	 */
	public String getCsName() {
		return csName;
	}
	/**
	 * 设置：渠道编码
	 */
	public void setChannelId(String channelId) {
		this.channelId = channelId;
	}
	/**
	 * 获取：渠道编码
	 */
	public String getChannelId() {
		return channelId;
	}

	public Integer getConnetType() {
		return connetType;
	}

	public void setConnetType(Integer connetType) {
		this.connetType = connetType;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}
	/**
	 * 设置：账号或URL
	 */
	public void setAccountOrUrl(String accountOrUrl) {
		this.accountOrUrl = accountOrUrl;
	}
	/**
	 * 获取：账号或URL
	 */
	public String getAccountOrUrl() {
		return accountOrUrl;
	}
	/**
	 * 设置：状态 1, 正常， 0，失效
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}
	/**
	 * 获取：状态 1, 正常， 0，失效
	 */
	public Integer getStatus() {
		return status;
	}
	/**
	 * 设置：创建时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：创建时间
	 */
	public Date getCreateTime() {
		return createTime;
	}
	/**
	 * 设置：创建人
	 */
	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}
	/**
	 * 获取：创建人
	 */
	public String getCreateBy() {
		return createBy;
	}
}
