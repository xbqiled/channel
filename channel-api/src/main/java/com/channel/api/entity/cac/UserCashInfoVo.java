package com.channel.api.entity.cac;


import java.io.Serializable;


public class UserCashInfoVo implements Serializable {

    private String bank;
    private String bankRealName;
    private String bankPayId;
    private String subBank;
    private String aliPayId;
    private String aliRealName;

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public String getBankRealName() {
        return bankRealName;
    }

    public void setBankRealName(String bankRealName) {
        this.bankRealName = bankRealName;
    }

    public String getBankPayId() {
        return bankPayId;
    }

    public void setBankPayId(String bankPayId) {
        this.bankPayId = bankPayId;
    }

    public String getSubBank() {
        return subBank;
    }

    public void setSubBank(String subBank) {
        this.subBank = subBank;
    }

    public String getAliPayId() {
        return aliPayId;
    }

    public void setAliPayId(String aliPayId) {
        this.aliPayId = aliPayId;
    }

    public String getAliRealName() {
        return aliRealName;
    }

    public void setAliRealName(String aliRealName) {
        this.aliRealName = aliRealName;
    }
}
