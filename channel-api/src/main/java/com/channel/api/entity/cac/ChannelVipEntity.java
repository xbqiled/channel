package com.channel.api.entity.cac;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;
import java.util.Date;

/**
 * VIEW
 *
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-12-17 14:48:34
 */
@TableName("v_channelvip")
public class ChannelVipEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    @TableId
    private String tChannel;
    /**
     *
     */
    private Integer tCfgtime;
    /**
     *
     */
    private Integer tOpentype;
    /**
     *
     */
    private byte[] tVipinfo;

    /**
     * 设置：
     */
    public void setTChannel(String tChannel) {
        this.tChannel = tChannel;
    }

    /**
     * 获取：
     */
    public String getTChannel() {
        return tChannel;
    }

    /**
     * 设置：
     */
    public void setTCfgtime(Integer tCfgtime) {
        this.tCfgtime = tCfgtime;
    }

    /**
     * 获取：
     */
    public Integer getTCfgtime() {
        return tCfgtime;
    }

    /**
     * 设置：
     */
    public void setTOpentype(Integer tOpentype) {
        this.tOpentype = tOpentype;
    }

    /**
     * 获取：
     */
    public Integer getTOpentype() {
        return tOpentype;
    }

    /**
     * 设置：
     */
    public void setTVipinfo(byte[] tVipinfo) {
        this.tVipinfo = tVipinfo;
    }

    /**
     * 获取：
     */
    public byte[] getTVipinfo() {
        return tVipinfo;
    }
}
