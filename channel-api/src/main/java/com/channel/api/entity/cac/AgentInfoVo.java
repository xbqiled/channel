package com.channel.api.entity.cac;

import java.io.Serializable;





public class AgentInfoVo implements Serializable {

    private Long accountID;
    private Long promoterId;
    private Long totalNum;

    private Long nextNum;
    private Long histWin;
    private Long canGet;

    private Long nextC;
    private Long otherC;

    private Boolean hasChildren;

    public Long getAccountID() {
        return accountID;
    }

    public void setAccountID(Long accountID) {
        this.accountID = accountID;
    }

    public Long getPromoterId() {
        return promoterId;
    }

    public void setPromoterId(Long promoterId) {
        this.promoterId = promoterId;
    }

    public Long getTotalNum() {
        return totalNum;
    }

    public void setTotalNum(Long totalNum) {
        this.totalNum = totalNum;
    }

    public Long getNextNum() {
        return nextNum;
    }

    public void setNextNum(Long nextNum) {
        this.nextNum = nextNum;
    }

    public Long getHistWin() {
        return histWin;
    }

    public void setHistWin(Long histWin) {
        this.histWin = histWin;
    }

    public Long getCanGet() {
        return canGet;
    }

    public void setCanGet(Long canGet) {
        this.canGet = canGet;
    }

    public Long getNextC() {
        return nextC;
    }

    public void setNextC(Long nextC) {
        this.nextC = nextC;
    }

    public Long getOtherC() {
        return otherC;
    }

    public void setOtherC(Long otherC) {
        this.otherC = otherC;
    }

    public Boolean getHasChildren() {
        return hasChildren;
    }

    public void setHasChildren(Boolean hasChildren) {
        this.hasChildren = hasChildren;
    }
}
