package com.channel.api.entity.cac;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;

/**
 * VIEW
 * 
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-03-28 15:24:52
 */
@TableName("V_bulletin2Channel")
public class Bulletin2channelEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 公告ID
	 */
	@TableId
	private String tBulletinid;
	/**
	 * 如果为空表示针对所有渠道
	 */
	private String tChannelid;

	/**
	 * 设置：公告ID
	 */
	public void setTBulletinid(String tBulletinid) {
		this.tBulletinid = tBulletinid;
	}
	/**
	 * 获取：公告ID
	 */
	public String getTBulletinid() {
		return tBulletinid;
	}
	/**
	 * 设置：如果为空表示针对所有渠道
	 */
	public void setTChannelid(String tChannelid) {
		this.tChannelid = tChannelid;
	}
	/**
	 * 获取：如果为空表示针对所有渠道
	 */
	public String getTChannelid() {
		return tChannelid;
	}
}
