package com.channel.api.entity.cac;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;

/**
 * 
 * 
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-02-23 14:34:24
 */
@TableName("v_userBaseAttr")
public class UserbaseattrEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 *
	 */
	@TableId
	private Integer tAccountid;
	/**
	 *
	 */
	private Integer tSex;
	/**
	 *
	 */
	private Integer tGoldcoin;
	/**
	 *
	 */
	private Integer tAvatarid;
	/**
	 *
	 */
	private String tBankpassword;
	/**
	 *
	 */
	private Integer tBankgoldcoin;
	/**
	 *
	 */
	private Integer tIsmodifiednick;
	/**
	 *
	 */
	private byte[] tModulescattereddata;
	/**
	 *
	 */
	private Integer tTotalgametime;
	/**
	 *
	 */
	private Integer tTodaygametime;
	/**
	 *
	 */
	private Integer tTodaygametimerefreshtime;
	/**
	 *
	 */
	private Integer tTotalcoinget;
	/**
	 *
	 */
	private Integer tTodaycoinget;
	/**
	 *
	 */
	private Integer tTodaycoinrefreshtime;
	/**
	 *
	 */
	private Integer tRechargemoney;
	/**
	 *
	 */
	private Integer tLastrechargetime;
	/**
	 *
	 */
	private byte[] tGameinfo;
	/**
	 *
	 */
	private Integer tLastgamebetscore;
	/**
	 *
	 */
	private Integer tTodaygamebetscore;
	/**
	 *
	 */
	private Integer tBetscorersftime;
	/**
	 *
	 */
	private Integer tDamaamount;
	/**
	 *
	 */
	private Integer tDamarechargemoney;
	/**
	 *
	 */
	private byte[] tSignin;
	/**
	 *
	 */
	private Integer tLastsignintime;
	/**
	 *
	 */
	private Integer tLastfirsttotaldeposit;
	/**
	 *
	 */
	private Integer tLastfirsttotaldeposittime;
	/**
	 *
	 */
	private Integer tIsrecivefirsttotaldeposit;
	/**
	 *
	 */
	private Integer tDaysharecnt;
	/**
	 *
	 */
	private Integer tLastdaysharetime;
	/**
	 *
	 */
	private byte[] tVipinfo;
	/**
	 *
	 */
	private Integer tSuccourcnt;
	/**
	 *
	 */
	private Integer tLastrecivesuccourtime;
	/**
	 *
	 */
	private Integer tViplevel;


	public Integer gettAccountid() {
		return tAccountid;
	}

	public void settAccountid(Integer tAccountid) {
		this.tAccountid = tAccountid;
	}

	public Integer gettSex() {
		return tSex;
	}

	public void settSex(Integer tSex) {
		this.tSex = tSex;
	}

	public Integer gettGoldcoin() {
		return tGoldcoin;
	}

	public void settGoldcoin(Integer tGoldcoin) {
		this.tGoldcoin = tGoldcoin;
	}

	public Integer gettAvatarid() {
		return tAvatarid;
	}

	public void settAvatarid(Integer tAvatarid) {
		this.tAvatarid = tAvatarid;
	}

	public String gettBankpassword() {
		return tBankpassword;
	}

	public void settBankpassword(String tBankpassword) {
		this.tBankpassword = tBankpassword;
	}

	public Integer gettBankgoldcoin() {
		return tBankgoldcoin;
	}

	public void settBankgoldcoin(Integer tBankgoldcoin) {
		this.tBankgoldcoin = tBankgoldcoin;
	}

	public Integer gettIsmodifiednick() {
		return tIsmodifiednick;
	}

	public void settIsmodifiednick(Integer tIsmodifiednick) {
		this.tIsmodifiednick = tIsmodifiednick;
	}

	public byte[] gettModulescattereddata() {
		return tModulescattereddata;
	}

	public void settModulescattereddata(byte[] tModulescattereddata) {
		this.tModulescattereddata = tModulescattereddata;
	}

	public Integer gettTotalgametime() {
		return tTotalgametime;
	}

	public void settTotalgametime(Integer tTotalgametime) {
		this.tTotalgametime = tTotalgametime;
	}

	public Integer gettTodaygametime() {
		return tTodaygametime;
	}

	public void settTodaygametime(Integer tTodaygametime) {
		this.tTodaygametime = tTodaygametime;
	}

	public Integer gettTodaygametimerefreshtime() {
		return tTodaygametimerefreshtime;
	}

	public void settTodaygametimerefreshtime(Integer tTodaygametimerefreshtime) {
		this.tTodaygametimerefreshtime = tTodaygametimerefreshtime;
	}

	public Integer gettTotalcoinget() {
		return tTotalcoinget;
	}

	public void settTotalcoinget(Integer tTotalcoinget) {
		this.tTotalcoinget = tTotalcoinget;
	}

	public Integer gettTodaycoinget() {
		return tTodaycoinget;
	}

	public void settTodaycoinget(Integer tTodaycoinget) {
		this.tTodaycoinget = tTodaycoinget;
	}

	public Integer gettTodaycoinrefreshtime() {
		return tTodaycoinrefreshtime;
	}

	public void settTodaycoinrefreshtime(Integer tTodaycoinrefreshtime) {
		this.tTodaycoinrefreshtime = tTodaycoinrefreshtime;
	}

	public Integer gettRechargemoney() {
		return tRechargemoney;
	}

	public void settRechargemoney(Integer tRechargemoney) {
		this.tRechargemoney = tRechargemoney;
	}

	public Integer gettLastrechargetime() {
		return tLastrechargetime;
	}

	public void settLastrechargetime(Integer tLastrechargetime) {
		this.tLastrechargetime = tLastrechargetime;
	}

	public byte[] gettGameinfo() {
		return tGameinfo;
	}

	public void settGameinfo(byte[] tGameinfo) {
		this.tGameinfo = tGameinfo;
	}

	public Integer gettLastgamebetscore() {
		return tLastgamebetscore;
	}

	public void settLastgamebetscore(Integer tLastgamebetscore) {
		this.tLastgamebetscore = tLastgamebetscore;
	}

	public Integer gettTodaygamebetscore() {
		return tTodaygamebetscore;
	}

	public void settTodaygamebetscore(Integer tTodaygamebetscore) {
		this.tTodaygamebetscore = tTodaygamebetscore;
	}

	public Integer gettBetscorersftime() {
		return tBetscorersftime;
	}

	public void settBetscorersftime(Integer tBetscorersftime) {
		this.tBetscorersftime = tBetscorersftime;
	}

	public Integer gettDamaamount() {
		return tDamaamount;
	}

	public void settDamaamount(Integer tDamaamount) {
		this.tDamaamount = tDamaamount;
	}

	public Integer gettDamarechargemoney() {
		return tDamarechargemoney;
	}

	public void settDamarechargemoney(Integer tDamarechargemoney) {
		this.tDamarechargemoney = tDamarechargemoney;
	}

	public byte[] gettSignin() {
		return tSignin;
	}

	public void settSignin(byte[] tSignin) {
		this.tSignin = tSignin;
	}

	public Integer gettLastsignintime() {
		return tLastsignintime;
	}

	public void settLastsignintime(Integer tLastsignintime) {
		this.tLastsignintime = tLastsignintime;
	}

	public Integer gettLastfirsttotaldeposit() {
		return tLastfirsttotaldeposit;
	}

	public void settLastfirsttotaldeposit(Integer tLastfirsttotaldeposit) {
		this.tLastfirsttotaldeposit = tLastfirsttotaldeposit;
	}

	public Integer gettLastfirsttotaldeposittime() {
		return tLastfirsttotaldeposittime;
	}

	public void settLastfirsttotaldeposittime(Integer tLastfirsttotaldeposittime) {
		this.tLastfirsttotaldeposittime = tLastfirsttotaldeposittime;
	}

	public Integer gettIsrecivefirsttotaldeposit() {
		return tIsrecivefirsttotaldeposit;
	}

	public void settIsrecivefirsttotaldeposit(Integer tIsrecivefirsttotaldeposit) {
		this.tIsrecivefirsttotaldeposit = tIsrecivefirsttotaldeposit;
	}

	public Integer gettDaysharecnt() {
		return tDaysharecnt;
	}

	public void settDaysharecnt(Integer tDaysharecnt) {
		this.tDaysharecnt = tDaysharecnt;
	}

	public Integer gettLastdaysharetime() {
		return tLastdaysharetime;
	}

	public void settLastdaysharetime(Integer tLastdaysharetime) {
		this.tLastdaysharetime = tLastdaysharetime;
	}

	public byte[] gettVipinfo() {
		return tVipinfo;
	}

	public void settVipinfo(byte[] tVipinfo) {
		this.tVipinfo = tVipinfo;
	}

	public Integer gettSuccourcnt() {
		return tSuccourcnt;
	}

	public void settSuccourcnt(Integer tSuccourcnt) {
		this.tSuccourcnt = tSuccourcnt;
	}

	public Integer gettLastrecivesuccourtime() {
		return tLastrecivesuccourtime;
	}

	public void settLastrecivesuccourtime(Integer tLastrecivesuccourtime) {
		this.tLastrecivesuccourtime = tLastrecivesuccourtime;
	}

	public Integer gettViplevel() {
		return tViplevel;
	}

	public void settViplevel(Integer tViplevel) {
		this.tViplevel = tViplevel;
	}
}
