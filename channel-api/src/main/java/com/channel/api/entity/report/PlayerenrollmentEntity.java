package com.channel.api.entity.report;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;

/**
 * VIEW
 * 
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-09-17 15:10:16
 */
@TableName("v_playerenrollment")
public class PlayerenrollmentEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private Integer id;
	/**
	 * 
	 */
	private Integer userid;
	/**
	 * 
	 */
	private Integer fee;
	/**
	 * 
	 */
	private Integer operatetype;
	/**
	 * 
	 */
	private Integer recordtime;
	/**
	 * 
	 */
	private Integer gametypeid;
	/**
	 * 
	 */
	private String matchguid;
	/**
	 * 
	 */
	private String businessguid;
	/**
	 * 
	 */
	private String channelno;

	/**
	 * 设置：
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * 设置：
	 */
	public void setUserid(Integer userid) {
		this.userid = userid;
	}
	/**
	 * 获取：
	 */
	public Integer getUserid() {
		return userid;
	}
	/**
	 * 设置：
	 */
	public void setFee(Integer fee) {
		this.fee = fee;
	}
	/**
	 * 获取：
	 */
	public Integer getFee() {
		return fee;
	}
	/**
	 * 设置：
	 */
	public void setOperatetype(Integer operatetype) {
		this.operatetype = operatetype;
	}
	/**
	 * 获取：
	 */
	public Integer getOperatetype() {
		return operatetype;
	}
	/**
	 * 设置：
	 */
	public void setRecordtime(Integer recordtime) {
		this.recordtime = recordtime;
	}
	/**
	 * 获取：
	 */
	public Integer getRecordtime() {
		return recordtime;
	}
	/**
	 * 设置：
	 */
	public void setGametypeid(Integer gametypeid) {
		this.gametypeid = gametypeid;
	}
	/**
	 * 获取：
	 */
	public Integer getGametypeid() {
		return gametypeid;
	}
	/**
	 * 设置：
	 */
	public void setMatchguid(String matchguid) {
		this.matchguid = matchguid;
	}
	/**
	 * 获取：
	 */
	public String getMatchguid() {
		return matchguid;
	}
	/**
	 * 设置：
	 */
	public void setBusinessguid(String businessguid) {
		this.businessguid = businessguid;
	}
	/**
	 * 获取：
	 */
	public String getBusinessguid() {
		return businessguid;
	}
	/**
	 * 设置：
	 */
	public void setChannelno(String channelno) {
		this.channelno = channelno;
	}
	/**
	 * 获取：
	 */
	public String getChannelno() {
		return channelno;
	}
}
