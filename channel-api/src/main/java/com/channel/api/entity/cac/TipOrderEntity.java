package com.channel.api.entity.cac;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * 
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-05-19 10:25:39
 */
@TableName("tip_order")
public class TipOrderEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键编码
	 */
	@TableId
	private Integer id;
	/**
	 * 订单编号
	 */
	private String orderNo;
	/**
	 * 类型: 1, 支付订单 2,提现订单
	 */
	private Integer type;
	/**
	 * 状态: 1, 已提醒
	 */
	private Integer status;
	/**
	 * 已提醒次数
	 */
	private Integer tipCount;
	/**
	 * 创建时间
	 */
	private Date createTime;

	/**
	 * 设置：主键编码
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * 获取：主键编码
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * 设置：订单编号
	 */
	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}
	/**
	 * 获取：订单编号
	 */
	public String getOrderNo() {
		return orderNo;
	}
	/**
	 * 设置：类型: 1, 支付订单 2,提现订单
	 */
	public void setType(Integer type) {
		this.type = type;
	}
	/**
	 * 获取：类型: 1, 支付订单 2,提现订单
	 */
	public Integer getType() {
		return type;
	}
	/**
	 * 设置：状态: 1, 已提醒
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}
	/**
	 * 获取：状态: 1, 已提醒
	 */
	public Integer getStatus() {
		return status;
	}
	/**
	 * 设置：已提醒次数
	 */
	public void setTipCount(Integer tipCount) {
		this.tipCount = tipCount;
	}
	/**
	 * 获取：已提醒次数
	 */
	public Integer getTipCount() {
		return tipCount;
	}
	/**
	 * 设置：创建时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：创建时间
	 */
	public Date getCreateTime() {
		return createTime;
	}
}
