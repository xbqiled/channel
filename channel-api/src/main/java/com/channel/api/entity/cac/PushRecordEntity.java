package com.channel.api.entity.cac;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * 
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2020-05-27 20:42:22
 */
@TableName("sys_push_record")
public class PushRecordEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键
	 */
	@TableId
	private Integer id;
	/**
	 * 消息id
	 */
	private String msgId;
	/**
	 * 渠道编码
	 */
	private String channelId;
	/**
	 * 平台： android,ios,winphone,all
	 */
	private String platform;
	/**
	 * 指定推送设备
	 */
	private String audience;
	/**
	 * 通知内容体
	 */
	private String title;
	/**
	 * 消息内容体
	 */
	private String content;
	/**
	 * 参数
	 */
	private String options;
	/**
	 * 回调参数
	 */
	private String callback;
	/**
	 * 配置id
	 */
	private Integer configId;
	/**
	 * 1 总控 2渠道
	 */
	private Integer sendSys;
	/**
	 * 平台id
	 */
	private Integer platformId;
	/**
	 * 固定时间
	 */
	private Integer timeType;
	/**
	 * 定时发送时间
	 */
	private Date retainTime;
	/**
	 * 推送操作人
	 */
	private String pushBy;
	/**
	 * 回调时间
	 */
	private Date callbackTime;
	/**
	 * 离线保留时长
	 */
	private Long timeLong;
	/**
	 * 发送类型
	 */
	private Integer sendType;
	/**
	 * 推送时间
	 */
	private Date pushTime;

	/**
	 * 设置：主键
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * 获取：主键
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * 设置：消息id
	 */
	public void setMsgId(String msgId) {
		this.msgId = msgId;
	}
	/**
	 * 获取：消息id
	 */
	public String getMsgId() {
		return msgId;
	}
	/**
	 * 设置：渠道编码
	 */
	public void setChannelId(String channelId) {
		this.channelId = channelId;
	}
	/**
	 * 获取：渠道编码
	 */
	public String getChannelId() {
		return channelId;
	}
	/**
	 * 设置：平台： android,ios,winphone,all
	 */
	public void setPlatform(String platform) {
		this.platform = platform;
	}
	/**
	 * 获取：平台： android,ios,winphone,all
	 */
	public String getPlatform() {
		return platform;
	}
	/**
	 * 设置：指定推送设备
	 */
	public void setAudience(String audience) {
		this.audience = audience;
	}
	/**
	 * 获取：指定推送设备
	 */
	public String getAudience() {
		return audience;
	}
	/**
	 * 设置：通知内容体
	 */
	public void setTitle(String title) {
		this.title = title;
	}
	/**
	 * 获取：通知内容体
	 */
	public String getTitle() {
		return title;
	}
	/**
	 * 设置：消息内容体
	 */
	public void setContent(String content) {
		this.content = content;
	}
	/**
	 * 获取：消息内容体
	 */
	public String getContent() {
		return content;
	}
	/**
	 * 设置：参数
	 */
	public void setOptions(String options) {
		this.options = options;
	}
	/**
	 * 获取：参数
	 */
	public String getOptions() {
		return options;
	}
	/**
	 * 设置：回调参数
	 */
	public void setCallback(String callback) {
		this.callback = callback;
	}
	/**
	 * 获取：回调参数
	 */
	public String getCallback() {
		return callback;
	}
	/**
	 * 设置：配置id
	 */
	public void setConfigId(Integer configId) {
		this.configId = configId;
	}
	/**
	 * 获取：配置id
	 */
	public Integer getConfigId() {
		return configId;
	}
	/**
	 * 设置：1 总控 2渠道
	 */
	public void setSendSys(Integer sendSys) {
		this.sendSys = sendSys;
	}
	/**
	 * 获取：1 总控 2渠道
	 */
	public Integer getSendSys() {
		return sendSys;
	}
	/**
	 * 设置：平台id
	 */
	public void setPlatformId(Integer platformId) {
		this.platformId = platformId;
	}
	/**
	 * 获取：平台id
	 */
	public Integer getPlatformId() {
		return platformId;
	}
	/**
	 * 设置：固定时间
	 */
	public void setTimeType(Integer timeType) {
		this.timeType = timeType;
	}
	/**
	 * 获取：固定时间
	 */
	public Integer getTimeType() {
		return timeType;
	}
	/**
	 * 设置：定时发送时间
	 */
	public void setRetainTime(Date retainTime) {
		this.retainTime = retainTime;
	}
	/**
	 * 获取：定时发送时间
	 */
	public Date getRetainTime() {
		return retainTime;
	}
	/**
	 * 设置：推送操作人
	 */
	public void setPushBy(String pushBy) {
		this.pushBy = pushBy;
	}
	/**
	 * 获取：推送操作人
	 */
	public String getPushBy() {
		return pushBy;
	}
	/**
	 * 设置：回调时间
	 */
	public void setCallbackTime(Date callbackTime) {
		this.callbackTime = callbackTime;
	}
	/**
	 * 获取：回调时间
	 */
	public Date getCallbackTime() {
		return callbackTime;
	}
	/**
	 * 设置：离线保留时长
	 */
	public void setTimeLong(Long timeLong) {
		this.timeLong = timeLong;
	}
	/**
	 * 获取：离线保留时长
	 */
	public Long getTimeLong() {
		return timeLong;
	}
	/**
	 * 设置：发送类型
	 */
	public void setSendType(Integer sendType) {
		this.sendType = sendType;
	}
	/**
	 * 获取：发送类型
	 */
	public Integer getSendType() {
		return sendType;
	}
	/**
	 * 设置：推送时间
	 */
	public void setPushTime(Date pushTime) {
		this.pushTime = pushTime;
	}
	/**
	 * 获取：推送时间
	 */
	public Date getPushTime() {
		return pushTime;
	}
}
