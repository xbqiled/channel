package com.channel.api.entity.report;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;

/**
 * VIEW
 * 
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-02-25 19:23:00
 */
@TableName("V_RecordUserPlatformLogin")
public class RecordloginplatformEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private Integer id;
	/**
	 * 
	 */
	private Integer userid;
	/**
	 * 
	 */
	private Integer usertype;
	/**
	 * 
	 */
	private Integer logintime;
	/**
	 * 
	 */
	private Integer logintype;
	/**
	 * 
	 */
	private String loginip;
	/**
	 * 
	 */
	private String machineserial;
	/**
	 * 
	 */
	private Integer networdstate;
	/**
	 * 
	 */
	private String wifiname;
	/**
	 * 
	 */
	private String channelno;
	/**
	 * 
	 */
	private String version;
	/**
	 * 
	 */
	private Integer terminaltype;
	/**
	 * 
	 */
	private String machinetype;
	/**
	 * 
	 */
	private String systemtype;
	/**
	 * 
	 */
	private String matchguid;
	/**
	 * 
	 */
	private Integer loginmode;

	/**
	 * 设置：
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * 设置：
	 */
	public void setUserid(Integer userid) {
		this.userid = userid;
	}
	/**
	 * 获取：
	 */
	public Integer getUserid() {
		return userid;
	}
	/**
	 * 设置：
	 */
	public void setUsertype(Integer usertype) {
		this.usertype = usertype;
	}
	/**
	 * 获取：
	 */
	public Integer getUsertype() {
		return usertype;
	}
	/**
	 * 设置：
	 */
	public void setLogintime(Integer logintime) {
		this.logintime = logintime;
	}
	/**
	 * 获取：
	 */
	public Integer getLogintime() {
		return logintime;
	}
	/**
	 * 设置：
	 */
	public void setLogintype(Integer logintype) {
		this.logintype = logintype;
	}
	/**
	 * 获取：
	 */
	public Integer getLogintype() {
		return logintype;
	}
	/**
	 * 设置：
	 */
	public void setLoginip(String loginip) {
		this.loginip = loginip;
	}
	/**
	 * 获取：
	 */
	public String getLoginip() {
		return loginip;
	}
	/**
	 * 设置：
	 */
	public void setMachineserial(String machineserial) {
		this.machineserial = machineserial;
	}
	/**
	 * 获取：
	 */
	public String getMachineserial() {
		return machineserial;
	}
	/**
	 * 设置：
	 */
	public void setNetwordstate(Integer networdstate) {
		this.networdstate = networdstate;
	}
	/**
	 * 获取：
	 */
	public Integer getNetwordstate() {
		return networdstate;
	}
	/**
	 * 设置：
	 */
	public void setWifiname(String wifiname) {
		this.wifiname = wifiname;
	}
	/**
	 * 获取：
	 */
	public String getWifiname() {
		return wifiname;
	}
	/**
	 * 设置：
	 */
	public void setChannelno(String channelno) {
		this.channelno = channelno;
	}
	/**
	 * 获取：
	 */
	public String getChannelno() {
		return channelno;
	}
	/**
	 * 设置：
	 */
	public void setVersion(String version) {
		this.version = version;
	}
	/**
	 * 获取：
	 */
	public String getVersion() {
		return version;
	}
	/**
	 * 设置：
	 */
	public void setTerminaltype(Integer terminaltype) {
		this.terminaltype = terminaltype;
	}
	/**
	 * 获取：
	 */
	public Integer getTerminaltype() {
		return terminaltype;
	}
	/**
	 * 设置：
	 */
	public void setMachinetype(String machinetype) {
		this.machinetype = machinetype;
	}
	/**
	 * 获取：
	 */
	public String getMachinetype() {
		return machinetype;
	}
	/**
	 * 设置：
	 */
	public void setSystemtype(String systemtype) {
		this.systemtype = systemtype;
	}
	/**
	 * 获取：
	 */
	public String getSystemtype() {
		return systemtype;
	}
	/**
	 * 设置：
	 */
	public void setMatchguid(String matchguid) {
		this.matchguid = matchguid;
	}
	/**
	 * 获取：
	 */
	public String getMatchguid() {
		return matchguid;
	}
	/**
	 * 设置：
	 */
	public void setLoginmode(Integer loginmode) {
		this.loginmode = loginmode;
	}
	/**
	 * 获取：
	 */
	public Integer getLoginmode() {
		return loginmode;
	}
}
