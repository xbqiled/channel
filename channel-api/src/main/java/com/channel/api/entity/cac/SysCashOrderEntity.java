package com.channel.api.entity.cac;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

import java.math.BigDecimal;
import java.io.Serializable;
import java.util.Date;

/**
 * 
 * 
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-02-24 19:28:16
 */
@TableName("sys_cash_order")
public class SysCashOrderEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * ID
	 */
	@TableId
	private Integer id;
	/**
	 * 
	 */
	private String cashNo;
	/**
	 * 
	 */
	private Integer userId;
	/**
	 * 
	 */
	private BigDecimal cashFee;
	/**
	 * 
	 */
	private Integer channelId;
	/**
	 *  状态: 0:提现申请 1,审批通过 2,审批失败
	 */
	private Integer status;
	/**
	 * 提现类型: 0支付宝,1微信,2银行卡
	 */
	private Integer cashType;
	/**
	 * 用户账号
	 */
	private String account;
	/**
	 * 姓名
	 */
	private String accountName;

	/**
	 * 银行名称
	 */
	private String bank;
	/**
	 * 分行名称
	 */
	private String subBank;
	/**
	 * 开户行地址
	 */
	private String bankAddress;
	/**
	 * 
	 */
	private Date createTime;
	/**
	 * SYSTME
	 */
	private String createBy;
	/**
	 * 
	 */
	private Date approveTime;
	/**
	 * 手动处理记录编码
	 */
	private String approveBy;
	/**
	 * 备注
	 */
	private String note;

	/**
	 * 设置：ID
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * 获取：ID
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * 设置：
	 */
	public void setCashNo(String cashNo) {
		this.cashNo = cashNo;
	}
	/**
	 * 获取：
	 */
	public String getCashNo() {
		return cashNo;
	}
	/**
	 * 设置：
	 */
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	/**
	 * 获取：
	 */
	public Integer getUserId() {
		return userId;
	}
	/**
	 * 设置：
	 */
	public void setCashFee(BigDecimal cashFee) {
		this.cashFee = cashFee;
	}
	/**
	 * 获取：
	 */
	public BigDecimal getCashFee() {
		return cashFee;
	}
	/**
	 * 设置：
	 */
	public void setChannelId(Integer channelId) {
		this.channelId = channelId;
	}
	/**
	 * 获取：
	 */
	public Integer getChannelId() {
		return channelId;
	}
	/**
	 * 设置： 状态: 0:提现申请 1,审批通过 2,审批失败
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}
	/**
	 * 获取： 状态: 0:提现申请 1,审批通过 2,审批失败
	 */
	public Integer getStatus() {
		return status;
	}
	/**
	 * 设置：提现类型: 0支付宝,1微信,2银行卡
	 */
	public void setCashType(Integer cashType) {
		this.cashType = cashType;
	}
	/**
	 * 获取：提现类型: 0支付宝,1微信,2银行卡
	 */
	public Integer getCashType() {
		return cashType;
	}
	/**
	 * 设置：用户账号
	 */
	public void setAccount(String account) {
		this.account = account;
	}
	/**
	 * 获取：用户账号
	 */
	public String getAccount() {
		return account;
	}

	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	public String getBank() {
		return bank;
	}

	public void setBank(String bank) {
		this.bank = bank;
	}

	public String getSubBank() {
		return subBank;
	}

	public void setSubBank(String subBank) {
		this.subBank = subBank;
	}

	public String getBankAddress() {
		return bankAddress;
	}

	public void setBankAddress(String bankAddress) {
		this.bankAddress = bankAddress;
	}

	/**
	 * 设置：
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：
	 */
	public Date getCreateTime() {
		return createTime;
	}
	/**
	 * 设置：SYSTME
	 */
	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}
	/**
	 * 获取：SYSTME
	 */
	public String getCreateBy() {
		return createBy;
	}
	/**
	 * 设置：
	 */
	public void setApproveTime(Date approveTime) {
		this.approveTime = approveTime;
	}
	/**
	 * 获取：
	 */
	public Date getApproveTime() {
		return approveTime;
	}
	/**
	 * 设置：手动处理记录编码
	 */
	public void setApproveBy(String approveBy) {
		this.approveBy = approveBy;
	}
	/**
	 * 获取：手动处理记录编码
	 */
	public String getApproveBy() {
		return approveBy;
	}
	/**
	 * 设置：备注
	 */
	public void setNote(String note) {
		this.note = note;
	}
	/**
	 * 获取：备注
	 */
	public String getNote() {
		return note;
	}
}
