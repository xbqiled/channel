package com.channel.api.entity.cac;


import java.io.Serializable;
import java.util.Date;


public class PayChannelVo implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * 主键
     */
    private Long id;
    /**
     * 支付名称
     */
    private String payName;
    /**
     1	微信公众账号支付
     2	微信扫码支付
     3	微信网页支付(微信H5)
     4	微信app支付
     5 微信转账支付
     6	支付宝扫码支付
     7 支付宝APP支付
     8 支付宝网页支付(手机H5)
     9 支付宝转账支付
     10	QQ钱包Wap支付
     11	QQ钱包手机扫码支付
     12 QQ转账支付
     13	京东扫码支付
     14	苏宁扫码支付
     15	银联二维码支付
     16	网银B2B支付
     17	网银B2C支付
     18	无卡快捷支付(不需要银行卡号的WAP快捷)
     19	WAP快捷(银联H5,需要正确银行卡号)
     20 银行卡转账支付
     */
    private Integer payType;
    /**
     * 平台通道类型:根据第三方提供的来
     */
    private String platformType;
    /**
     * 支付大类型:1，在线支付， 2，银行打款,  3,支付宝打款,   4,微信打款
     */
    private Integer type;
    /**
     * 收款人/商户号
     */
    private String merchantCode;
    /**
     * 收款银行/秘钥
     */
    private String merchantKey;
    /**
     * 是否有效
     */
    private Boolean isEffective;
    /**
     * 开户网点/订单URL
     */
    private String publicKey;
    /**
     * 收款账号/账号
     */
    private String account;

    private String appKey;
    /**
     *  最小支付金额
     */
    private Long min;
    /**
     *  最大支付金额
     */
    private Long max;
    /**
     * 是否默认
     */
    private Integer def;
    /**
     * 状态: 1, 有效， 0无效
     */
    private Integer status;
    /**
     * 渠道编码
     */
    private Long channelId;
    /**
     * 图片地址
     */
    private String icon;
    /**
     * 通道ID
     */
    private Long payPlatformId;
    /**
     * 支付网关
     */
    private String payGetway;
    /**
     * 排序
     */
    private Integer sortNo;
    /**
     * 支付说明
     */
    private String payDesc;
    /**
     * 返利值
     */
    private Integer returnRate;

    /**
     * 按钮配置
     */
    private String payValueConfig;

    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 创建人
     */
    private String createBy;
    /**
     *
     */
    private String remark;
    /**
     * 组织名称
     */
    private String company;

    public Boolean getEffective() {
        return isEffective;
    }

    public void setEffective(Boolean effective) {
        isEffective = effective;
    }

    public String getAppKey() {
        return appKey;
    }

    public void setAppKey(String appKey) {
        this.appKey = appKey;
    }

    public String getPublicKey() {
        return publicKey;
    }

    public void setPublicKey(String publicKey) {
        this.publicKey = publicKey;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPayName() {
        return payName;
    }

    public void setPayName(String payName) {
        this.payName = payName;
    }

    public Integer getPayType() {
        return payType;
    }

    public void setPayType(Integer payType) {
        this.payType = payType;
    }

    public String getPlatformType() {
        return platformType;
    }

    public void setPlatformType(String platformType) {
        this.platformType = platformType;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getMerchantCode() {
        return merchantCode;
    }

    public void setMerchantCode(String merchantCode) {
        this.merchantCode = merchantCode;
    }

    public String getMerchantKey() {
        return merchantKey;
    }

    public void setMerchantKey(String merchantKey) {
        this.merchantKey = merchantKey;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public Long getMin() {
        return min;
    }

    public void setMin(Long min) {
        this.min = min;
    }

    public Long getMax() {
        return max;
    }

    public void setMax(Long max) {
        this.max = max;
    }

    public Integer getDef() {
        return def;
    }

    public void setDef(Integer def) {
        this.def = def;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Long getChannelId() {
        return channelId;
    }

    public void setChannelId(Long channelId) {
        this.channelId = channelId;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public Long getPayPlatformId() {
        return payPlatformId;
    }

    public void setPayPlatformId(Long payPlatformId) {
        this.payPlatformId = payPlatformId;
    }

    public String getPayGetway() {
        return payGetway;
    }

    public void setPayGetway(String payGetway) {
        this.payGetway = payGetway;
    }

    public Integer getSortNo() {
        return sortNo;
    }

    public void setSortNo(Integer sortNo) {
        this.sortNo = sortNo;
    }

    public String getPayDesc() {
        return payDesc;
    }

    public void setPayDesc(String payDesc) {
        this.payDesc = payDesc;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public Boolean getIsEffective() {
        return isEffective;
    }

    public void setIsEffective(Boolean isEffective) {
        this.isEffective = isEffective;
    }


    public Integer getReturnRate() {
        return returnRate;
    }

    public void setReturnRate(Integer returnRate) {
        this.returnRate = returnRate;
    }

    public String getPayValueConfig() {
        return payValueConfig;
    }

    public void setPayValueConfig(String payValueConfig) {
        this.payValueConfig = payValueConfig;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
