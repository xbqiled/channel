package com.channel.api.entity.cac;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class BindVo implements Serializable {

    /**
     * 渠道编码
     */
    private String channelId;

    /**
     * 渠道名称
     */
    private String channelName;
    /**
     * 赠送值
     */
    private BigDecimal giveValue;

    /**
     *
     */
    private Integer rebate;
    /**
     * 是否开放兑换 1-开放 0-关闭
     */
    private Integer exchange;
    /**
     * 版本号
     */
    private String versionNo;
    /**
     * 最低兑换额度, 单位分, 即100元

     */
    private Integer minExchange;

    private Integer initCoin;

    private Integer damaMulti;

    private Integer rebateDamaMulti;

    private Integer promoteType;

    private Integer exchangeCount;

    private Integer resetDamaLeftCoin;

    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 修改人
     */
    private String createBy;
    /**
     * 修改时间
     */
    private Date modifyTime;
    /**
     * 修改人
     */
    private String modifyBy;

    /**
     * 设置：渠道编码
     */
    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }
    /**
     * 获取：渠道编码
     */
    public String getChannelId() {
        return channelId;
    }
    /**
     * 设置：赠送值
     */
    public void setGiveValue(BigDecimal giveValue) {
        this.giveValue = giveValue;
    }
    /**
     * 获取：赠送值
     */
    public BigDecimal getGiveValue() {
        return giveValue;
    }
    /**
     * 设置：创建时间
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
    /**
     * 获取：创建时间
     */
    public Date getCreateTime() {
        return createTime;
    }
    /**
     * 设置：修改人
     */
    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }
    /**
     * 获取：修改人
     */
    public String getCreateBy() {
        return createBy;
    }
    /**
     * 设置：修改时间
     */
    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }
    /**
     * 获取：修改时间
     */
    public Date getModifyTime() {
        return modifyTime;
    }
    /**
     * 设置：修改人
     */
    public void setModifyBy(String modifyBy) {
        this.modifyBy = modifyBy;
    }
    /**
     * 获取：修改人
     */
    public String getModifyBy() {
        return modifyBy;
    }

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    public Integer getRebate() {
        return rebate;
    }

    public void setRebate(Integer rebate) {
        this.rebate = rebate;
    }

    public Integer getExchange() {
        return exchange;
    }

    public void setExchange(Integer exchange) {
        this.exchange = exchange;
    }

    public String getVersionNo() {
        return versionNo;
    }

    public void setVersionNo(String versionNo) {
        this.versionNo = versionNo;
    }

    public Integer getMinExchange() {
        return minExchange;
    }

    public void setMinExchange(Integer minExchange) {
        this.minExchange = minExchange;
    }


    public Integer getInitCoin() {
        return initCoin;
    }

    public void setInitCoin(Integer initCoin) {
        this.initCoin = initCoin;
    }


    public Integer getDamaMulti() {
        return damaMulti;
    }

    public void setDamaMulti(Integer damaMulti) {
        this.damaMulti = damaMulti;
    }

    public Integer getRebateDamaMulti() {
        return rebateDamaMulti;
    }

    public void setRebateDamaMulti(Integer rebateDamaMulti) {
        this.rebateDamaMulti = rebateDamaMulti;
    }


    public Integer getPromoteType() {
        return promoteType;
    }

    public void setPromoteType(Integer promoteType) {
        this.promoteType = promoteType;
    }

    public Integer getExchangeCount() {
        return exchangeCount;
    }

    public void setExchangeCount(Integer exchangeCount) {
        this.exchangeCount = exchangeCount;
    }

    public Integer getResetDamaLeftCoin() {
        return resetDamaLeftCoin;
    }

    public void setResetDamaLeftCoin(Integer resetDamaLeftCoin) {
        this.resetDamaLeftCoin = resetDamaLeftCoin;
    }
}
