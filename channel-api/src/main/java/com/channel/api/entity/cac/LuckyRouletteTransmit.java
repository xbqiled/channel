package com.channel.api.entity.cac;

import java.io.Serializable;


public class LuckyRouletteTransmit implements Serializable {

    private String tChannel;

    private Integer tOpentype;

    private byte[] tSilvercfg;

    private Long tSilvercost;

    private byte[] tGoldcfg;

    private Long tGoldcost;

    private byte[] tDiamondcfg;

    private Long tDiamondcost;

    private Integer tStart;

    private Integer tFinish;

    private String startTimeStr;

    private String finishTimeStr;

    public String gettChannel() {
        return tChannel;
    }

    public void settChannel(String tChannel) {
        this.tChannel = tChannel;
    }

    public byte[] gettSilvercfg() {
        return tSilvercfg;
    }

    public void settSilvercfg(byte[] tSilvercfg) {
        this.tSilvercfg = tSilvercfg;
    }

    public Long gettSilvercost() {
        return tSilvercost;
    }

    public void settSilvercost(Long tSilvercost) {
        this.tSilvercost = tSilvercost;
    }

    public byte[] gettGoldcfg() {
        return tGoldcfg;
    }

    public void settGoldcfg(byte[] tGoldcfg) {
        this.tGoldcfg = tGoldcfg;
    }

    public Long gettGoldcost() {
        return tGoldcost;
    }

    public void settGoldcost(Long tGoldcost) {
        this.tGoldcost = tGoldcost;
    }

    public byte[] gettDiamondcfg() {
        return tDiamondcfg;
    }

    public void settDiamondcfg(byte[] tDiamondcfg) {
        this.tDiamondcfg = tDiamondcfg;
    }

    public Long gettDiamondcost() {
        return tDiamondcost;
    }

    public void settDiamondcost(Long tDiamondcost) {
        this.tDiamondcost = tDiamondcost;
    }

    public Integer gettStart() {
        return tStart;
    }

    public void settStart(Integer tStart) {
        this.tStart = tStart;
    }

    public Integer gettFinish() {
        return tFinish;
    }

    public void settFinish(Integer tFinish) {
        this.tFinish = tFinish;
    }

    public String getStartTimeStr() {
        return startTimeStr;
    }

    public void setStartTimeStr(String startTimeStr) {
        this.startTimeStr = startTimeStr;
    }

    public String getFinishTimeStr() {
        return finishTimeStr;
    }

    public void setFinishTimeStr(String finishTimeStr) {
        this.finishTimeStr = finishTimeStr;
    }

    public Integer gettOpentype() {
        return tOpentype;
    }

    public void settOpentype(Integer tOpentype) {
        this.tOpentype = tOpentype;
    }
}
