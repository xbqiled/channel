package com.channel.api.entity.report;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

import java.math.BigDecimal;
import java.io.Serializable;
import java.util.Date;

/**
 * VIEW
 * 
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-08-24 11:49:24
 */
@TableName("v_recorddamachange")
public class RecorddamachangeEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private Integer id;
	/**
	 * 
	 */
	private Integer userid;
	/**
	 * 
	 */
	private BigDecimal beforeamount;
	/**
	 * 
	 */
	private BigDecimal changeamount;
	/**
	 * 
	 */
	private BigDecimal afteramount;
	/**
	 * 
	 */
	private BigDecimal beforeneed;
	/**
	 * 
	 */
	private BigDecimal changeneed;
	/**
	 * 
	 */
	private BigDecimal afterneed;
	/**
	 * 
	 */
	private Integer type;
	/**
	 * 
	 */
	private Integer recordtime;
	/**
	 * 
	 */
	private String channelno;

	/**
	 * 设置：
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * 设置：
	 */
	public void setUserid(Integer userid) {
		this.userid = userid;
	}
	/**
	 * 获取：
	 */
	public Integer getUserid() {
		return userid;
	}
	/**
	 * 设置：
	 */
	public void setBeforeamount(BigDecimal beforeamount) {
		this.beforeamount = beforeamount;
	}
	/**
	 * 获取：
	 */
	public BigDecimal getBeforeamount() {
		return beforeamount;
	}
	/**
	 * 设置：
	 */
	public void setChangeamount(BigDecimal changeamount) {
		this.changeamount = changeamount;
	}
	/**
	 * 获取：
	 */
	public BigDecimal getChangeamount() {
		return changeamount;
	}
	/**
	 * 设置：
	 */
	public void setAfteramount(BigDecimal afteramount) {
		this.afteramount = afteramount;
	}
	/**
	 * 获取：
	 */
	public BigDecimal getAfteramount() {
		return afteramount;
	}
	/**
	 * 设置：
	 */
	public void setBeforeneed(BigDecimal beforeneed) {
		this.beforeneed = beforeneed;
	}
	/**
	 * 获取：
	 */
	public BigDecimal getBeforeneed() {
		return beforeneed;
	}
	/**
	 * 设置：
	 */
	public void setChangeneed(BigDecimal changeneed) {
		this.changeneed = changeneed;
	}
	/**
	 * 获取：
	 */
	public BigDecimal getChangeneed() {
		return changeneed;
	}
	/**
	 * 设置：
	 */
	public void setAfterneed(BigDecimal afterneed) {
		this.afterneed = afterneed;
	}
	/**
	 * 获取：
	 */
	public BigDecimal getAfterneed() {
		return afterneed;
	}
	/**
	 * 设置：
	 */
	public void setType(Integer type) {
		this.type = type;
	}
	/**
	 * 获取：
	 */
	public Integer getType() {
		return type;
	}
	/**
	 * 设置：
	 */
	public void setRecordtime(Integer recordtime) {
		this.recordtime = recordtime;
	}
	/**
	 * 获取：
	 */
	public Integer getRecordtime() {
		return recordtime;
	}
	/**
	 * 设置：
	 */
	public void setChannelno(String channelno) {
		this.channelno = channelno;
	}
	/**
	 * 获取：
	 */
	public String getChannelno() {
		return channelno;
	}
}
