package com.channel.api.entity.cac;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * 
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-02-24 19:27:36
 */
@TableName("sys_pay_config")
public class SysPayConfigEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	 @TableId
	 private Long id;
	 /**
	  * 支付名称
	 */
	private String payName;
	/**
	 * 1	微信公众账号支付
	 2	微信扫码支付
	 3	微信网页支付(微信H5)
	 4	微信app支付
	 5 微信转账支付
	 6	支付宝扫码支付
	 7 支付宝APP支付
	 8 支付宝网页支付(手机H5)
	 9 支付宝转账支付
	 10	QQ钱包Wap支付
	 11	QQ钱包手机扫码支付
	 12  QQ转账支付
	 13	京东扫码支付
	 14	苏宁扫码支付
	 15	银联二维码支付
	 16	网银B2B支付
	 17	网银B2C支付
	 18	无卡快捷支付(不需要银行卡号的WAP快捷)
	 19	WAP快捷(银联H5,需要正确银行卡号)
	 20  银行卡转账支付
	 */
	private Integer payType;
	/**
	 * 平台通道类型:根据第三方提供的来
	 */
	private String platformType;
	/**
	 * 支付大类型:1，在线支付， 2，银行打款,  3,支付宝打款,   4,微信打款
	 */
	private Integer type;
	/**
	 * 收款人/商户号
	 */
	private String merchantCode;
	/**
	 * 收款银行/秘钥
	 */
	private String merchantKey;

	private String appKey;
	/**
	 * 开户网点/订单URL
	 */
	private String publicKey;
	/**
	 * 收款账号/账号
	 */
	private String account;
	/**
	 *  最小支付金额
	 */
	private Long min;
	/**
	 *  最大支付金额
	 */
	private Long max;
	/**
	 * 是否默认
	 */
	private Integer def;
	/**
	 * 状态: 1, 有效， 0无效
	 */
	private Integer status;
	/**
	 * 渠道编码
	 */
	private String channelId;
	/**
	 * 图片地址
	 */
	private String icon;
	/**
	 * 通道ID
	 */
	private Long payPlatformId;
	/**
	 * 支付网关
	 */
	private String payGetway;
	/**
	 * 排序
	 */
	private Integer sortNo;
	/**
	 * 支付说明
	 */
	private String payDesc;


	/**
	 * 返利值
	 */
	private Integer returnRate;

	/**
	 * 按钮配置
	 */
	private String payValueConfig;


	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 创建人
	 */
	private String createBy;


	/**
	 *
	 */
	private String remark;


	public String getAppKey() {
		return appKey;
	}

	public void setAppKey(String appKey) {
		this.appKey = appKey;
	}

	/**
	 * 设置：主键
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * 获取：主键
	 */
	public Long getId() {
		return id;
	}
	/**
	 * 设置：支付名称
	 */
	public void setPayName(String payName) {
		this.payName = payName;
	}
	/**
	 * 获取：支付名称
	 */
	public String getPayName() {
		return payName;
	}
	/**
	 * 设置：1	微信公众账号支付
	 2	微信扫码支付
	 3	微信网页支付(微信H5)
	 4	微信app支付
	 5 微信转账支付
	 6	支付宝扫码支付
	 7 支付宝APP支付
	 8 支付宝网页支付(手机H5)
	 9 支付宝转账支付
	 10	QQ钱包Wap支付
	 11	QQ钱包手机扫码支付
	 12  QQ转账支付
	 13	京东扫码支付
	 14	苏宁扫码支付
	 15	银联二维码支付
	 16	网银B2B支付
	 17	网银B2C支付
	 18	无卡快捷支付(不需要银行卡号的WAP快捷)
	 19	WAP快捷(银联H5,需要正确银行卡号)
	 20  银行卡转账支付
	 */
	public void setPayType(Integer payType) {
		this.payType = payType;
	}
	/**
	 * 获取：1	微信公众账号支付
	 2	微信扫码支付
	 3	微信网页支付(微信H5)
	 4	微信app支付
	 5 微信转账支付
	 6	支付宝扫码支付
	 7 支付宝APP支付
	 8 支付宝网页支付(手机H5)
	 9 支付宝转账支付
	 10	QQ钱包Wap支付
	 11	QQ钱包手机扫码支付
	 12  QQ转账支付
	 13	京东扫码支付
	 14	苏宁扫码支付
	 15	银联二维码支付
	 16	网银B2B支付
	 17	网银B2C支付
	 18	无卡快捷支付(不需要银行卡号的WAP快捷)
	 19	WAP快捷(银联H5,需要正确银行卡号)
	 20  银行卡转账支付
	 */
	public Integer getPayType() {
		return payType;
	}
	/**
	 * 设置：平台通道类型:根据第三方提供的来
	 */
	public void setPlatformType(String platformType) {
		this.platformType = platformType;
	}
	/**
	 * 获取：平台通道类型:根据第三方提供的来
	 */
	public String getPlatformType() {
		return platformType;
	}
	/**
	 * 设置：支付大类型:1，在线支付， 2，银行打款,  3,支付宝打款,   4,微信打款
	 */
	public void setType(Integer type) {
		this.type = type;
	}
	/**
	 * 获取：支付大类型:1，在线支付， 2，银行打款,  3,支付宝打款,   4,微信打款
	 */
	public Integer getType() {
		return type;
	}
	/**
	 * 设置：收款人/商户号
	 */
	public void setMerchantCode(String merchantCode) {
		this.merchantCode = merchantCode;
	}
	/**
	 * 获取：收款人/商户号
	 */
	public String getMerchantCode() {
		return merchantCode;
	}
	/**
	 * 设置：收款银行/秘钥
	 */
	public void setMerchantKey(String merchantKey) {
		this.merchantKey = merchantKey;
	}
	/**
	 * 获取：收款银行/秘钥
	 */
	public String getMerchantKey() {
		return merchantKey;
	}
	/**
	 * 设置：收款账号/账号
	 */
	public void setAccount(String account) {
		this.account = account;
	}
	/**
	 * 获取：收款账号/账号
	 */
	public String getAccount() {
		return account;
	}
	/**
	 * 设置： 最小支付金额
	 */
	public void setMin(Long min) {
		this.min = min;
	}
	/**
	 * 获取： 最小支付金额
	 */
	public Long getMin() {
		return min;
	}
	/**
	 * 设置： 最大支付金额
	 */
	public void setMax(Long max) {
		this.max = max;
	}
	/**
	 * 获取： 最大支付金额
	 */
	public Long getMax() {
		return max;
	}
	/**
	 * 设置：是否默认
	 */
	public void setDef(Integer def) {
		this.def = def;
	}
	/**
	 * 获取：是否默认
	 */
	public Integer getDef() {
		return def;
	}
	/**
	 * 设置：状态: 1, 有效， 0无效
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}
	/**
	 * 获取：状态: 1, 有效， 0无效
	 */
	public Integer getStatus() {
		return status;
	}
	/**
	 * 设置：渠道编码
	 */
	public void setChannelId(String channelId) {
		this.channelId = channelId;
	}
	/**
	 * 获取：渠道编码
	 */
	public String getChannelId() {
		return channelId;
	}
	/**
	 * 设置：图片地址
	 */
	public void setIcon(String icon) {
		this.icon = icon;
	}
	/**
	 * 获取：图片地址
	 */
	public String getIcon() {
		return icon;
	}
	/**
	 * 设置：通道ID
	 */
	public void setPayPlatformId(Long payPlatformId) {
		this.payPlatformId = payPlatformId;
	}
	/**
	 * 获取：通道ID
	 */
	public Long getPayPlatformId() {
		return payPlatformId;
	}
	/**
	 * 设置：支付网关
	 */
	public void setPayGetway(String payGetway) {
		this.payGetway = payGetway;
	}
	/**
	 * 获取：支付网关
	 */
	public String getPayGetway() {
		return payGetway;
	}
	/**
	 * 设置：排序
	 */
	public void setSortNo(Integer sortNo) {
		this.sortNo = sortNo;
	}
	/**
	 * 获取：排序
	 */
	public Integer getSortNo() {
		return sortNo;
	}
	/**
	 * 设置：支付说明
	 */
	public void setPayDesc(String payDesc) {
		this.payDesc = payDesc;
	}
	/**
	 * 获取：支付说明
	 */
	public String getPayDesc() {
		return payDesc;
	}
	/**
	 * 设置：创建时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：创建时间
	 */
	public Date getCreateTime() {
		return createTime;
	}
	/**
	 * 设置：创建人
	 */
	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}
	/**
	 * 获取：创建人
	 */
	public String getCreateBy() {
		return createBy;
	}


	public String getPublicKey() {
		return publicKey;
	}

	public void setPublicKey(String publicKey) {
		this.publicKey = publicKey;
	}


	public Integer getReturnRate() {
		return returnRate;
	}

	public void setReturnRate(Integer returnRate) {
		this.returnRate = returnRate;
	}

	public String getPayValueConfig() {
		return payValueConfig;
	}

	public void setPayValueConfig(String payValueConfig) {
		this.payValueConfig = payValueConfig;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}
}
