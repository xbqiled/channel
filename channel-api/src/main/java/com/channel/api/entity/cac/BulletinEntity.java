package com.channel.api.entity.cac;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;

/**
 * 
 * 
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-03-06 16:05:02
 */
@TableName("V_bulletin")
public class BulletinEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 公告ID
	 */
	@TableId
	private String tId;
	/**
	 * 公告类型 1-公告栏 2-跑马灯
	 */
	private Integer tType;
	/**
	 * 公告标题
	 */
	private String tTitle;
	/**
	 * 公告内容, 其中包括公告作用时间, 内容
	 */
	private byte[] tContent;
	/**
	 * 公告超链接,没有则为空,指向图片等url地址
	 */
	private String tHyperlink;
	/**
	 * 公告开始时间
	 */
	private Integer tStart;
	/**
	 * 公告结束时间
	 */
	private Integer tFinish;
	/**
	 * 影响渠道,json格式,记录所有渠道, 如果之所有渠道这里为空
	 */
	private byte[] tChannellist;
	/**
	 * 创建时间
	 */
	private Integer tCreatetime;

	/**
	 * 设置：公告ID
	 */
	public void setTId(String tId) {
		this.tId = tId;
	}
	/**
	 * 获取：公告ID
	 */
	public String getTId() {
		return tId;
	}
	/**
	 * 设置：公告类型 1-公告栏 2-跑马灯
	 */
	public void setTType(Integer tType) {
		this.tType = tType;
	}
	/**
	 * 获取：公告类型 1-公告栏 2-跑马灯
	 */
	public Integer getTType() {
		return tType;
	}
	/**
	 * 设置：公告标题
	 */
	public void setTTitle(String tTitle) {
		this.tTitle = tTitle;
	}
	/**
	 * 获取：公告标题
	 */
	public String getTTitle() {
		return tTitle;
	}
	/**
	 * 设置：公告内容, 其中包括公告作用时间, 内容
	 */
	public void setTContent(byte[] tContent) {
		this.tContent = tContent;
	}
	/**
	 * 获取：公告内容, 其中包括公告作用时间, 内容
	 */
	public byte[] getTContent() {
		return tContent;
	}
	/**
	 * 设置：公告超链接,没有则为空,指向图片等url地址
	 */
	public void setTHyperlink(String tHyperlink) {
		this.tHyperlink = tHyperlink;
	}
	/**
	 * 获取：公告超链接,没有则为空,指向图片等url地址
	 */
	public String getTHyperlink() {
		return tHyperlink;
	}
	/**
	 * 设置：公告开始时间
	 */
	public void setTStart(Integer tStart) {
		this.tStart = tStart;
	}
	/**
	 * 获取：公告开始时间
	 */
	public Integer getTStart() {
		return tStart;
	}
	/**
	 * 设置：公告结束时间
	 */
	public void setTFinish(Integer tFinish) {
		this.tFinish = tFinish;
	}
	/**
	 * 获取：公告结束时间
	 */
	public Integer getTFinish() {
		return tFinish;
	}
	/**
	 * 设置：影响渠道,json格式,记录所有渠道, 如果之所有渠道这里为空
	 */
	public void setTChannellist(byte[] tChannellist) {
		this.tChannellist = tChannellist;
	}
	/**
	 * 获取：影响渠道,json格式,记录所有渠道, 如果之所有渠道这里为空
	 */
	public byte[] getTChannellist() {
		return tChannellist;
	}
	/**
	 * 设置：创建时间
	 */
	public void setTCreatetime(Integer tCreatetime) {
		this.tCreatetime = tCreatetime;
	}
	/**
	 * 获取：创建时间
	 */
	public Integer getTCreatetime() {
		return tCreatetime;
	}
}
