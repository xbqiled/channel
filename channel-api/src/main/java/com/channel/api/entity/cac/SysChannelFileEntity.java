package com.channel.api.entity.cac;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;
import java.util.Date;

/**
 * 文件上传
 * 
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-04-05 16:04:25
 */
@TableName("sys_channel_file")
public class SysChannelFileEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private Long id;
	/**
	 * 文件名称
	 */
	private String name;
	/**
	 * 全名
	 */
	private String fileName;
	/**
	 * 后缀
	 */
	private String suffix;
	/**
	 * 类型
	 */
	private String type;
	/**
	 * 下载URL
	 */
	private String fileUrl;
	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 渠道ID
	 */
	private String channelId;

	/**
	 * 设置：
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public Long getId() {
		return id;
	}
	/**
	 * 设置：文件名称
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * 获取：文件名称
	 */
	public String getName() {
		return name;
	}
	/**
	 * 设置：全名
	 */
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	/**
	 * 获取：全名
	 */
	public String getFileName() {
		return fileName;
	}
	/**
	 * 设置：后缀
	 */
	public void setSuffix(String suffix) {
		this.suffix = suffix;
	}
	/**
	 * 获取：后缀
	 */
	public String getSuffix() {
		return suffix;
	}
	/**
	 * 设置：类型
	 */
	public void setType(String type) {
		this.type = type;
	}
	/**
	 * 获取：类型
	 */
	public String getType() {
		return type;
	}
	/**
	 * 设置：下载URL
	 */
	public void setFileUrl(String fileUrl) {
		this.fileUrl = fileUrl;
	}
	/**
	 * 获取：下载URL
	 */
	public String getFileUrl() {
		return fileUrl;
	}
	/**
	 * 设置：创建时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：创建时间
	 */
	public Date getCreateTime() {
		return createTime;
	}
	/**
	 * 设置：渠道ID
	 */
	public void setChannelId(String channelId) {
		this.channelId = channelId;
	}
	/**
	 * 获取：渠道ID
	 */
	public String getChannelId() {
		return channelId;
	}
}
