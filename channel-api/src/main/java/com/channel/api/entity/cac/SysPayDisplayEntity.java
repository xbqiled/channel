package com.channel.api.entity.cac;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * 
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-10-06 17:15:45
 */
@TableName("sys_pay_display")
public class SysPayDisplayEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private Integer id;
	/**
	 * 处理配置
	 */
	private String displayConfig;
	/**
	 * 渠道编码
	 */
	private String channelId;
	/**
	 * 创建时间， 1,创建时间
	 */
	private Date createTime;
	/**
	 * 创建人
	 */
	private String createBy;

	/**
	 * 设置：
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public Integer getId() {
		return id;
	}

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public String getDisplayConfig() {
		return displayConfig;
	}

	public void setDisplayConfig(String displayConfig) {
		this.displayConfig = displayConfig;
	}

	/**
	 * 设置：渠道编码
	 */
	public void setChannelId(String channelId) {
		this.channelId = channelId;
	}
	/**
	 * 获取：渠道编码
	 */
	public String getChannelId() {
		return channelId;
	}
	/**
	 * 设置：创建时间， 1,创建时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：创建时间， 1,创建时间
	 */
	public Date getCreateTime() {
		return createTime;
	}
	/**
	 * 设置：创建人
	 */
	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}
	/**
	 * 获取：创建人
	 */
	public String getCreateBy() {
		return createBy;
	}
}
