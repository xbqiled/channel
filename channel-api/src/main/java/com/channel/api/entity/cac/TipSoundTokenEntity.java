package com.channel.api.entity.cac;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * 
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-05-19 14:18:20
 */
@TableName("tip_sound_token")
public class TipSoundTokenEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键
	 */
	@TableId
	private Integer id;
	/**
	 * 渠道编码
	 */
	private String channelId;
	/**
	 * 
	 */
	private String accessToken;
	/**
	 * 
	 */
	private String sessionKey;
	/**
	 * 
	 */
	private String scope;
	/**
	 * 
	 */
	private String refreshToken;
	/**
	 * 
	 */
	private String sessionSecret;
	/**
	 * 
	 */
	private Date expiresTime;
	/**
	 * 创建时间
	 */
	private Date createTime;

	/**
	 * 设置：主键
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * 获取：主键
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * 设置：渠道编码
	 */
	public void setChannelId(String channelId) {
		this.channelId = channelId;
	}
	/**
	 * 获取：渠道编码
	 */
	public String getChannelId() {
		return channelId;
	}
	/**
	 * 设置：
	 */
	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}
	/**
	 * 获取：
	 */
	public String getAccessToken() {
		return accessToken;
	}
	/**
	 * 设置：
	 */
	public void setSessionKey(String sessionKey) {
		this.sessionKey = sessionKey;
	}
	/**
	 * 获取：
	 */
	public String getSessionKey() {
		return sessionKey;
	}
	/**
	 * 设置：
	 */
	public void setScope(String scope) {
		this.scope = scope;
	}
	/**
	 * 获取：
	 */
	public String getScope() {
		return scope;
	}
	/**
	 * 设置：
	 */
	public void setRefreshToken(String refreshToken) {
		this.refreshToken = refreshToken;
	}
	/**
	 * 获取：
	 */
	public String getRefreshToken() {
		return refreshToken;
	}
	/**
	 * 设置：
	 */
	public void setSessionSecret(String sessionSecret) {
		this.sessionSecret = sessionSecret;
	}
	/**
	 * 获取：
	 */
	public String getSessionSecret() {
		return sessionSecret;
	}
	/**
	 * 设置：
	 */
	public void setExpiresTime(Date expiresTime) {
		this.expiresTime = expiresTime;
	}
	/**
	 * 获取：
	 */
	public Date getExpiresTime() {
		return expiresTime;
	}
	/**
	 * 设置：创建时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：创建时间
	 */
	public Date getCreateTime() {
		return createTime;
	}
}
