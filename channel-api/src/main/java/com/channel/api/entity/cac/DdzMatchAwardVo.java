package com.channel.api.entity.cac;


import java.io.Serializable;

/**
 * VIEW
 * 
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-09-17 20:44:19
 */
public class DdzMatchAwardVo implements Serializable {
	private static final long serialVersionUID = 1L;

	private String channelId;

	private String channelName;

	private String iconUrl;

	private Integer depositType;

	private Boolean openType;

	private Integer optionType;

	private byte[] strjsonaward;

	private Integer cfgTime;

	private String cfgTimeStr;


	public String getChannelId() {
		return channelId;
	}

	public void setChannelId(String channelId) {
		this.channelId = channelId;
	}

	public String getChannelName() {
		return channelName;
	}

	public void setChannelName(String channelName) {
		this.channelName = channelName;
	}

	public String getIconUrl() {
		return iconUrl;
	}

	public void setIconUrl(String iconUrl) {
		this.iconUrl = iconUrl;
	}

	public Integer getDepositType() {
		return depositType;
	}

	public void setDepositType(Integer depositType) {
		this.depositType = depositType;
	}

	public Boolean getOpenType() {
		return openType;
	}

	public void setOpenType(Boolean openType) {
		this.openType = openType;
	}

	public Integer getOptionType() {
		return optionType;
	}

	public void setOptionType(Integer optionType) {
		this.optionType = optionType;
	}

	public byte[] getStrjsonaward() {
		return strjsonaward;
	}

	public void setStrjsonaward(byte[] strjsonaward) {
		this.strjsonaward = strjsonaward;
	}

	public Integer getCfgTime() {
		return cfgTime;
	}

	public void setCfgTime(Integer cfgTime) {
		this.cfgTime = cfgTime;
	}

	public String getCfgTimeStr() {
		return cfgTimeStr;
	}

	public void setCfgTimeStr(String cfgTimeStr) {
		this.cfgTimeStr = cfgTimeStr;
	}
}
