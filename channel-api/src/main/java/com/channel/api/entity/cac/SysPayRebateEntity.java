package com.channel.api.entity.cac;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

import java.math.BigDecimal;
import java.io.Serializable;
import java.util.Date;

/**
 * 
 * 
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-06-05 11:46:18
 */
@TableName("sys_pay_rebate")
public class SysPayRebateEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private Integer id;
	/**
	 * 订单号
	 */
	private String orderNo;
	/**
	 * 用户ID
	 */
	private Integer userId;
	/**
	 * 1,支付成功
	 */
	private Integer status;


	private String channelId;
	/**
	 * 返利金额
	 */
	private BigDecimal rebateFee;
	/**
	 * 返利类型
	 */
	private Integer rebateType;
	/**
	 * 创建时间， 1,创建时间
	 */
	private Date createTime;
	/**
	 * 创建人
	 */
	private String createBy;

	public String getChannelId() {
		return channelId;
	}

	public void setChannelId(String channelId) {
		this.channelId = channelId;
	}

	/**
	 * 设置：
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * 设置：订单号
	 */
	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}
	/**
	 * 获取：订单号
	 */
	public String getOrderNo() {
		return orderNo;
	}
	/**
	 * 设置：用户ID
	 */
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	/**
	 * 获取：用户ID
	 */
	public Integer getUserId() {
		return userId;
	}
	/**
	 * 设置：1,支付成功
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}
	/**
	 * 获取：1,支付成功
	 */
	public Integer getStatus() {
		return status;
	}
	/**
	 * 设置：返利金额
	 */
	public void setRebateFee(BigDecimal rebateFee) {
		this.rebateFee = rebateFee;
	}
	/**
	 * 获取：返利金额
	 */
	public BigDecimal getRebateFee() {
		return rebateFee;
	}
	/**
	 * 设置：返利类型
	 */
	public void setRebateType(Integer rebateType) {
		this.rebateType = rebateType;
	}
	/**
	 * 获取：返利类型
	 */
	public Integer getRebateType() {
		return rebateType;
	}
	/**
	 * 设置：创建时间， 1,创建时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：创建时间， 1,创建时间
	 */
	public Date getCreateTime() {
		return createTime;
	}
	/**
	 * 设置：创建人
	 */
	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}
	/**
	 * 获取：创建人
	 */
	public String getCreateBy() {
		return createBy;
	}
}
