package com.channel.api.entity.report;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;

/**
 * VIEW
 * 
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-03-30 19:46:22
 */
@TableName("V_RecordGameRebateDetail")
public class GameRebateEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private Long id;
	/**
	 * 
	 */
	private Integer accountid;
	/**
	 * 
	 */
	private String nickname;
	/**
	 * 
	 */
	private Integer promoterid;
	/**
	 * 
	 */
	private String promoternickname;
	/**
	 * 
	 */
	private Integer level;
	/**
	 * 
	 */
	private Integer count;
	/**
	 * 
	 */
	private Integer recordtime;
	/**
	 * 
	 */
	private String channelno;

	/**
	 * 设置：
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public Long getId() {
		return id;
	}
	/**
	 * 设置：
	 */
	public void setAccountid(Integer accountid) {
		this.accountid = accountid;
	}
	/**
	 * 获取：
	 */
	public Integer getAccountid() {
		return accountid;
	}
	/**
	 * 设置：
	 */
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
	/**
	 * 获取：
	 */
	public String getNickname() {
		return nickname;
	}
	/**
	 * 设置：
	 */
	public void setPromoterid(Integer promoterid) {
		this.promoterid = promoterid;
	}
	/**
	 * 获取：
	 */
	public Integer getPromoterid() {
		return promoterid;
	}
	/**
	 * 设置：
	 */
	public void setPromoternickname(String promoternickname) {
		this.promoternickname = promoternickname;
	}
	/**
	 * 获取：
	 */
	public String getPromoternickname() {
		return promoternickname;
	}
	/**
	 * 设置：
	 */
	public void setLevel(Integer level) {
		this.level = level;
	}
	/**
	 * 获取：
	 */
	public Integer getLevel() {
		return level;
	}
	/**
	 * 设置：
	 */
	public void setCount(Integer count) {
		this.count = count;
	}
	/**
	 * 获取：
	 */
	public Integer getCount() {
		return count;
	}
	/**
	 * 设置：
	 */
	public void setRecordtime(Integer recordtime) {
		this.recordtime = recordtime;
	}
	/**
	 * 获取：
	 */
	public Integer getRecordtime() {
		return recordtime;
	}
	/**
	 * 设置：
	 */
	public void setChannelno(String channelno) {
		this.channelno = channelno;
	}
	/**
	 * 获取：
	 */
	public String getChannelno() {
		return channelno;
	}
}
