package com.channel.api.entity.cac;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;

/**
 * VIEW
 * 
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-05-31 11:20:19
 */
@TableName("V_depositChannel")
public class DepositchannelEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private String tChannel;
	/**
	 * 
	 */
	private byte[] tCfginfo;
	/**
	 * 
	 */
	private Long tStarttime;
	/**
	 * 
	 */
	private Long tEndtime;
	/**
	 * 
	 */
	private Integer tOpentype;

	/**
	 * 设置：
	 */
	public void setTChannel(String tChannel) {
		this.tChannel = tChannel;
	}
	/**
	 * 获取：
	 */
	public String getTChannel() {
		return tChannel;
	}
	/**
	 * 设置：
	 */
	public void setTCfginfo(byte[] tCfginfo) {
		this.tCfginfo = tCfginfo;
	}
	/**
	 * 获取：
	 */
	public byte[] getTCfginfo() {
		return tCfginfo;
	}
	/**
	 * 设置：
	 */
	public void setTStarttime(Long tStarttime) {
		this.tStarttime = tStarttime;
	}
	/**
	 * 获取：
	 */
	public Long getTStarttime() {
		return tStarttime;
	}
	/**
	 * 设置：
	 */
	public void setTEndtime(Long tEndtime) {
		this.tEndtime = tEndtime;
	}
	/**
	 * 获取：
	 */
	public Long getTEndtime() {
		return tEndtime;
	}
	/**
	 * 设置：
	 */
	public void setTOpentype(Integer tOpentype) {
		this.tOpentype = tOpentype;
	}
	/**
	 * 获取：
	 */
	public Integer getTOpentype() {
		return tOpentype;
	}
}
