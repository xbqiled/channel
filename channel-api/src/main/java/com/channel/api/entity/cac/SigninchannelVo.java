package com.channel.api.entity.cac;

import java.io.Serializable;


/**
 * VIEW
 *
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-05-28 14:35:42
 */
public class SigninchannelVo implements Serializable {
    private static final long serialVersionUID = 1L;

    private String channelId;

    private Boolean isCirculate;

    private byte[] cfginfo;

    private Long startTime;

    private Long endTime;

    private String startTimeStr;

    private String endTimeStr;

    private Boolean openType;

    private Integer depositType;

    private String channelName;

    private String iconUrl;

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public Boolean getIsCirculate() {
        return isCirculate;
    }

    public void setIsCirculate(Boolean isCirculate) {
        this.isCirculate = isCirculate;
    }

    public byte[] getCfginfo() {
        return cfginfo;
    }

    public void setCfginfo(byte[] cfginfo) {
        this.cfginfo = cfginfo;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public Long getEndTime() {
        return endTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    public String getStartTimeStr() {
        return startTimeStr;
    }

    public void setStartTimeStr(String startTimeStr) {
        this.startTimeStr = startTimeStr;
    }

    public String getEndTimeStr() {
        return endTimeStr;
    }

    public void setEndTimeStr(String endTimeStr) {
        this.endTimeStr = endTimeStr;
    }

    public Boolean getOpenType() {
        return openType;
    }

    public void setOpenType(Boolean openType) {
        this.openType = openType;
    }

    public Integer getDepositType() {
        return depositType;
    }

    public void setDepositType(Integer depositType) {
        this.depositType = depositType;
    }

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    public String getIconUrl() {
        return iconUrl;
    }

    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }
}
