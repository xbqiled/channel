package com.channel.api.entity.cac;

import java.io.Serializable;
import java.math.BigDecimal;


public class VideoBalanceVo implements Serializable {

    private String channelId;

    private String channelName;

    private String iconUrl;

    private BigDecimal videoBalance;

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    public String getIconUrl() {
        return iconUrl;
    }

    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }

    public BigDecimal getVideoBalance() {
        return videoBalance;
    }

    public void setVideoBalance(BigDecimal videoBalance) {
        this.videoBalance = videoBalance;
    }
}
