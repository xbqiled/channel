package com.channel.api.entity.report;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * VIEW
 * 
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-04-24 19:09:09
 */
@TableName("V_UserBetScoreChange")
public class UserbetscorechangeEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	@TableId
	private Integer id;
	/**
	 * 
	 */
	private Integer userid;
	/**
	 * 
	 */
	private BigDecimal lastbeforecount;
	/**
	 * 
	 */
	private BigDecimal lastgaincount;
	/**
	 * 
	 */
	private BigDecimal lastaftercount;
	/**
	 * 
	 */
	private BigDecimal beforecount;
	/**
	 * 
	 */
	private BigDecimal gaincount;
	/**
	 * 
	 */
	private BigDecimal aftercount;
	/**
	 * 
	 */
	private Integer type;
	/**
	 * 
	 */
	private Integer gametypeid;
	/**
	 * 
	 */
	private Integer recordtime;
	/**
	 * 
	 */
	private String channelno;

	/**
	 * 设置：
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * 设置：
	 */
	public void setUserid(Integer userid) {
		this.userid = userid;
	}
	/**
	 * 获取：
	 */
	public Integer getUserid() {
		return userid;
	}
	/**
	 * 设置：
	 */
	public void setLastbeforecount(BigDecimal lastbeforecount) {
		this.lastbeforecount = lastbeforecount;
	}
	/**
	 * 获取：
	 */
	public BigDecimal getLastbeforecount() {
		return lastbeforecount;
	}
	/**
	 * 设置：
	 */
	public void setLastgaincount(BigDecimal lastgaincount) {
		this.lastgaincount = lastgaincount;
	}
	/**
	 * 获取：
	 */
	public BigDecimal getLastgaincount() {
		return lastgaincount;
	}
	/**
	 * 设置：
	 */
	public void setLastaftercount(BigDecimal lastaftercount) {
		this.lastaftercount = lastaftercount;
	}
	/**
	 * 获取：
	 */
	public BigDecimal getLastaftercount() {
		return lastaftercount;
	}
	/**
	 * 设置：
	 */
	public void setBeforecount(BigDecimal beforecount) {
		this.beforecount = beforecount;
	}
	/**
	 * 获取：
	 */
	public BigDecimal getBeforecount() {
		return beforecount;
	}
	/**
	 * 设置：
	 */
	public void setGaincount(BigDecimal gaincount) {
		this.gaincount = gaincount;
	}
	/**
	 * 获取：
	 */
	public BigDecimal getGaincount() {
		return gaincount;
	}
	/**
	 * 设置：
	 */
	public void setAftercount(BigDecimal aftercount) {
		this.aftercount = aftercount;
	}
	/**
	 * 获取：
	 */
	public BigDecimal getAftercount() {
		return aftercount;
	}
	/**
	 * 设置：
	 */
	public void setType(Integer type) {
		this.type = type;
	}
	/**
	 * 获取：
	 */
	public Integer getType() {
		return type;
	}
	/**
	 * 设置：
	 */
	public void setGametypeid(Integer gametypeid) {
		this.gametypeid = gametypeid;
	}
	/**
	 * 获取：
	 */
	public Integer getGametypeid() {
		return gametypeid;
	}
	/**
	 * 设置：
	 */
	public void setRecordtime(Integer recordtime) {
		this.recordtime = recordtime;
	}
	/**
	 * 获取：
	 */
	public Integer getRecordtime() {
		return recordtime;
	}
	/**
	 * 设置：
	 */
	public void setChannelno(String channelno) {
		this.channelno = channelno;
	}
	/**
	 * 获取：
	 */
	public String getChannelno() {
		return channelno;
	}
}
