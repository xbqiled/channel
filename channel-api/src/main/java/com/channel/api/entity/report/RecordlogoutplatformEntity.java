package com.channel.api.entity.report;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;

/**
 * VIEW
 * 
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-02-25 19:18:37
 */
@TableName("V_RecordUserPlatformLogout")
public class RecordlogoutplatformEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private Integer id;
	/**
	 * 
	 */
	private Integer userid;
	/**
	 * 
	 */
	private Integer onlineduration;
	/**
	 * 
	 */
	private Integer logouttime;
	/**
	 * 
	 */
	private String matchguid;
	/**
	 * 
	 */
	private Integer logouttype;
	/**
	 * 
	 */
	private String channelno;

	/**
	 * 设置：
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * 设置：
	 */
	public void setUserid(Integer userid) {
		this.userid = userid;
	}
	/**
	 * 获取：
	 */
	public Integer getUserid() {
		return userid;
	}
	/**
	 * 设置：
	 */
	public void setOnlineduration(Integer onlineduration) {
		this.onlineduration = onlineduration;
	}
	/**
	 * 获取：
	 */
	public Integer getOnlineduration() {
		return onlineduration;
	}
	/**
	 * 设置：
	 */
	public void setLogouttime(Integer logouttime) {
		this.logouttime = logouttime;
	}
	/**
	 * 获取：
	 */
	public Integer getLogouttime() {
		return logouttime;
	}
	/**
	 * 设置：
	 */
	public void setMatchguid(String matchguid) {
		this.matchguid = matchguid;
	}
	/**
	 * 获取：
	 */
	public String getMatchguid() {
		return matchguid;
	}
	/**
	 * 设置：
	 */
	public void setLogouttype(Integer logouttype) {
		this.logouttype = logouttype;
	}
	/**
	 * 获取：
	 */
	public Integer getLogouttype() {
		return logouttype;
	}
	/**
	 * 设置：
	 */
	public void setChannelno(String channelno) {
		this.channelno = channelno;
	}
	/**
	 * 获取：
	 */
	public String getChannelno() {
		return channelno;
	}
}
