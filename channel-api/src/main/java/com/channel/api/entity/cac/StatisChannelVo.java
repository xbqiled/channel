package com.channel.api.entity.cac;


import java.io.Serializable;
import java.math.BigDecimal;



public class StatisChannelVo implements Serializable {

    private Long id;
    /**
     * 统计时间
     */
    private String statisTime;
    /**
     * 渠道编码
     */
    private String channelId;
    /**
     * 充值金额
     */
    private BigDecimal rechargeFee;
    /**
     * 充值人数
     */
    private Long rechargeUserCount;
    /**
     * 充值笔数
     */
    private Long rechargeCount;
    /**
     * 提现金额
     */
    private BigDecimal cashFee;
    /**
     * 提现人数
     */
    private Long cashUserCount;
    /**
     * 提现笔数
     */
    private Long cashCount;
    /**
     * 盈亏统计
     */
    private BigDecimal loss;
    /**
     * 新增注册人数
     */
    private Long regUserCount;
    /**
     * 新增游戏人数
     */
    private Long newGameUserCount;
    /**
     * 游戏次数
     */
    private Long gameCount;
    /**
     * 游戏人数
     */
    private Long gameUserCount;
    /**
     * 登录次数
     */
    private Long loginCount;
    /**
     * 登录人数
     */
    private Long loginUserCount;
    /**
     * 日活跃用户
     */
    private Long dailyActiveCount;
    /**
     * 首冲金额
     */
    private BigDecimal firstRechargeFee;
    /**
     * 首次充值人数
     */
    private Long firstRechargeCount;
    /**
     * 用户游戏总时长
     */
    private Long timeLength;
    /**
     * 用户平均时长
     */
    private Long userAvgGametime;
    /**
     * 充值率
     */
    private Long rechargeRate;
    /**
     * 步骤
     */
    private Integer step;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStatisTime() {
        return statisTime;
    }

    public void setStatisTime(String statisTime) {
        this.statisTime = statisTime;
    }

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public BigDecimal getRechargeFee() {
        return rechargeFee;
    }

    public void setRechargeFee(BigDecimal rechargeFee) {
        this.rechargeFee = rechargeFee;
    }

    public Long getRechargeUserCount() {
        return rechargeUserCount;
    }

    public void setRechargeUserCount(Long rechargeUserCount) {
        this.rechargeUserCount = rechargeUserCount;
    }

    public Long getRechargeCount() {
        return rechargeCount;
    }

    public void setRechargeCount(Long rechargeCount) {
        this.rechargeCount = rechargeCount;
    }

    public BigDecimal getCashFee() {
        return cashFee;
    }

    public void setCashFee(BigDecimal cashFee) {
        this.cashFee = cashFee;
    }

    public Long getCashUserCount() {
        return cashUserCount;
    }

    public void setCashUserCount(Long cashUserCount) {
        this.cashUserCount = cashUserCount;
    }

    public Long getCashCount() {
        return cashCount;
    }

    public void setCashCount(Long cashCount) {
        this.cashCount = cashCount;
    }

    public BigDecimal getLoss() {
        return loss;
    }

    public void setLoss(BigDecimal loss) {
        this.loss = loss;
    }

    public Long getRegUserCount() {
        return regUserCount;
    }

    public void setRegUserCount(Long regUserCount) {
        this.regUserCount = regUserCount;
    }

    public Long getNewGameUserCount() {
        return newGameUserCount;
    }

    public void setNewGameUserCount(Long newGameUserCount) {
        this.newGameUserCount = newGameUserCount;
    }

    public Long getGameCount() {
        return gameCount;
    }

    public void setGameCount(Long gameCount) {
        this.gameCount = gameCount;
    }

    public Long getGameUserCount() {
        return gameUserCount;
    }

    public void setGameUserCount(Long gameUserCount) {
        this.gameUserCount = gameUserCount;
    }

    public Long getLoginCount() {
        return loginCount;
    }

    public void setLoginCount(Long loginCount) {
        this.loginCount = loginCount;
    }

    public Long getLoginUserCount() {
        return loginUserCount;
    }

    public void setLoginUserCount(Long loginUserCount) {
        this.loginUserCount = loginUserCount;
    }

    public Long getDailyActiveCount() {
        return dailyActiveCount;
    }

    public void setDailyActiveCount(Long dailyActiveCount) {
        this.dailyActiveCount = dailyActiveCount;
    }

    public BigDecimal getFirstRechargeFee() {
        return firstRechargeFee;
    }

    public void setFirstRechargeFee(BigDecimal firstRechargeFee) {
        this.firstRechargeFee = firstRechargeFee;
    }

    public Long getFirstRechargeCount() {
        return firstRechargeCount;
    }

    public void setFirstRechargeCount(Long firstRechargeCount) {
        this.firstRechargeCount = firstRechargeCount;
    }

    public Long getTimeLength() {
        return timeLength;
    }

    public void setTimeLength(Long timeLength) {
        this.timeLength = timeLength;
    }

    public Long getUserAvgGametime() {
        return userAvgGametime;
    }

    public void setUserAvgGametime(Long userAvgGametime) {
        this.userAvgGametime = userAvgGametime;
    }

    public Long getRechargeRate() {
        return rechargeRate;
    }

    public void setRechargeRate(Long rechargeRate) {
        this.rechargeRate = rechargeRate;
    }

    public Integer getStep() {
        return step;
    }

    public void setStep(Integer step) {
        this.step = step;
    }
}
