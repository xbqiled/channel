package com.channel.api.entity.cac;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

import java.math.BigDecimal;
import java.io.Serializable;
import java.util.Date;

/**
 * 
 * 
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2020-03-02 21:28:00
 */
@TableName("tmp_statis_game")
public class StatisGameEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键
	 */
	@TableId
	private Long id;
	/**
	 * 游戏ID
	 */
	private Long gameId;
	/**
	 * 用户Id
	 */
	private Long userId;
	/**
	 * 游戏次数
	 */
	private Long gameCount;
	/**
	 * 渠道编码
	 */
	private String channelId;
	/**
	 * 赢取金额
	 */
	private BigDecimal gameFee;
	/**
	 * 统计时间
	 */
	private Date statisTime;
	/**
	 * 下注金额
	 */
	private BigDecimal betFee;
	/**
	 * 税手金额
	 */
	private BigDecimal revenueFee;
	/**
	 * 是否比赛
	 */
	private Integer isMatch;
	/**
	 * 游戏时长
	 */
	private Long gameTimeLength;

	/**
	 * 设置：主键
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * 获取：主键
	 */
	public Long getId() {
		return id;
	}
	/**
	 * 设置：游戏ID
	 */
	public void setGameId(Long gameId) {
		this.gameId = gameId;
	}
	/**
	 * 获取：游戏ID
	 */
	public Long getGameId() {
		return gameId;
	}
	/**
	 * 设置：用户Id
	 */
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	/**
	 * 获取：用户Id
	 */
	public Long getUserId() {
		return userId;
	}
	/**
	 * 设置：游戏次数
	 */
	public void setGameCount(Long gameCount) {
		this.gameCount = gameCount;
	}
	/**
	 * 获取：游戏次数
	 */
	public Long getGameCount() {
		return gameCount;
	}
	/**
	 * 设置：渠道编码
	 */
	public void setChannelId(String channelId) {
		this.channelId = channelId;
	}
	/**
	 * 获取：渠道编码
	 */
	public String getChannelId() {
		return channelId;
	}
	/**
	 * 设置：赢取金额
	 */
	public void setGameFee(BigDecimal gameFee) {
		this.gameFee = gameFee;
	}
	/**
	 * 获取：赢取金额
	 */
	public BigDecimal getGameFee() {
		return gameFee;
	}
	/**
	 * 设置：统计时间
	 */
	public void setStatisTime(Date statisTime) {
		this.statisTime = statisTime;
	}
	/**
	 * 获取：统计时间
	 */
	public Date getStatisTime() {
		return statisTime;
	}
	/**
	 * 设置：下注金额
	 */
	public void setBetFee(BigDecimal betFee) {
		this.betFee = betFee;
	}
	/**
	 * 获取：下注金额
	 */
	public BigDecimal getBetFee() {
		return betFee;
	}
	/**
	 * 设置：税手金额
	 */
	public void setRevenueFee(BigDecimal revenueFee) {
		this.revenueFee = revenueFee;
	}
	/**
	 * 获取：税手金额
	 */
	public BigDecimal getRevenueFee() {
		return revenueFee;
	}
	/**
	 * 设置：是否比赛
	 */
	public void setIsMatch(Integer isMatch) {
		this.isMatch = isMatch;
	}
	/**
	 * 获取：是否比赛
	 */
	public Integer getIsMatch() {
		return isMatch;
	}
	/**
	 * 设置：游戏时长
	 */
	public void setGameTimeLength(Long gameTimeLength) {
		this.gameTimeLength = gameTimeLength;
	}
	/**
	 * 获取：游戏时长
	 */
	public Long getGameTimeLength() {
		return gameTimeLength;
	}
}
