package com.channel.api.entity.report;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;

/**
 * VIEW
 * 
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-06-02 13:08:43
 */
@TableName("V_RedBlackRewardPool")
public class RedblackpoolEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private Integer id;
	/**
	 * 
	 */
	private String recordid;
	/**
	 * 
	 */
	private Integer periodical;
	/**
	 * 
	 */
	private Integer userid;
	/**
	 * 
	 */
	private Integer betorder;
	/**
	 * 
	 */
	private Integer beforecoin;
	/**
	 * 
	 */
	private Integer reward;
	/**
	 * 
	 */
	private Integer aftercoin;
	/**
	 * 
	 */
	private Integer starttime;
	/**
	 * 
	 */
	private Integer endtime;
	/**
	 * 
	 */
	private Integer gametypeid;
	/**
	 * 
	 */
	private String channelno;

	/**
	 * 设置：
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * 设置：
	 */
	public void setRecordid(String recordid) {
		this.recordid = recordid;
	}
	/**
	 * 获取：
	 */
	public String getRecordid() {
		return recordid;
	}
	/**
	 * 设置：
	 */
	public void setPeriodical(Integer periodical) {
		this.periodical = periodical;
	}
	/**
	 * 获取：
	 */
	public Integer getPeriodical() {
		return periodical;
	}
	/**
	 * 设置：
	 */
	public void setUserid(Integer userid) {
		this.userid = userid;
	}
	/**
	 * 获取：
	 */
	public Integer getUserid() {
		return userid;
	}
	/**
	 * 设置：
	 */
	public void setBetorder(Integer betorder) {
		this.betorder = betorder;
	}
	/**
	 * 获取：
	 */
	public Integer getBetorder() {
		return betorder;
	}
	/**
	 * 设置：
	 */
	public void setBeforecoin(Integer beforecoin) {
		this.beforecoin = beforecoin;
	}
	/**
	 * 获取：
	 */
	public Integer getBeforecoin() {
		return beforecoin;
	}
	/**
	 * 设置：
	 */
	public void setReward(Integer reward) {
		this.reward = reward;
	}
	/**
	 * 获取：
	 */
	public Integer getReward() {
		return reward;
	}
	/**
	 * 设置：
	 */
	public void setAftercoin(Integer aftercoin) {
		this.aftercoin = aftercoin;
	}
	/**
	 * 获取：
	 */
	public Integer getAftercoin() {
		return aftercoin;
	}
	/**
	 * 设置：
	 */
	public void setStarttime(Integer starttime) {
		this.starttime = starttime;
	}
	/**
	 * 获取：
	 */
	public Integer getStarttime() {
		return starttime;
	}
	/**
	 * 设置：
	 */
	public void setEndtime(Integer endtime) {
		this.endtime = endtime;
	}
	/**
	 * 获取：
	 */
	public Integer getEndtime() {
		return endtime;
	}
	/**
	 * 设置：
	 */
	public void setGametypeid(Integer gametypeid) {
		this.gametypeid = gametypeid;
	}
	/**
	 * 获取：
	 */
	public Integer getGametypeid() {
		return gametypeid;
	}
	/**
	 * 设置：
	 */
	public void setChannelno(String channelno) {
		this.channelno = channelno;
	}
	/**
	 * 获取：
	 */
	public String getChannelno() {
		return channelno;
	}
}
