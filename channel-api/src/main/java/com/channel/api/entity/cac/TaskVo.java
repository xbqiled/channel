package com.channel.api.entity.cac;

import java.io.Serializable;


public class TaskVo implements Serializable {

    private String channelId;

    private Boolean openType;

    private Integer activityOpenType;

    private byte[] taskInfo;

    private Integer activityTimeType;

    private String beginTime;

    private String endTime;

    private String cfgTime;

    private String iconUrl;

    private String channelName;

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public Boolean getOpenType() {
        return openType;
    }

    public void setOpenType(Boolean openType) {
        this.openType = openType;
    }

    public Integer getActivityOpenType() {
        return activityOpenType;
    }

    public void setActivityOpenType(Integer activityOpenType) {
        this.activityOpenType = activityOpenType;
    }

    public byte[] getTaskInfo() {
        return taskInfo;
    }

    public void setTaskInfo(byte[] taskInfo) {
        this.taskInfo = taskInfo;
    }

    public Integer getActivityTimeType() {
        return activityTimeType;
    }

    public void setActivityTimeType(Integer activityTimeType) {
        this.activityTimeType = activityTimeType;
    }

    public String getBeginTime() {
        return beginTime;
    }

    public void setBeginTime(String beginTime) {
        this.beginTime = beginTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getCfgTime() {
        return cfgTime;
    }

    public void setCfgTime(String cfgTime) {
        this.cfgTime = cfgTime;
    }

    public String getIconUrl() {
        return iconUrl;
    }

    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }
}
