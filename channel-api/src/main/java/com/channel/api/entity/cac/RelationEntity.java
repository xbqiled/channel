package com.channel.api.entity.cac;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;
import java.util.Date;

/**
 * VIEW
 * 
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-05-07 17:05:21
 */
@TableName("V_relation")
public class RelationEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private Integer tAccountid;
	/**
	 * 
	 */
	private Integer tPromoterid;
	/**
	 * 
	 */
	private byte[] tExtend;

	/**
	 * 设置：
	 */
	public void setTAccountid(Integer tAccountid) {
		this.tAccountid = tAccountid;
	}
	/**
	 * 获取：
	 */
	public Integer getTAccountid() {
		return tAccountid;
	}
	/**
	 * 设置：
	 */
	public void setTPromoterid(Integer tPromoterid) {
		this.tPromoterid = tPromoterid;
	}
	/**
	 * 获取：
	 */
	public Integer getTPromoterid() {
		return tPromoterid;
	}
	/**
	 * 设置：
	 */
	public void setTExtend(byte[] tExtend) {
		this.tExtend = tExtend;
	}
	/**
	 * 获取：
	 */
	public byte[] getTExtend() {
		return tExtend;
	}
}
