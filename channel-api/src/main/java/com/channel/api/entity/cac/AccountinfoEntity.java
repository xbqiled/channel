package com.channel.api.entity.cac;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;

/**
 * 
 * 
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-02-23 14:34:23
 */
@TableName("V_accountInfo")
public class AccountinfoEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	@TableId
	private Integer tAccountid;
	/**
	 *
	 */
	private String tAccountname;
	/**
	 *
	 */
	private String tPhonenumber;
	/**
	 *
	 */
	private String tChannelkey;
	/**
	 *
	 */
	private String tAccountkey;
	/**
	 *
	 */
	private String tNickname;
	/**
	 *
	 */
	private Integer tBisregularaccount;
	/**
	 *
	 */
	private String tIdcard;
	/**
	 *
	 */
	private String tLockmachine;
	/**
	 *
	 */
	private Integer tLastlogintime;
	/**
	 *
	 */
	private Integer tLastlogouttime;
	/**
	 *
	 */
	private String tCommondevices;
	/**
	 *
	 */
	private String tCommonips;
	/**
	 *
	 */
	private Integer tAccountstate;
	/**
	 *
	 */
	private Integer tAuthpwdfailtimes;
	/**
	 *
	 */
	private String tExcepverifyinfo;
	/**
	 *
	 */
	private Integer tRegtime;
	/**
	 *
	 */
	private Integer tRegterminaltype;
	/**
	 *
	 */
	private String tRegip;
	/**
	 *
	 */
	private String tRegdevice;
	/**
	 *
	 */
	private Integer tIsuniqdevicecode;
	/**
	 *
	 */
	private Integer tSharetype;
	/**
	 *
	 */
	private Integer tSharetriggerfuncid;
	/**
	 *
	 */
	private String tAlipayinfo;
	/**
	 *
	 */
	private String tBankinfo;

	private String tOrigin;

	private String tRealname;

	private String tDevicetoken;

	public Integer gettAccountid() {
		return tAccountid;
	}

	public void settAccountid(Integer tAccountid) {
		this.tAccountid = tAccountid;
	}

	public String gettAccountname() {
		return tAccountname;
	}

	public void settAccountname(String tAccountname) {
		this.tAccountname = tAccountname;
	}

	public String gettPhonenumber() {
		return tPhonenumber;
	}

	public void settPhonenumber(String tPhonenumber) {
		this.tPhonenumber = tPhonenumber;
	}

	public String gettChannelkey() {
		return tChannelkey;
	}

	public void settChannelkey(String tChannelkey) {
		this.tChannelkey = tChannelkey;
	}

	public String gettAccountkey() {
		return tAccountkey;
	}

	public void settAccountkey(String tAccountkey) {
		this.tAccountkey = tAccountkey;
	}

	public String gettNickname() {
		return tNickname;
	}

	public void settNickname(String tNickname) {
		this.tNickname = tNickname;
	}

	public Integer gettBisregularaccount() {
		return tBisregularaccount;
	}

	public void settBisregularaccount(Integer tBisregularaccount) {
		this.tBisregularaccount = tBisregularaccount;
	}

	public String gettIdcard() {
		return tIdcard;
	}

	public void settIdcard(String tIdcard) {
		this.tIdcard = tIdcard;
	}

	public String gettLockmachine() {
		return tLockmachine;
	}

	public void settLockmachine(String tLockmachine) {
		this.tLockmachine = tLockmachine;
	}

	public Integer gettLastlogintime() {
		return tLastlogintime;
	}

	public void settLastlogintime(Integer tLastlogintime) {
		this.tLastlogintime = tLastlogintime;
	}

	public Integer gettLastlogouttime() {
		return tLastlogouttime;
	}

	public void settLastlogouttime(Integer tLastlogouttime) {
		this.tLastlogouttime = tLastlogouttime;
	}

	public String gettCommondevices() {
		return tCommondevices;
	}

	public void settCommondevices(String tCommondevices) {
		this.tCommondevices = tCommondevices;
	}

	public String gettCommonips() {
		return tCommonips;
	}

	public void settCommonips(String tCommonips) {
		this.tCommonips = tCommonips;
	}

	public Integer gettAccountstate() {
		return tAccountstate;
	}

	public void settAccountstate(Integer tAccountstate) {
		this.tAccountstate = tAccountstate;
	}

	public Integer gettAuthpwdfailtimes() {
		return tAuthpwdfailtimes;
	}

	public void settAuthpwdfailtimes(Integer tAuthpwdfailtimes) {
		this.tAuthpwdfailtimes = tAuthpwdfailtimes;
	}

	public String gettExcepverifyinfo() {
		return tExcepverifyinfo;
	}

	public void settExcepverifyinfo(String tExcepverifyinfo) {
		this.tExcepverifyinfo = tExcepverifyinfo;
	}

	public Integer gettRegtime() {
		return tRegtime;
	}

	public void settRegtime(Integer tRegtime) {
		this.tRegtime = tRegtime;
	}

	public Integer gettRegterminaltype() {
		return tRegterminaltype;
	}

	public void settRegterminaltype(Integer tRegterminaltype) {
		this.tRegterminaltype = tRegterminaltype;
	}

	public String gettRegip() {
		return tRegip;
	}

	public void settRegip(String tRegip) {
		this.tRegip = tRegip;
	}

	public String gettRegdevice() {
		return tRegdevice;
	}

	public void settRegdevice(String tRegdevice) {
		this.tRegdevice = tRegdevice;
	}

	public Integer gettIsuniqdevicecode() {
		return tIsuniqdevicecode;
	}

	public void settIsuniqdevicecode(Integer tIsuniqdevicecode) {
		this.tIsuniqdevicecode = tIsuniqdevicecode;
	}

	public Integer gettSharetype() {
		return tSharetype;
	}

	public void settSharetype(Integer tSharetype) {
		this.tSharetype = tSharetype;
	}

	public Integer gettSharetriggerfuncid() {
		return tSharetriggerfuncid;
	}

	public void settSharetriggerfuncid(Integer tSharetriggerfuncid) {
		this.tSharetriggerfuncid = tSharetriggerfuncid;
	}

	public String gettAlipayinfo() {
		return tAlipayinfo;
	}

	public void settAlipayinfo(String tAlipayinfo) {
		this.tAlipayinfo = tAlipayinfo;
	}

	public String gettBankinfo() {
		return tBankinfo;
	}

	public void settBankinfo(String tBankinfo) {
		this.tBankinfo = tBankinfo;
	}

	public String gettOrigin() {
		return tOrigin;
	}

	public void settOrigin(String tOrigin) {
		this.tOrigin = tOrigin;
	}


	public String gettRealname() {
		return tRealname;
	}

	public void settRealname(String tRealname) {
		this.tRealname = tRealname;
	}


	public String gettDevicetoken() {
		return tDevicetoken;
	}

	public void settDevicetoken(String tDevicetoken) {
		this.tDevicetoken = tDevicetoken;
	}
}
