package com.channel.api.entity.cac;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;
import java.util.Date;

/**
 * VIEW
 * 
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-09-17 20:44:19
 */
@TableName("v_ddzmatchrankaward")
public class DdzmatchrankawardEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private String tChannel;
	/**
	 * 
	 */
	private Integer tOption;
	/**
	 * 
	 */
	private Integer tOpentype;
	/**
	 * 
	 */
	private byte[] tStrjsonaward;
	/**
	 * 
	 */
	private Integer tCfgtime;

	/**
	 * 设置：
	 */
	public void setTChannel(String tChannel) {
		this.tChannel = tChannel;
	}
	/**
	 * 获取：
	 */
	public String getTChannel() {
		return tChannel;
	}
	/**
	 * 设置：
	 */
	public void setTOption(Integer tOption) {
		this.tOption = tOption;
	}
	/**
	 * 获取：
	 */
	public Integer getTOption() {
		return tOption;
	}
	/**
	 * 设置：
	 */
	public void setTOpentype(Integer tOpentype) {
		this.tOpentype = tOpentype;
	}
	/**
	 * 获取：
	 */
	public Integer getTOpentype() {
		return tOpentype;
	}
	/**
	 * 设置：
	 */
	public void setTStrjsonaward( byte[]  tStrjsonaward) {
		this.tStrjsonaward = tStrjsonaward;
	}
	/**
	 * 获取：
	 */
	public  byte[]  getTStrjsonaward() {
		return tStrjsonaward;
	}
	/**
	 * 设置：
	 */
	public void setTCfgtime(Integer tCfgtime) {
		this.tCfgtime = tCfgtime;
	}
	/**
	 * 获取：
	 */
	public Integer getTCfgtime() {
		return tCfgtime;
	}
}
