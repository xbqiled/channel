package com.channel.api.entity.cac;

import java.io.Serializable;
import java.util.Date;


public class AccountInfoVo implements Serializable {

    private String accountid;

    private String accountname;

    private String phonenumber;

    private String channelkey;

    private String accountkey;

    private String nickname;

    private String bisregularaccount;

    private String idcard;

    private String lockmachine;

    private String lastlogintime;

    private String lastlogouttime;

    private String commondevices;

    private String commonips;

    private String accountstate;

    private String authpwdfailtimes;

    private String excepverifyinfo;

    private String regtime;

    private String regterminaltype;

    private String regip;

    private String regdevice;

    private String isuniqdevicecode;

    private String sharetype;

    private String sharetriggerfuncid;

    private byte[] alipayinfo;

    private byte[] bankinfo;

    private String origin;

    private String realName;

    private String deviceToken;

    private String sex;

    private byte[] vipInfo;

    private String goldcoin;

    private String avatarid;

    private String bankpassword;

    private String bankgoldcoin;

    private String vipLevel;

    private String ismodifiednick;

    private String totalgametime;

    private String todaygametime;

    private String todaygametimerefreshtime;

    private String totalcoinget;

    private String todaycoinget;

    private String todaycoinrefreshtime;

    private String rechargemoney;

    private String lastGameBetScore;

    private String todayGameBetScore;

    private Date betScoreRsfTime;

    private String note;

    public String getAccountid() {
        return accountid;
    }

    public void setAccountid(String accountid) {
        this.accountid = accountid;
    }

    public String getAccountname() {
        return accountname;
    }

    public void setAccountname(String accountname) {
        this.accountname = accountname;
    }

    public String getPhonenumber() {
        return phonenumber;
    }

    public void setPhonenumber(String phonenumber) {
        this.phonenumber = phonenumber;
    }

    public String getChannelkey() {
        return channelkey;
    }

    public void setChannelkey(String channelkey) {
        this.channelkey = channelkey;
    }

    public String getAccountkey() {
        return accountkey;
    }

    public void setAccountkey(String accountkey) {
        this.accountkey = accountkey;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getBisregularaccount() {
        return bisregularaccount;
    }

    public void setBisregularaccount(String bisregularaccount) {
        this.bisregularaccount = bisregularaccount;
    }

    public String getIdcard() {
        return idcard;
    }

    public void setIdcard(String idcard) {
        this.idcard = idcard;
    }

    public String getLockmachine() {
        return lockmachine;
    }

    public void setLockmachine(String lockmachine) {
        this.lockmachine = lockmachine;
    }

    public String getLastlogintime() {
        return lastlogintime;
    }

    public void setLastlogintime(String lastlogintime) {
        this.lastlogintime = lastlogintime;
    }

    public String getLastlogouttime() {
        return lastlogouttime;
    }

    public void setLastlogouttime(String lastlogouttime) {
        this.lastlogouttime = lastlogouttime;
    }

    public String getCommondevices() {
        return commondevices;
    }

    public void setCommondevices(String commondevices) {
        this.commondevices = commondevices;
    }

    public String getCommonips() {
        return commonips;
    }

    public void setCommonips(String commonips) {
        this.commonips = commonips;
    }

    public String getAccountstate() {
        return accountstate;
    }

    public void setAccountstate(String accountstate) {
        this.accountstate = accountstate;
    }

    public String getAuthpwdfailtimes() {
        return authpwdfailtimes;
    }

    public void setAuthpwdfailtimes(String authpwdfailtimes) {
        this.authpwdfailtimes = authpwdfailtimes;
    }

    public String getExcepverifyinfo() {
        return excepverifyinfo;
    }

    public void setExcepverifyinfo(String excepverifyinfo) {
        this.excepverifyinfo = excepverifyinfo;
    }

    public String getRegtime() {
        return regtime;
    }

    public void setRegtime(String regtime) {
        this.regtime = regtime;
    }

    public String getRegterminaltype() {
        return regterminaltype;
    }

    public void setRegterminaltype(String regterminaltype) {
        this.regterminaltype = regterminaltype;
    }

    public String getRegip() {
        return regip;
    }

    public void setRegip(String regip) {
        this.regip = regip;
    }

    public String getRegdevice() {
        return regdevice;
    }

    public void setRegdevice(String regdevice) {
        this.regdevice = regdevice;
    }

    public String getIsuniqdevicecode() {
        return isuniqdevicecode;
    }

    public void setIsuniqdevicecode(String isuniqdevicecode) {
        this.isuniqdevicecode = isuniqdevicecode;
    }

    public String getSharetype() {
        return sharetype;
    }

    public void setSharetype(String sharetype) {
        this.sharetype = sharetype;
    }

    public String getSharetriggerfuncid() {
        return sharetriggerfuncid;
    }

    public void setSharetriggerfuncid(String sharetriggerfuncid) {
        this.sharetriggerfuncid = sharetriggerfuncid;
    }

    public byte[] getAlipayinfo() {
        return alipayinfo;
    }

    public void setAlipayinfo(byte[] alipayinfo) {
        this.alipayinfo = alipayinfo;
    }

    public byte[] getBankinfo() {
        return bankinfo;
    }

    public void setBankinfo(byte[] bankinfo) {
        this.bankinfo = bankinfo;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public String getDeviceToken() {
        return deviceToken;
    }

    public void setDeviceToken(String deviceToken) {
        this.deviceToken = deviceToken;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public byte[] getVipInfo() {
        return vipInfo;
    }

    public void setVipInfo(byte[] vipInfo) {
        this.vipInfo = vipInfo;
    }

    public String getGoldcoin() {
        return goldcoin;
    }

    public void setGoldcoin(String goldcoin) {
        this.goldcoin = goldcoin;
    }

    public String getAvatarid() {
        return avatarid;
    }

    public void setAvatarid(String avatarid) {
        this.avatarid = avatarid;
    }

    public String getBankpassword() {
        return bankpassword;
    }

    public void setBankpassword(String bankpassword) {
        this.bankpassword = bankpassword;
    }

    public String getBankgoldcoin() {
        return bankgoldcoin;
    }

    public void setBankgoldcoin(String bankgoldcoin) {
        this.bankgoldcoin = bankgoldcoin;
    }

    public String getVipLevel() {
        return vipLevel;
    }

    public void setVipLevel(String vipLevel) {
        this.vipLevel = vipLevel;
    }

    public String getIsmodifiednick() {
        return ismodifiednick;
    }

    public void setIsmodifiednick(String ismodifiednick) {
        this.ismodifiednick = ismodifiednick;
    }

    public String getTotalgametime() {
        return totalgametime;
    }

    public void setTotalgametime(String totalgametime) {
        this.totalgametime = totalgametime;
    }

    public String getTodaygametime() {
        return todaygametime;
    }

    public void setTodaygametime(String todaygametime) {
        this.todaygametime = todaygametime;
    }

    public String getTodaygametimerefreshtime() {
        return todaygametimerefreshtime;
    }

    public void setTodaygametimerefreshtime(String todaygametimerefreshtime) {
        this.todaygametimerefreshtime = todaygametimerefreshtime;
    }

    public String getTotalcoinget() {
        return totalcoinget;
    }

    public void setTotalcoinget(String totalcoinget) {
        this.totalcoinget = totalcoinget;
    }

    public String getTodaycoinget() {
        return todaycoinget;
    }

    public void setTodaycoinget(String todaycoinget) {
        this.todaycoinget = todaycoinget;
    }

    public String getTodaycoinrefreshtime() {
        return todaycoinrefreshtime;
    }

    public void setTodaycoinrefreshtime(String todaycoinrefreshtime) {
        this.todaycoinrefreshtime = todaycoinrefreshtime;
    }

    public String getRechargemoney() {
        return rechargemoney;
    }

    public void setRechargemoney(String rechargemoney) {
        this.rechargemoney = rechargemoney;
    }

    public String getLastGameBetScore() {
        return lastGameBetScore;
    }

    public void setLastGameBetScore(String lastGameBetScore) {
        this.lastGameBetScore = lastGameBetScore;
    }

    public String getTodayGameBetScore() {
        return todayGameBetScore;
    }

    public void setTodayGameBetScore(String todayGameBetScore) {
        this.todayGameBetScore = todayGameBetScore;
    }

    public Date getBetScoreRsfTime() {
        return betScoreRsfTime;
    }

    public void setBetScoreRsfTime(Date betScoreRsfTime) {
        this.betScoreRsfTime = betScoreRsfTime;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
}
