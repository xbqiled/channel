package com.channel.api.entity.cac;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

import java.math.BigDecimal;
import java.io.Serializable;
import java.util.Date;

/**
 * 
 * 
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-11-15 15:21:28
 */
@TableName("tmp_bgbet_record")
public class BgbetRecordEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 订单ID
	 */
	@TableId
	private Long orderId;
	/**
	 * 订单ID
	 */
	private Long tranId;
	/**
	 * 厅代码
	 */
	private String sn;
	/**
	 * 用户ID
	 */
	private Long uid;
	/**
	 * 用户登录ID
	 */
	private String loginId;
	/**
	 * 模块ID
	 */
	private Long moduleId;
	/**
	 * 模块名称
	 */
	private String moduleName;
	/**
	 * 游戏ID
	 */
	private Long gameId;
	/**
	 * 玩法名称
	 */
	private String gameName;
	/**
	 * 注单状态
	 */
	private Long orderStatus;
	/**
	 * 下注额
	 */
	private BigDecimal bAmount;
	/**
	 * 结算额
	 */
	private BigDecimal aAmount;
	/**
	 * 订单来源
	 */
	private Long orderFrom;
	/**
	 * 订单时间
	 */
	private Date orderTime;
	/**
	 * 最后修改时间
	 */
	private Date lastUpdateTime;
	/**
	 * 下注来源IP
	 */
	private String fromIp;
	/**
	 * 下注来源IP归属地
	 */
	private String fromIpAddr;
	/**
	 * 下注期数
	 */
	private String issueId;
	/**
	 * 玩法ID
	 */
	private String playId;
	/**
	 * 玩法名称
	 */
	private String playName;
	/**
	 * 玩法名称(En)
	 */
	private String playNameEn;
	/**
	 * 打码量
	 */
	private Float validBet;
	/**
	 * 派彩(输赢)
	 */
	private Float payment;
	/**
	 * 是否同步打码量
	 */
	private String userId;


	private String detailUrl;

	/**
	 * 设置：订单ID
	 */
	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}
	/**
	 * 获取：订单ID
	 */
	public Long getOrderId() {
		return orderId;
	}
	/**
	 * 设置：订单ID
	 */
	public void setTranId(Long tranId) {
		this.tranId = tranId;
	}
	/**
	 * 获取：订单ID
	 */
	public Long getTranId() {
		return tranId;
	}
	/**
	 * 设置：厅代码
	 */
	public void setSn(String sn) {
		this.sn = sn;
	}
	/**
	 * 获取：厅代码
	 */
	public String getSn() {
		return sn;
	}
	/**
	 * 设置：用户ID
	 */
	public void setUid(Long uid) {
		this.uid = uid;
	}
	/**
	 * 获取：用户ID
	 */
	public Long getUid() {
		return uid;
	}
	/**
	 * 设置：用户登录ID
	 */
	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}
	/**
	 * 获取：用户登录ID
	 */
	public String getLoginId() {
		return loginId;
	}
	/**
	 * 设置：模块ID
	 */
	public void setModuleId(Long moduleId) {
		this.moduleId = moduleId;
	}
	/**
	 * 获取：模块ID
	 */
	public Long getModuleId() {
		return moduleId;
	}
	/**
	 * 设置：模块名称
	 */
	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}
	/**
	 * 获取：模块名称
	 */
	public String getModuleName() {
		return moduleName;
	}
	/**
	 * 设置：游戏ID
	 */
	public void setGameId(Long gameId) {
		this.gameId = gameId;
	}
	/**
	 * 获取：游戏ID
	 */
	public Long getGameId() {
		return gameId;
	}
	/**
	 * 设置：玩法名称
	 */
	public void setGameName(String gameName) {
		this.gameName = gameName;
	}
	/**
	 * 获取：玩法名称
	 */
	public String getGameName() {
		return gameName;
	}
	/**
	 * 设置：注单状态
	 */
	public void setOrderStatus(Long orderStatus) {
		this.orderStatus = orderStatus;
	}
	/**
	 * 获取：注单状态
	 */
	public Long getOrderStatus() {
		return orderStatus;
	}
	/**
	 * 设置：下注额
	 */
	public void setBAmount(BigDecimal bAmount) {
		this.bAmount = bAmount;
	}
	/**
	 * 获取：下注额
	 */
	public BigDecimal getBAmount() {
		return bAmount;
	}
	/**
	 * 设置：结算额
	 */
	public void setAAmount(BigDecimal aAmount) {
		this.aAmount = aAmount;
	}
	/**
	 * 获取：结算额
	 */
	public BigDecimal getAAmount() {
		return aAmount;
	}
	/**
	 * 设置：订单来源
	 */
	public void setOrderFrom(Long orderFrom) {
		this.orderFrom = orderFrom;
	}
	/**
	 * 获取：订单来源
	 */
	public Long getOrderFrom() {
		return orderFrom;
	}
	/**
	 * 设置：订单时间
	 */
	public void setOrderTime(Date orderTime) {
		this.orderTime = orderTime;
	}
	/**
	 * 获取：订单时间
	 */
	public Date getOrderTime() {
		return orderTime;
	}
	/**
	 * 设置：最后修改时间
	 */
	public void setLastUpdateTime(Date lastUpdateTime) {
		this.lastUpdateTime = lastUpdateTime;
	}
	/**
	 * 获取：最后修改时间
	 */
	public Date getLastUpdateTime() {
		return lastUpdateTime;
	}
	/**
	 * 设置：下注来源IP
	 */
	public void setFromIp(String fromIp) {
		this.fromIp = fromIp;
	}
	/**
	 * 获取：下注来源IP
	 */
	public String getFromIp() {
		return fromIp;
	}
	/**
	 * 设置：下注来源IP归属地
	 */
	public void setFromIpAddr(String fromIpAddr) {
		this.fromIpAddr = fromIpAddr;
	}
	/**
	 * 获取：下注来源IP归属地
	 */
	public String getFromIpAddr() {
		return fromIpAddr;
	}
	/**
	 * 设置：下注期数
	 */
	public void setIssueId(String issueId) {
		this.issueId = issueId;
	}
	/**
	 * 获取：下注期数
	 */
	public String getIssueId() {
		return issueId;
	}
	/**
	 * 设置：玩法ID
	 */
	public void setPlayId(String playId) {
		this.playId = playId;
	}
	/**
	 * 获取：玩法ID
	 */
	public String getPlayId() {
		return playId;
	}
	/**
	 * 设置：玩法名称
	 */
	public void setPlayName(String playName) {
		this.playName = playName;
	}
	/**
	 * 获取：玩法名称
	 */
	public String getPlayName() {
		return playName;
	}
	/**
	 * 设置：玩法名称(En)
	 */
	public void setPlayNameEn(String playNameEn) {
		this.playNameEn = playNameEn;
	}
	/**
	 * 获取：玩法名称(En)
	 */
	public String getPlayNameEn() {
		return playNameEn;
	}
	/**
	 * 设置：打码量
	 */
	public void setValidBet(Float validBet) {
		this.validBet = validBet;
	}
	/**
	 * 获取：打码量
	 */
	public Float getValidBet() {
		return validBet;
	}
	/**
	 * 设置：派彩(输赢)
	 */
	public void setPayment(Float payment) {
		this.payment = payment;
	}
	/**
	 * 获取：派彩(输赢)
	 */
	public Float getPayment() {
		return payment;
	}

	public BigDecimal getbAmount() {
		return bAmount;
	}

	public void setbAmount(BigDecimal bAmount) {
		this.bAmount = bAmount;
	}

	public BigDecimal getaAmount() {
		return aAmount;
	}

	public void setaAmount(BigDecimal aAmount) {
		this.aAmount = aAmount;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getDetailUrl() {
		return detailUrl;
	}

	public void setDetailUrl(String detailUrl) {
		this.detailUrl = detailUrl;
	}
}
