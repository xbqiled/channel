package com.channel.api.entity.cac;

import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;

/**
 * VIEW
 * 
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-12-06 18:42:05
 */
@TableName("v_promoterbindurl")
public class BindUrlEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	private String tUrl;

	private String tAccountid;

	private String tChannel;

	/**
	 * 设置：
	 */
	public void setTUrl(String tUrl) {
		this.tUrl = tUrl;
	}
	/**
	 * 获取：
	 */
	public String getTUrl() {
		return tUrl;
	}
	/**
	 * 设置：
	 */
	public void setTAccountid(String tAccountid) {
		this.tAccountid = tAccountid;
	}
	/**
	 * 获取：
	 */
	public String getTAccountid() {
		return tAccountid;
	}

	public String gettUrl() {
		return tUrl;
	}

	public void settUrl(String tUrl) {
		this.tUrl = tUrl;
	}


	public String gettChannel() {
		return tChannel;
	}

	public void settChannel(String tChannel) {
		this.tChannel = tChannel;
	}
}
