package com.channel.api.entity.cac;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;
import java.util.Date;

/**
 * VIEW
 * 
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-05-21 15:14:50
 */
@TableName("V_onlinePlayer")
public class OnlineplayerEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 玩家id
	 */
	@TableId
	private Integer tAccountid;
	/**
	 * 游戏ID
	 */
	private Integer tGameid;
	/**
	 * 昵称
	 */
	private String tNickname;
	/**
	 * 进入时间
	 */
	private Integer tEntertime;
	/**
	 * 当前金币
	 */
	private Integer tCoin;
	/**
	 * 渠道
	 */
	private String tChannel;

	/**
	 * 设置：玩家id
	 */
	public void setTAccountid(Integer tAccountid) {
		this.tAccountid = tAccountid;
	}
	/**
	 * 获取：玩家id
	 */
	public Integer getTAccountid() {
		return tAccountid;
	}
	/**
	 * 设置：游戏ID
	 */
	public void setTGameid(Integer tGameid) {
		this.tGameid = tGameid;
	}
	/**
	 * 获取：游戏ID
	 */
	public Integer getTGameid() {
		return tGameid;
	}
	/**
	 * 设置：昵称
	 */
	public void setTNickname(String tNickname) {
		this.tNickname = tNickname;
	}
	/**
	 * 获取：昵称
	 */
	public String getTNickname() {
		return tNickname;
	}
	/**
	 * 设置：进入时间
	 */
	public void setTEntertime(Integer tEntertime) {
		this.tEntertime = tEntertime;
	}
	/**
	 * 获取：进入时间
	 */
	public Integer getTEntertime() {
		return tEntertime;
	}
	/**
	 * 设置：当前金币
	 */
	public void setTCoin(Integer tCoin) {
		this.tCoin = tCoin;
	}
	/**
	 * 获取：当前金币
	 */
	public Integer getTCoin() {
		return tCoin;
	}
	/**
	 * 设置：渠道
	 */
	public void setTChannel(String tChannel) {
		this.tChannel = tChannel;
	}
	/**
	 * 获取：渠道
	 */
	public String getTChannel() {
		return tChannel;
	}
}
