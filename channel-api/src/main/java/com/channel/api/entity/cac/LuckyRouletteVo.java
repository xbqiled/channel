package com.channel.api.entity.cac;

import java.io.Serializable;



public class LuckyRouletteVo implements Serializable {

    private String channelId;

    private byte[] silverCfg;

    private Long silverCost;

    private byte[] goldCfg;

    private Long goldCost;

    private byte[] diamondCfg;

    private Long diamondCost;

    private Integer depositType;

    private Boolean openType;

    private Long start;

    private Long finish;

    private String startTimeStr;

    private String finishTimeStr;

    private String channelName;

    private String iconUrl;


    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public byte[] getSilverCfg() {
        return silverCfg;
    }

    public void setSilverCfg(byte[] silverCfg) {
        this.silverCfg = silverCfg;
    }

    public Long getSilverCost() {
        return silverCost;
    }

    public void setSilverCost(Long silverCost) {
        this.silverCost = silverCost;
    }

    public byte[] getGoldCfg() {
        return goldCfg;
    }

    public void setGoldCfg(byte[] goldCfg) {
        this.goldCfg = goldCfg;
    }

    public Long getGoldCost() {
        return goldCost;
    }

    public void setGoldCost(Long goldCost) {
        this.goldCost = goldCost;
    }

    public byte[] getDiamondCfg() {
        return diamondCfg;
    }

    public void setDiamondCfg(byte[] diamondCfg) {
        this.diamondCfg = diamondCfg;
    }

    public Long getDiamondCost() {
        return diamondCost;
    }

    public void setDiamondCost(Long diamondCost) {
        this.diamondCost = diamondCost;
    }

    public Integer getDepositType() {
        return depositType;
    }

    public void setDepositType(Integer depositType) {
        this.depositType = depositType;
    }

    public Boolean getOpenType() {
        return openType;
    }

    public void setOpenType(Boolean openType) {
        this.openType = openType;
    }

    public Long getStart() {
        return start;
    }

    public void setStart(Long start) {
        this.start = start;
    }

    public Long getFinish() {
        return finish;
    }

    public void setFinish(Long finish) {
        this.finish = finish;
    }

    public String getStartTimeStr() {
        return startTimeStr;
    }

    public void setStartTimeStr(String startTimeStr) {
        this.startTimeStr = startTimeStr;
    }

    public String getFinishTimeStr() {
        return finishTimeStr;
    }

    public void setFinishTimeStr(String finishTimeStr) {
        this.finishTimeStr = finishTimeStr;
    }

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    public String getIconUrl() {
        return iconUrl;
    }

    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }
}
