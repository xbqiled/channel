package com.channel.api.entity.cac;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;
import java.util.Date;

/**
 * VIEW
 *
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-04-22 16:07:30
 */
@TableName("V_luckyRoulette")
public class LuckyrouletteEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 渠道ID
     */
    @TableId
    private String tChannel;
    /**
     * 白银转盘数据
     */
    private byte[] tSilvercfg;
    /**
     * 白银转盘单次抽奖消耗金币
     */
    private Long tSilvercost;
    /**
     * 黄金转盘数据
     */
    private byte[] tGoldcfg;
    /**
     * 黄金转盘单次抽奖消耗金币
     */
    private Long tGoldcost;
    /**
     * 钻石转盘数据
     */
    private byte[] tDiamondcfg;
    /**
     * 钻石转盘单次抽奖消耗金币
     */
    private Long tDiamondcost;
    /**
     *
     */
    private Integer tOpentype;
    /**
     * 活动开始时间
     */
    private Long tStart;
    /**
     * 活动结束时间
     */
    private Long tFinish;

    /**
     * 设置：渠道ID
     */
    public void setTChannel(String tChannel) {
        this.tChannel = tChannel;
    }

    /**
     * 获取：渠道ID
     */
    public String getTChannel() {
        return tChannel;
    }

    /**
     * 设置：白银转盘数据
     */
    public void setTSilvercfg(byte[] tSilvercfg) {
        this.tSilvercfg = tSilvercfg;
    }

    /**
     * 获取：白银转盘数据
     */
    public byte[] getTSilvercfg() {
        return tSilvercfg;
    }

    /**
     * 设置：白银转盘单次抽奖消耗金币
     */
    public void setTSilvercost(Long tSilvercost) {
        this.tSilvercost = tSilvercost;
    }

    /**
     * 获取：白银转盘单次抽奖消耗金币
     */
    public Long getTSilvercost() {
        return tSilvercost;
    }

    /**
     * 设置：黄金转盘数据
     */
    public void setTGoldcfg(byte[] tGoldcfg) {
        this.tGoldcfg = tGoldcfg;
    }

    /**
     * 获取：黄金转盘数据
     */
    public byte[] getTGoldcfg() {
        return tGoldcfg;
    }

    /**
     * 设置：黄金转盘单次抽奖消耗金币
     */
    public void setTGoldcost(Long tGoldcost) {
        this.tGoldcost = tGoldcost;
    }

    /**
     * 获取：黄金转盘单次抽奖消耗金币
     */
    public Long getTGoldcost() {
        return tGoldcost;
    }

    /**
     * 设置：钻石转盘数据
     */
    public void setTDiamondcfg(byte[] tDiamondcfg) {
        this.tDiamondcfg = tDiamondcfg;
    }

    /**
     * 获取：钻石转盘数据
     */
    public byte[] getTDiamondcfg() {
        return tDiamondcfg;
    }

    /**
     * 设置：钻石转盘单次抽奖消耗金币
     */
    public void setTDiamondcost(Long tDiamondcost) {
        this.tDiamondcost = tDiamondcost;
    }

    /**
     * 获取：钻石转盘单次抽奖消耗金币
     */
    public Long getTDiamondcost() {
        return tDiamondcost;
    }

    /**
     * 设置：活动开始时间
     */
    public void setTStart(Long tStart) {
        this.tStart = tStart;
    }

    /**
     * 获取：活动开始时间
     */
    public Long getTStart() {
        return tStart;
    }

    /**
     * 设置：活动结束时间
     */
    public void setTFinish(Long tFinish) {
        this.tFinish = tFinish;
    }

    /**
     * 获取：活动结束时间
     */
    public Long getTFinish() {
        return tFinish;
    }

    public String gettChannel() {
        return tChannel;
    }

    public void settChannel(String tChannel) {
        this.tChannel = tChannel;
    }

    public byte[] gettSilvercfg() {
        return tSilvercfg;
    }

    public void settSilvercfg(byte[] tSilvercfg) {
        this.tSilvercfg = tSilvercfg;
    }

    public Long gettSilvercost() {
        return tSilvercost;
    }

    public void settSilvercost(Long tSilvercost) {
        this.tSilvercost = tSilvercost;
    }

    public byte[] gettGoldcfg() {
        return tGoldcfg;
    }

    public void settGoldcfg(byte[] tGoldcfg) {
        this.tGoldcfg = tGoldcfg;
    }

    public Long gettGoldcost() {
        return tGoldcost;
    }

    public void settGoldcost(Long tGoldcost) {
        this.tGoldcost = tGoldcost;
    }

    public byte[] gettDiamondcfg() {
        return tDiamondcfg;
    }

    public void settDiamondcfg(byte[] tDiamondcfg) {
        this.tDiamondcfg = tDiamondcfg;
    }

    public Long gettDiamondcost() {
        return tDiamondcost;
    }

    public void settDiamondcost(Long tDiamondcost) {
        this.tDiamondcost = tDiamondcost;
    }

    public Integer gettOpentype() {
        return tOpentype;
    }

    public void settOpentype(Integer tOpentype) {
        this.tOpentype = tOpentype;
    }
}
