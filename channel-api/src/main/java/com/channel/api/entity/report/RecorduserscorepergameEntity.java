package com.channel.api.entity.report;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

import java.math.BigDecimal;
import java.io.Serializable;
import java.util.Date;

/**
 * VIEW
 * 
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-09-21 11:05:04
 */
@TableName("v_recorduserscorepergame")
public class RecorduserscorepergameEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private Integer id;
	/**
	 * 
	 */
	private String recordid;
	/**
	 * 
	 */
	private Integer userid;
	/**
	 * 
	 */
	private Integer basicscore;
	/**
	 * 
	 */
	private Integer topscore;
	/**
	 * 
	 */
	private Integer tableid;
	/**
	 * 
	 */
	private Integer chairid;
	/**
	 * 
	 */
	private Integer beforescore;
	/**
	 * 
	 */
	private Integer score;
	/**
	 * 
	 */
	private Integer afterscore;
	/**
	 * 
	 */
	private BigDecimal revenue;
	/**
	 * 
	 */
	private BigDecimal stake;
	/**
	 * 
	 */
	private Integer istrust;
	/**
	 * 
	 */
	private Integer ispunish;
	/**
	 * 
	 */
	private Integer player;
	/**
	 * 
	 */
	private Integer settlementtype;
	/**
	 * 
	 */
	private Integer starttime;
	/**
	 * 
	 */
	private Integer endtime;
	/**
	 * 
	 */
	private Integer gametypeid;
	/**
	 * 
	 */
	private String matchguid;
	/**
	 * 
	 */
	private String keyguid;
	/**
	 * 
	 */
	private String businessguid;
	/**
	 * 
	 */
	private Integer terminaltype;
	/**
	 * 
	 */
	private String channelno;

	/**
	 * 设置：
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * 设置：
	 */
	public void setRecordid(String recordid) {
		this.recordid = recordid;
	}
	/**
	 * 获取：
	 */
	public String getRecordid() {
		return recordid;
	}
	/**
	 * 设置：
	 */
	public void setUserid(Integer userid) {
		this.userid = userid;
	}
	/**
	 * 获取：
	 */
	public Integer getUserid() {
		return userid;
	}
	/**
	 * 设置：
	 */
	public void setBasicscore(Integer basicscore) {
		this.basicscore = basicscore;
	}
	/**
	 * 获取：
	 */
	public Integer getBasicscore() {
		return basicscore;
	}
	/**
	 * 设置：
	 */
	public void setTopscore(Integer topscore) {
		this.topscore = topscore;
	}
	/**
	 * 获取：
	 */
	public Integer getTopscore() {
		return topscore;
	}
	/**
	 * 设置：
	 */
	public void setTableid(Integer tableid) {
		this.tableid = tableid;
	}
	/**
	 * 获取：
	 */
	public Integer getTableid() {
		return tableid;
	}
	/**
	 * 设置：
	 */
	public void setChairid(Integer chairid) {
		this.chairid = chairid;
	}
	/**
	 * 获取：
	 */
	public Integer getChairid() {
		return chairid;
	}
	/**
	 * 设置：
	 */
	public void setBeforescore(Integer beforescore) {
		this.beforescore = beforescore;
	}
	/**
	 * 获取：
	 */
	public Integer getBeforescore() {
		return beforescore;
	}
	/**
	 * 设置：
	 */
	public void setScore(Integer score) {
		this.score = score;
	}
	/**
	 * 获取：
	 */
	public Integer getScore() {
		return score;
	}
	/**
	 * 设置：
	 */
	public void setAfterscore(Integer afterscore) {
		this.afterscore = afterscore;
	}
	/**
	 * 获取：
	 */
	public Integer getAfterscore() {
		return afterscore;
	}
	/**
	 * 设置：
	 */
	public void setRevenue(BigDecimal revenue) {
		this.revenue = revenue;
	}
	/**
	 * 获取：
	 */
	public BigDecimal getRevenue() {
		return revenue;
	}
	/**
	 * 设置：
	 */
	public void setStake(BigDecimal stake) {
		this.stake = stake;
	}
	/**
	 * 获取：
	 */
	public BigDecimal getStake() {
		return stake;
	}
	/**
	 * 设置：
	 */
	public void setIstrust(Integer istrust) {
		this.istrust = istrust;
	}
	/**
	 * 获取：
	 */
	public Integer getIstrust() {
		return istrust;
	}
	/**
	 * 设置：
	 */
	public void setIspunish(Integer ispunish) {
		this.ispunish = ispunish;
	}
	/**
	 * 获取：
	 */
	public Integer getIspunish() {
		return ispunish;
	}
	/**
	 * 设置：
	 */
	public void setPlayer(Integer player) {
		this.player = player;
	}
	/**
	 * 获取：
	 */
	public Integer getPlayer() {
		return player;
	}
	/**
	 * 设置：
	 */
	public void setSettlementtype(Integer settlementtype) {
		this.settlementtype = settlementtype;
	}
	/**
	 * 获取：
	 */
	public Integer getSettlementtype() {
		return settlementtype;
	}
	/**
	 * 设置：
	 */
	public void setStarttime(Integer starttime) {
		this.starttime = starttime;
	}
	/**
	 * 获取：
	 */
	public Integer getStarttime() {
		return starttime;
	}
	/**
	 * 设置：
	 */
	public void setEndtime(Integer endtime) {
		this.endtime = endtime;
	}
	/**
	 * 获取：
	 */
	public Integer getEndtime() {
		return endtime;
	}
	/**
	 * 设置：
	 */
	public void setGametypeid(Integer gametypeid) {
		this.gametypeid = gametypeid;
	}
	/**
	 * 获取：
	 */
	public Integer getGametypeid() {
		return gametypeid;
	}
	/**
	 * 设置：
	 */
	public void setMatchguid(String matchguid) {
		this.matchguid = matchguid;
	}
	/**
	 * 获取：
	 */
	public String getMatchguid() {
		return matchguid;
	}
	/**
	 * 设置：
	 */
	public void setKeyguid(String keyguid) {
		this.keyguid = keyguid;
	}
	/**
	 * 获取：
	 */
	public String getKeyguid() {
		return keyguid;
	}
	/**
	 * 设置：
	 */
	public void setBusinessguid(String businessguid) {
		this.businessguid = businessguid;
	}
	/**
	 * 获取：
	 */
	public String getBusinessguid() {
		return businessguid;
	}
	/**
	 * 设置：
	 */
	public void setTerminaltype(Integer terminaltype) {
		this.terminaltype = terminaltype;
	}
	/**
	 * 获取：
	 */
	public Integer getTerminaltype() {
		return terminaltype;
	}
	/**
	 * 设置：
	 */
	public void setChannelno(String channelno) {
		this.channelno = channelno;
	}
	/**
	 * 获取：
	 */
	public String getChannelno() {
		return channelno;
	}
}
