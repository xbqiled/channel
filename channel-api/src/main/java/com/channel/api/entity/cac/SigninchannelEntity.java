package com.channel.api.entity.cac;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;


/**
 * VIEW
 *
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-05-28 14:35:42
 */
@TableName("V_signInChannel")
public class SigninchannelEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    @TableId
    private String tChannel;
    /**
     *
     */
    private byte[] tCfginfo;
    /**
     *
     */
    private Long tStarttime;
    /**
     *
     */
    private Long tEndtime;
    /**
     *
     */
    private Integer tIsCirculate;

    /**
     *
     */
    private Integer tOpenType;


    public Integer gettOpenType() {
        return tOpenType;
    }

    public void settOpenType(Integer tOpenType) {
        this.tOpenType = tOpenType;
    }

    public String gettChannel() {
        return tChannel;
    }

    public void settChannel(String tChannel) {
        this.tChannel = tChannel;
    }

    public byte[] gettCfginfo() {
        return tCfginfo;
    }

    public void settCfginfo(byte[] tCfginfo) {
        this.tCfginfo = tCfginfo;
    }

    public Long gettStarttime() {
        return tStarttime;
    }

    public void settStarttime(Long tStarttime) {
        this.tStarttime = tStarttime;
    }

    public Long gettEndtime() {
        return tEndtime;
    }

    public void settEndtime(Long tEndtime) {
        this.tEndtime = tEndtime;
    }

    public Integer gettIsCirculate() {
        return tIsCirculate;
    }

    public void settIsCirculate(Integer tIsCirculate) {
        this.tIsCirculate = tIsCirculate;
    }
}
