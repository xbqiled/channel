package com.channel.api.entity.cac;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;
import java.util.Date;

/**
 * VIEW
 * 
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-07-31 12:40:41
 */
@TableName("V_invite_Deposit")
public class InviteDepositEntity implements Serializable {
	private static final long serialVersionUID = 1L;
	/**
	 * 
	 */
	@TableId
	private String tChannel;
	/**
	 * 
	 */
	private Integer tOpentype;
	/**
	 * 
	 */
	private Integer tTimetype;
	/**
	 * 
	 */
	private Integer tBegintime;
	/**
	 * 
	 */
	private Integer tEndtime;
	/**
	 * 
	 */
	private Integer tFirsttotaldeposit;
	/**
	 * 
	 */
	private Integer tSelfreward;
	/**
	 * 
	 */
	private Integer tUpperreward;
	/**
	 * 
	 */
	private Integer tCfgtime;

	/**
	 * 设置：
	 */
	public void setTChannel(String tChannel) {
		this.tChannel = tChannel;
	}
	/**
	 * 获取：
	 */
	public String getTChannel() {
		return tChannel;
	}
	/**
	 * 设置：
	 */
	public void setTOpentype(Integer tOpentype) {
		this.tOpentype = tOpentype;
	}
	/**
	 * 获取：
	 */
	public Integer getTOpentype() {
		return tOpentype;
	}
	/**
	 * 设置：
	 */
	public void setTTimetype(Integer tTimetype) {
		this.tTimetype = tTimetype;
	}
	/**
	 * 获取：
	 */
	public Integer getTTimetype() {
		return tTimetype;
	}
	/**
	 * 设置：
	 */
	public void setTBegintime(Integer tBegintime) {
		this.tBegintime = tBegintime;
	}
	/**
	 * 获取：
	 */
	public Integer getTBegintime() {
		return tBegintime;
	}
	/**
	 * 设置：
	 */
	public void setTEndtime(Integer tEndtime) {
		this.tEndtime = tEndtime;
	}
	/**
	 * 获取：
	 */
	public Integer getTEndtime() {
		return tEndtime;
	}
	/**
	 * 设置：
	 */
	public void setTFirsttotaldeposit(Integer tFirsttotaldeposit) {
		this.tFirsttotaldeposit = tFirsttotaldeposit;
	}
	/**
	 * 获取：
	 */
	public Integer getTFirsttotaldeposit() {
		return tFirsttotaldeposit;
	}
	/**
	 * 设置：
	 */
	public void setTSelfreward(Integer tSelfreward) {
		this.tSelfreward = tSelfreward;
	}
	/**
	 * 获取：
	 */
	public Integer getTSelfreward() {
		return tSelfreward;
	}
	/**
	 * 设置：
	 */
	public void setTUpperreward(Integer tUpperreward) {
		this.tUpperreward = tUpperreward;
	}
	/**
	 * 获取：
	 */
	public Integer getTUpperreward() {
		return tUpperreward;
	}
	/**
	 * 设置：
	 */
	public void setTCfgtime(Integer tCfgtime) {
		this.tCfgtime = tCfgtime;
	}
	/**
	 * 获取：
	 */
	public Integer getTCfgtime() {
		return tCfgtime;
	}
}
