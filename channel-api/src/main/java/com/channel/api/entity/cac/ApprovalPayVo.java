package com.channel.api.entity.cac;


import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;


public class ApprovalPayVo implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer id;
    /**
     * 订单号
     */
    private String orderNo;
    /**
     * 用户ID
     */
    private Integer userId;
    /**
     * 充值金额
     */
    private BigDecimal payFee;
    /**
     * 手续费
     */
    private BigDecimal poundage;
    /**
     * 渠道ID
     */
    private String channelId;
    /**
     * 状态，0:等待支付, 1,支付成功, 2,支付失败
     */
    private Integer status;
    /**
     * 支付方式编码
     */
    private Integer payConfigId;
    /**
     * 支付方式名称
     */
    private String payName;
    /**
     * 姓名
     */
    private String depositor;
    /**
     * 用户账号
     */
    private String account;
    /**
     * 处理方式（1、 手动处理,2、系统处理）
     */
    private Integer handlerType;
    /**
     *
     */
    private LocalDateTime createTime;

    /**
     * 创建时间
     */
    private String createTimeStr;
    /**
     * 如果系统处理，那么记录SYSTME, 手动处理记录编码
     */
    private String createBy;
    /**
     *
     */
    private LocalDateTime modifyTime;

    /**
     * 修改时间
     */
    private String modifyTimeStr;
    /**
     * 如果系统处理，那么记录SYSTME, 手动处理记录编码
     */
    private String modifyBy;
    /**
     * 备注
     */
    private String note;


    private Integer returnRate;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public BigDecimal getPayFee() {
        return payFee;
    }

    public void setPayFee(BigDecimal payFee) {
        this.payFee = payFee;
    }

    public BigDecimal getPoundage() {
        return poundage;
    }

    public void setPoundage(BigDecimal poundage) {
        this.poundage = poundage;
    }

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getPayConfigId() {
        return payConfigId;
    }

    public void setPayConfigId(Integer payConfigId) {
        this.payConfigId = payConfigId;
    }

    public String getPayName() {
        return payName;
    }

    public void setPayName(String payName) {
        this.payName = payName;
    }

    public String getDepositor() {
        return depositor;
    }

    public void setDepositor(String depositor) {
        this.depositor = depositor;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public Integer getHandlerType() {
        return handlerType;
    }

    public void setHandlerType(Integer handlerType) {
        this.handlerType = handlerType;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public String getCreateTimeStr() {
        return createTimeStr;
    }

    public void setCreateTimeStr(String createTimeStr) {
        this.createTimeStr = createTimeStr;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public LocalDateTime getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(LocalDateTime modifyTime) {
        this.modifyTime = modifyTime;
    }

    public String getModifyTimeStr() {
        return modifyTimeStr;
    }

    public void setModifyTimeStr(String modifyTimeStr) {
        this.modifyTimeStr = modifyTimeStr;
    }

    public String getModifyBy() {
        return modifyBy;
    }

    public void setModifyBy(String modifyBy) {
        this.modifyBy = modifyBy;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Integer getReturnRate() {
        return returnRate;
    }

    public void setReturnRate(Integer returnRate) {
        this.returnRate = returnRate;
    }
}
