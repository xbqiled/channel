package com.channel.api.entity.cac;

import java.io.Serializable;



public class CancelResultVo implements Serializable {

    private int ret;
    private String msg;
    private Long realCancel;
    private String orderId;

    public CancelResultVo(int ret, String msg, Long realCancel, String orderId) {
        this.ret = ret;
        this.msg = msg;
        this.realCancel = realCancel;
        this.orderId = orderId;
    }


    public CancelResultVo(int ret) {
        this.ret = ret;
    }


    public int getRet() {
        return ret;
    }

    public void setRet(int ret) {
        this.ret = ret;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Long getRealCancel() {
        return realCancel;
    }

    public void setRealCancel(Long realCancel) {
        this.realCancel = realCancel;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }
}

