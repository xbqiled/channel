package com.channel.api.entity.report;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;
import java.util.Date;

/**
 * VIEW
 * 
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-09-17 15:12:03
 */
@TableName("v_matchplayerrank")
public class PlayerrankEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private Integer id;
	/**
	 * 
	 */
	private Integer userid;
	/**
	 * 
	 */
	private String matches;
	/**
	 * 
	 */
	private Integer matchrank;
	/**
	 * GameTypeID
	 */
	private Integer matchpoint;
	/**
	 * 
	 */
	private Integer boardamount;
	/**
	 * 
	 */
	private Integer starttime;
	/**
	 * 
	 */
	private Integer endtime;
	/**
	 * 
	 */
	private Integer gametypeid;
	/**
	 * 
	 */
	private String matchguid;
	/**
	 * 
	 */
	private String businessguid;
	/**
	 * 
	 */
	private String channelno;

	/**
	 *
	 */
	private Long winscore;

	/**
	 * 设置：
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * 设置：
	 */
	public void setUserid(Integer userid) {
		this.userid = userid;
	}
	/**
	 * 获取：
	 */
	public Integer getUserid() {
		return userid;
	}
	/**
	 * 设置：
	 */
	public void setMatches(String matches) {
		this.matches = matches;
	}
	/**
	 * 获取：
	 */
	public String getMatches() {
		return matches;
	}
	/**
	 * 设置：
	 */
	public void setMatchrank(Integer matchrank) {
		this.matchrank = matchrank;
	}
	/**
	 * 获取：
	 */
	public Integer getMatchrank() {
		return matchrank;
	}
	/**
	 * 设置：
	 */
	public void setMatchpoint(Integer matchpoint) {
		this.matchpoint = matchpoint;
	}
	/**
	 * 获取：
	 */
	public Integer getMatchpoint() {
		return matchpoint;
	}
	/**
	 * 设置：
	 */
	public void setBoardamount(Integer boardamount) {
		this.boardamount = boardamount;
	}
	/**
	 * 获取：
	 */
	public Integer getBoardamount() {
		return boardamount;
	}
	/**
	 * 设置：
	 */
	public void setStarttime(Integer starttime) {
		this.starttime = starttime;
	}
	/**
	 * 获取：
	 */
	public Integer getStarttime() {
		return starttime;
	}
	/**
	 * 设置：
	 */
	public void setEndtime(Integer endtime) {
		this.endtime = endtime;
	}
	/**
	 * 获取：
	 */
	public Integer getEndtime() {
		return endtime;
	}
	/**
	 * 设置：
	 */
	public void setGametypeid(Integer gametypeid) {
		this.gametypeid = gametypeid;
	}
	/**
	 * 获取：
	 */
	public Integer getGametypeid() {
		return gametypeid;
	}
	/**
	 * 设置：
	 */
	public void setMatchguid(String matchguid) {
		this.matchguid = matchguid;
	}
	/**
	 * 获取：
	 */
	public String getMatchguid() {
		return matchguid;
	}
	/**
	 * 设置：
	 */
	public void setBusinessguid(String businessguid) {
		this.businessguid = businessguid;
	}
	/**
	 * 获取：
	 */
	public String getBusinessguid() {
		return businessguid;
	}
	/**
	 * 设置：
	 */
	public void setChannelno(String channelno) {
		this.channelno = channelno;
	}
	/**
	 * 获取：
	 */
	public String getChannelno() {
		return channelno;
	}


	public Long getWinscore() {
		return winscore;
	}

	public void setWinscore(Long winscore) {
		this.winscore = winscore;
	}
}
