package com.channel.api.entity.cac;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;

/**
 * 
 * 
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-09-20 15:21:18
 */
@TableName("t_user_ext")
public class UserExtEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键
	 */
	@TableId
	private Integer id;
	/**
	 * 用户账号ID
	 */
	private Integer accountid;
	/**
	 * 备注
	 */
	private String note;
	/**
	 * 备用1
	 */
	private String ext1;
	/**
	 * 备用2
	 */
	private String ext2;
	/**
	 * 备用2
	 */
	private String ext3;

	/**
	 * 设置：主键
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * 获取：主键
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * 设置：用户账号ID
	 */
	public void setAccountid(Integer accountid) {
		this.accountid = accountid;
	}
	/**
	 * 获取：用户账号ID
	 */
	public Integer getAccountid() {
		return accountid;
	}
	/**
	 * 设置：备注
	 */
	public void setNote(String note) {
		this.note = note;
	}
	/**
	 * 获取：备注
	 */
	public String getNote() {
		return note;
	}
	/**
	 * 设置：备用1
	 */
	public void setExt1(String ext1) {
		this.ext1 = ext1;
	}
	/**
	 * 获取：备用1
	 */
	public String getExt1() {
		return ext1;
	}
	/**
	 * 设置：备用2
	 */
	public void setExt2(String ext2) {
		this.ext2 = ext2;
	}
	/**
	 * 获取：备用2
	 */
	public String getExt2() {
		return ext2;
	}
	/**
	 * 设置：备用2
	 */
	public void setExt3(String ext3) {
		this.ext3 = ext3;
	}
	/**
	 * 获取：备用2
	 */
	public String getExt3() {
		return ext3;
	}
}
