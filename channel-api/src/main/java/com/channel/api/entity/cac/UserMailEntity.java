package com.channel.api.entity.cac;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;

/**
 * 
 * 
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-03-07 13:39:08
 */
@TableName("V_userMail")
public class UserMailEntity implements Serializable {
	private static final long serialVersionUID = 1L;


	/**
	 * 邮件ID
	 */
	@TableId
	private Integer tId;
	/**
	 * 用户ID
	 */
	private Integer tAccountid;
	/**
	 * 邮件标题
	 */
	private String tTitle;
	/**
	 * 邮件内容
	 */
	private  byte[] tContent;
	/**
	 * 邮件附件奖励列表
	 */
	private String tRewardlist;
	/**
	 * 发送人
	 */
	private String tSender;
	/**
	 * 邮件状态 1-未读 2-已读 3-领取
	 */
	private Integer tState;
	/**
	 * 是否文本邮件 0-否 1-是
	 */
	private Integer tIstextmail;
	/**
	 * 是否群发邮件 0-否 1-是
	 */
	private Integer tIsgroupmail;
	/**
	 * 是否模板邮件 0-否 1-是
	 */
	private Integer tIstemplatemail;
	/**
	 * 模板邮件参数
	 */
	private String tTemplatemailinfo;
	/**
	 * 发送日期
	 */
	private Integer tSenddate;
	/**
	 * 接收日期
	 */
	private Integer tRecvdate;
	/**
	 * 触发邮件游戏类型
	 */
	private Integer tTriggergametype;
	/**
	 * 业务类型
	 */
	private Integer tBusinesstype;
	/**
	 * 业务关联标识
	 */
	private String tBusinessguid;

	/**
	 * 设置：邮件ID
	 */
	public void setTId(Integer tId) {
		this.tId = tId;
	}
	/**
	 * 获取：邮件ID
	 */
	public Integer getTId() {
		return tId;
	}
	/**
	 * 设置：用户ID
	 */
	public void setTAccountid(Integer tAccountid) {
		this.tAccountid = tAccountid;
	}
	/**
	 * 获取：用户ID
	 */
	public Integer getTAccountid() {
		return tAccountid;
	}
	/**
	 * 设置：邮件标题
	 */
	public void setTTitle(String tTitle) {
		this.tTitle = tTitle;
	}
	/**
	 * 获取：邮件标题
	 */
	public String getTTitle() {
		return tTitle;
	}
	/**
	 * 设置：邮件内容
	 */
	public void setTContent( byte[] tContent) {
		this.tContent = tContent;
	}
	/**
	 * 获取：邮件内容
	 */
	public byte[] getTContent() {
		return tContent;
	}
	/**
	 * 设置：邮件附件奖励列表
	 */
	public void setTRewardlist(String tRewardlist) {
		this.tRewardlist = tRewardlist;
	}
	/**
	 * 获取：邮件附件奖励列表
	 */
	public String getTRewardlist() {
		return tRewardlist;
	}
	/**
	 * 设置：发送人
	 */
	public void setTSender(String tSender) {
		this.tSender = tSender;
	}
	/**
	 * 获取：发送人
	 */
	public String getTSender() {
		return tSender;
	}
	/**
	 * 设置：邮件状态 1-未读 2-已读 3-领取
	 */
	public void setTState(Integer tState) {
		this.tState = tState;
	}
	/**
	 * 获取：邮件状态 1-未读 2-已读 3-领取
	 */
	public Integer getTState() {
		return tState;
	}
	/**
	 * 设置：是否文本邮件 0-否 1-是
	 */
	public void setTIstextmail(Integer tIstextmail) {
		this.tIstextmail = tIstextmail;
	}
	/**
	 * 获取：是否文本邮件 0-否 1-是
	 */
	public Integer getTIstextmail() {
		return tIstextmail;
	}
	/**
	 * 设置：是否群发邮件 0-否 1-是
	 */
	public void setTIsgroupmail(Integer tIsgroupmail) {
		this.tIsgroupmail = tIsgroupmail;
	}
	/**
	 * 获取：是否群发邮件 0-否 1-是
	 */
	public Integer getTIsgroupmail() {
		return tIsgroupmail;
	}
	/**
	 * 设置：是否模板邮件 0-否 1-是
	 */
	public void setTIstemplatemail(Integer tIstemplatemail) {
		this.tIstemplatemail = tIstemplatemail;
	}
	/**
	 * 获取：是否模板邮件 0-否 1-是
	 */
	public Integer getTIstemplatemail() {
		return tIstemplatemail;
	}
	/**
	 * 设置：模板邮件参数
	 */
	public void setTTemplatemailinfo(String tTemplatemailinfo) {
		this.tTemplatemailinfo = tTemplatemailinfo;
	}
	/**
	 * 获取：模板邮件参数
	 */
	public String getTTemplatemailinfo() {
		return tTemplatemailinfo;
	}
	/**
	 * 设置：发送日期
	 */
	public void setTSenddate(Integer tSenddate) {
		this.tSenddate = tSenddate;
	}
	/**
	 * 获取：发送日期
	 */
	public Integer getTSenddate() {
		return tSenddate;
	}
	/**
	 * 设置：接收日期
	 */
	public void setTRecvdate(Integer tRecvdate) {
		this.tRecvdate = tRecvdate;
	}
	/**
	 * 获取：接收日期
	 */
	public Integer getTRecvdate() {
		return tRecvdate;
	}
	/**
	 * 设置：触发邮件游戏类型
	 */
	public void setTTriggergametype(Integer tTriggergametype) {
		this.tTriggergametype = tTriggergametype;
	}
	/**
	 * 获取：触发邮件游戏类型
	 */
	public Integer getTTriggergametype() {
		return tTriggergametype;
	}
	/**
	 * 设置：业务类型
	 */
	public void setTBusinesstype(Integer tBusinesstype) {
		this.tBusinesstype = tBusinesstype;
	}
	/**
	 * 获取：业务类型
	 */
	public Integer getTBusinesstype() {
		return tBusinesstype;
	}
	/**
	 * 设置：业务关联标识
	 */
	public void setTBusinessguid(String tBusinessguid) {
		this.tBusinessguid = tBusinessguid;
	}
	/**
	 * 获取：业务关联标识
	 */
	public String getTBusinessguid() {
		return tBusinessguid;
	}
}
