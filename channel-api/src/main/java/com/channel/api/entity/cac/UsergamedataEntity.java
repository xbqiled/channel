package com.channel.api.entity.cac;



import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;


/**
 * VIEW
 *
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-04-16 16:49:58
 */
@TableName("V_userGameData")
public class UsergamedataEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	@TableId
	private Integer tAccountid;
	/**
	 *
	 */
	private Integer tGameid;
	/**
	 *
	 */
	private Integer tTotalround;
	/**
	 *
	 */
	private Integer tHistwin;
	/**
	 *
	 */
	private Integer tTodaywin;
	/**
	 *
	 */
	private Integer tTodayrsftime;
	/**
	 *
	 */
	private Integer tTotalplaycount;
	/**
	 *
	 */
	private Integer tFirstentertime;
	/**
	 *
	 */
	private Integer tLastentertime;
	/**
	 *
	 */
	private byte[]  tExtend;
	/**
	 *
	 */
	private Integer tType;
	/**
	 *
	 */
	private Integer tSettime;
	/**
	 *
	 */
	private Integer tBetlimit;
	/**
	 *
	 */
	private Integer tRound;
	/**
	 *
	 */
	private Integer tRate;
	/**
	 *
	 */
	private Integer tCtrlround;
	/**
	 *
	 */
	private Integer tCtlscore;
	/**
	 *
	 */
	private Integer tCtltotalscore;
	/**
	 *
	 */
	private Integer tCurscore;
	/**
	 *
	 */
	private String tChannel;

	/**
	 * 设置：
	 */
	public void setTAccountid(Integer tAccountid) {
		this.tAccountid = tAccountid;
	}
	/**
	 * 获取：
	 */
	public Integer getTAccountid() {
		return tAccountid;
	}
	/**
	 * 设置：
	 */
	public void setTGameid(Integer tGameid) {
		this.tGameid = tGameid;
	}
	/**
	 * 获取：
	 */
	public Integer getTGameid() {
		return tGameid;
	}
	/**
	 * 设置：
	 */
	public void setTTotalround(Integer tTotalround) {
		this.tTotalround = tTotalround;
	}
	/**
	 * 获取：
	 */
	public Integer getTTotalround() {
		return tTotalround;
	}
	/**
	 * 设置：
	 */
	public void setTHistwin(Integer tHistwin) {
		this.tHistwin = tHistwin;
	}
	/**
	 * 获取：
	 */
	public Integer getTHistwin() {
		return tHistwin;
	}
	/**
	 * 设置：
	 */
	public void setTTodaywin(Integer tTodaywin) {
		this.tTodaywin = tTodaywin;
	}
	/**
	 * 获取：
	 */
	public Integer getTTodaywin() {
		return tTodaywin;
	}
	/**
	 * 设置：
	 */
	public void setTTodayrsftime(Integer tTodayrsftime) {
		this.tTodayrsftime = tTodayrsftime;
	}
	/**
	 * 获取：
	 */
	public Integer getTTodayrsftime() {
		return tTodayrsftime;
	}
	/**
	 * 设置：
	 */
	public void setTTotalplaycount(Integer tTotalplaycount) {
		this.tTotalplaycount = tTotalplaycount;
	}
	/**
	 * 获取：
	 */
	public Integer getTTotalplaycount() {
		return tTotalplaycount;
	}
	/**
	 * 设置：
	 */
	public void setTFirstentertime(Integer tFirstentertime) {
		this.tFirstentertime = tFirstentertime;
	}
	/**
	 * 获取：
	 */
	public Integer getTFirstentertime() {
		return tFirstentertime;
	}
	/**
	 * 设置：
	 */
	public void setTLastentertime(Integer tLastentertime) {
		this.tLastentertime = tLastentertime;
	}
	/**
	 * 获取：
	 */
	public Integer getTLastentertime() {
		return tLastentertime;
	}
	/**
	 * 设置：
	 */
	public void setTExtend(byte[]  tExtend) {
		this.tExtend = tExtend;
	}
	/**
	 * 获取：
	 */
	public byte[] getTExtend() {
		return tExtend;
	}
	/**
	 * 设置：
	 */
	public void setTType(Integer tType) {
		this.tType = tType;
	}
	/**
	 * 获取：
	 */
	public Integer getTType() {
		return tType;
	}
	/**
	 * 设置：
	 */
	public void setTSettime(Integer tSettime) {
		this.tSettime = tSettime;
	}
	/**
	 * 获取：
	 */
	public Integer getTSettime() {
		return tSettime;
	}
	/**
	 * 设置：
	 */
	public void setTBetlimit(Integer tBetlimit) {
		this.tBetlimit = tBetlimit;
	}
	/**
	 * 获取：
	 */
	public Integer getTBetlimit() {
		return tBetlimit;
	}
	/**
	 * 设置：
	 */
	public void setTRound(Integer tRound) {
		this.tRound = tRound;
	}
	/**
	 * 获取：
	 */
	public Integer getTRound() {
		return tRound;
	}
	/**
	 * 设置：
	 */
	public void setTRate(Integer tRate) {
		this.tRate = tRate;
	}
	/**
	 * 获取：
	 */
	public Integer getTRate() {
		return tRate;
	}
	/**
	 * 设置：
	 */
	public void setTCtrlround(Integer tCtrlround) {
		this.tCtrlround = tCtrlround;
	}
	/**
	 * 获取：
	 */
	public Integer getTCtrlround() {
		return tCtrlround;
	}
	/**
	 * 设置：
	 */
	public void setTCtlscore(Integer tCtlscore) {
		this.tCtlscore = tCtlscore;
	}
	/**
	 * 获取：
	 */
	public Integer getTCtlscore() {
		return tCtlscore;
	}
	/**
	 * 设置：
	 */
	public void setTCtltotalscore(Integer tCtltotalscore) {
		this.tCtltotalscore = tCtltotalscore;
	}
	/**
	 * 获取：
	 */
	public Integer getTCtltotalscore() {
		return tCtltotalscore;
	}
	/**
	 * 设置：
	 */
	public void setTCurscore(Integer tCurscore) {
		this.tCurscore = tCurscore;
	}
	/**
	 * 获取：
	 */
	public Integer getTCurscore() {
		return tCurscore;
	}
	/**
	 * 设置：
	 */
	public void setTChannel(String tChannel) {
		this.tChannel = tChannel;
	}
	/**
	 * 获取：
	 */
	public String getTChannel() {
		return tChannel;
	}
}
