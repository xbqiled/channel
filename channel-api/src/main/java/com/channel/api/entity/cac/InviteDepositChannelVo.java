package com.channel.api.entity.cac;

import java.io.Serializable;



public class InviteDepositChannelVo implements Serializable {
    /**
     *
     */
    private String channelId;
    /**
     *
     */
    private Long timeType;
    /**
     *
     */
    private Long firstTotalDeposit;
    /**
     *
     */
    private String beginTimeStr;
    /**
     *
     */
    private String endTimeStr;
    /**
     *
     */
    private Long beginTime;
    /**
     *
     */
    private Long endTime;
    /**
     *
     */
    private Long selfReward;
    /**
     *
     */
    private Long upperReward;
    /**
     *
     */
    private Long cfgTime;
    /**
     *
     */
    private Boolean openType;
    /**
     *
     */
    private Integer depositType;
    /**
     *
     */
    private String channelName;

    /**
     *
     */
    private String iconUrl;


    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public Long getTimeType() {
        return timeType;
    }

    public void setTimeType(Long timeType) {
        this.timeType = timeType;
    }

    public Long getFirstTotalDeposit() {
        return firstTotalDeposit;
    }

    public void setFirstTotalDeposit(Long firstTotalDeposit) {
        this.firstTotalDeposit = firstTotalDeposit;
    }

    public String getBeginTimeStr() {
        return beginTimeStr;
    }

    public void setBeginTimeStr(String beginTimeStr) {
        this.beginTimeStr = beginTimeStr;
    }

    public String getEndTimeStr() {
        return endTimeStr;
    }

    public void setEndTimeStr(String endTimeStr) {
        this.endTimeStr = endTimeStr;
    }

    public Long getBeginTime() {
        return beginTime;
    }

    public void setBeginTime(Long beginTime) {
        this.beginTime = beginTime;
    }

    public Long getEndTime() {
        return endTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    public Long getSelfReward() {
        return selfReward;
    }

    public void setSelfReward(Long selfReward) {
        this.selfReward = selfReward;
    }

    public Long getUpperReward() {
        return upperReward;
    }

    public void setUpperReward(Long upperReward) {
        this.upperReward = upperReward;
    }

    public Long getCfgTime() {
        return cfgTime;
    }

    public void setCfgTime(Long cfgTime) {
        this.cfgTime = cfgTime;
    }

    public Boolean getOpenType() {
        return openType;
    }

    public void setOpenType(Boolean openType) {
        this.openType = openType;
    }

    public Integer getDepositType() {
        return depositType;
    }

    public void setDepositType(Integer depositType) {
        this.depositType = depositType;
    }

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    public String getIconUrl() {
        return iconUrl;
    }

    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }
}
