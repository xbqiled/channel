package com.channel.api.service.cac;

import com.baomidou.mybatisplus.service.IService;
import com.channel.api.entity.cac.MessageEntity;
import com.channel.api.entity.chl.ChlUserEntity;
import com.channel.common.utils.PageUtils;

import java.util.Map;

/**
 * @author Howard
 * @email howardwong2312@gmail.com
 * @date 2020-02-03 16:01:03
 */
public interface MessageService extends IService<MessageEntity> {

    PageUtils queryPage(Map<String, Object> params,ChlUserEntity entity);

    int send(MessageEntity messageEntity,ChlUserEntity entity);

    MessageEntity queryById(Long id);
}

