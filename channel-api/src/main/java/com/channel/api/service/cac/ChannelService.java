package com.channel.api.service.cac;

import com.baomidou.mybatisplus.service.IService;
import com.channel.api.entity.cac.VideoBalanceVo;
import com.channel.api.entity.cac.ChannelEntity;
import com.channel.api.entity.chl.ChlUserEntity;
import com.channel.common.utils.PageUtils;
import java.util.Map;

/**
 * 
 *
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-03-30 14:16:02
 */
public interface ChannelService extends IService<ChannelEntity> {
    PageUtils queryPage(Map<String, Object> params);

    ChannelEntity findById(ChlUserEntity chlUserEntity);

    VideoBalanceVo getVideoBalance(ChlUserEntity chlUserEntity);

}

