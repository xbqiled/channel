package com.channel.api.service.cac;

import com.baomidou.mybatisplus.service.IService;
import com.channel.api.entity.cac.SysCashOrderEntity;
import com.channel.api.entity.chl.ChlUserEntity;
import com.channel.common.utils.PageUtils;


import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-02-24 19:28:16
 */
public interface SysCashOrderService extends IService<SysCashOrderEntity> {

    PageUtils queryOrderPage(Map<String, Object> params, ChlUserEntity userEntity);

    PageUtils queryPage(Map<String, Object> params, ChlUserEntity userEntity);

    Boolean approval(String id, String status, String note, ChlUserEntity entity) ;

    List<SysCashOrderEntity> queryCashOrderList(String channelId);

}

