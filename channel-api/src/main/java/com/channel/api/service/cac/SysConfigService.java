package com.channel.api.service.cac;

import com.baomidou.mybatisplus.service.IService;
import com.channel.api.entity.cac.SysConfigEntity;
import com.channel.common.utils.PageUtils;


import java.util.Map;

/**
 * 系统配置信息表
 *
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-04-05 15:43:49
 */
public interface SysConfigService extends IService<SysConfigEntity> {

    PageUtils queryPage(Map<String, Object> params);

    <T> T getConfigObject(String key, Class<T> clazz);

    String getValue(String key);
}

