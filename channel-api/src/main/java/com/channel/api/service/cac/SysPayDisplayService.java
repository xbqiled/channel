package com.channel.api.service.cac;

import com.baomidou.mybatisplus.service.IService;
import com.channel.api.entity.cac.SysPayDisplayEntity;
import com.channel.common.utils.PageUtils;

import java.util.Map;

/**
 * 
 *
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-10-06 17:15:45
 */
public interface SysPayDisplayService extends IService<SysPayDisplayEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

