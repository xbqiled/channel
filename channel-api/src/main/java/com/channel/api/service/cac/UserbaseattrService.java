package com.channel.api.service.cac;

import com.baomidou.mybatisplus.service.IService;
import com.channel.api.entity.cac.UserbaseattrEntity;
import com.channel.common.utils.PageUtils;

import java.util.Map;

/**
 * 
 *
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-02-23 14:34:24
 */
public interface UserbaseattrService extends IService<UserbaseattrEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

