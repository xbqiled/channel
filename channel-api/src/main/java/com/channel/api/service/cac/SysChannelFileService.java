package com.channel.api.service.cac;

import com.baomidou.mybatisplus.service.IService;
import com.channel.api.entity.cac.SysChannelFileEntity;
import com.channel.api.entity.chl.ChlUserEntity;
import com.channel.common.utils.PageUtils;
import java.util.Map;

/**
 * 文件上传
 *
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-04-05 16:04:25
 */
public interface SysChannelFileService extends IService<SysChannelFileEntity> {

    PageUtils queryPage(Map<String, Object> params);


    PageUtils queryFilePage(Map<String, Object> params, ChlUserEntity userEntity);
}

