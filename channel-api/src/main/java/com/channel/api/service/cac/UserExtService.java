package com.channel.api.service.cac;

import com.baomidou.mybatisplus.service.IService;
import com.channel.api.entity.cac.UserExtEntity;
import com.channel.api.entity.chl.ChlUserEntity;

/**
 * 
 *
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-09-20 15:21:18
 */
public interface UserExtService extends IService<UserExtEntity> {

    Boolean editNote (String note, String userId, ChlUserEntity userEntity);

}

