package com.channel.api.service.chl;

import com.baomidou.mybatisplus.service.IService;
import com.channel.api.entity.chl.ChlUserEntity;
import com.channel.api.entity.chl.ChlWhiteIpEntity;
import com.channel.common.utils.PageUtils;


import java.util.Map;

/**
 * 
 *
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2018-12-26 16:31:59
 */
public interface ChlWhiteIpService extends IService<ChlWhiteIpEntity> {

    PageUtils queryPage(Map<String, Object> params, ChlUserEntity userEntity);

    void save(ChlWhiteIpEntity chlWhiteIpEntity, ChlUserEntity userEntity);

    ChlWhiteIpEntity queryByIp(String ip, String userId);
}

