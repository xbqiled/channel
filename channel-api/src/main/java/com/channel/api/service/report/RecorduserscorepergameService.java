package com.channel.api.service.report;

import com.baomidou.mybatisplus.service.IService;
import com.channel.api.entity.chl.ChlUserEntity;
import com.channel.api.entity.report.RecorduserscorepergameEntity;
import com.channel.common.utils.PageUtils;

import java.util.Map;

/**
 * VIEW
 *
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-09-21 11:05:04
 */
public interface RecorduserscorepergameService extends IService<RecorduserscorepergameEntity> {

    PageUtils queryPage(Map<String, Object> params);

    PageUtils queryUserScoreperGame(Map<String, Object> params, ChlUserEntity userEntity);

    PageUtils queryGameBetPage(Map<String, Object> params, ChlUserEntity userEntity);
}

