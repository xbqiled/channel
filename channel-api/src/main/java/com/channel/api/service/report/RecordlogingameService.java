package com.channel.api.service.report;

import com.baomidou.mybatisplus.service.IService;
import com.channel.api.entity.chl.ChlUserEntity;
import com.channel.api.entity.report.RecordlogingameEntity;
import com.channel.common.utils.PageUtils;

import java.util.Map;

/**
 * VIEW
 *
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-05-29 12:12:03
 */
public interface RecordlogingameService extends IService<RecordlogingameEntity> {

    PageUtils queryPage(Map<String, Object> params);

    PageUtils queryLoginGameReport(Map<String, Object> params, ChlUserEntity userEntity);
}

