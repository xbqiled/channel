package com.channel.api.service.cac;

import com.baomidou.mybatisplus.service.IService;
import com.channel.api.entity.cac.RelationEntity;
import com.channel.api.entity.chl.ChlUserEntity;
import com.channel.common.utils.PageUtils;

import java.util.Map;

/**
 * VIEW
 *
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-05-07 17:05:21
 */
public interface RelationService extends IService<RelationEntity> {
    PageUtils queryPage(Map<String, Object> params);

    PageUtils queryRelationPage(Map<String, Object> params, ChlUserEntity userEntity);
}

