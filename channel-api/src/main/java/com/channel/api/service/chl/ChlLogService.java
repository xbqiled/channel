package com.channel.api.service.chl;


import com.baomidou.mybatisplus.service.IService;
import com.channel.api.entity.chl.ChlLogEntity;
import com.channel.api.entity.chl.ChlUserEntity;
import com.channel.common.utils.PageUtils;

import java.util.Map;


/**
 * 系统日志
 */
public interface ChlLogService extends IService<ChlLogEntity> {

    PageUtils queryPage(Map<String, Object> params);

    PageUtils queryLogPage(Map<String, Object> params, ChlUserEntity userEntity);

}
