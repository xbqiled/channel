package com.channel.api.service.cac;

import com.baomidou.mybatisplus.service.IService;
import com.channel.api.entity.cac.SysPayRebateEntity;
import com.channel.common.utils.PageUtils;
import org.apache.ibatis.annotations.Param;

import java.util.Map;

/**
 * 
 *
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-06-05 11:46:18
 */
public interface SysPayRebateService extends IService<SysPayRebateEntity> {

    PageUtils queryPage(Map<String, Object> params);

    SysPayRebateEntity getPayRebateByChannel(String channelId);

    SysPayRebateEntity getPayRebateByOrder(String orderNo);
}

