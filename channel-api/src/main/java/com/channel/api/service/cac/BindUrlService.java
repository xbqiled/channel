package com.channel.api.service.cac;

import com.baomidou.mybatisplus.service.IService;
import com.channel.api.entity.cac.BindUrlEntity;
import com.channel.api.entity.chl.ChlUserEntity;
import com.channel.common.utils.PageUtils;

import java.util.Map;

/**
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-12-06 18:42:05
 */
public interface BindUrlService extends IService<BindUrlEntity> {

    PageUtils queryBindUrlPage(Map<String, Object> params, ChlUserEntity userEntity);

    BindUrlEntity findByChannelId(String url, String accountId, ChlUserEntity userEntity);

    int saveEntity(String url, String accountId, ChlUserEntity userEntity);

    int updateEntity(String url, String accountId, ChlUserEntity userEntity);

    int delEntity(String url, String accountId, ChlUserEntity userEntity);

}

