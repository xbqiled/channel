package com.channel.api.service.cac;

import com.baomidou.mybatisplus.service.IService;
import com.channel.api.entity.cac.BgenterConfigEntity;
import com.channel.api.entity.chl.ChlUserEntity;
import com.channel.common.utils.PageUtils;


import java.util.Map;

/**
 * 
 *
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-11-15 13:23:00
 */
public interface BgenterConfigService extends IService<BgenterConfigEntity> {

    PageUtils queryBgEnterGamePage(Map<String, Object> params, ChlUserEntity userEntity);

    BgenterConfigEntity getConfigByChannelId(String channelId);

    Boolean saveEnterConfig(BgenterConfigEntity bgenterConfig, ChlUserEntity userEntity);

    Boolean modifyEnterConfig(BgenterConfigEntity bgenterConfig, ChlUserEntity userEntity);
}

