package com.channel.api.service.cac;

import com.baomidou.mybatisplus.service.IService;
import com.channel.api.entity.chl.ChlUserEntity;
import com.channel.api.entity.report.RedblackpoolEntity;
import com.channel.common.utils.PageUtils;


import java.util.Map;

/**
 * VIEW
 *
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-06-02 13:08:43
 */
public interface RedblackpoolService extends IService<RedblackpoolEntity> {

    PageUtils queryPage(Map<String, Object> params);


    PageUtils queryHhdzPoolPage(Map<String, Object> params,  ChlUserEntity userEntity);

}

