package com.channel.api.service.cac;

import com.baomidou.mybatisplus.service.IService;
import com.channel.api.entity.cac.InviteDepositChannelVo;
import com.channel.api.entity.cac.InviteDepositEntity;
import com.channel.api.entity.chl.ChlUserEntity;
import com.channel.common.utils.PageUtils;

import java.util.Map;

/**
 * VIEW
 *
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-07-31 12:40:41
 */
public interface InviteDepositService extends IService<InviteDepositEntity> {

    PageUtils queryPage(Map<String, Object> params);

    InviteDepositChannelVo findByChannelId(String channelId);

    PageUtils queryInviteDepositPage(Map<String, Object> params,  ChlUserEntity userEntity);

    int saveInviteDepositCfg(String channelId, Integer openType, Long timeType, Long beginTime, Long endTime,
                                    Long firstTotalDeposit, Long selfReward, Long upperReward);

    int channelOpenType(String channelId, String openType);

}

