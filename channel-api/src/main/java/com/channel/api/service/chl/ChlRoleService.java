package com.channel.api.service.chl;


import com.baomidou.mybatisplus.service.IService;
import com.channel.api.entity.chl.ChlRoleEntity;
import com.channel.common.utils.PageUtils;

import java.util.List;
import java.util.Map;


/**
 * 角色
 *
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2018-11-22 15:58:41
 */
public interface ChlRoleService extends IService<ChlRoleEntity> {

	PageUtils queryPage(Map<String, Object> params);

	PageUtils queryAdminPage(Map<String, Object> params);

	void save(ChlRoleEntity role);

	void update(ChlRoleEntity role);

	void deleteBatch(Long[] roleIds);

	/**
	 * 查询用户创建的角色ID列表
	 */
	List<Long> queryRoleIdList(Long createUserId);
}
