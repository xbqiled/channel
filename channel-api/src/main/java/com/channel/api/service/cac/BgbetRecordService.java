package com.channel.api.service.cac;

import com.baomidou.mybatisplus.service.IService;
import com.channel.api.entity.cac.BgbetRecordEntity;
import com.channel.api.entity.chl.ChlUserEntity;
import com.channel.common.utils.PageUtils;

import java.util.Map;

/**
 * 
 *
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-11-15 15:21:28
 */
public interface BgbetRecordService extends IService<BgbetRecordEntity> {

    PageUtils queryBgBetRecordPage(Map<String, Object> params, ChlUserEntity userEntity);

    PageUtils queryStaticBgBetRecordPage(Map<String, Object> params, ChlUserEntity userEntity);

}

