package com.channel.api.service.chl;

import com.baomidou.mybatisplus.service.IService;
import com.channel.api.entity.chl.ChlUserEntity;
import com.channel.common.utils.PageUtils;

import java.util.List;
import java.util.Map;

/**
 * ${comments}
 *
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2018-12-03 13:03:04
 */
public interface ChlUserService extends IService<ChlUserEntity> {

    PageUtils queryPage(Map<String, Object> params);


    ChlUserEntity selectOne(ChlUserEntity sysUserEntity);

    /**
     * 查询用户的所有权限
     * @param userId  用户ID
     */
    List<String> queryAllPerms(Long userId);

    /**
     * 查询用户的所有菜单ID
     */
    List<Long> queryAllMenuId(Long userId);

    /**
     * 根据用户名，查询系统用户
     */
    ChlUserEntity queryByUserName(String username);

    /**
     * 保存用户
     */
    void save(ChlUserEntity user);

    /**
     * 修改用户
     */
    void update(ChlUserEntity user);

    /**
     * 删除用户
     */
    void deleteBatch(Long[] userIds);

    /**
     * 修改密码
     * @param userId       用户ID
     * @param password     原密码
     * @param newPassword  新密码
     */
    boolean updatePassword(Long userId, String password, String newPassword);

}

