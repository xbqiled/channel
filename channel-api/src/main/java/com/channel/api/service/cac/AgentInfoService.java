package com.channel.api.service.cac;

import com.baomidou.mybatisplus.service.IService;
import com.channel.api.entity.cac.AgentInfoVo;
import com.channel.api.entity.cac.AccountinfoEntity;
import com.channel.api.entity.chl.ChlUserEntity;
import com.channel.common.utils.PageUtils;

import java.util.List;
import java.util.Map;

/**
 * VIEW
 *
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-05-21 15:14:50
 */
public interface AgentInfoService extends IService<AccountinfoEntity> {

     List<AgentInfoVo> getAgentInfoPage(Integer accountId, ChlUserEntity userEntity);

     List<AgentInfoVo> getChildNoteList(Integer accountId, ChlUserEntity userEntity);

     PageUtils getChildlistPage(Map<String, Object> params, ChlUserEntity userEntity);


}

