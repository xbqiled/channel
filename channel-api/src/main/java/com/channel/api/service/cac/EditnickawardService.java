package com.channel.api.service.cac;

import com.baomidou.mybatisplus.service.IService;
import com.channel.api.entity.cac.EditnickawardEntity;
import com.channel.api.entity.cac.EditnickawardVo;
import com.channel.api.entity.cac.InviteDepositChannelVo;
import com.channel.api.entity.chl.ChlUserEntity;
import com.channel.common.utils.PageUtils;

import java.util.Map;

/**
 * VIEW
 *
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-09-18 18:58:08
 */
public interface EditnickawardService extends IService<EditnickawardEntity> {

    PageUtils  queryNickAwardPage(Map<String, Object> params, ChlUserEntity userEntity);

    EditnickawardVo findByChannelId(String channelId);

    int saveNcikAwardCfg(String channelId, Integer openType, Integer award);

    int channelOpenType(String channelId, String openType);

}

