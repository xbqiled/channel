package com.channel.api.service.report;

import com.baomidou.mybatisplus.service.IService;
import com.channel.api.entity.chl.ChlUserEntity;
import com.channel.api.entity.report.PlayerrankEntity;
import com.channel.common.utils.PageUtils;

import java.util.Map;

/**
 * VIEW
 *
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-09-17 15:12:03
 */
public interface PlayerrankService extends IService<PlayerrankEntity> {

    PageUtils queryPlayerRankPage(Map<String, Object> params, ChlUserEntity userEntity);

}

