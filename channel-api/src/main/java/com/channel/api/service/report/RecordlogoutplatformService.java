package com.channel.api.service.report;

import com.baomidou.mybatisplus.service.IService;
import com.channel.api.entity.chl.ChlUserEntity;
import com.channel.api.entity.report.RecordlogoutplatformEntity;
import com.channel.common.utils.PageUtils;


import java.util.Map;

/**
 * VIEW
 *
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-02-25 19:18:37
 */
public interface RecordlogoutplatformService extends IService<RecordlogoutplatformEntity> {

    PageUtils queryPage(Map<String, Object> params);

    PageUtils queryLogoutReport(Map<String, Object> params, ChlUserEntity userEntity);
}

