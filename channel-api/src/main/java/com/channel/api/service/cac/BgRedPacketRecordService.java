package com.channel.api.service.cac;

import com.baomidou.mybatisplus.service.IService;
import com.channel.api.entity.cac.BgRedPacketRecordEntity;
import com.channel.api.entity.chl.ChlUserEntity;
import com.channel.common.utils.PageUtils;

import java.util.Map;

/**
 * 
 *
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2020-01-14 14:39:56
 */
public interface BgRedPacketRecordService extends IService<BgRedPacketRecordEntity> {

    PageUtils queryRedPacketPage(Map<String, Object> params, ChlUserEntity userEntity);
}

