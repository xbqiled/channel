package com.channel.api.service.cac;

import com.baomidou.mybatisplus.service.IService;
import com.channel.api.entity.cac.TipOrderEntity;
import com.channel.common.utils.PageUtils;

import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-05-19 10:25:39
 */
public interface TipOrderService extends IService<TipOrderEntity> {

    PageUtils queryPage(Map<String, Object> params);

    List<Map<String, Object>> queryTipInfoOrder (String channelId);
}

