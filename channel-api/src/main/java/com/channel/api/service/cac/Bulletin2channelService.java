package com.channel.api.service.cac;

import com.baomidou.mybatisplus.service.IService;
import com.channel.api.entity.cac.Bulletin2channelEntity;
import com.channel.common.utils.PageUtils;


import java.util.Map;

/**
 * VIEW
 *
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-03-28 15:24:52
 */
public interface Bulletin2channelService extends IService<Bulletin2channelEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

