package com.channel.api.service.cac;

import com.baomidou.mybatisplus.service.IService;
import com.channel.api.entity.cac.TipSoundConfigEntity;
import com.channel.common.utils.PageUtils;

import java.util.Map;

/**
 * 
 *
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-05-19 14:18:20
 */
public interface TipSoundConfigService extends IService<TipSoundConfigEntity> {

    PageUtils queryPage(Map<String, Object> params);

    TipSoundConfigEntity getConfigByChannelId(String channelId);

}

