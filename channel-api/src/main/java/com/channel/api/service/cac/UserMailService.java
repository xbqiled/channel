package com.channel.api.service.cac;

import com.baomidou.mybatisplus.service.IService;
import com.channel.api.entity.cac.UserMailEntity;
import com.channel.api.entity.cac.UserMailVo;
import com.channel.api.entity.chl.ChlUserEntity;
import com.channel.common.utils.PageUtils;

import java.util.Map;

/**
 * 
 *
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-03-07 13:39:08
 */
public interface UserMailService extends IService<UserMailEntity> {

    PageUtils queryUserMailPage(Map<String, Object> params, ChlUserEntity entity);

    int sendMail(UserMailVo userMailVo, ChlUserEntity entity);

    UserMailEntity findbyId(String id, String userId,  ChlUserEntity userEntity);
}

