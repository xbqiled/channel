package com.channel.api.service.chl;

import com.baomidou.mybatisplus.service.IService;
import com.channel.api.entity.chl.ChlRoleMenuEntity;
import java.util.List;


/**
 * 角色与菜单对应关系
 *
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2018-11-22 15:58:41
 */
public interface ChlRoleMenuService extends IService<ChlRoleMenuEntity> {
	
	void saveOrUpdate(Long roleId, List<Long> menuIdList);
	
	/**
	 * 根据角色ID，获取菜单ID列表
	 */
	List<Long> queryMenuIdList(Long roleId);

	/**
	 * 根据角色ID数组，批量删除
	 */
	int deleteBatch(Long[] roleIds);

}
