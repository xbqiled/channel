package com.channel.api.service.cac;

import com.baomidou.mybatisplus.service.IService;
import com.channel.api.entity.cac.OnlineplayerEntity;
import com.channel.api.entity.chl.ChlUserEntity;
import com.channel.common.utils.PageUtils;

import java.util.List;
import java.util.Map;

/**
 * VIEW
 *
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-05-21 15:14:50
 */
public interface OnlineplayerService extends IService<OnlineplayerEntity> {

    PageUtils queryPage(Map<String, Object> params);

    PageUtils queryOnLinePlayerPage(Map<String, Object> params,  ChlUserEntity userEntity);

    Long queryOnlineNum(ChlUserEntity userEntity);

    Long queryHallNum(ChlUserEntity userEntity);

    PageUtils queryLoginByTime(Map<String, Object> params, ChlUserEntity userEntity);

}

