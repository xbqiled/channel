package com.channel.api.service.chl;


import com.baomidou.mybatisplus.service.IService;
import com.channel.api.entity.chl.ChlMenuEntity;

import java.util.List;


/**
 * 菜单管理
 *
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2018-11-18 15:58:41
 */
public interface ChlMenuService extends IService<ChlMenuEntity> {

	List<ChlMenuEntity> queryChannelList(Long userId);

	/**
	 * 根据父菜单，查询子菜单
	 * @param parentId 父菜单ID
	 * @param menuIdList  用户菜单ID
	 */
	List<ChlMenuEntity> queryListParentId(Long parentId, List<Long> menuIdList);

	/**
	 * 根据父菜单，查询子菜单
	 * @param parentId 父菜单ID
	 */
	List<ChlMenuEntity> queryListParentId(Long parentId);
	
	/**
	 * 获取不包含按钮的菜单列表
	 */
	List<ChlMenuEntity> queryNotButtonList();
	
	/**
	 * 获取用户菜单列表
	 */
	List<ChlMenuEntity> getUserMenuList(Long userId);

	/**
	 * 删除
	 */
	void delete(Long menuId);

	/**
	 * <B>查询所有的</B>
	 */

	List<ChlMenuEntity> queryAll();


}
