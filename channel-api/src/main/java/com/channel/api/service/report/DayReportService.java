package com.channel.api.service.report;

import com.baomidou.mybatisplus.service.IService;
import com.channel.api.entity.chl.ChlUserEntity;
import com.channel.api.entity.report.DayReportEntity;
import com.channel.common.utils.PageUtils;

import java.util.List;
import java.util.Map;

/**
 * VIEW
 *
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-02-24 14:04:07
 */
public interface DayReportService extends IService<DayReportEntity> {

    PageUtils queryPage(Map<String, Object> params);

    List<Map<String, Object>> queryPlayGameCount(String startTime, String endTime,  ChlUserEntity userEntity);

    PageUtils queryDayReport(Map<String, Object> params, ChlUserEntity userEntity);

    PageUtils queryMonthReport(Map<String, Object> params, ChlUserEntity userEntity);

    PageUtils queryUserAccount(Map<String, Object> params, ChlUserEntity userEntity);

    PageUtils queryWealthRank(Map<String, Object> params, ChlUserEntity userEntity);

    List<DayReportEntity> queryDayReportByChannel(ChlUserEntity userEntity);

    PageUtils  queryDdzBoard(Map<String, Object> params, ChlUserEntity userEntity);

    Map<String, Object> analysisDdzBoard(Map<String, Object> params, ChlUserEntity userEntity);

    PageUtils  queryPdkBoard(Map<String, Object> params, ChlUserEntity userEntity);

    Map<String, Object> analysisPdkBoard(Map<String, Object> params, ChlUserEntity userEntity);

    PageUtils  queryLhBoard(Map<String, Object> params, ChlUserEntity userEntity);

    PageUtils  queryByBoard(Map<String, Object> params, ChlUserEntity userEntity);

    Map<String, Object> analysisByBoard(Map<String, Object> params, ChlUserEntity userEntity);

    PageUtils  queryNnBoard(Map<String, Object> params, ChlUserEntity userEntity);

    Map<String, Object> analysisNnBoard(Map<String, Object> params, ChlUserEntity userEntity);

    PageUtils  queryBhsBoard(Map<String, Object> params, ChlUserEntity userEntity);

    Map<String, Object> analysisBhsBoard(Map<String, Object> params, ChlUserEntity userEntity);

    PageUtils  queryBjlBoard(Map<String, Object> params, ChlUserEntity userEntity);

    Map<String, Object> analysisBjlBoard(Map<String, Object> params, ChlUserEntity userEntity);

    PageUtils  queryZjhBoard(Map<String, Object> params, ChlUserEntity userEntity);

    Map<String, Object> analysisZjhBoard(Map<String, Object> params, ChlUserEntity userEntity);

    PageUtils queryCsdBoard(Map<String, Object> params, ChlUserEntity userEntity);

    List<Map<String, Object>> analysisCsdBoard(String id);

    PageUtils queryHhdzBoard(Map<String, Object> params, ChlUserEntity userEntity);

    Map<String, Object> analysisHhdzBoard(Map<String, Object> params, ChlUserEntity userEntity);

    PageUtils queryHbjlBoard(Map<String, Object> params, ChlUserEntity userEntity);

    PageUtils queryQznnBoard(Map<String, Object> params, ChlUserEntity userEntity);

    Map<String, Object> analysisQznnBoard(Map<String, Object> params, ChlUserEntity userEntity);

    PageUtils queryFqzsBoard(Map<String, Object> params, ChlUserEntity userEntity);

    Map<String, Object> analysisFqzsBoard(Map<String, Object> params, ChlUserEntity userEntity);

    PageUtils queryBcbmBoard(Map<String, Object> params, ChlUserEntity userEntity);

    Map<String, Object> analysisBcbmBoard(Map<String, Object> params, ChlUserEntity userEntity);

    PageUtils querySgjBoard(Map<String, Object> params, ChlUserEntity userEntity);

    Map<String, Object> analysisSgjBoard(Map<String, Object> params, ChlUserEntity userEntity);

    PageUtils queryEsydBoard(Map<String, Object> params, ChlUserEntity userEntity);

    Map<String, Object> analysisEsydBoard(Map<String, Object> params, ChlUserEntity userEntity);
}

