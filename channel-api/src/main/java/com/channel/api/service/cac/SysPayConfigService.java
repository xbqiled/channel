package com.channel.api.service.cac;

import com.baomidou.mybatisplus.service.IService;
import com.channel.api.entity.cac.SysPayConfigEntity;
import com.channel.api.entity.cac.SysPayConfigVo;
import com.channel.api.entity.chl.ChlUserEntity;
import com.channel.common.json.GsonPayValue;
import com.channel.common.utils.PageUtils;


import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-02-24 19:27:36
 */
public interface SysPayConfigService extends IService<SysPayConfigEntity> {

    PageUtils queryPage(Map<String, Object> params, ChlUserEntity userEntity);

    PageUtils queryPayConfigPay(Map<String, Object> params, ChlUserEntity userEntity);

    Boolean saveConfig(SysPayConfigVo sysPayConfigVo, ChlUserEntity chlUserEntity);

    Boolean updateConfig(SysPayConfigVo sysPayConfigVo, ChlUserEntity chlUserEntity);

    Boolean updateConfigStatus(String id, String status);

    Boolean updatePayValueConfig(Integer id, GsonPayValue [] gsonPayValue);

    List<GsonPayValue> getPayValueConfig(String id);
}

