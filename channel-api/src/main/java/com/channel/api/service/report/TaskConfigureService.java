package com.channel.api.service.report;


import com.baomidou.mybatisplus.service.IService;
import com.channel.api.entity.report.TaskConfigureEntity;
import com.channel.common.utils.PageUtils;

import java.util.Map;

/**
 * 
 *
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-12-30 13:46:48
 */
public interface TaskConfigureService extends IService<TaskConfigureEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

