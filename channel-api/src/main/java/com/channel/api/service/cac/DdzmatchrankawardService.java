package com.channel.api.service.cac;

import com.baomidou.mybatisplus.service.IService;
import com.channel.api.entity.cac.DdzMatchAwardVo;
import com.channel.api.entity.cac.DdzmatchrankawardEntity;
import com.channel.api.entity.chl.ChlUserEntity;
import com.channel.common.json.GsonMatchRankAward;
import com.channel.common.utils.PageUtils;

import java.util.List;
import java.util.Map;

/**
 * VIEW
 *
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-09-17 20:44:19
 */
public interface DdzmatchrankawardService extends IService<DdzmatchrankawardEntity> {

    PageUtils  queryDdzMatchAwardPage(Map<String, Object> params, ChlUserEntity userEntity);

    DdzMatchAwardVo findByChannelId(String channelId, String optionType);

    int saveDdzMatchAwardCfg(String channelId, Integer openType, Integer optionType, List<GsonMatchRankAward> cfgList);

    int channelOpenType(String channelId, String optionType, String openType);


}

