package com.channel.api.service.cac;

import com.baomidou.mybatisplus.service.IService;
import com.channel.api.entity.cac.BindMobileEntity;
import com.channel.api.entity.chl.ChlUserEntity;
import com.channel.common.utils.PageUtils;

import java.util.Map;

/**
 * 
 *
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-04-09 13:47:05
 */
public interface BindMobileService extends IService<BindMobileEntity> {


    PageUtils queryPage(Map<String, Object> params);


    BindMobileEntity getByChannelId(ChlUserEntity chlUserEntity);


    Boolean saveOrUpdateBind(BindMobileEntity bindMobile, ChlUserEntity chlUserEntity);

}

