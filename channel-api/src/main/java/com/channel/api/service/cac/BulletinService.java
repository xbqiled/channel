package com.channel.api.service.cac;

import com.baomidou.mybatisplus.service.IService;
import com.channel.api.entity.cac.BulletinEntity;
import com.channel.api.entity.cac.BulletinVo;
import com.channel.api.entity.chl.ChlUserEntity;
import com.channel.common.utils.PageUtils;


import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-03-06 16:05:02
 */
public interface BulletinService extends IService<BulletinEntity> {

    PageUtils queryPage(Map<String, Object> params);

    PageUtils queryBulletinPage(Map<String, Object> params, ChlUserEntity userEntity);

    List<Map<String, Object>> getBulletinInfo(String id, ChlUserEntity userEntity);

    int addBulletin(BulletinVo bulletinVo, ChlUserEntity userEntity);

    int modifyBulletin(BulletinVo bulletinVo, ChlUserEntity userEntity);

    int delBulletin(String [] bulletinId);
}

