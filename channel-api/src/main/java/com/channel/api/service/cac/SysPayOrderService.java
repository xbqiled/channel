package com.channel.api.service.cac;

import com.baomidou.mybatisplus.service.IService;
import com.channel.api.entity.cac.CancelResultVo;
import com.channel.api.entity.cac.SysCashOrderEntity;
import com.channel.api.entity.cac.SysPayOrderEntity;
import com.channel.api.entity.chl.ChlUserEntity;
import com.channel.common.json.GsonReduceResult;
import com.channel.common.utils.PageUtils;

import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-02-24 19:28:02
 */
public interface SysPayOrderService extends IService<SysPayOrderEntity> {

    PageUtils queryPage(Map<String, Object> params);

    Boolean approval(String id, String status, String note, ChlUserEntity entity) ;

    PageUtils queryOrderPage(Map<String, Object> params, ChlUserEntity userEntity);

    PageUtils queryRechargeStatisPage(Map<String, Object> params, ChlUserEntity userEntity);

    PageUtils queryRechargeUserNumPage(Map<String, Object> params, ChlUserEntity userEntity);

    Boolean recharge(String userId, Integer isManual, Double damaMulti, String payFee, String depositor, String account,
                     String remark, String payType, Integer rate, ChlUserEntity chlUserEntity);

    CancelResultVo rechargeCancel(Integer id, String note, Integer force, ChlUserEntity userEntity);

    GsonReduceResult reduceFee(String userId, String note, String payFee, Integer force, ChlUserEntity userEntity);

    Map<String, Object> queryAllRechargeData(Map<String, Object> params, ChlUserEntity userEntity);

    List<SysPayOrderEntity> queryRechargeOrderList(String channelId);

}

