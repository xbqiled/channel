package com.channel.api.service.cac;

import com.baomidou.mybatisplus.service.IService;
import com.channel.api.entity.cac.ChanneltaskEntity;
import com.channel.api.entity.chl.ChlUserEntity;
import com.channel.common.json.GsonTask;
import com.channel.common.utils.PageUtils;

import java.util.Map;

/**
 * VIEW
 *
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-12-31 12:30:32
 */
public interface ChanneltaskService extends IService<ChanneltaskEntity> {

    PageUtils queryTaskPage(Map<String, Object> params,  ChlUserEntity userEntity);

    int channelOpenType(String channelId, String openType);

    Map<String, Object> findByChannelId(String channelId);

    int saveOrUpdateConfig(ChlUserEntity userEntity, Integer openType, Integer activityTimeType, String beginTime,
                          String endTime, GsonTask[] taskArr);
}

