package com.channel.api.service.cac;

import com.baomidou.mybatisplus.service.IService;
import com.channel.api.entity.cac.DayShareEntity;
import com.channel.api.entity.chl.ChlUserEntity;
import com.channel.common.json.GsonDayShare;
import com.channel.common.utils.PageUtils;

import java.util.List;
import java.util.Map;

/**
 * VIEW
 *
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-12-15 16:02:59
 */
public interface DayShareService extends IService<DayShareEntity> {

    PageUtils queryDaySharePage(Map<String, Object> params, ChlUserEntity chlUser);

    int channelOpenType(String channelId, String openType);

    DayShareEntity findByChannelId(String channelId);

    int saveOrUpdateConfig(ChlUserEntity userEntity, Integer opType, Long dayAward, Long awardDayShareCnt, List<GsonDayShare> dayShareList);

}

