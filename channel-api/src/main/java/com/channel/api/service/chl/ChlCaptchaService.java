package com.channel.api.service.chl;

import com.baomidou.mybatisplus.service.IService;
import com.channel.api.entity.chl.ChlCaptchaEntity;

/**
 * 验证码
 *
 */
public interface ChlCaptchaService extends IService<ChlCaptchaEntity> {

    /**
     * 获取图片验证码
     */
    boolean getCaptcha(String uuid, String code);

    /**
     * 验证码效验
     * @param uuid  uuid
     * @param code  验证码
     * @return  true：成功  false：失败
     */
    boolean validate(String uuid, String code);
}
