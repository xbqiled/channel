package com.channel.api.service.cac;

import com.baomidou.mybatisplus.service.IService;
import com.channel.api.entity.cac.StatisChannelEntity;
import com.channel.api.entity.chl.ChlUserEntity;
import com.channel.common.utils.PageUtils;

import java.util.Map;

/**
 * 
 *
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2020-02-28 14:18:36
 */
public interface StatisChannelService extends IService<StatisChannelEntity> {

    PageUtils getStatisDayReportPage(Map<String, Object> params, ChlUserEntity userEntity);

    PageUtils getStatisMonthReportPage(Map<String, Object> params, ChlUserEntity userEntity);
}

