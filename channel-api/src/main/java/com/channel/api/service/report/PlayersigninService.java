package com.channel.api.service.report;

import com.baomidou.mybatisplus.service.IService;
import com.channel.api.entity.chl.ChlUserEntity;
import com.channel.api.entity.report.PlayersigninEntity;
import com.channel.common.utils.PageUtils;
import java.util.Map;

/**
 * VIEW
 *
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-05-28 14:18:49
 */
public interface PlayersigninService extends IService<PlayersigninEntity> {

    PageUtils queryPage(Map<String, Object> params);

    PageUtils querySginPage(Map<String, Object> params, ChlUserEntity userEntity);
}

