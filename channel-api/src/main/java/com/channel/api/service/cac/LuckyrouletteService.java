package com.channel.api.service.cac;

import com.baomidou.mybatisplus.service.IService;
import com.channel.api.entity.cac.LuckyrouletteEntity;
import com.channel.api.entity.chl.ChlUserEntity;
import com.channel.common.json.GsonLuckRoulette;
import com.channel.common.utils.PageUtils;

import java.util.List;
import java.util.Map;

/**
 * VIEW
 *
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-04-22 16:07:30
 */
public interface LuckyrouletteService extends IService<LuckyrouletteEntity> {

    PageUtils queryPage(Map<String, Object> params);

    int channelOpenType(String channelId, String openType);

    PageUtils queryLuckyRoulettePage(Map<String, Object> params, ChlUserEntity userEntity);

    int saveLuckRoulette(ChlUserEntity userEntity, Integer openType, List<GsonLuckRoulette> diamondcfg, List<GsonLuckRoulette> goldcfg, List<GsonLuckRoulette> silvercfg, String startTimeStr, String finishTimeStr, Integer goldcost, Integer diamondcost, Integer silvercost);

}

