package com.channel.api.service.cac;

import com.baomidou.mybatisplus.service.IService;
import com.channel.api.entity.cac.SysPayPlatformEntity;
import com.channel.common.utils.PageUtils;

import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-02-24 19:27:36
 */
public interface SysPayPlatformService extends IService<SysPayPlatformEntity> {

    PageUtils queryPage(Map<String, Object> params);

    List<Map<String, Object>> getPayPlatFormData();

    String getPlatformName(Integer configId);
}

