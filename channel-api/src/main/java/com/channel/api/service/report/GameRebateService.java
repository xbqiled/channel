package com.channel.api.service.report;

import com.baomidou.mybatisplus.service.IService;
import com.channel.api.entity.chl.ChlUserEntity;
import com.channel.api.entity.report.GameRebateEntity;
import com.channel.common.utils.PageUtils;
import java.util.Map;

/**
 * VIEW
 *
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-03-30 19:46:22
 */
public interface GameRebateService extends IService<GameRebateEntity> {

    PageUtils queryPage(Map<String, Object> params);


    PageUtils queryGameRebatePage(Map<String, Object> params, ChlUserEntity chlUserEntity);
}

