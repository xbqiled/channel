package com.channel.api.service.report;

import com.baomidou.mybatisplus.service.IService;
import com.channel.api.entity.chl.ChlUserEntity;
import com.channel.api.entity.report.RecordloginplatformEntity;
import com.channel.common.utils.PageUtils;

import java.util.Map;

/**
 * VIEW
 *
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-02-25 19:23:00
 */
public interface RecordloginplatformService extends IService<RecordloginplatformEntity> {
    PageUtils queryPage(Map<String, Object> params);

    PageUtils queryLoginReport(Map<String, Object> params, ChlUserEntity userEntity);

    PageUtils queryUserRetainList(Map<String, Object> params, ChlUserEntity userEntity);

}

