package com.channel.api.service.cac;

import com.baomidou.mybatisplus.service.IService;
import com.channel.api.entity.cac.ChannelVipEntity;
import com.channel.api.entity.chl.ChlUserEntity;
import com.channel.common.json.GsonVip;
import com.channel.common.json.RequestVip;
import com.channel.common.utils.PageUtils;

import java.util.Map;

/**
 * VIEW
 *
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-12-17 14:48:34
 */
public interface ChannelVipService extends IService<ChannelVipEntity> {

    PageUtils queryChannelVipPage(Map<String, Object> params, ChlUserEntity chlUser);

    int channelOpenType(String channelId, String openType);

    RequestVip findByChannelId(String channelId);

    int saveOrUpdateConfig(ChlUserEntity userEntity, Integer openType, GsonVip vip1, GsonVip vip2, GsonVip vip3, GsonVip vip4, GsonVip vip5,
                                  GsonVip vip6, GsonVip vip7, GsonVip vip8, GsonVip vip9);
}

