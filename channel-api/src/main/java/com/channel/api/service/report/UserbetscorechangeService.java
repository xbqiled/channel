package com.channel.api.service.report;

import com.baomidou.mybatisplus.service.IService;
import com.channel.api.entity.chl.ChlUserEntity;
import com.channel.api.entity.report.UserbetscorechangeEntity;
import com.channel.common.utils.PageUtils;

import java.util.List;
import java.util.Map;

/**
 * VIEW
 *
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-04-24 19:09:09
 */
public interface UserbetscorechangeService extends IService<UserbetscorechangeEntity> {
    PageUtils queryPage(Map<String, Object> params);

    PageUtils queryRecordPage(Map<String, Object> params, ChlUserEntity userEntity);

    List<Map<String, Object>> queryOperatorList();
}

