package com.channel.api.service.report;

import com.baomidou.mybatisplus.service.IService;
import com.channel.api.entity.chl.ChlUserEntity;
import com.channel.api.entity.report.RecordlogoutgameEntity;
import com.channel.common.utils.PageUtils;

import java.util.Map;

/**
 * VIEW
 *
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-05-29 12:18:47
 */
public interface RecordlogoutgameService extends IService<RecordlogoutgameEntity> {

    PageUtils queryPage(Map<String, Object> params);

    PageUtils queryLogoutGameReport(Map<String, Object> params, ChlUserEntity userEntity);
}

