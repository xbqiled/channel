package com.channel.api.service.cac;

import com.baomidou.mybatisplus.service.IService;
import com.channel.api.entity.cac.DepositchannelEntity;
import com.channel.api.entity.chl.ChlUserEntity;
import com.channel.common.json.GsonRechargeRebateCfg;
import com.channel.common.utils.PageUtils;

import java.util.List;
import java.util.Map;

/**
 * VIEW
 *
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-05-31 11:20:19
 */
public interface DepositchannelService extends IService<DepositchannelEntity> {

    PageUtils queryPage(Map<String, Object> params);

    DepositchannelEntity findByChannelId(String channelId);

    PageUtils queryDepositPage(Map<String, Object> params,  ChlUserEntity userEntity);

    int channelOpenType(String channelId, String openType);

    int saveRechargeRebateCfg(String channel, String beginTime, String endTime, Integer openType, List<GsonRechargeRebateCfg> rabateCfg);
}

