package com.channel.api.service.cac;

import com.baomidou.mybatisplus.service.IService;
import com.channel.api.entity.cac.CsInfoEntity;
import com.channel.api.entity.chl.ChlUserEntity;
import com.channel.common.utils.PageUtils;

import java.util.Map;

/**
 * 
 *
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-03-29 17:46:03
 */
public interface CsInfoService extends IService<CsInfoEntity> {

    PageUtils queryPage(Map<String, Object> params);


    PageUtils queryCsInfoList(Map<String, Object> params, ChlUserEntity userEntity);


    Boolean saveCsInfo(CsInfoEntity csInfoEntity,  ChlUserEntity userEntity);


    Boolean modifyCsInfo(CsInfoEntity csInfoEntity,  ChlUserEntity userEntity);
}

