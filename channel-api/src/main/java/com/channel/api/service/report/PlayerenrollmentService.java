package com.channel.api.service.report;

import com.baomidou.mybatisplus.service.IService;
import com.channel.api.entity.chl.ChlUserEntity;
import com.channel.api.entity.report.PlayerenrollmentEntity;
import com.channel.common.utils.PageUtils;

import java.util.Map;

/**
 * VIEW
 *
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-09-17 15:10:16
 */
public interface PlayerenrollmentService extends IService<PlayerenrollmentEntity> {

    PageUtils queryPlayerEnrollmentPage(Map<String, Object> params, ChlUserEntity userEntity);

}

