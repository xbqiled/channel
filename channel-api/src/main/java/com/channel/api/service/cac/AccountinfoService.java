package com.channel.api.service.cac;

import com.baomidou.mybatisplus.service.IService;
import com.channel.api.entity.cac.AccountinfoEntity;
import com.channel.api.entity.chl.ChlUserEntity;
import com.channel.common.json.GsonResult;
import com.channel.common.utils.PageUtils;

import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-02-23 14:34:23
 */
public interface AccountinfoService extends IService<AccountinfoEntity> {

    PageUtils quereyAccountPage(Map<String, Object> params, ChlUserEntity userEntity);

    PageUtils queryUserPhoneList(Map<String, Object> params, ChlUserEntity userEntity);

    PageUtils quereyDetailIpListPage(Map<String, Object> params, ChlUserEntity userEntity);

    Map<String, Object> queryIpAccountPage(Map<String, Object> params, ChlUserEntity userEntity);

    GsonResult updateStatus(String tAccountid, String status);

    Map<String, Object> getUserInfoById(String accountId);

    /**
     * <B>踢人</B>
     * @param userId
     * @return
     */
    GsonResult kickUser(Integer userId);

    /**
     * <B> 查询用户所在游戏</B>
     * @return
     */
    List<Map<String, Object>> isInGame(Integer userId);

    /**
     * <B>用户密码修改</B>
     * @param userId
     * @param password
     * @return
     */
    int modifyLoginPw(String userId, String password);


    int updateUserNote(String userId, String note, ChlUserEntity userEntity);

    /**
     *
     * @param userId
     * @return
     */
    GsonResult  clearCommonDev(Integer userId);


    /**
     * <B>清空设备信息</B>
     * @param userId
     * @return
     */
    GsonResult  unbindPhoneNum(Integer userId);

    /**
     * <B>绑定手机</B>
     * @param userId
     * @param phoneNum
     * @return
     */
    GsonResult  bindPhoneNum(Integer userId, String phoneNum);

    /**
     * <B>添加上级节点</B>
     * @param channel
     * @param accountId
     * @param promoterId
     * @return
     */
    GsonResult addRelation(String channel, Long accountId, Long promoterId);


    /***
     * <B>修改真实姓名</B>
     * @param accountId
     * @param realName
     * @return
     */
    GsonResult modifyRealName(Long accountId, String realName);
}

