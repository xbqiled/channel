package com.channel.api.service.cac;

import com.baomidou.mybatisplus.service.IService;
import com.channel.api.entity.cac.ChannelsuccourEntity;
import com.channel.api.entity.chl.ChlUserEntity;
import com.channel.common.json.GsonSuccour;
import com.channel.common.utils.PageUtils;

import java.util.Map;

/**
 * VIEW
 *
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-12-31 12:23:45
 */
public interface ChannelsuccourService extends IService<ChannelsuccourEntity> {

    PageUtils querySuccourPage(Map<String, Object> params, ChlUserEntity chlUser);

    int channelOpenType(String channelId, String openType);

    GsonSuccour findByChannelId(String channelId);

    int saveOrUpdateConfig(ChlUserEntity userEntity, Integer openType,  Long coinLimit, Long succourAward, Integer totalSuccourCnt,Integer shareConditionCnt);
}

