package com.channel.api.service.cac;

import com.baomidou.mybatisplus.service.IService;
import com.channel.api.entity.cac.PushConfigEntity;

/**
 * 
 *
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2020-05-27 20:42:22
 */
public interface PushConfigService extends IService<PushConfigEntity> {


}

