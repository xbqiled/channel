package com.channel.api.service.report;

import com.baomidou.mybatisplus.service.IService;
import com.channel.api.entity.chl.ChlUserEntity;
import com.channel.api.entity.report.RecorddamachangeEntity;
import com.channel.common.json.GsonChangeTrading;
import com.channel.common.utils.PageUtils;

import java.util.Map;

/**
 * VIEW
 *
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-08-24 11:49:24
 */
public interface RecorddamachangeService extends IService<RecorddamachangeEntity> {

    PageUtils queryStaticTradinglist(Map<String, Object> params, ChlUserEntity userEntity);

    PageUtils queryTradinglist(Map<String, Object> params, ChlUserEntity userEntity);

    PageUtils getUserTrading(Map<String, Object> params, ChlUserEntity userEntity);

    PageUtils queryStaticLoss(Map<String, Object> params, ChlUserEntity userEntity);

    GsonChangeTrading changeTrading(String userId, Integer opType, Long damaChange, ChlUserEntity userEntity);

}

