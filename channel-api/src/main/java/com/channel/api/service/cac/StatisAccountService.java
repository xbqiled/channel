package com.channel.api.service.cac;

import com.baomidou.mybatisplus.service.IService;
import com.channel.api.entity.cac.StatisAccountEntity;
import com.channel.api.entity.chl.ChlUserEntity;
import com.channel.common.utils.PageUtils;

import java.util.Map;

/**
 * 
 *
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-06-16 15:38:03
 */
public interface StatisAccountService extends IService<StatisAccountEntity> {

    Map<String, Object> queryStatisAccountData(Map<String, Object> params, ChlUserEntity userEntity);

    PageUtils queryStatisAccountPage(Map<String, Object> params, ChlUserEntity userEntity);

}

