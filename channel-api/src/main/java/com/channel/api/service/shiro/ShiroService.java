package com.channel.api.service.shiro;



import com.channel.api.entity.chl.ChlUserEntity;
import com.channel.api.entity.chl.ChlUserTokenEntity;
import java.util.Set;

/**
 * shiro相关接口
 */
public interface ShiroService {
    /**
     * 获取用户权限列表
     */
    Set<String> getUserPermissions(long userId);

    ChlUserTokenEntity queryByToken(String token);

    /**
     * 根据用户ID，查询用户
     * @param userId
     */
    ChlUserEntity queryUser(Long userId);
}
