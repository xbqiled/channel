package com.channel.api.service.cac;

import cn.jpush.api.report.MessagesResult;
import cn.jpush.api.report.ReceivedsResult;
import com.baomidou.mybatisplus.service.IService;
import com.channel.api.entity.cac.PushRecordEntity;
import com.channel.api.entity.chl.ChlUserEntity;
import com.channel.common.utils.PageUtils;

import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author pengbin
 * @date 2020-05-27 20:42:22
 */
public interface PushRecordService extends IService<PushRecordEntity> {

    PageUtils queryPushRecordPage(Map<String, Object> params, ChlUserEntity userEntity);

    Boolean sendPush(ChlUserEntity userEntity, String devicePlatform,
             Integer timeType,
             String retainTime,
             Long timeLong,
             Integer sendType,
             String alias,
             String title,
             String content);


    Map<String, Object> getObj(Integer id);

    ReceivedsResult getReport(Integer id);
}

