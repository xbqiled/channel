package com.channel.api.service.chl;

import com.baomidou.mybatisplus.service.IService;
import com.channel.api.entity.chl.ChlUserTokenEntity;
import com.channel.common.utils.R;


/**
 * 用户Token
 *
 */
public interface ChlUserTokenService extends IService<ChlUserTokenEntity> {

	/**
	 * 生成token
	 * @param userId  用户ID
	 */
	R createToken(long userId);

	/**
	 * 退出，修改token值
	 * @param userId  用户ID
	 */
	void logout(long userId);


	ChlUserTokenEntity queryByToken(String token);

}
