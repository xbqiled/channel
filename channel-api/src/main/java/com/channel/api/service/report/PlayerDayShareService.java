package com.channel.api.service.report;

import com.baomidou.mybatisplus.service.IService;
import com.channel.api.entity.chl.ChlUserEntity;
import com.channel.api.entity.report.PlayerDayShareEntity;
import com.channel.common.utils.PageUtils;


import java.util.Map;

/**
 * VIEW
 *
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-12-23 15:48:43
 */
public interface PlayerDayShareService extends IService<PlayerDayShareEntity> {

    PageUtils queryDaySharePage(Map<String, Object> params, ChlUserEntity userEntity);

}

