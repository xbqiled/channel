package com.channel.api.service.cac;

import com.baomidou.mybatisplus.service.IService;
import com.channel.api.entity.cac.SigninchannelEntity;
import com.channel.api.entity.chl.ChlUserEntity;
import com.channel.common.json.GsonSignAwardCfg;
import com.channel.common.utils.PageUtils;

import java.util.List;
import java.util.Map;

/**
 * VIEW
 *
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-05-28 14:35:42
 */
public interface SignchannelService extends IService<SigninchannelEntity> {

    PageUtils queryPage(Map<String, Object> params);

    PageUtils querySignCfgPage(Map<String, Object> params, ChlUserEntity userEntity);

    SigninchannelEntity findByChannelId (String channelId);

    int channelOpenType(String channelId, String openType);

    int channelLoop(String channelId, String isCirculate);

    int saveSignCfg(String channel, String beginTime, String endTime, Integer openType, Integer isCirculate, Integer num, List<GsonSignAwardCfg> rabateCfg);
}

