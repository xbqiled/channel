package com.channel.api.service.cac;

import com.baomidou.mybatisplus.service.IService;
import com.channel.api.entity.cac.StatisGameEntity;
import com.channel.api.entity.chl.ChlUserEntity;
import com.channel.common.utils.PageUtils;

import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2020-03-02 21:28:00
 */
public interface StatisGameService extends IService<StatisGameEntity> {

      PageUtils queryStatisGamePage(Map<String, Object> params, ChlUserEntity chlUser);

      PageUtils queryStatisUserGamePage(Map<String, Object> params, ChlUserEntity chlUser);

      List<Map<String, String>> queryGameList();

}

