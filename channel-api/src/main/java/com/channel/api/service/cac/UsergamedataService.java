package com.channel.api.service.cac;

import com.baomidou.mybatisplus.service.IService;
import com.channel.api.entity.cac.UsergamedataEntity;
import com.channel.api.entity.chl.ChlUserEntity;
import com.channel.common.utils.PageUtils;

import java.util.List;
import java.util.Map;

/**
 * VIEW
 *
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-04-10 12:50:44
 */
public interface UsergamedataService extends IService<UsergamedataEntity> {

    PageUtils queryPage(Map<String, Object> params);

    PageUtils queryUserCtrlPage(Map<String, Object> params, ChlUserEntity userEntity);

    PageUtils queryUserOnlinePage(Map<String, Object> params,  ChlUserEntity userEntity);

    List<Map<String, Object>> queryGameRoom();

    int doSet(String accountId, String type, String betLimit, String rate, String round, String roomList, ChlUserEntity userEntity);

    int cancelCtrl(String accountId, String roomId);

    int delCtrlData(String accountId, String roomId);

    List<Map<String, Object>> getRoomTreeData();

    List<Map<String, Object>> getAllRoomTreeData();
}

