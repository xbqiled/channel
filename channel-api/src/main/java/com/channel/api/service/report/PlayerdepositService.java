package com.channel.api.service.report;

import com.baomidou.mybatisplus.service.IService;
import com.channel.api.entity.chl.ChlUserEntity;
import com.channel.api.entity.report.PlayerdepositEntity;
import com.channel.common.utils.PageUtils;

import java.util.Map;

/**
 * VIEW
 *
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-05-29 13:35:09
 */
public interface PlayerdepositService extends IService<PlayerdepositEntity> {

    PageUtils queryPage(Map<String, Object> params);

    PageUtils queryDepositPage(Map<String, Object> params, ChlUserEntity userEntity);
}

